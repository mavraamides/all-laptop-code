import pandas as pd 
import random
from statemachine import StateMachine, State
from extract_ev_charts import EVCalc

def get_yardline_category(yardline):
    if yardline >= 99:
        category = 0
    elif yardline >= 98:
        category = 1
    elif yardline >= 95:
        category = 2
    elif yardline >= 90:
        category = 3
    elif yardline >= 80:
        category = 4
    elif yardline >= 50:
        category = 5
    elif yardline >= 30:
        category = 6
    elif yardline >= 20:
        category = 7
    elif yardline >= 10:
        category = 8
    elif yardline >= 5:
        category = 9
    elif yardline >= 2:
        category = 10
    else:
        category = 11
    return category

def get_distance_category(distance):
    if distance >= 20:
        category = 0
    elif distance >= 15:
        category = 1 
    elif distance >= 10:
        category = 2 
    elif distance >= 5:
        category = 3 
    elif distance >= 2:
        category = 4 
    else:
        category = 5
    return category
    
def get_time_category(time_remaining):
    if time_remaining > 1800 + 240:
        category = 0
    elif time_remaining > 1800 + 120:
        category = 1
    elif time_remaining > 1800 + 60:
        category = 2
    elif time_remaining > 1800:
        category = 3
    elif time_remaining > 240:
        category = 4
    elif time_remaining > 120:
        category = 5
    elif time_remaining > 60:
        category = 6
    else:
        category = 7
    return category

def get_score_diff_category(score_diff):
    test_score = abs(score_diff)
    if test_score == 0:
        category = 0
    elif test_score <= 3:
        category = 1
    elif test_score <= 6:
        category = 2
    elif test_score <= 8:
        category = 3
    elif test_score <= 16:
        category = 4
    else:
        category = 5
    if score_diff < 0:
        category = category * -1
    return category

def add_if_match(t1, t2, initval, addval, clear_if_not_match = False):
    if t1 == t2:
        return initval + addval
    elif clear_if_not_match:
        return 0
    else:
        return initval

def square(val):
    try:
        return int(val * val)
    except:
        return val * val

class GameClock:
    def __init__(self):
        self.reset()

    def reset(self):
        self.quarter = 1
        self.game_seconds_remaining = 3600

    def get_quarter_desc(self):
        if self.quarter == 1:
            return 'first quarter'
        elif self.quarter == 2:
            return 'second quarter'
        elif self.quarter == 3:
            return 'third quarter'
        elif self.quarter == 4:
            return 'fourth quarter'
        elif self.quarter == 5:
            return 'overtime'

    def get_quarter_seconds_remaining(self):
        if self.quarter == 1:
            return self.game_seconds_remaining - 2700
        elif self.quarter == 2:
            return self.game_seconds_remaining - 1800
        elif self.quarter == 3:
            return self.game_seconds_remaining - 900
        else:
            return self.game_seconds_remaining

    def get_half_seconds_remaining(self):
        if self.quarter == 1 or self.quarter == 2:
            return self.game_seconds_remaining - 1800
        else:
            return self.game_seconds_remaining

    def get_clock_desc(self):
        minutes = int(int(self.get_quarter_seconds_remaining()) / 60)
        seconds = int(int(self.get_quarter_seconds_remaining()) % 60)
        return "{:d}".format(minutes) + ':' + "{:02d}".format(seconds)

    def print(self):
        print (self.get_clock_desc(), 'left in', self.get_quarter_desc())
        # print(self.get_quarter_seconds_remaining(), self.get_half_seconds_remaining(), self.game_seconds_remaining)

    def advance(self, seconds, gs):
        if self.get_half_seconds_remaining() > 120 and seconds >= self.get_half_seconds_remaining() - 120:
            print ('Two minute warning!')
            seconds = self.get_half_seconds_remaining() - 120

        if seconds >= self.get_quarter_seconds_remaining():
            seconds = self.get_quarter_seconds_remaining()
            if not gs.statemachine.is_ready_for_extra_point_state:
                print ('End of ' + self.get_quarter_desc())
                gs.print_score_table()
                
                if self.quarter < 4:
                    self.quarter += 1

                    if self.quarter == 3:
                        gs.homepossession = gs.homesecondhalfchoice
                        gs.balltracker.yardline = 35
                        gs.balltracker.reset_dandd()
                        if gs.statemachine.is_ready_for_scrimmage_state:
                            gs.statemachine.seond_half_kickoff_event()

        self.game_seconds_remaining -= seconds

        if self.game_seconds_remaining < 0:
            self.game_seconds_remaining = 0

class BallTracker:
    def __init__(self):
        self.reset_dandd()
        self.yardline = 35

    def reset_dandd(self):
        self.down = 1
        self.distance = 10

    def gain_yards(self, yards, suppress_fd = False):
        self.distance -= yards
        if self.distance <= 0:
            self.reset_dandd()
            if not suppress_fd:
                print ('First down!')
        else:
            self.down += 1
        self.yardline -= yards
        if self.yardline < 0:
            self.yardline == 0
        if self.distance >= self.yardline:
            self.distane = self.yardline

    def get_desc(self):
        desc = ''
        if self.down == 1:
            desc = '1st'
        elif self.down == 2:
            desc = '2nd'
        elif self.down == 3:
            desc = '3rd'
        else:
            desc = '4th'
        if self.distance >= self.yardline:
            desc += ' and goal to go'
        else:
            desc += ' and ' + str(self.distance)
    
        return desc

class GameStateMachine(StateMachine):
    game_start_state = State('Start', initial = True)
    ready_for_kickoff_state = State('Ready For Kickoff')
    ready_for_scrimmage_state = State('Ready For Scrimmage')
    ready_for_extra_point_state = State('Ready For Extra Point')

    coin_flip_event = game_start_state.to(ready_for_kickoff_state)

    kickoff_received_event = ready_for_kickoff_state.to(ready_for_scrimmage_state)
    own_kickoff_recovered_event = ready_for_kickoff_state.to(ready_for_scrimmage_state)
    touchdown_on_kickoff_return_event = ready_for_kickoff_state.to(ready_for_extra_point_state)

    turnover_on_downs_event = ready_for_scrimmage_state.to(ready_for_scrimmage_state)
    touchdown_on_scrimmage_event = ready_for_scrimmage_state.to(ready_for_extra_point_state)
    safety_on_scrimmage_event = ready_for_scrimmage_state.to(ready_for_kickoff_state)
    seond_half_kickoff_event = ready_for_scrimmage_state.to(ready_for_kickoff_state)
    field_goal_good_event = ready_for_scrimmage_state.to(ready_for_kickoff_state)

    extra_point_good_event = ready_for_extra_point_state.to(ready_for_kickoff_state)
    extra_point_two_points_good_event = ready_for_extra_point_state.to(ready_for_kickoff_state)
    extra_point_failed_event = ready_for_extra_point_state.to(ready_for_kickoff_state)

        
    def set_game_state(self, gamestate):
        self.gamestate = gamestate

    def on_coin_flip_event(self):
        print('Coin flip!')
        choice = random.choice(('Heads','Tails'))
        print (self.gamestate.awayteam + ' chooses ' + choice)
        flip = random.choice(('Heads','Tails'))
        print('Coin comes up ' + flip)
        if choice == flip:
            print (self.gamestate.awayteam + ' wins the flip and defers ')
            self.gamestate.homesecondhalfchoice = False
            self.gamestate.homepossession = True
        else:
            print (self.gamestate.hometeam + ' wins the flip and defers ')
            self.gamestate.homesecondhalfchoice = True
            self.gamestate.homepossession = False

    def on_kickoff_received_event(self):
        pass

    def on_own_kickoff_recovered_event(self):
        print ("Onside Kick Recovered By Kicking Team!")

    def on_touchdown_on_kickoff_return_event(self):
        print ("Touchdown!")
        self.gamestate.add_score(6)
        self.gamestate.print_score()
        self.gamestate.balltracker.yardline = 35

    def on_turnover_on_downs_event(self):
        print ("Ball turned over on downs!")
        self.gamestate.change_of_possession()

    def on_touchdown_on_scrimmage_event(self):
        print ("Touchdown!")
        self.gamestate.add_score(6)
        self.gamestate.print_score()
        self.gamestate.balltracker.yardline = 35

    def on_safety_on_scrimmage_event(self):
        print ("Safety!")
        self.gamestate.change_of_possession()
        self.gamestate.add_score(2)
        self.gamestate.print_score()
        self.gamestate.balltracker.yardline = 20

    def on_field_goal_good_event(self):
        self.gamestate.add_score(3)
        self.gamestate.print_score()
        self.gamestate.change_of_possession()
        self.gamestate.balltracker.yardline = 35

    def on_extra_point_good_event(self):
        self.gamestate.add_score(1)
        self.gamestate.print_score()
        self.gamestate.change_of_possession()
        self.gamestate.balltracker.yardline = 35

    def on_extra_point_two_points_good_event(self):
        self.gamestate.add_score(2)
        self.gamestate.print_score()
        self.gamestate.change_of_possession()
        self.gamestate.balltracker.yardline = 35

    def on_extra_point_failed_event(self):
        self.gamestate.print_score()
        self.gamestate.change_of_possession()
        self.gamestate.balltracker.yardline = 35

class GameState:
    def __init__(self, hometeam, awayteam, homepossession):
        self.hometeam = hometeam
        self.awayteam = awayteam
        self.homescore = 0
        self.awayscore = 0
        self.homescore_by_quarter = [self.hometeam, 0, 0, 0, 0, 0]
        self.awayscore_by_quarter = [self.awayteam, 0, 0, 0, 0, 0]
        self.home_to = 3
        self.away_to = 3
        self.homepossession = homepossession
        self.homesecondhalfchoice = True
        self.statemachine = GameStateMachine()
        self.statemachine.set_game_state(self)
        self.balltracker = BallTracker()
        self.gameclock = GameClock()

    def game_over(self):
        if self.gameclock.game_seconds_remaining <= 0:
            return True
        return False

    def change_of_possession(self):
        self.homepossession = not self.homepossession
        self.balltracker.reset_dandd()
        self.balltracker.yardline = 100 - self.balltracker.yardline

    def set_yardline_after_kickoff(self, yl, play_time):
        self.balltracker.yardline = yl
        if yl <= 0:
            self.statemachine.touchdown_on_kickoff_return_event()
        else:
            self.statemachine.kickoff_received_event()
        self.gameclock.advance(play_time, self)

    def set_yardline_after_punt(self, yl, play_time):
        self.balltracker.yardline = yl
        if yl <= 0:
            self.statemachine.touchdown_on_scrimmage_event()
        elif yl >= 100:
            self.balltracker.yardline = 80
            self.balltracker.reset_dandd()
        else:
            self.balltracker.reset_dandd()
        self.gameclock.advance(play_time, self)

    def gain_yards_on_scrimmage(self, gain, play_time, suppress_fd = False):
        self.balltracker.gain_yards(gain, suppress_fd)
        if self.balltracker.down > 4:
            self.statemachine.turnover_on_downs_event()
        elif self.balltracker.yardline <= 0:
            self.statemachine.touchdown_on_scrimmage_event()
        elif self.balltracker.yardline >= 100:
            self.statemachine.safety_on_scrimmage_event()
        self.gameclock.advance(play_time, self)

    def add_score(self, score):
        if self.homepossession:
            self.homescore += score
            if self.gameclock.quarter >= 1 and self.gameclock.quarter <= 5:
                self.homescore_by_quarter[self.gameclock.quarter] += score
        else:
            self.awayscore += score
            if self.gameclock.quarter >= 1 and self.gameclock.quarter <= 5:
                self.awayscore_by_quarter[self.gameclock.quarter] += score

    def get_posteam(self):
        if self.homepossession:
            return self.hometeam
        else:
            return self.awayteam

    def get_defteam(self):
        if self.homepossession:
            return self.awayteam
        else:
            return self.hometeam

    def get_score_diff_post(self):
        if self.homepossession:
            return self.homescore - self.awayscore
        else:    
            return self.awayscore - self.homescore

    def print_score(self):
        print (self.awayteam + ' ' + str(self.awayscore) + ' ' + self.hometeam + ' ' + str(self.homescore))

    def print_score_table(self):
        '''
        +----+----+----+----+----+----+-----+
        |Team| 1st| 2nd| 3rd| 4th| OT |Total|
        +----+----+----+----+----+----+-----+
        | SF |  0 |  0 |  0 |  0 |  0 |   0 |
        |ARI |  0 |  0 |  0 |  0 |  0 |   0 |
        +----+----+----+----+----+----+-----+
        '''
        print ('+----+----+----+----+----+----+-----+')
        print ('|Team| 1st| 2nd| 3rd| 4th| OT |Total|')
        print ('+----+----+----+----+----+----+-----+')
        
        for x in range(len(self.homescore_by_quarter)):
            if x <= self.gameclock.quarter:
                print("|{:>3s} ".format(str(self.homescore_by_quarter[x])), end='')
            else:
                print("|  - ", end='')
        print("| {:>3s} |".format(str(self.homescore)))

        for x in range(len(self.awayscore_by_quarter)):
            if x <= self.gameclock.quarter:
                print("|{:>3s} ".format(str(self.awayscore_by_quarter[x])), end='')
            else:
                print("|  - ", end='')
        print("| {:>3s} |".format(str(self.awayscore)))

        print ('+----+----+----+----+----+----+-----+')

class PlayChooser:
    # def calc_remaining_possessions(self, hometeam):
    #     if hometeam:
    #         tos = self.home_to
    #     else:
    #         tos = self.away_to

    #     time_left = self.gameclock.game_seconds_remaining
    #     if time_left > 120: time_left += 30
    #     time_left += 30 * tos
    #     remaining_possessions = time_left / 240
    #     return remaining_possessions

    # def calc_numbeer_of_scores_lead(self, hometeam):
    #     if hometeam:
    #         return (self.homescore - self.awayscore) / 8.0
    #     else:    
    #         return (self.awayscore - self.homescore) / 8.0

    # def calc_onside_kick(self):
    #     remaining_posessions = self.calc_remaining_possessions(not self.homepossession)
    #     number_of_scores = self.calc_numbeer_of_scores_lead(self.homepossession)
    #     return number_of_scores >= remaining_posessions

    # def add_time_of_half_score(self, candidates):
    #     candidates['match_score'] = candidates['match_score'] + 1000.0 / (abs(candidates['half_seconds_remaining'] - self.half_seconds_remaining) + 10)
    #     return candidates

    def __init__(self):
        self.longest_FGA = {}
        self.evc = EVCalc()

    def use_pos_team(self):
        result = random.choice((True,False))
        return result

    def set_scores(self, candidates, value):
        candidates['match_score'] = value
        return candidates

    def square_scores(self, candidates):
        candidates['match_score'] = candidates['match_score'].apply(square)

    def add_down_match(self, candidates, gamestate, value, clear_if_not_match = False):
        candidates["match_score"] = candidates.apply (lambda row: add_if_match(row["down"], 
            gamestate.balltracker.down, row["match_score"], value, clear_if_not_match), axis=1)

    def add_distance_match(self, candidates, gamestate, value, clear_if_not_match = False):
        candidates["match_score"] = candidates.apply (lambda row: add_if_match(row["distance_category"], 
            get_distance_category(gamestate.balltracker.distance), row["match_score"], value, clear_if_not_match), axis=1)
    
    def add_yardline_match(self, candidates, gamestate, value, clear_if_not_match = False):
        candidates["match_score"] = candidates.apply (lambda row: add_if_match(row["yardline_category"], 
            get_yardline_category(gamestate.balltracker.yardline), row["match_score"], value, clear_if_not_match), axis=1)
    
    def add_time_match(self, candidates, gamestate, value, clear_if_not_match = False):
        candidates["match_score"] = candidates.apply (lambda row: add_if_match(row["time_category"], 
            get_time_category(gamestate.gameclock.game_seconds_remaining), row["match_score"], value, clear_if_not_match), axis=1)
    
    def add_score_diff_match(self, candidates, gamestate, value, clear_if_not_match = False):
        candidates["match_score"] = candidates.apply (lambda row: add_if_match(row["score_diff_category"], 
            get_score_diff_category(gamestate.get_score_diff_post()), row["match_score"], value, clear_if_not_match), axis=1)
            
    def calc_longest_FGA(self, gamestate):
        for team in (gamestate.hometeam, gamestate.awayteam):
            candidates = game_data[game_data['posteam'] == team]
            candidates = candidates[candidates['play_type_nfl'] == 'FIELD_GOAL']
            columns = candidates["yardline_100"]
            self.longest_FGA[team] = int(columns.max())
            print(team, self.longest_FGA[team] )

    # down
    # distance
    # yardline
    # time
    # score diff
    def find_candidates(self, gamestate, reuse):
        if reuse:
            candidates = game_data[game_data['used'] <= 1]
        else:
            candidates = game_data[game_data['used'] == 0]
        if self.use_pos_team():
        # if True:
            candidates = candidates[candidates['posteam'] == gamestate.get_posteam()]
            longest_FGA = self.longest_FGA.get(gamestate.get_posteam(), 35)
        else:
            candidates = candidates[candidates['defteam'] == gamestate.get_defteam()]
            longest_FGA = self.longest_FGA.get(gamestate.get_defteam(), 35)
        if gamestate.statemachine.is_ready_for_kickoff_state:
            candidates = candidates[candidates['play_type_nfl'] == 'KICK_OFF']
            self.set_scores(candidates, 1)
            self.add_time_match(candidates, gamestate, 10)
            self.add_score_diff_match(candidates, gamestate, 10)
            self.square_scores(candidates)
        elif gamestate.statemachine.is_ready_for_scrimmage_state:
            choice = 'SCRIMMAGE'
            if gamestate.balltracker.down == 4:
                choice = self.evc.GetBestOption(gamestate.get_posteam(), gamestate.get_defteam(), gamestate.balltracker.yardline, gamestate.balltracker.distance)
                if gamestate.balltracker.yardline > 50:
                    choice = 'PUNT'
            
            if choice == 'SCRIMMAGE':
                candidate_list = ['RUSH','PASS']
            else:
                candidate_list = [choice]
            
            if choice in ['SCRIMMAGE','RUSH','PASS']:
                candidates = candidates[candidates['play_type_nfl'].isin(candidate_list)]
                candidates = candidates[candidates['qb_kneel'] == 0]
                candidates = candidates[candidates['qb_spike'] == 0]
                # candidates = candidates[candidates['qb_scramble'] == 1]
                # candidates = candidates[candidates['fumble'] == 1]
                # candidates = candidates[candidates['play_type_nfl'].isin(["PASS","PENALTY","RUSH","SACK","TIMEOUT","FIELD_GOAL"])]
                self.set_scores(candidates, 1)
                self.add_time_match(candidates, gamestate, 10)
                self.add_score_diff_match(candidates, gamestate, 10)
                self.add_yardline_match(candidates, gamestate, 10)
                self.add_distance_match(candidates, gamestate, 10)
                self.add_down_match(candidates, gamestate, 10)

            elif choice == 'PUNT':
                candidate_list = ['PUNT']
                candidates = candidates[candidates['play_type_nfl'].isin(candidate_list)]
                self.set_scores(candidates, 1)
                candidates.loc[(candidates['play_type_nfl'] == 'PUNT') & ((candidates['yardline_100'] < gamestate.balltracker.yardline - 10) | (candidates['yardline_100'] > gamestate.balltracker.yardline + 10)), 'match_score'] = 0

            if choice == 'FGA':
                candidate_list = ['FIELD_GOAL']
                candidates = candidates[candidates['play_type_nfl'].isin(candidate_list)]
                self.set_scores(candidates, 1)
                candidates.loc[(candidates['play_type_nfl'] == 'FIELD_GOAL') & ((candidates['yardline_100'] < gamestate.balltracker.yardline - 7) | (candidates['yardline_100'] > gamestate.balltracker.yardline + 8)), 'match_score'] = 0

            self.square_scores(candidates)

        elif gamestate.statemachine.is_ready_for_extra_point_state:
            self.set_scores(candidates, 1)
            candidates = candidates[candidates['play_type_nfl'].isin(["XP_KICK","PAT2"])]
            self.add_time_match(candidates, gamestate, 10)
            self.add_score_diff_match(candidates, gamestate, 10)
            self.square_scores(candidates)
        return candidates
'''
    "passer_player_name",
    "receiver_player_name",
    "rusher_player_name",
    "punter_player_name",
    "kickoff_returner_player_name",

    "interception_player_name",
    "punt_returner_player_name",
    "kicker_player_name",
'''
class PlayEecutor:
    def __init__(self, home, away):
        self.home = home
        self.away = away
        self.home_player_names = {}
        self.away_player_names = {}
        self.home_starting_qb = None
        self.away_starting_qb = None

    def get_pos_desc(self, posteam, defteam, yardline):
        if yardline == 50:
            return 'midfield'
        if yardline > 50:
            return 'the ' + posteam + ' ' + str(100 - yardline)
        if yardline < 50:
            return 'the ' + defteam + ' ' + str(yardline)

    def get_yardline_desc(self, posteam, defteam, yardline):
        if yardline == 50:
            return 'at midfield'
        if yardline > 50:
            return 'on ' + posteam + ' ' + str(100 - yardline)
        if yardline < 50:
            return 'on ' + defteam + ' ' + str(yardline)

    def accum_single_set(self, teamname, posteam, category, playtype = None):
        if posteam:
            candidates = game_data[game_data['posteam'] == teamname]
        else:
            candidates = game_data[game_data['defteam'] == teamname]
        if playtype:
            candidates = candidates[candidates['play_type_nfl'] == playtype]

        passers = candidates.groupby(category)
        names = []
        values = []
        for k, gb in passers:
            names.append(str(k))
            values.append(len(gb.index))
        df = pd.DataFrame(list(zip(names, values)), 
            columns =['name', 'value']) 
        return df
   
    def accum_player_names(self):
        self.home_player_names['passer_player_name'] = self.accum_single_set(self.home, True, 'passer_player_name')
        self.home_player_names['receiver_player_name'] = self.accum_single_set(self.home, True, 'receiver_player_name')
        self.home_player_names['rusher_player_name'] = self.accum_single_set(self.home, True, 'rusher_player_name')
        self.home_player_names['punter_player_name'] = self.accum_single_set(self.home, True, 'punter_player_name')
        self.home_player_names['kickoff_returner_player_name'] = self.accum_single_set(self.home, True, 'kickoff_returner_player_name')
        self.home_player_names['interception_player_name'] = self.accum_single_set(self.home, False, 'interception_player_name')
        self.home_player_names['punt_returner_player_name'] = self.accum_single_set(self.home, False, 'punt_returner_player_name')
        self.home_player_names['kicker_player_name_KO'] = self.accum_single_set(self.home, False, 'kicker_player_name', 'KICK_OFF')
        self.home_player_names['kicker_player_name_FG'] = self.accum_single_set(self.home, True, 'kicker_player_name', 'FIELD_GOAL')

        self.away_player_names['passer_player_name'] = self.accum_single_set(self.away, True, 'passer_player_name')
        self.away_player_names['receiver_player_name'] = self.accum_single_set(self.away, True, 'receiver_player_name')
        self.away_player_names['rusher_player_name'] = self.accum_single_set(self.away, True, 'rusher_player_name')
        self.away_player_names['punter_player_name'] = self.accum_single_set(self.away, True, 'punter_player_name')
        self.away_player_names['kickoff_returner_player_name'] = self.accum_single_set(self.away, True, 'kickoff_returner_player_name')
        self.away_player_names['interception_player_name'] = self.accum_single_set(self.away, False, 'interception_player_name')
        self.away_player_names['punt_returner_player_name'] = self.accum_single_set(self.away, False, 'punt_returner_player_name')
        self.away_player_names['kicker_player_name_KO'] = self.accum_single_set(self.away, False, 'kicker_player_name', 'KICK_OFF')
        self.away_player_names['kicker_player_name_FG'] = self.accum_single_set(self.away, True, 'kicker_player_name', 'FIELD_GOAL')

        # for key in self.home_player_names.keys():
        #     print (key)
        #     print (self.home_player_names[key])

        # for key in self.away_player_names.keys():
        #     print (key)
        #     print (self.away_player_names[key])

    def get_player_name(self, type, home):
        if home:
            table = self.home_player_names[type]
        else:
            table = self.away_player_names[type]
        player = table.sample(n=1, weights='value')['name'].values[0]
        return player

    def get_or_substitute(self, substitute, subname, home, row, rowname):
        if subname == 'passer_player_name':
            if home:
                if not self.home_starting_qb:
                    self.home_starting_qb = self.get_player_name(subname, home)
                player = self.home_starting_qb
            else:
                if not self.away_starting_qb:
                    self.away_starting_qb = self.get_player_name(subname, home)
                player = self.away_starting_qb
        else:
            if substitute:
                player = self.get_player_name(subname, home)
            else:
                player = row[rowname].values[0]
                if len(str(player)) < 4:
                    player = self.get_player_name(subname, home)

        return str(player) 

    def get_starting_qbs(self):
        posplayer = self.get_or_substitute(True, 'passer_player_name', True, None, 'passer_player_name')
        defplayer = self.get_or_substitute(True, 'passer_player_name', False, None, 'passer_player_name')
        print (posplayer + ' is starting at quarterback for ' + self.home)
        print (defplayer + ' is starting at quarterback for ' + self.away)

    def print_state(self, gs):
        if gs.statemachine.is_ready_for_kickoff_state:
            print(gs.get_posteam(), 'receiving kickoff from', gs.get_defteam(), self.get_yardline_desc(gs.get_posteam(), gs.get_defteam(), gs.balltracker.yardline))
        elif gs.statemachine.is_ready_for_scrimmage_state:
            print(gs.get_posteam() + ' has the ball ' + gs.balltracker.get_desc() + ' ' + self.get_yardline_desc(gs.get_posteam(), gs.get_defteam(), gs.balltracker.yardline))
        gs.gameclock.print()

    def interception_check(self, gs, row, interception_player_name, yards_gained, play_time):
        interception = self.get_int_value(row, 'interception', 0)
        return_yards = self.get_int_value(row, 'return_yards', 0)
        desc = ''
        if interception == 1:
            desc += "It's picked off by " + interception_player_name + '! '
            if return_yards > 0:
                desc += 'He returns it for ' + str(return_yards) + ' yards.'
            elif return_yards == 0:
                desc += "He's tackled immediately."
            else:
                desc += "He tries to cut back and loses " + str(- return_yards) + ' yards.'
            print (desc)
            gs.balltracker.gain_yards(yards_gained, True)
            gs.change_of_possession()
            gs.gain_yards_on_scrimmage(return_yards, play_time, True)
            gs.balltracker.reset_dandd()
        return interception

    def fumble_check(self, gs, row):
        fumble_out_of_bounds = row['fumble_out_of_bounds'].values[0]
        fumble = row['fumble'].values[0]
        fumble_lost = row['fumble_lost'].values[0]
        try:
            fumble_recovery_1_yards = int(row['fumble_recovery_1_yards'].values[0])
        except:
            fumble_recovery_1_yards = 0
        desc = ''
        if fumble == 1:
            desc += 'Fumble!! '
            if fumble_out_of_bounds == 1:
                desc += 'Out of bounds. '
            if fumble_lost == 1:
                desc += 'Defense recovers and returns it ' + str(fumble_recovery_1_yards) + ' yards.'
                gs.change_of_possession()
                gs.gain_yards_on_scrimmage(fumble_recovery_1_yards, 0)
                gs.balltracker.reset_dandd()
            else:
                desc += 'Offense maintains possession. '
        print (desc)
        return fumble_recovery_1_yards

    def mark_as_used(self, ndx):
        game_data["used"] = game_data.apply (lambda row: add_if_match(row["ndx"], 
            ndx, row["used"], 1), axis=1)

    def get_int_value(self, row, field, default):
        try:
            value = int(row[field].values[0])
        except:
            value = default
        return value

    def print(self, gs, row):
        posteam = row['posteam'].values[0]
        defteam = row['defteam'].values[0]
        touchdown = int(row['touchdown'].values[0])
        shotgun = int(row['shotgun'].values[0])
        qb_scramble = int(row['qb_scramble'].values[0])
        play_time = int(row['play_time'].values[0])
        air_yards = self.get_int_value(row, 'air_yards', 0)
        incomplete_pass = int(row['incomplete_pass'].values[0])
        yl = int(row['yardline_100'].values[0])
        yards_gained = int(row['yards_gained'].values[0])
        if yards_gained > gs.balltracker.yardline:
            yards_gained = gs.balltracker.yardline

        nndx = row['ndx'].values[0] + 1
        nrow = game_data.loc[game_data['ndx'] == nndx]
        try:
            nextyl = int(nrow['yardline_100'].values[0])
        except:
            nextyl = yl
        nextpos = nrow['posteam'].values[0]
        changeofpos = posteam != nextpos

        substituteposplayers = False
        substitutedefplayers = False
        posishome = False

        if posteam == self.home:
            posishome = True
        elif defteam == self.away:
            posishome = True
        
        if posteam not in (self.home, self.away):
            substituteposplayers = True
        if defteam not in (self.home, self.away):
            substitutedefplayers = True

        if posteam == self.home:
            posishome = True
        elif defteam == self.away:
            posishome = True

        if posishome:
            posteamname = self.home
            defteamname = self.away
        else:
            posteamname = self.away
            defteamname = self.home

        play_type_nfl = row["play_type_nfl"].values[0]
        # print (row['ndx'].values[0], row['desc'].values[0])
        if  play_type_nfl == 'KICK_OFF':
            defplayer = self.get_or_substitute(substitutedefplayers, 'kicker_player_name_KO', not posishome, row, 'kicker_player_name')
            posplayer = self.get_or_substitute(substituteposplayers, 'kickoff_returner_player_name', posishome, row, 'kickoff_returner_player_name')
            kick_distance = row['kick_distance'].values[0]
            touchback = row['touchback'].values[0]
            if gs.balltracker.yardline != 35:
                nextyl = nextyl + gs.balltracker.yardline - 35
            if changeofpos:
                nextyl = 100 - nextyl
            if gs.balltracker.yardline < 35:
                if touchback == 1:
                    kick_distance = 65
                touchback = 0
            desc = defplayer + ' (' + defteamname + ') kicks off from ' + self.get_pos_desc(posteamname, defteamname, gs.balltracker.yardline)
            if touchback == 1:
                desc += ' into the endzone for a touchback.'
            else:
                if kick_distance > 0:
                    kick_distance = int(kick_distance)
                    desc += '. It goes ' + str(kick_distance) + ' yards.'
                if posplayer != '':
                    if touchdown == 1:
                        desc += ' It is returned by ' + posplayer + ' (' + posteamname + ') into the end zone!'
                    else:
                        desc += ' It is returned by ' + posplayer + ' (' + posteamname + ') to ' + self.get_pos_desc(posteamname, defteamname, nextyl) + '.'
            print (desc)
            if touchdown == 1:
                gs.set_yardline_after_kickoff(0, play_time)
            else:
                gs.set_yardline_after_kickoff(nextyl, play_time)
                self.fumble_check(gs, row)

        elif play_type_nfl == 'RUSH':
            run_location = row['run_location'].values[0]
            run_gap = row['run_gap'].values[0]
            if qb_scramble:
                posplayer = self.get_or_substitute(substituteposplayers, 'passer_player_name', posishome, row, 'rusher_player_name')
                desc = posplayer + ' scrambles out of the pocket and '
            else:
                posplayer = self.get_or_substitute(substituteposplayers, 'rusher_player_name', posishome, row, 'rusher_player_name')
                desc = posplayer + ' '

            if run_location == 'middle':
                desc += 'runs up the middle '
            elif run_location in ('right', 'left'):
                if run_gap in ('guard', 'tackle'):
                    desc += 'runs off the ' + run_location + ' ' + run_gap + ' '
                elif run_gap == 'end':
                    desc += 'runs around the ' + run_location + ' ' + run_gap + ' '
                else:
                    desc += 'runs '
            else:
                desc += 'runs '

            if yards_gained < 0:
                desc += 'for a loss of ' + str(-1 * yards_gained) + '. '
            elif yards_gained > 0:
                desc += 'for a gain of ' + str(yards_gained) + '. '
            else:
                desc += 'for no gain. '

            print (desc)
            gs.gain_yards_on_scrimmage(yards_gained, play_time)
            self.fumble_check(gs, row)
            # print (substituteposplayers, substitutedefplayers, posishome)
        elif play_type_nfl == 'PASS':
            pass_length = row['pass_length'].values[0]
            if air_yards > gs.balltracker.yardline:
                air_yards = gs.balltracker.yardline
            if pass_length == 'deep' and gs.balltracker.yardline < 20:
                pass_length = 'short'
            pass_location = row['pass_location'].values[0]
            passer = self.get_or_substitute(substituteposplayers, 'passer_player_name', posishome, row, 'passer_player_name')
            receiver = self.get_or_substitute(substituteposplayers, 'receiver_player_name', posishome, row, 'receiver_player_name')
            if shotgun:
                desc = passer + ' is in the shotgun. He gets the snap and '
            else:
                desc = passer + ' is under center. He takes the snap and '
            if qb_scramble:
                desc = 'He scrambles out of the pocket and '

            if air_yards < 0:
                if pass_location == 'right':
                    desc += 'dumps it off behind the line of scrimmage to the right to '
                elif pass_location == 'left':
                    desc += 'dumps it off behind the line of scrimmage to the left to '
                else:
                    desc += 'trhows a middle screen to '
            elif pass_length == 'short':
                if pass_location == 'right':
                    desc += 'throws a short pass to the right to '
                elif pass_location == 'left':
                    desc += 'throws a short pass to the left to '
                else:
                    desc += 'throws a short pass over the middle to '
            else:
                if pass_location == 'right':
                    desc += 'goes deep down the right sideline to '
                elif pass_location == 'left':
                    desc += 'goes deep down the left sideline to '
                else:
                    desc += 'goes deep down over the middle to '
            if receiver == 'NA':
                desc += 'no one. '
            else:
                desc +=  receiver + ' '
                if air_yards > 0:
                    desc += str(air_yards) + ' yards '
            
            print (desc)
            desc = ''
            yac = yards_gained - air_yards
            interception_player_name = self.get_or_substitute(substitutedefplayers, 'interception_player_name', not posishome, row, 'interception_player_name')
            interception = self.interception_check(gs, row, interception_player_name, air_yards, play_time)
            # interception = 0
            if interception == 0:
                if incomplete_pass:
                    desc += 'Incomplete!'
                else:
                    if yac == 0:
                        desc += "He makes the grab and is tackled immediately "
                    elif yac > 0:
                        desc += "He catches the ball and goes " + str(yac) + " yards "
                    else:
                        desc += "He tries to cut back and loses " + str(-yac) + " yards "
                    if yards_gained < 0:
                        desc += 'for a loss of ' + str(-1 * yards_gained) + '. '
                    elif yards_gained > 0:
                        desc += 'for a gain of ' + str(yards_gained) + '. '
                    else:
                        desc += 'for no gain. '
                    
                print (desc)
                gs.gain_yards_on_scrimmage(yards_gained, play_time)
            # print (substituteposplayers, substitutedefplayers, posishome)
        elif play_type_nfl == 'XP_KICK':
            extra_point_result = row['extra_point_result'].values[0]
            desc = gs.get_posteam() + ' lines up for the extra point.'
            if extra_point_result == 'good':
                desc += " It's up and it's good!"
                print(desc)
                gs.statemachine.extra_point_good_event()
                gs.gameclock.advance(0, gs)
            elif extra_point_result == 'blocked':
                desc += " It's blocked!! No good!"
                print(desc)
                gs.statemachine.extra_point_failed_event()
                gs.gameclock.advance(0, gs)
            else:
                desc += " It's no good!"
                print(desc)
                gs.statemachine.extra_point_failed_event()
                gs.gameclock.advance(0, gs)
        elif play_type_nfl == 'PAT2':
            two_point_conv_result = row['two_point_conv_result'].values[0]
            desc = gs.get_posteam() + ' is going for two.'
            if two_point_conv_result == 'success':
                desc += ' They score!!'
                print(desc)
                gs.statemachine.extra_point_two_points_good_event()
                gs.gameclock.advance(0, gs)
            else:
                desc += ' ' + gs.get_defteam() + ' holds! No good!!'
                print(desc)
                gs.statemachine.extra_point_failed_event()
                gs.gameclock.advance(0, gs)
        elif play_type_nfl == 'FIELD_GOAL':
            ball_kicked_from = gs.balltracker.yardline + 8
            new_position = ball_kicked_from
            if new_position < 20:
                new_position = 20
            extra_point_result = row['field_goal_result'].values[0]
            desc = gs.get_posteam() + ' lines up for a ' + str(ball_kicked_from + 10) + ' yard field goal attempt.'
            if extra_point_result == 'made':
                desc += " It's up and it's good!"
                print(desc)
                gs.statemachine.field_goal_good_event()
                gs.gameclock.advance(play_time, gs)
            elif extra_point_result == 'blocked':
                desc += " It's blocked!! No good!"
                gs.balltracker.yardline = new_position
                gs.change_of_possession()
                desc += " " + gs.get_posteam() + ' takes over on the ' + self.get_pos_desc(defteamname, posteamname, new_position)
                print(desc)
                gs.gameclock.advance(play_time, gs)
            else:
                desc += " It's no good!"
                gs.balltracker.yardline = new_position
                gs.change_of_possession()
                desc += " " + gs.get_posteam() + ' takes over on the ' + self.get_pos_desc(defteamname, posteamname, new_position)
                print(desc)
                gs.gameclock.advance(play_time, gs)
        elif play_type_nfl == 'PUNT':
            '''
                "punt_blocked",
                "punt_inside_twenty",
                "punt_in_endzone",
                "punt_out_of_bounds",
                "punt_downed",
                "punt_fair_catch",
                fumble_not_forced
            '''
            punt_blocked = row['punt_blocked'].values[0]
            punt_in_endzone = row['punt_in_endzone'].values[0]
            punt_out_of_bounds = row['punt_out_of_bounds'].values[0]
            punt_downed = row['punt_downed'].values[0]
            punt_fair_catch = row['punt_fair_catch'].values[0]
            fumble_not_forced = row['fumble_not_forced'].values[0]
            fumble_lost = row['fumble_lost'].values[0]
            try:
                kick_distance = int(row['kick_distance'].values[0])
            except:
                kick_distance = gs.balltracker.yardline
                if kick_distance > 60:
                    kick_distance = 60        
                else:
                    punt_in_endzone = 1
            try:
                return_yards = int(row['return_yards'].values[0])
            except:
                return_yards = 0

            punter_player_name = self.get_or_substitute(substituteposplayers, 'punter_player_name', posishome, row, 'punter_player_name')
            punt_returner_player_name = self.get_or_substitute(substitutedefplayers, 'punt_returner_player_name', not posishome, row, 'punt_returner_player_name')
            desc = gs.get_posteam() + ' in punt formation. ' + punter_player_name + ' takes the snap. '
            if punt_blocked > 0:
                print (desc) 
                desc = 'Blocked!! ' + gs.get_defteam() + ' takes possession!'
                print (desc)
                desc = ''
                gs.change_of_possession()
                new_yl_after_punt = gs.balltracker.yardline - random.randrange(-10, 30)
                gs.set_yardline_after_punt(new_yl_after_punt, play_time)
            elif punt_out_of_bounds:
                desc += "It's punted " + str(kick_distance) + " yards out of bounds."
                gs.change_of_possession()
                new_yl_after_punt = gs.balltracker.yardline + kick_distance
                gs.set_yardline_after_punt(new_yl_after_punt, play_time)
            else:
                if punt_in_endzone > 0:
                    desc += 'He punts it into the endzone for a touchback.'
                    gs.change_of_possession()
                    new_yl_after_punt = 80
                    gs.set_yardline_after_punt(new_yl_after_punt, play_time)
                else:
                    desc += 'He punts it ' + str(kick_distance) + ' yards.'
                    print (desc)
                    desc = ''
                    if punt_fair_catch > 0:
                        desc += punt_returner_player_name + ' signals for a fair catch. '
                    if fumble_not_forced > 0:
                        desc += punt_returner_player_name + ' muffs the punt! '
                        if fumble_lost > 0:
                            desc += gs.get_posteam() + ' recovers! '
                            new_yl_after_punt = gs.balltracker.yardline - kick_distance + return_yards
                            gs.set_yardline_after_punt(new_yl_after_punt, play_time)
                        else:
                            desc += gs.get_defteam() + ' recovers and maintains possession! '
                            gs.change_of_possession()
                            new_yl_after_punt = gs.balltracker.yardline + kick_distance - return_yards
                            gs.set_yardline_after_punt(new_yl_after_punt, play_time)

                    else:
                        desc += 'The punt is fielded cleanly by ' + punt_returner_player_name
                        if punt_fair_catch > 0 or punt_downed > 0:
                            desc += ' who downs it.'
                        else:
                            desc += ' who returns it ' + str(return_yards) + ' yards ' 
                        gs.change_of_possession()
                        new_yl_after_punt = gs.balltracker.yardline + kick_distance - return_yards
                        gs.set_yardline_after_punt(new_yl_after_punt, play_time)
                        print (desc)
                        desc = ''
                        self.fumble_check(gs, row)

            print (desc)

                 


random.seed()
gs = GameState('SF', 'ARI', True)
# gs.print()

game_data = pd.read_csv('game_data.csv')
game_data["used"] = 0
game_data["match_score"] = 0
game_data["yardline_category"] = game_data.apply (lambda row: get_yardline_category(row["yardline_100"]), axis=1)
game_data["distance_category"] = game_data.apply (lambda row: get_distance_category(row["ydstogo"]), axis=1)
game_data["time_category"] = game_data.apply (lambda row: get_time_category(row["game_seconds_remaining"]), axis=1)
game_data["score_diff_category"] = game_data.apply (lambda row: get_score_diff_category(row["score_differential_post"]), axis=1)

pe = PlayEecutor(gs.hometeam, gs.awayteam)
pe.accum_player_names()
pe.get_starting_qbs()
pc = PlayChooser()
pc.calc_longest_FGA(gs)

playlist = []
pl_ndx = 0
recordlist = []
gs.statemachine.coin_flip_event()


holdhomepossession = gs.homepossession
holdhomesecondhalfchoice = gs.homesecondhalfchoice
print ('----------------------------------------------------------')
pe.print_state(gs)
print ('----------------------------------------------------------')

for x in range(1000):

    if len(playlist) > pl_ndx:
        choice = game_data[game_data['ndx'] == playlist[pl_ndx]]
        pl_ndx += 1
        skipinput = True
    else:
        candidates = pc.find_candidates(gs, False)
        try:
            choice = candidates.sample(n=1, weights="match_score")
        except:
            print ('NO CANDIDATES')
            candidates.to_csv('candidates.csv')
            print ('playlist = ', recordlist)
            print ('gs.homepossession = ', holdhomepossession)
            print ('gs.homesecondhalfchoice = ', holdhomesecondhalfchoice) 
            break
        skipinput = False
    
    recordlist.append(choice["ndx"].values[0])
    pe.print(gs, choice)
    pe.mark_as_used(choice["ndx"].values[0])

    if not skipinput:
        x = input()
        if x == 's':
            candidates.to_csv('candidates.csv')
        if x == 'p':
            print ('playlist = ', recordlist)
            print ('gs.homepossession = ', holdhomepossession)
            print ('gs.homesecondhalfchoice = ', holdhomesecondhalfchoice) 
        if x == 'd':
            print (choice['ndx'].values[0], choice['desc'].values[0])
        if x == 'q':
            break
        
    print ('----------------------------------------------------------')
    pe.print_state(gs)
    print ('----------------------------------------------------------')
    if gs.game_over():
        print()
        print ('Final Score:')
        gs.print_score_table()
        break
# pe.print_state(gs)
# candidates.to_csv('candidates.csv')

# game_data.to_csv('game_data.csv')
'''
[X] Marking play as used
[X] Extra point 
[X] 2 pt conv
[X] Safety
[X] Safety KO
[X] 2:00 minute warning
[X] End of quarter score
[X] EP at end of quarter should be untimed
[X] Table version of score
[X] Coin flip
[X] Choose ko / rec or defer
[X] Halftime
[X] FG's - Only include if yl <= longest ever
[X] Blocked FG's
[X] Punts
[X] Blocked Punts
[X] End of game
[X] Passes
[X] Interceptions
[ ] Penalties
[ ] Turnover returned for TD
[ ] Turnover touchback
[ ] Timeouts
[ ] Spikes
'''