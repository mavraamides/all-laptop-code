import pandas as pd
from penalty import Penalty

penalties = Penalty()

# [X] Port
class Stage:
    def __init__(self):
        self.clear()

    def clear(self):
        self.token = 'NONE'
        self.value = 0
        self.description = ''
        self.team_tags = []
        self.calc_tags = []
        self.extra_info = []

    def set(self, t, v, d, tt, ct, ei = []):
        self.token = t
        self.value = v
        self.description = d
        self.team_tags = tt
        self.calc_tags = ct
        self.extra_info = ei

    def dump(self):
        print(self.token,self.value,self.description,self.team_tags,self.calc_tags,self.extra_info)
        
# [X] Port
class StageSet:
    def __init__(self):
        self.clear()

    
    def add_stage(self, t, v, d, tt, ct, ei = []):
        s = Stage()
        s.set(t, v, d, tt, ct, ei)
        self.stages.append(s)

    def clear(self):
        self.encoded = 0
        self.stages = []

    def decode(self, row):
        self.stages = []
        token_list = str(row['stage_tokens'].values[0]).split(':')
        value_list = str(row['stage_values'].values[0]).split(':')
        description_list = str(row['stage_descriptions'].values[0]).split(':')
        team_tags_list = str(row['team_tags'].values[0]).split(':')
        calc_tags_list = str(row['calc_tags'].values[0]).split(':')
        extra_info_list = str(row['stage_extra_info'].values[0]).split(':')
        for x in range(len(token_list)):
            s = Stage()
            s.clear()
            t = token_list[x]
            try:
                v = int(value_list[x])
            except:
                v = 0
            d = description_list[x]
            tt = team_tags_list[x].split(',')
            ct = calc_tags_list[x].split(',')
            ei = extra_info_list[x].split(',')
            self.add_stage(t, v, d, tt, ct, ei)

    def get_int_value(self, row, field, default):
        try:
            value = int(row[field])
        except:
            value = default
        return value
 
    def to_series(self):
        token_list = []
        value_list = []
        description_list = []
        team_tags_list = []
        calc_tags_list = []
        extra_info_list = []
        for stage in self.stages:
            token_list.append(stage.token)
            value_list.append(str(stage.value))
            description_list.append(stage.description)
            team_tags_list.append(",".join(stage.team_tags))
            calc_tags_list.append(",".join(stage.calc_tags))
            extra_info_list.append(",".join(stage.extra_info))
        series_list = [
            self.encoded,
            ":".join(token_list),
            ":".join(value_list),
            ":".join(description_list),
            ":".join(team_tags_list),
            ":".join(calc_tags_list),
            ":".join(extra_info_list),
        ]
        return pd.Series(series_list)

    def fumble_check(self, row):
        ss = StageSet()
        fumble = row['fumble']
        fumble_lost = row['fumble_lost']
        fumble_out_of_bounds = row['fumble_out_of_bounds']
        if fumble == 1:

            description = 'Fumble!! '
            if fumble_out_of_bounds == 1:
                value = 0
                description += 'Out of bounds. '
            elif fumble_lost == 1:
                value = 1
            else:
                value = 0
                description += 'Offense recovers and maintains possession. '
            ss.add_stage('CHANGE_OF_POSSESSION', value, description, [], [])

            if fumble_lost == 1:
                description = 'Defense recovers and returns it [stage_value] yards.'
                ss.add_stage('RETURN', self.get_int_value(row, 'fumble_recovery_1_yards', 0), description, [], [])

        return ss.stages

    def interception_check(self, row):
        ss = StageSet()
        interception = row['interception']
        if interception:
            CALC_interception_return_yards_PASS = int(row['return_yards'])
            ss.add_stage('CHANGE_OF_POSSESSION', 1, "It's picked off!", [], [])
            if CALC_interception_return_yards_PASS == 0:
                description = 'Defender is tackled immediately.'
            elif CALC_interception_return_yards_PASS > 0:
                description = 'Defender returns it [stage_value] yards.'
            else:
                description = 'Defender cuts back and loses [stage_value] yards.'
            ss.add_stage('RETURN', CALC_interception_return_yards_PASS, description, [], [])
        return ss.stages

    def penalty_check(self, row):
        ss = StageSet()
        play_type_nfl = str(row['play_type_nfl'])
        scrimmage_play = False
        if play_type_nfl in ['RUSH','PASS','PENALTY','SACK']:
            scrimmage_play = True;  
        penalty_type = str(row['penalty_type'])
        penalty = self.get_int_value(row, 'penalty', 0)
        penalty_yards = self.get_int_value(row, 'penalty_yards', 0)
        penalty_team = row['penalty_team']
        posteam = row['posteam']

        valid = False
        if play_type_nfl == 'PENALTY': 
            valid = penalties.is_scrimmage_penalty(penalty_type)
        elif penalty == 1:
            valid = penalties.is_valid_penalty(penalty_type)

        if valid:
            description = 'Penalty! ' + penalty_type + ', [stage_value] yards.'
            penalty_tags = penalties.get_token_list(penalty_type)
            if 'SPOTFOUL' not in penalty_tags:
                penalty_yards = penalties.get_yards(penalty_type)
            if posteam == penalty_team:
                description += 'On [posteamname]. '
                penalty_tags.append('ONOFFENSE')
            else:
                description += 'On [defteamname]. '
                penalty_tags.append('ONDEFENSE')
            if not scrimmage_play:
                penalty_tags.append('AUTOFIRSTDOWN')
            ss.add_stage('PENALTY', penalty_yards, description, ['posteamname','defteamname'], [], penalty_tags)
        
        return ss.stages

    def encode(self, row, game_data):
        self.clear()
        play_type_nfl = row["play_type_nfl"]
        if play_type_nfl not in ['KICK_OFF','RUSH','PASS','XP_KICK','PAT2','PUNT','FIELD_GOAL','PENALTY','SACK']:
            return

        if row["qb_spike"] == 1 or row["qb_kneel"] == 1:
            return
            
        posteam = row['posteam']
        qb_scramble = int(row['qb_scramble'])
        
        # play_time = int(row['play_time'])
        try:
            yl = int(row['yardline_100'])
        except:
            if play_type_nfl == 'PENALTY':
                yl = 0
            else:
                print(row["play_id"], row["posteam"], row["defteam"])
                return
        nndx = row['ndx'] + 1
        nrow = game_data.loc[game_data['ndx'] == nndx]
        try:
            nextyl = int(nrow['yardline_100'])
        except:
            nextyl = yl

        CALC_yards_gained = self.get_int_value(row, 'yards_gained', 65)

        nndx = row['ndx'] + 1
        nrow = game_data.loc[game_data['ndx'] == nndx]
        nextpos = nrow['posteam']
        # changeofpos = posteam != nextpos

        if  play_type_nfl == 'KICK_OFF':
            CALC_kick_distance_KO = self.get_int_value(row, 'kick_distance', 65)
            CALC_touchback_KO = row['touchback']
            CALC_touchdown_KO = row['touchdown']
            CALC_nextyl_after_KO = nextyl

            if CALC_touchdown_KO == 1:
                CALC_nextyl_after_KO = 0
            elif CALC_touchback_KO == 1:
                CALC_kick_distance_KO = 65
                CALC_nextyl_after_KO = 75

            desc = '[kicker_player_name] ([defteamname]) kicks off from [CALC_starting_yardline]'
            if CALC_touchback_KO != 1:
                desc += '. It goes [stage_value] yards.'
            self.add_stage('KICK_OFF', CALC_kick_distance_KO, desc, ['kicker_player_name','defteamname'], ['get_pos_desc=CALC_starting_yardline'])

            if CALC_touchback_KO == 1:
                desc = 'It goes into the endzone for a touchback.'
            elif row['kickoff_returner_player_name'] == '':
                desc = 'It is returned by [posteamname] to [CALC_nextyl_after_KO].'
            else:
                if CALC_touchdown_KO == 1:
                    desc = 'It is returned by [kickoff_returner_player_name] ([posteamname]) into the end zone! Touchdown!!'
                else:
                    desc = 'It is returned by [kickoff_returner_player_name] ([posteamname]) to [CALC_nextyl_after_KO].'
            self.add_stage('KICK_OFF_RETURN', CALC_nextyl_after_KO, desc, ['kickoff_returner_player_name','posteamname'], ['get_pos_desc=CALC_nextyl_after_KO'])

            self.stages.extend(self.fumble_check(row))
        elif play_type_nfl == 'RUSH':
            if qb_scramble:
                desc = '[qb_rusher_player_name] scrambles out of the pocket and '
            else:
                desc = '[rusher_player_name] ' 

            run_location = row['run_location']
            run_gap = row['run_gap']

            if run_location == 'middle':
                desc += 'runs up the middle '
            elif run_location in ('right', 'left'):
                if run_gap in ('guard', 'tackle'):
                    desc += 'runs off the ' + run_location + ' ' + run_gap + ' '
                elif run_gap == 'end':
                    desc += 'runs around the ' + run_location + ' ' + run_gap + ' '
                else:
                    desc += 'runs '
            else:
                desc += 'runs '

            if CALC_yards_gained < 0:
                desc += 'for a loss of [stage_value]. '
            elif CALC_yards_gained > 0:
                desc += 'for a gain of [stage_value]. '
            else:
                desc += 'for no gain. '

            self.add_stage('GAIN', CALC_yards_gained, desc, ['qb_rusher_player_name','rusher_player_name'], [])
            self.stages.extend(self.fumble_check(row))

        elif play_type_nfl == 'SACK':
            shotgun = int(row['shotgun'])
            if shotgun:
                desc = "[passer_player_name] is in the shotgun. He gets the snap and he's SACKED!! [posteamname] loses [stage_value] yards."
            else:
                desc = "[passer_player_name] is under center. He takes the snap, drops back and he's SACKED!! [posteamname] loses [stage_value] yards."
            self.add_stage('GAIN', CALC_yards_gained, desc, ['passer_player_name','posteamname'], [])
            self.stages.extend(self.fumble_check(row))

        elif play_type_nfl == 'PASS':
            # interception_player_name = self.get_or_substitute(substitutedefplayers, 'interception_player_name', not posishome, row, 'interception_player_name')
            # passer = self.get_or_substitute(substituteposplayers, 'passer_player_name', posishome, row, 'passer_player_name')
            # receiver = self.get_or_substitute(substituteposplayers, 'receiver_player_name', posishome, row, 'receiver_player_name')

            incomplete_pass = int(row['incomplete_pass'])
            receiver_player_name = row['receiver_player_name']
            pass_length = row['pass_length']
            shotgun = int(row['shotgun'])

            CALC_air_yards_PASS = self.get_int_value(row, 'air_yards', 0)

            pass_location = row['pass_location']
            if shotgun:
                desc = '[passer_player_name] is in the shotgun. He gets the snap and '
            else:
                desc = '[passer_player_name] is under center. He takes the snap and '
            if qb_scramble:
                desc = 'He scrambles out of the pocket and '

            if CALC_air_yards_PASS < 0:
                if pass_location == 'right':
                    desc += 'dumps it off behind the line of scrimmage to the right to '
                elif pass_location == 'left':
                    desc += 'dumps it off behind the line of scrimmage to the left to '
                else:
                    desc += 'trhows a middle screen to '
            elif pass_length == 'short':
                if pass_location == 'right':
                    desc += 'throws a short pass to the right to '
                elif pass_location == 'left':
                    desc += 'throws a short pass to the left to '
                else:
                    desc += 'throws a short pass over the middle to '
            else:
                if pass_location == 'right':
                    desc += 'goes [pass_length] down the right sideline to '
                elif pass_location == 'left':
                    desc += 'goes [pass_length] down the left sideline to '
                else:
                    desc += 'goes [pass_length] down over the middle to '
            if receiver_player_name == 'NA':
                desc += 'no one. '
            else:
                desc +=  '[receiver_player_name] [stage_value] yards.'
            self.add_stage('PASS', CALC_air_yards_PASS, desc, ['passer_player_name','receiver_player_name'], ['pass_length'])

            interception_stages = self.interception_check(row)
            if len(interception_stages) > 0:
                self.stages.extend(interception_stages)
            else:
                CALC_yards_after_catch = CALC_yards_gained - CALC_air_yards_PASS

                if incomplete_pass == 1:
                    desc = 'Incomplete!'
                else:
                    if CALC_yards_after_catch == 0:
                        desc = "He makes the grab and is tackled immediately "
                    elif CALC_yards_after_catch > 0:
                        desc = "He catches the ball and goes [CALC_yards_after_catch] yards "
                    else:
                        desc = "He tries to cut back and loses [CALC_yards_after_catch] yards "
                    if CALC_yards_gained < 0:
                        desc += 'for a loss of [stage_value]. '
                    elif CALC_yards_gained > 0:
                        desc += 'for a gain of [stage_value]. '
                    else:
                        desc += 'for no gain. '

                self.add_stage('GAIN_AFTER_CATCH', CALC_yards_gained, desc, [], ['CALC_yards_after_catch'])
            self.stages.extend(self.fumble_check(row))

        elif play_type_nfl == 'PUNT':
            punt_blocked = row['punt_blocked']
            punt_out_of_bounds = row['punt_out_of_bounds']
            punt_downed = row['punt_downed']
            punt_fair_catch = row['punt_fair_catch']
            fumble_not_forced = row['fumble_not_forced']
            fumble_lost = row['fumble_lost']

            CALC_punt_in_endzone_PUNT = row['punt_in_endzone']
            CALC_kick_distance_PUNT = self.get_int_value(row, 'kick_distance', 50)
            CALC_return_yards_PUNT = self.get_int_value(row, 'return_yards', 0)

            desc = '[posteamname] in punt formation. [punter_player_name] takes the snap. '
            self.add_stage('DESCRIPTION', 0, desc, ['posteamname','punter_player_name'],[])
            if punt_blocked > 0:
                desc = 'Blocked!! [defteamname] takes possession!'
                self.add_stage('PUNT_BLOCKED', 0, desc, ['defteamname'], [])
            elif punt_out_of_bounds:
                desc = "It's punted [stage_value] yards out of bounds."
                self.add_stage('PUNT', CALC_kick_distance_PUNT, desc, [],[])
            else:
                desc = 'He punts it [stage_value] yards.'
                self.add_stage('PUNT', CALC_kick_distance_PUNT, desc, [],[])
                if punt_fair_catch > 0:
                    desc = '[punt_returner_player_name] signals for a fair catch. '
                    self.add_stage('RETURN', 0, desc, ['punt_returner_player_name'], [])
                elif fumble_not_forced > 0:
                    desc = '[punt_returner_player_name] muffs the punt! '
                    self.add_stage('DESCRIPTION', 0, desc, ['punt_returner_player_name'], [])
                    if fumble_lost > 0:
                        desc = '[posteamname] recovers! '
                        self.add_stage('CHANGE_OF_POSSESSION', 1, desc, ['posteamname'], [])
                    else:
                        desc = '[defteamname] recovers and maintains possession! '
                        self.add_stage('CHANGE_OF_POSSESSION', 0, desc, ['defteamname'], [])
                else:
                    desc = 'The punt is fielded cleanly by [punt_returner_player_name]'
                    if punt_fair_catch > 0 or punt_downed > 0:
                        desc += ' who downs it.'
                        self.add_stage('RETURN', 0, desc, ['punt_returner_player_name'], [])
                    else:
                        desc += ' who returns it [stage_value] yards '
                        self.add_stage('RETURN', CALC_return_yards_PUNT, desc, ['punt_returner_player_name'], [])
            if fumble_not_forced == 0:
                self.stages.extend(self.fumble_check(row))

        elif play_type_nfl == 'FIELD_GOAL':
            field_goal_result = row['field_goal_result']
            desc = '[kicker_player_name] ([posteamname]) lines up for a [CALC_field_goal_distane_FG] yard field goal attempt.'
            if field_goal_result == 'made':
                desc += " It's up and it's good!"
                value = 3
            elif field_goal_result == 'blocked':
                desc += " It's blocked!! No good!"
                value = 0
            else:
                desc += " It's no good!"
                value = 0
            self.add_stage('FIELD_GOAL', value, desc, ['posteamname','kicker_player_name'], ['CALC_field_goal_distane_FG'])

        elif play_type_nfl == 'XP_KICK':        
            extra_point_result = row['extra_point_result']
            desc = '[posteamname] lines up for the extra point.'
            if extra_point_result == 'good':
                desc += " It's up and it's good!"
                value = 1
            elif extra_point_result == 'blocked':
                desc += " It's blocked!! No good!"
                value = 0
            else:
                desc += " It's no good!"
                value = 0
            self.add_stage('XP_KICK', value, desc, ['posteamname'], [])

        elif play_type_nfl == 'PAT2':
            two_point_conv_result = row['two_point_conv_result']
            desc = '[posteamname] is going for two.'
            if two_point_conv_result == 'success':
                desc += ' They score!!'
                value = 1
            else:
                desc += ' [defteamname] holds! No good!!'
                value = 0
            self.add_stage('PAT2', value, desc, ['posteamname','defteamname'], [])
        
        self.stages.extend(self.penalty_check(row))
        if len(self.stages) > 0:
            self.encoded = 1

    def dump(self):
        for stage in self.stages:
            stage.dump()
