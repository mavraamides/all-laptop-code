import pandas as pd


class Penalty:
    def __init__(self):
        self.penalties = pd.read_csv('penalties.csv', low_memory=False)

        self.valid = []
        self.yards = {}
        for index, row in self.penalties.iterrows():
            penalty_type = row['penalty_type']
            self.valid.append(penalty_type)
            self.yards[penalty_type] = row['yards']

        self.scrimmage_penalties = []
        scrimmage_penalties = self.penalties[self.penalties["scrimmage_penalty"] == 1]
        for index, row in scrimmage_penalties.iterrows():
            self.scrimmage_penalties.append(row['penalty_type'])

        self.auto_first_down_penalties = []
        auto_first_down_penalties = self.penalties[self.penalties["auto_first_down"] == 1]
        for index, row in auto_first_down_penalties.iterrows():
            self.auto_first_down_penalties.append(row['penalty_type'])

        self.spot_foul_penalties = []
        spot_foul_penalties = self.penalties[self.penalties["spot_foul"] == 1]
        for index, row in spot_foul_penalties.iterrows():
            self.spot_foul_penalties.append(row['penalty_type'])

        self.down_over_penalties = []
        down_over_penalties = self.penalties[self.penalties["down_over"] == 1]
        for index, row in down_over_penalties.iterrows():
            self.down_over_penalties.append(row['penalty_type'])

        self.add_to_yardage_penalties = []
        add_to_yardage_penalties = self.penalties[self.penalties["add_to_yardage"] == 1]
        for index, row in add_to_yardage_penalties.iterrows():
            self.add_to_yardage_penalties.append(row['penalty_type'])

        self.tokens = {}
        for penalty in self.valid:
            token_list = []
            if self.is_auto_first_down(penalty):
                token_list.append('AUTOFIRSTDOWN')
            if self.is_spot_foul(penalty):
                token_list.append('SPOTFOUL')
            if self.is_down_over(penalty):
                token_list.append('DOWNOVER')
            if self.is_add_to_yardage(penalty):
                token_list.append('ADDTOYARDAGE')
            self.tokens[penalty] = token_list

    def get_yards(self, key):
        return self.yards.get(key, 5)
        
    def get_token_list(self, key):
        return self.tokens.get(key, []).copy()

    def is_valid_penalty(self, key):
        return key in self.valid

    def is_scrimmage_penalty(self, key):
        return key in self.scrimmage_penalties

    def is_auto_first_down(self, key):
        return key in self.auto_first_down_penalties

    def is_spot_foul(self, key):
        return key in self.spot_foul_penalties

    def is_down_over(self, key):
        return key in self.down_over_penalties

    def is_add_to_yardage(self, key):
        return key in self.add_to_yardage_penalties

    def dump(self):
        for key in sorted(self.valid):
            print (key, self.get_token_list(key))

if __name__ == '__main__':
    p = Penalty()
    p.dump()