

def get_yardline_category(yardline):
    if yardline >= 99:
        category = 0
    elif yardline >= 98:
        category = 1
    elif yardline >= 95:
        category = 2
    elif yardline >= 90:
        category = 3
    elif yardline >= 80:
        category = 4
    elif yardline >= 50:
        category = 5
    elif yardline >= 30:
        category = 6
    elif yardline >= 20:
        category = 7
    elif yardline >= 10:
        category = 8
    elif yardline >= 5:
        category = 9
    elif yardline >= 2:
        category = 10
    else:
        category = 11
    return category

def get_distance_category(distance):
    if distance >= 20:
        category = 0
    elif distance >= 15:
        category = 1
    elif distance >= 10:
        category = 2
    elif distance >= 5:
        category = 3
    elif distance >= 2:
        category = 4
    else:
        category = 5
    return category

def get_time_category(time_remaining):
    if time_remaining > 1800 + 240:
        category = 0
    elif time_remaining > 1800 + 120:
        category = 1
    elif time_remaining > 1800 + 60:
        category = 2
    elif time_remaining > 1800:
        category = 3
    elif time_remaining > 240:
        category = 4
    elif time_remaining > 120:
        category = 5
    elif time_remaining > 60:
        category = 6
    else:
        category = 7
    return category

def get_score_diff_category(score_diff):
    test_score = abs(score_diff)
    if test_score == 0:
        category = 0
    elif test_score <= 3:
        category = 1
    elif test_score <= 6:
        category = 2
    elif test_score <= 8:
        category = 3
    elif test_score <= 16:
        category = 4
    else:
        category = 5
    if score_diff < 0:
        category = category * -1
    return category

def add_if_match(t1, t2, initval, addval, clear_if_not_match = False):
    if t1 == t2:
        return initval + addval
    elif clear_if_not_match:
        return 0
    else:
        return initval

def square(val):
    try:
        return int(val * val)
    except:
        return val * val
