import pandas as pd 
import math

'''
EV for FG attampt = 
    EV(MAKE) + EV(MISS)
    EV(MAKE) = p(MAKE) * (3 - YardlineValueForOponent(YL after KO))
    EV(MISS) = p(MISS) * - YardlineValueForOponent(YL after KO)
'''
game_data = pd.read_csv('play_by_play_2020.csv')

def fround(x):
    return math.floor(x + 0.5)

def get_punt_ev(yardline, candidates):
    playsinrange = candidates[candidates['yardline_100'] >= yardline - 10]
    playsinrange = playsinrange[playsinrange['yardline_100'] <= yardline + 10]
    
    totalplays = playsinrange.shape[0]
    total = 0
    for index, row in playsinrange.iterrows():
        yardline_100 = row['yardline_100']
        kick_distance = row['kick_distance']
        return_yards = row['return_yards']
        punt_blocked = row['punt_blocked']
        if punt_blocked == 1:
            new_yardline = yardline_100 + 10
        else:
            new_yardline = yardline_100 - kick_distance + return_yards
        if new_yardline <= 0:
            new_yardline = 20
        if new_yardline > 99:
            new_yardline = 99
        try: 
            test = int(new_yardline)
        except:
            new_yardline = 20
        new_yardline = 100 - new_yardline
        total += new_yardline

        # print(yardline_100, kick_distance, return_yards, punt_blocked, new_yardline)
    if totalplays > 0:
        return total / totalplays
    else:
        return 80

def get_fg_ev(yardline, candidates):
    madefgs = candidates[candidates['field_goal_result'] == 'made']
    columns = madefgs["yardline_100"]
    max = int(columns.max())
    if yardline > max:
        return 0
    playsinrange = candidates[candidates['yardline_100'] >= yardline - 8]
    playsinrange = playsinrange[playsinrange['yardline_100'] <= yardline + 7]
    total = playsinrange.shape[0]
    playsinrange = playsinrange[playsinrange['field_goal_result'] == 'made']
    made = playsinrange.shape[0]
    if total == 0:
        return 0
    else:
        return made / total

def get_yl_ev(yardline, candidates):
    candidates = candidates[candidates['yardline_100'] >= yardline - 5]
    candidates = candidates[candidates['yardline_100'] <= yardline + 5]
    # print (candidates)
    total = candidates.shape[0]
    # print ('total', total)
    tds = candidates[candidates['fixed_drive_result'] == 'Touchdown'].shape[0]
    # print ('tds', tds)
    fgs = candidates[candidates['fixed_drive_result'] == 'Field goal'].shape[0]
    # print ('fgs', fgs)
    sts = candidates[candidates['fixed_drive_result'] == 'Safety'].shape[0]
    # print ('sts', sts)
    ev = (7 * tds + 3 * fgs - 2 * sts) / total
    # print ('ev', ev)
    return ev

def get_scrimmage_ev(togo, candidates):
    total_plays = 0
    total_made = 0
    total_fail = 0
    yards_on_made = 0
    yards_on_fail = 0

    for index, row in candidates.iterrows():
        yards_gained = row['yards_gained']
        total_plays += 1
        if yards_gained >= togo:
            total_made += 1
            yards_on_made += yards_gained
        else:
            total_fail += 1
            yards_on_fail += yards_gained
    if total_plays > 0:
        p = total_made / total_plays
    else:
        p = 0
    if total_made > 0:
        avg_on_made = fround(yards_on_made / total_made)
    else:
        avg_on_made = 0
    if total_fail > 0:
        avg_on_fail = fround(yards_on_fail / total_fail)
    else:
        avg_on_fail = 0
    return (p, avg_on_made, avg_on_fail)

def add_empty_team(team, evs):
    for x in range(1,100):
        evs = evs.append(
            {
                'Team':team,                       # [X]
                'yardline_100':x,                  # [X]
                'pos_yl_after_punt':0.0,           # [X]
                'pos_p_FG':0.0,                    # [X]
                'pos_EV_yl':0.0,                   # [X]
                'pos_p_Run':0.0,                   # [X]
                'pos_gain_when_Run_made':0.0,      # [X]
                'pos_gain_when_Run_fail':0.0,      # [X]
                'pos_p_Pass':0.0,                  # [ ]
                'pos_gain_when_Pass_made':0.0,     # [ ]
                'pos_gain_when_Pass_fail':0.0,     # [ ]
                'def_yl_after_punt':0.0,           # [X]
                'def_p_FG':0.0,                    # [X]
                'def_EV_yl':0.0,                   # [X]
                'def_p_Run':0.0,                   # [X]
                'def_gain_when_Run_made':0.0,      # [X]
                'def_gain_when_Run_fail':0.0,      # [X]
                'def_p_Pass':0.,                   # [ ]
                'def_gain_when_Pass_made':0.0,     # [ ]
                'def_gain_when_Pass_fail':0.0      # [ ]
            },
            ignore_index=True
        )
    return evs

def get_stats_for_team(team, evs):
    teampos = game_data[game_data['posteam'] == team] 
    teamdef = game_data[game_data['defteam'] == team] 

    candidates = teampos[teampos['play_type_nfl'] == 'FIELD_GOAL']
    candidates = candidates[["yardline_100","play_type_nfl","field_goal_result"]] 
    for x in range(1, 100):
        evs.loc[(evs["Team"] == team) & (evs["yardline_100"] == x), 'pos_p_FG'] = get_fg_ev(x, candidates)
    candidates = teamdef[teamdef['play_type_nfl'] == 'FIELD_GOAL']
    candidates = candidates[["yardline_100","play_type_nfl","field_goal_result"]] 
    for x in range(1, 100):
        evs.loc[(evs["Team"] == team) & (evs["yardline_100"] == x), 'def_p_FG'] = get_fg_ev(x, candidates)

    candidates = teampos[teampos['play_type_nfl'] == 'PUNT']
    candidates = candidates[["yardline_100","kick_distance","return_yards","punt_blocked"]] 
    for x in range(1, 100):
        evs.loc[(evs["Team"] == team) & (evs["yardline_100"] == x), 'pos_yl_after_punt'] = get_punt_ev(x, candidates)
    candidates = teamdef[teamdef['play_type_nfl'] == 'PUNT']
    candidates = candidates[["yardline_100","kick_distance","return_yards","punt_blocked"]] 
    for x in range(1, 100):
        evs.loc[(evs["Team"] == team) & (evs["yardline_100"] == x), 'def_yl_after_punt'] = get_punt_ev(x, candidates)

    candidates = teampos[teampos['down'] == 1]
    candidates = candidates[["yardline_100","fixed_drive_result"]]
    for x in range(1, 100):
        evs.loc[(evs["Team"] == team) & (evs["yardline_100"] == x), 'pos_EV_yl'] = get_yl_ev(x, candidates)
    candidates = teamdef[teamdef['down'] == 1]
    candidates = candidates[["yardline_100","fixed_drive_result"]]
    for x in range(1, 100):
        evs.loc[(evs["Team"] == team) & (evs["yardline_100"] == x), 'def_EV_yl'] = get_yl_ev(x, candidates)

    candidates = teampos[teampos['down'] == 4]
    candidates = candidates[candidates['play_type_nfl'] == 'RUSH']
    candidates = candidates[["yards_gained"]]
    for x in range(1, 100):
        (p, avg_on_made, avg_on_fail) = get_scrimmage_ev(x, candidates)
        evs.loc[(evs["Team"] == team) & (evs["yardline_100"] == x), 'pos_p_Run'] = p
        evs.loc[(evs["Team"] == team) & (evs["yardline_100"] == x), 'pos_gain_when_Run_made'] = avg_on_made
        evs.loc[(evs["Team"] == team) & (evs["yardline_100"] == x), 'pos_gain_when_Run_fail'] = avg_on_fail
    candidates = teamdef[teamdef['down'] == 4]
    candidates = candidates[candidates['play_type_nfl'] == 'RUSH']
    candidates = candidates[["yards_gained"]]
    for x in range(1, 100):
        (p, avg_on_made, avg_on_fail) = get_scrimmage_ev(x, candidates)
        evs.loc[(evs["Team"] == team) & (evs["yardline_100"] == x), 'def_p_Run'] = p
        evs.loc[(evs["Team"] == team) & (evs["yardline_100"] == x), 'def_gain_when_Run_made'] = avg_on_made
        evs.loc[(evs["Team"] == team) & (evs["yardline_100"] == x), 'def_gain_when_Run_fail'] = avg_on_fail

    candidates = teampos[teampos['down'] == 4]
    candidates = candidates[candidates['play_type_nfl'] == 'PASS']
    candidates = candidates[["yards_gained"]]
    for x in range(1, 100):
        (p, avg_on_made, avg_on_fail) = get_scrimmage_ev(x, candidates)
        evs.loc[(evs["Team"] == team) & (evs["yardline_100"] == x), 'pos_p_Pass'] = p
        evs.loc[(evs["Team"] == team) & (evs["yardline_100"] == x), 'pos_gain_when_Pass_made'] = avg_on_made
        evs.loc[(evs["Team"] == team) & (evs["yardline_100"] == x), 'pos_gain_when_Pass_fail'] = avg_on_fail
    candidates = teamdef[teamdef['down'] == 4]
    candidates = candidates[candidates['play_type_nfl'] == 'PASS']
    candidates = candidates[["yards_gained"]]
    for x in range(1, 100):
        (p, avg_on_made, avg_on_fail) = get_scrimmage_ev(x, candidates)
        evs.loc[(evs["Team"] == team) & (evs["yardline_100"] == x), 'def_p_Pass'] = p
        evs.loc[(evs["Team"] == team) & (evs["yardline_100"] == x), 'def_gain_when_Pass_made'] = avg_on_made
        evs.loc[(evs["Team"] == team) & (evs["yardline_100"] == x), 'def_gain_when_Pass_fail'] = avg_on_fail

    return evs
    

class EVCalc:
    def __init__(self):
        self.GetTable()


    def GetTable(self):
        tablename = 'evs.csv'
        try:
            self.evs = pd.read_csv(tablename)
            print(self.evs.shape)
        except:
            print('Building evs.csv')
            self.evs = pd.DataFrame(
                columns = [
                    'Team', 
                    'yardline_100', 
                    'pos_yl_after_punt',
                    'pos_p_FG', 
                    'pos_EV_yl',
                    'pos_p_Run',
                    'pos_gain_when_Run_made',
                    'pos_gain_when_Run_fail',
                    'pos_p_Pass',
                    'pos_gain_when_Pass_made',
                    'pos_gain_when_Pass_fail',
                    'def_yl_after_punt',
                    'def_p_FG', 
                    'def_EV_yl',
                    'def_p_Run',
                    'def_gain_when_Run_made',
                    'def_gain_when_Run_fail',
                    'def_p_Pass',
                    'def_gain_when_Pass_made',
                    'def_gain_when_Pass_fail'
                    ]
            )

            for team in game_data['posteam'].unique().tolist():
                if str(team) == 'nan':
                    continue
                print (team)
                self.evs = add_empty_team(team, self.evs)
                self.evs = get_stats_for_team(team, self.evs)
            # print(self.evs)
            self.evs.to_csv(tablename)

    def GetAvg(self, posteam, defteam, stat, ndx):
        # print(stat, ndx)
        if ndx < 1:
            ndx = 1
        if ndx > 99:
            ndx = 99
        posval = self.evs.loc[(self.evs["Team"] == posteam) & (self.evs["yardline_100"] == ndx)]['pos_' + stat].values[0]
        # print(posval)
        defval = self.evs.loc[(self.evs["Team"] == defteam) & (self.evs["yardline_100"] == ndx)]['def_' + stat].values[0]
        # print(defval)
        avgval = (posval + defval) / 2
        return avgval

    def CalcPuntEV(self, posteam, defteam, yardline):
        '''
        EV for Punt attampt = 
            - YardlineValueForOponent(YL after Punt)
        '''
        expected_yl = fround(self.GetAvg(posteam, defteam, 'yl_after_punt', yardline))
        return -1 * self.GetAvg(posteam, defteam, 'EV_yl', expected_yl)

    def CalcFGEV(self, posteam, defteam, yardline):
        '''
        EV for FG attampt = 
            EV(MAKE) + EV(MISS)
            EV(MAKE) = p(MAKE) * (3 - YardlineValueForOponent(YL after KO))
            EV(MISS) = p(MISS) * - YardlineValueForOponent(YL after KO)
        '''
        pMake = self.GetAvg(posteam, defteam, 'p_FG', yardline)
        ylv_after_KO = self.GetAvg(posteam, defteam, 'EV_yl', 75)
        new_yl = 100 - yardline - 8
        ylv_after_Miss = self.GetAvg(posteam, defteam, 'EV_yl', new_yl)
        ev = pMake * (3.0 - ylv_after_KO) - (1.0 - pMake) * ylv_after_Miss
        # print(pMake, ylv_after_KO, ylv_after_Miss)
        return ev

    def CalcRunEV(self, posteam, defteam, yardline, togo):
        '''
        EV for Run attampt = 
            EV(MAKE) + EV(MISS)
            EV(MAKE) = p(MAKE) * YardlineValue(YL after MAKW)
            EV(MISS) = p(MISS) * - YardlineValueForOponent(YL after MISS)
        '''
        pMake = self.GetAvg(posteam, defteam, 'p_Run', togo)
        yl_after_make = yardline - fround(self.GetAvg(posteam, defteam, 'gain_when_Run_made', togo))
        ylv_make = self.GetAvg(posteam, defteam, 'EV_yl', yl_after_make)
        yl_after_miss = 100 - (yardline - fround(self.GetAvg(posteam, defteam, 'gain_when_Run_fail', togo)))
        ylv_miss = self.GetAvg(posteam, defteam, 'EV_yl', yl_after_miss)
        ylv_after_KO = self.GetAvg(posteam, defteam, 'EV_yl', 75)
        # print(pMake, yl_after_make, ylv_make, yl_after_miss, ylv_miss, ylv_after_KO, temp)
        ev = pMake * ylv_make - (1 - pMake) * ylv_miss - ylv_after_KO
        return ev
    
    def CalcPassEV(self, posteam, defteam, yardline, togo):
        '''
        EV for Pass attampt = 
            EV(MAKE) + EV(MISS)
            EV(MAKE) = p(MAKE) * YardlineValue(YL after MAKW)
            EV(MISS) = p(MISS) * - YardlineValueForOponent(YL after MISS)
        '''
        pMake = self.GetAvg(posteam, defteam, 'p_Pass', togo)
        yl_after_make = yardline - fround(self.GetAvg(posteam, defteam, 'gain_when_Pass_made', togo))
        ylv_make = self.GetAvg(posteam, defteam, 'EV_yl', yl_after_make)
        yl_after_miss = 100 - (yardline - fround(self.GetAvg(posteam, defteam, 'gain_when_Pass_fail', togo)))
        ylv_miss = self.GetAvg(posteam, defteam, 'EV_yl', yl_after_miss)
        ylv_after_KO = self.GetAvg(posteam, defteam, 'EV_yl', 75)
        # print(pMake, yl_after_make, ylv_make, yl_after_miss, ylv_miss)
        ev = pMake * ylv_make - (1 - pMake) * ylv_miss - ylv_after_KO
        return ev

    def GetBestOption(self, posteam, defteam, yardline, togo, suppress_print = True):
        bestscore = self.CalcPuntEV(posteam, defteam, yardline)
        bestoption = "PUNT"
        if not suppress_print:
            print ("PUNT: ", bestscore)

        ev = self.CalcFGEV(posteam, defteam, yardline)
        if not suppress_print:
            print ("FGA : ", ev)
        if ev > bestscore:
            bestscore = ev
            bestoption = "FGA"
                    
        ev = self.CalcRunEV(posteam, defteam, yardline, togo)
        if not suppress_print:
            print ("RUSH: ", ev)
        if ev > bestscore:
            bestscore = ev
            bestoption = "RUSH"
         
        ev = self.CalcPassEV(posteam, defteam, yardline, togo)
        if not suppress_print:
            print ("PASS: ", ev)
        if ev > bestscore:
            bestscore = ev
            bestoption = "PASS"

        return bestoption

    def MakeChart(self, posteam, defteam):
        skip = 0
        for yardline in range(1,100):
            if skip <= 0:
                print('  ',end='')
                for togo in range(1,21):
                    print('{:4d} '.format(togo), end='')
                print('')
                skip = 10
            print('{:2d} '.format(yardline),end='')
            for togo in range(1,21):
                targetline = yardline - togo
                if targetline >= 90:
                    result = '----'
                else:
                    result = self.GetBestOption(posteam, defteam, yardline, togo)
                print('{:4s} '.format(result), end='')
            print('')
            skip -= 1

if __name__ == "__main__":
    evc = EVCalc()
    yl = 35
    togo = 3
    posteam = 'PIT'
    defteam = 'KC'
    print('Punt', evc.CalcPuntEV(posteam, defteam, yl))
    print('FG', evc.CalcFGEV(posteam, defteam, yl))
    print('RUN', evc.CalcRunEV(posteam, defteam, yl, togo))
    print('PASS', evc.CalcPassEV(posteam, defteam, yl, togo))
    print(evc.GetBestOption(posteam,defteam, yl, togo))

    # evc.MakeChart('KC','MIN')