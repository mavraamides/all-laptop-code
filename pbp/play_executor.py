class PlayEecutor:

    def interception_check(self, gs, row, yards_gained, play_time):
        interception = self.get_int_value(row, 'interception', 0)
        return_yards = self.get_int_value(row, 'return_yards', 0)
        if interception == 1:
            gs.balltracker.gain_yards(yards_gained, True)
            gs.change_of_possession()
            gs.gain_yards_on_scrimmage(return_yards, play_time, True)
            gs.balltracker.reset_dandd()
        return interception

    def fumble_check(self, gs, row):
        fumble_out_of_bounds = row['fumble_out_of_bounds'].values[0]
        fumble = row['fumble'].values[0]
        fumble_lost = row['fumble_lost'].values[0]
        try:
            fumble_recovery_1_yards = int(row['fumble_recovery_1_yards'].values[0])
        except:
            fumble_recovery_1_yards = 0
        desc = ''
        if fumble == 1:
            if fumble_lost == 1:
                gs.change_of_possession()
                gs.gain_yards_on_scrimmage(fumble_recovery_1_yards, 0)
                gs.balltracker.reset_dandd()
        return fumble_recovery_1_yards

    def mark_as_used(self, ndx):
        game_data["used"] = game_data.apply (lambda row: add_if_match(row["ndx"], 
            ndx, row["used"], 1), axis=1)

    def get_int_value(self, row, field, default):
        try:
            value = int(row[field].values[0])
        except:
            value = default
        return value

    def print(self, gs, row):
        posteam = row['posteam'].values[0]
        defteam = row['defteam'].values[0]
        touchdown = int(row['touchdown'].values[0])
        play_time = int(row['play_time'].values[0])
        air_yards = self.get_int_value(row, 'air_yards', 0)
        yl = int(row['yardline_100'].values[0])
        yards_gained = int(row['yards_gained'].values[0])
        if yards_gained > gs.balltracker.yardline:
            yards_gained = gs.balltracker.yardline

        nndx = row['ndx'].values[0] + 1
        nrow = game_data.loc[game_data['ndx'] == nndx]
        nextyl = self.get_int_value(row, 'yardline_100, yl')
        nextpos = nrow['posteam'].values[0]
        changeofpos = posteam != nextpos

        play_type_nfl = row["play_type_nfl"].values[0]
        if  play_type_nfl == 'KICK_OFF':
            kick_distance = row['kick_distance'].values[0]
            touchback = row['touchback'].values[0]
            if gs.balltracker.yardline != 35:
                nextyl = nextyl + gs.balltracker.yardline - 35
            if changeofpos:
                nextyl = 100 - nextyl
            if gs.balltracker.yardline < 35:
                if touchback == 1:
                    kick_distance = 65
                touchback = 0
            if touchdown == 1:
                gs.set_yardline_after_kickoff(0, play_time)
            else:
                gs.set_yardline_after_kickoff(nextyl, play_time)
                self.fumble_check(gs, row)

        elif play_type_nfl == 'RUSH':
            gs.gain_yards_on_scrimmage(yards_gained, play_time)
            self.fumble_check(gs, row)

        elif play_type_nfl == 'PASS':
            pass_length = row['pass_length'].values[0]
            if air_yards > gs.balltracker.yardline:
                air_yards = gs.balltracker.yardline
            if pass_length == 'deep' and gs.balltracker.yardline < 20:
                pass_length = 'short'
            yac = yards_gained - air_yards
            interception = self.interception_check(gs, row,  air_yards, play_time)
            if interception == 0:
                gs.gain_yards_on_scrimmage(yards_gained, play_time)

        elif play_type_nfl == 'XP_KICK':
            extra_point_result = row['extra_point_result'].values[0]
            if extra_point_result == 'good':
                gs.statemachine.extra_point_good_event()
                gs.gameclock.advance(0, gs)
            elif extra_point_result == 'blocked':
                gs.statemachine.extra_point_failed_event()
                gs.gameclock.advance(0, gs)
            else:
                gs.statemachine.extra_point_failed_event()
                gs.gameclock.advance(0, gs)

        elif play_type_nfl == 'PAT2':
            two_point_conv_result = row['two_point_conv_result'].values[0]
            if two_point_conv_result == 'success':
                gs.statemachine.extra_point_two_points_good_event()
                gs.gameclock.advance(0, gs)
            else:
                gs.statemachine.extra_point_failed_event()
                gs.gameclock.advance(0, gs)

        elif play_type_nfl == 'FIELD_GOAL':
            ball_kicked_from = gs.balltracker.yardline + 8
            new_position = ball_kicked_from
            if new_position < 20:
                new_position = 20
            extra_point_result = row['field_goal_result'].values[0]
            if extra_point_result == 'made':
                gs.statemachine.field_goal_good_event()
                gs.gameclock.advance(play_time, gs)
            elif extra_point_result == 'blocked':
                gs.balltracker.yardline = new_position
                gs.change_of_possession()
                gs.gameclock.advance(play_time, gs)
            else:
                gs.balltracker.yardline = new_position
                gs.change_of_possession()
                gs.gameclock.advance(play_time, gs)

        elif play_type_nfl == 'PUNT':
            punt_blocked = row['punt_blocked'].values[0]
            punt_in_endzone = row['punt_in_endzone'].values[0]
            punt_out_of_bounds = row['punt_out_of_bounds'].values[0]
            fumble_not_forced = row['fumble_not_forced'].values[0]
            fumble_lost = row['fumble_lost'].values[0]
            try:
                kick_distance = int(row['kick_distance'].values[0])
            except:
                kick_distance = gs.balltracker.yardline
                if kick_distance > 60:
                    kick_distance = 60        
                else:
                    punt_in_endzone = 1
            try:
                return_yards = int(row['return_yards'].values[0])
            except:
                return_yards = 0

            if punt_blocked > 0:
                gs.change_of_possession()
                new_yl_after_punt = gs.balltracker.yardline - random.randrange(-10, 30)
                gs.set_yardline_after_punt(new_yl_after_punt, play_time)
            elif punt_out_of_bounds:
                gs.change_of_possession()
                new_yl_after_punt = gs.balltracker.yardline + kick_distance
                gs.set_yardline_after_punt(new_yl_after_punt, play_time)
            else:
                if punt_in_endzone > 0:
                    gs.change_of_possession()
                    new_yl_after_punt = 80
                    gs.set_yardline_after_punt(new_yl_after_punt, play_time)
                else:
                    if fumble_not_forced > 0:
                        if fumble_lost > 0:
                            new_yl_after_punt = gs.balltracker.yardline - kick_distance + return_yards
                            gs.set_yardline_after_punt(new_yl_after_punt, play_time)
                        else:
                            gs.change_of_possession()
                            new_yl_after_punt = gs.balltracker.yardline + kick_distance - return_yards
                            gs.set_yardline_after_punt(new_yl_after_punt, play_time)

                    else:
                        gs.change_of_possession()
                        new_yl_after_punt = gs.balltracker.yardline + kick_distance - return_yards
                        gs.set_yardline_after_punt(new_yl_after_punt, play_time)
                        self.fumble_check(gs, row)