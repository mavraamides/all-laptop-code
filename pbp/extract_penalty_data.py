import pandas as pd 


game_data = pd.read_csv('play_by_play_2020.csv')

penalty_stats = {}

last_down = 1
last_penalty_type = ''
last_pos_pen = False
for index, row in game_data.iterrows():
    
    try:
        penalty_yards = int(row['penalty_yards'])
    except:
        penalty_yards = 0
    try:
        down = int(row['down'])
    except:
        down = 0
    penalty_type = str(row['penalty_type'])
    penalty_team = str(row['penalty_team'])
    posteam = str(row['posteam'])
    row_data = penalty_stats.get(penalty_type, [0, 0, 0, 0, 0, 0])
    row_data[0] += 1
    row_data[1] += penalty_yards
    if penalty_team == posteam:
        row_data[2] += 1
        pos_pen = True
    else:
        row_data[3] += 1
        pos_pen = False
    penalty_stats[penalty_type] = row_data

    if not last_pos_pen and down == 1:
        row_data = penalty_stats.get(last_penalty_type, [0, 0, 0, 0, 0, 0])
        row_data[4] += 1
        penalty_stats[last_penalty_type] = row_data
    if last_pos_pen and down == last_down:
        row_data = penalty_stats.get(last_penalty_type, [0, 0, 0, 0, 0, 0])
        row_data[5] += 1
        penalty_stats[last_penalty_type] = row_data

    last_down = down
    last_penalty_type = penalty_type
    last_pos_pen = pos_pen
    
keys = penalty_stats.keys()

for key in sorted(penalty_stats.keys()):
    row_data = penalty_stats[key]
    cnt = row_data[0]
    t_yards = row_data[1]
    t_pos = row_data[2]
    t_def = row_data[3]
    fdown = row_data[4]
    down_over = row_data[5]
    t_pos_div = t_pos
    if t_pos_div == 0:
        t_pos_div = 1
    t_def_div = t_def
    if t_def_div == 0:
        t_def_div = 1
    print ("{:40}".format(key), end='')
    print ("{:8.2f}".format(t_yards / cnt), end='')
    print ("{:8.2f}".format(t_yards), end='')
    print ("{:8.2f}".format(t_pos), end='')
    print ("{:8.2f}".format(t_def), end='')
    print ("{:8.2f}".format(fdown / t_def_div), end='')
    print ("{:8.2f}".format(down_over / t_pos_div), end='')
    print('')
