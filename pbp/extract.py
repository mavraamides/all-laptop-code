# down, ydstogo, yardline_100, postteam, defteam, quarter_seconds_remaining, half_seconds_remaining, game_seconds_remaining, posteam_timeouts_remaining, defteam_timeouts_remaining, postteam_score, defteam_score, play_type_nfl 
# 
import pandas as pd
from stageset import Stage, StageSet
season = pd.read_csv('play_by_play_2020.csv', low_memory=False)
from categories import *

# game_data = season[[
#     "play_id",
#     "down",
#     "ydstogo",
#     "yardline_100", 
#     "yards_gained",
#     "fumble_recovery_1_yards",
#     "posteam", 
#     "defteam",
#     "quarter_seconds_remaining",
#     "half_seconds_remaining",
#     "game_seconds_remaining",
#     "posteam_timeouts_remaining",
#     "defteam_timeouts_remaining",
#     "posteam_score",
#     "defteam_score",
#     "air_yards",
#     "play_type_nfl",
#     "pass_length",
#     "pass_location",
#     "incomplete_pass",
#     "shotgun",
#     "qb_spike",
#     "qb_kneel",
#     "qb_scramble",
#     "run_location",
#     "run_gap",
#     "play_clock",
#     "penalty_yards",
#     "penalty_type",
#     "kick_distance",
#     "extra_point_result",
#     "two_point_conv_result",
#     "field_goal_result",
#     "return_yards",
#     "punt_blocked",
#     "incomplete_pass",
#     "touchback",
#     "interception",
#     "punt_inside_twenty",
#     "punt_in_endzone",
#     "punt_out_of_bounds",
#     "punt_downed",
#     "punt_fair_catch",
#     "kickoff_out_of_bounds",
#     "kickoff_fair_catch",
#     "fumble_forced",
#     "fumble_not_forced",
#     "fumble_out_of_bounds",
#     "safety",
#     "penalty",
#     "fumble_lost",
#     "own_kickoff_recovery",
#     "own_kickoff_recovery_td",
#     "sack",
#     "touchdown",
#     "extra_point_attempt",
#     "two_point_attempt",
#     "field_goal_attempt",
#     "kickoff_attempt",
#     "punt_attempt",
#     "fumble",
#     "complete_pass",
#     "own_kickoff_recovery",
#     "score_differential_post",
#     "passer_player_name",
#     "receiver_player_name",
#     "rusher_player_name",
#     "interception_player_name",
#     "punt_returner_player_name",
#     "kickoff_returner_player_name",
#     "punter_player_name",
#     "kicker_player_name",
#     "desc",
#     ]]

game_data = season[[
    "play_id",
    "down",
    "ydstogo",
    "yardline_100", 
    "yards_gained",
    "home_team",
    "posteam", 
    "defteam",
    "game_seconds_remaining",
    "score_differential_post",
    "posteam_timeouts_remaining",
    "defteam_timeouts_remaining",
    "passer_player_name",
    "receiver_player_name",
    "rusher_player_name",
    "interception_player_name",
    "punt_returner_player_name",
    "kickoff_returner_player_name",
    "punter_player_name",
    "kicker_player_name",
    "desc",


    "fumble_recovery_1_yards",
    "posteam_score",
    "defteam_score",
    "air_yards",
    "play_type_nfl",
    "pass_length",
    "pass_location",
    "incomplete_pass",
    "shotgun",
    "qb_spike",
    "qb_kneel",
    "qb_scramble",
    "run_location",
    "run_gap",
    "play_clock",
    "penalty_yards",
    "penalty_type",
    "penalty_team",
    "kick_distance",
    "extra_point_result",
    "two_point_conv_result",
    "field_goal_result",
    "return_yards",
    "punt_blocked",
    "touchback",
    "interception",
    "punt_inside_twenty",
    "punt_in_endzone",
    "punt_out_of_bounds",
    "punt_downed",
    "punt_fair_catch",
    "kickoff_out_of_bounds",
    "kickoff_fair_catch",
    "fumble_forced",
    "fumble_not_forced",
    "fumble_out_of_bounds",
    "safety",
    "penalty",
    "fumble_lost",
    "own_kickoff_recovery",
    "own_kickoff_recovery_td",
    "sack",
    "touchdown",
    "extra_point_attempt",
    "two_point_attempt",
    "field_goal_attempt",
    "kickoff_attempt",
    "punt_attempt",
    "fumble",
    "complete_pass",
    "own_kickoff_recovery",
    ]]

def update_play_time(row):
    ndx = row["ndx"]
    if ndx == 46405:
        print ("!!!")
    all_same = False
    try:
        incomplete_pass = int(row["incomplete_pass"].values[0])
    except:
        incomplete_pass = 0
    try:
        penalty = int(row["penalty"])
    except:
        penalty = 0
    if row["play_type_nfl"] not in ['RUSH','PASS','SACK','PENALTY']:
        all_same = True
    elif incomplete_pass == 1:
        all_same = True
    play_time = row['play_time']
    play_time_run_clock = 40
    play_time_hurry_up = 20
    if all_same:
        play_time_run_clock = play_time
        play_time_hurry_up = play_time
    if penalty == 1:
        play_time_run_clock = play_time
    if play_time > 40: play_time_run_clock = play_time
    if play_time < 20: play_time_hurry_up = play_time
    return pd.Series([str(play_time_run_clock), str(play_time_hurry_up)])

def update_row(row):
    ss = StageSet()
    ss.encode(row, game_data)
    return ss.to_series()

# game_data = game_data[game_data['posteam'] == 'ARI']

game_data["stage_encoded"] = ""
game_data["stage_tokens"] = ""
game_data["stage_values"] = ""
game_data["stage_descriptions"] = ""
game_data["team_tags"] = ""
game_data["calc_tags"] = ""
game_data["stage_extra_info"] = ""
game_data['ndx'] = list(range(len(game_data.index)))
game_data["play_time"] = game_data['game_seconds_remaining'].diff(periods=-1)
game_data["play_time_run_clock"] = ""
game_data["play_time_hurry_up"] = ""
game_data[['play_time_run_clock','play_time_hurry_up', ]] = game_data.apply(update_play_time, axis=1)
game_data[['stage_encoded','stage_tokens', 'stage_values','stage_descriptions','team_tags','calc_tags', 'stage_extra_info']] = game_data.apply(update_row, axis=1)
game_data["used"] = 0
game_data["match_score"] = 0
game_data["yardline_category"] = game_data.apply (lambda row: get_yardline_category(row["yardline_100"]), axis=1)
game_data["distance_category"] = game_data.apply (lambda row: get_distance_category(row["ydstogo"]), axis=1)
game_data["time_category"] = game_data.apply (lambda row: get_time_category(row["game_seconds_remaining"]), axis=1)
game_data["score_diff_category"] = game_data.apply (lambda row: get_score_diff_category(row["score_differential_post"]), axis=1)


game_data = game_data[[
    'ndx',
    "down",
    "ydstogo",
    "yardline_100", 
    "yards_gained",
    "home_team",
    "posteam", 
    "defteam",
    "game_seconds_remaining",
    "score_differential_post",
    "passer_player_name",
    "receiver_player_name",
    "rusher_player_name",
    "interception_player_name",
    "punt_returner_player_name",
    "kickoff_returner_player_name",
    "punter_player_name",
    "kicker_player_name",
    "desc",
    "play_type_nfl",
    "incomplete_pass",
    "stage_tokens",
    "stage_values",
    "stage_descriptions",
    "team_tags",
    "calc_tags",
    "stage_encoded",
    "stage_extra_info",
    "play_time",
    "play_time_run_clock",
    "play_time_hurry_up",
    "used",
    "match_score",
    "yardline_category",
    "distance_category",
    "time_category",
    "score_diff_category",
]]
game_data.to_csv('game_data.csv')

# player_data = pd.DataFrame(
#     columns = [
#         'Team', 
#         'category', 
#         'player_name',
#         'plays'
#         ]
# )

# def add_player_data(team, category, name):
#     global player_data
#     if not pd.isnull(name):
#         player_data = player_data.append({
#             "Team":team,
#             "category":category,
#             "player_name":name,
#             "plays":1
#         },
#         ignore_index=True)

# for ndx, row in game_data.iterrows():
#     add_player_data(row["posteam"], "passer_player_name", row["passer_player_name"])
#     add_player_data(row["posteam"], "receiver_player_name", row["receiver_player_name"])
#     add_player_data(row["posteam"], "rusher_player_name", row["rusher_player_name"])
#     add_player_data(row["defteam"], "interception_player_name", row["interception_player_name"])
#     add_player_data(row["defteam"], "punt_returner_player_name", row["punt_returner_player_name"])
#     add_player_data(row["posteam"], "kickoff_returner_player_name", row["kickoff_returner_player_name"])
#     add_player_data(row["posteam"], "punter_player_name", row["punter_player_name"])
#     if row["play_type_nfl"] == 'KICK_OFF':
#         add_player_data(row["defteam"], "kicker_player_name_KO", row["kicker_player_name"])
#     elif row["play_type_nfl"] == 'FIELD_GOAL':
#         add_player_data(row["posteam"], "kicker_player_name_FG", row["kicker_player_name"])

# player_data.to_csv("player_data.csv")


# ss = StageSet()
# print (ss.to_series())
# s = Stage()
# s.token = 'T'
# s.value = 1
# s.description = "Team's Desscription."
# s.team_tags = ['tt1','tt2']
# s.calc_tags = ['ct1','ct2']
# ss.stages.append(s)
# print (ss.to_series())
# ss.stages.append(s)
# print (ss.to_series())


# kick_data = game_data[game_data["kick_distance"] > 0]
# kick_data = kick_data[kick_data["play_type_nfl"] == 'KICK_OFF']
# kick_data.to_csv('kick_data.csv')

# columns = season.columns.values
# penalty_types = season.groupby('penalty_type')['play_id'].nunique()
# # columns.sort()
# print(columns)
# print(penalty_types)
# play_types = game_data.groupby('play_type_nfl')['ndx'].nunique()
# print(play_types)

# print(game_data.groupby('run_location')['ndx'].nunique())
# print(game_data.groupby('run_gap')['ndx'].nunique())
# print(game_data.groupby('extra_point_result')['ndx'].nunique())
# print(game_data.groupby('two_point_conv_result')['ndx'].nunique())
# print(game_data.groupby('field_goal_result')['ndx'].nunique())
print(game_data.groupby('posteam')['ndx'].nunique())

