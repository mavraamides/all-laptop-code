import pandas as pd
import random
from statemachine import StateMachine, State
from extract_ev_charts import EVCalc
from stageset import Stage, StageSet
from go_for_2 import GoForTwo
from categories import *
from time import time
# [X] PORT
class GameClock:
    def __init__(self):
        self.reset()

    def reset(self):
        self.quarter = 1
        self.game_seconds_remaining = 3600

    def get_quarter_desc(self):
        if self.quarter == 1:
            return 'first quarter'
        elif self.quarter == 2:
            return 'second quarter'
        elif self.quarter == 3:
            return 'third quarter'
        elif self.quarter == 4:
            return 'fourth quarter'
        elif self.quarter == 5:
            return 'overtime'

    def get_quarter_seconds_remaining(self):
        if self.quarter == 1:
            return self.game_seconds_remaining - 2700
        elif self.quarter == 2:
            return self.game_seconds_remaining - 1800
        elif self.quarter == 3:
            return self.game_seconds_remaining - 900
        else:
            return self.game_seconds_remaining

    def get_half_seconds_remaining(self):
        if self.quarter == 1 or self.quarter == 2:
            return self.game_seconds_remaining - 1800
        else:
            return self.game_seconds_remaining

    def get_clock_desc(self):
        minutes = int(int(self.get_quarter_seconds_remaining()) / 60)
        seconds = int(int(self.get_quarter_seconds_remaining()) % 60)
        return "{:d}".format(minutes) + ':' + "{:02d}".format(seconds)

    def print(self):
        print (self.get_clock_desc(), 'left in', self.get_quarter_desc())
        # print(self.get_quarter_seconds_remaining(), self.get_half_seconds_remaining(), self.game_seconds_remaining)

    def advance(self, seconds, gs):
        if self.get_half_seconds_remaining() > 120 and seconds >= self.get_half_seconds_remaining() - 120:
            print ('Two minute warning!')
            seconds = self.get_half_seconds_remaining() - 120

        if seconds >= self.get_quarter_seconds_remaining():
            seconds = self.get_quarter_seconds_remaining()
            if not gs.statemachine.is_ready_for_extra_point_state:
                print ('End of ' + self.get_quarter_desc())
                gs.print_score_table()

                if self.quarter < 4:
                    self.quarter += 1

                    if self.quarter == 3:
                        gs.homepossession = gs.homesecondhalfchoice
                        gs.balltracker.yardline = 35
                        gs.balltracker.reset_dandd()
                        if gs.statemachine.is_ready_for_scrimmage_state:
                            gs.statemachine.seond_half_kickoff_event()

        self.game_seconds_remaining -= seconds

        if self.game_seconds_remaining < 0:
            self.game_seconds_remaining = 0

# [X] PORT
class BallTracker:
    def __init__(self):
        self.reset_dandd()
        self.yardline = 35
        self.already_advanced_down = False

    def start_play(self):
        self.already_advanced_down = False

    def reset_dandd(self):
        self.down = 1
        self.distance = 10

    def gain_yards(self, yards):
        firstdown = 0
        self.distance -= yards
        if self.distance <= 0:
            self.reset_dandd()
            firstdown = 1
        else:
            if not self.already_advanced_down:
                self.down += 1
                self.already_advanced_down = True

        self.yardline -= yards
        if self.yardline < 0:
            self.yardline = 0
        if self.distance >= self.yardline:
            self.distane = self.yardline
        return firstdown

    def get_desc(self):
        desc = ''
        if self.down == 1:
            desc = '1st'
        elif self.down == 2:
            desc = '2nd'
        elif self.down == 3:
            desc = '3rd'
        else:
            desc = '4th'
        if self.distance >= self.yardline:
            desc += ' and goal to go'
        else:
            desc += ' and ' + str(self.distance)

        return desc

# [X] PORT
class GameStateMachine(StateMachine):
    game_start_state = State('Start', initial = True)
    ready_for_kickoff_state = State('Ready For Kickoff')
    ready_for_scrimmage_state = State('Ready For Scrimmage')
    ready_for_extra_point_state = State('Ready For Extra Point')

    coin_flip_event = game_start_state.to(ready_for_kickoff_state)

    kickoff_received_event = ready_for_kickoff_state.to(ready_for_scrimmage_state)
    own_kickoff_recovered_event = ready_for_kickoff_state.to(ready_for_scrimmage_state)
    touchdown_on_kickoff_return_event = ready_for_kickoff_state.to(ready_for_extra_point_state)

    turnover_on_downs_event = ready_for_scrimmage_state.to(ready_for_scrimmage_state)
    touchdown_on_scrimmage_event = ready_for_scrimmage_state.to(ready_for_extra_point_state)
    safety_on_scrimmage_event = ready_for_scrimmage_state.to(ready_for_kickoff_state)
    seond_half_kickoff_event = ready_for_scrimmage_state.to(ready_for_kickoff_state)
    field_goal_good_event = ready_for_scrimmage_state.to(ready_for_kickoff_state)

    extra_point_good_event = ready_for_extra_point_state.to(ready_for_kickoff_state)
    extra_point_two_points_good_event = ready_for_extra_point_state.to(ready_for_kickoff_state)
    extra_point_failed_event = ready_for_extra_point_state.to(ready_for_kickoff_state)

    def set_game_state(self, gamestate):
        self.gamestate = gamestate

    def on_coin_flip_event(self):
        print('Coin flip!')
        choice = random.choice(('Heads','Tails'))
        print (self.gamestate.awayteam + ' chooses ' + choice)
        flip = random.choice(('Heads','Tails'))
        print('Coin comes up ' + flip)
        if choice == flip:
            print (self.gamestate.awayteam + ' wins the flip and defers ')
            self.gamestate.homesecondhalfchoice = False
            self.gamestate.homepossession = True
        else:
            print (self.gamestate.hometeam + ' wins the flip and defers ')
            self.gamestate.homesecondhalfchoice = True
            self.gamestate.homepossession = False

    def on_kickoff_received_event(self):
        pass

    def on_own_kickoff_recovered_event(self):
        print ("Onside Kick Recovered By Kicking Team!")

    def on_touchdown_on_kickoff_return_event(self):
        self.gamestate.add_score(6)
        self.gamestate.balltracker.yardline = 35

    def on_turnover_on_downs_event(self):
        self.gamestate.change_of_possession()

    def on_touchdown_on_scrimmage_event(self):
        self.gamestate.add_score(6)
        self.gamestate.balltracker.yardline = 35

    def on_safety_on_scrimmage_event(self):
        self.gamestate.change_of_possession()
        self.gamestate.add_score(2)
        self.gamestate.balltracker.yardline = 20

    def on_field_goal_good_event(self):
        self.gamestate.add_score(3)
        self.gamestate.change_of_possession()
        self.gamestate.balltracker.yardline = 35

    def on_extra_point_good_event(self):
        self.gamestate.add_score(1)
        self.gamestate.change_of_possession()
        self.gamestate.balltracker.yardline = 35

    def on_extra_point_two_points_good_event(self):
        self.gamestate.add_score(2)
        self.gamestate.change_of_possession()
        self.gamestate.balltracker.yardline = 35

    def on_extra_point_failed_event(self):
        self.gamestate.change_of_possession()
        self.gamestate.balltracker.yardline = 35

# [X] PORT
class GameState:
    def __init__(self, hometeam, awayteam, homepossession):
        self.hometeam = hometeam
        self.awayteam = awayteam
        self.homescore = 0
        self.awayscore = 0
        self.homescore_by_quarter = [self.hometeam, 0, 0, 0, 0, 0]
        self.awayscore_by_quarter = [self.awayteam, 0, 0, 0, 0, 0]
        self.home_to = 3
        self.away_to = 3
        self.homepossession = homepossession
        self.homesecondhalfchoice = True
        self.statemachine = GameStateMachine()
        self.statemachine.set_game_state(self)
        self.balltracker = BallTracker()
        self.gameclock = GameClock()

    def game_over(self):
        if self.gameclock.game_seconds_remaining <= 0:
            return True
        return False

    def change_of_possession(self):
        self.homepossession = not self.homepossession
        self.balltracker.reset_dandd()
        self.balltracker.yardline = 100 - self.balltracker.yardline

    def set_yardline_after_kickoff(self, yl, play_time):
        self.balltracker.yardline = yl
        if yl <= 0:
            self.statemachine.touchdown_on_kickoff_return_event()
        else:
            self.statemachine.kickoff_received_event()
        self.gameclock.advance(play_time, self)

    def set_yardline_after_punt(self, calc_data, yl, play_time):
        self.balltracker.yardline = yl
        if yl <= 0:
            self.statemachine.touchdown_on_scrimmage_event()
            calc_data["CALC_touchdonw_PUNT"] = 1
        elif yl >= 100:
            self.balltracker.yardline = 80
            self.balltracker.reset_dandd()
        else:
            self.balltracker.reset_dandd()
        self.gameclock.advance(play_time, self)

    def gain_yards_on_return(self, calc_data, gain, play_time):
        terminate_series = False
        rs = StageSet()

        self.balltracker.yardline -= gain
        self.balltracker.reset_dandd()
        gain_adjustment = 0
        if self.balltracker.yardline <= 0:
            gain_adjustment = self.balltracker.yardline
            self.statemachine.touchdown_on_scrimmage_event()
            rs.add_stage('DESCRIPTION', 0, '+Touchdown!!', [], [])
            rs.add_stage('DESCRIPTION', 0, self.get_score_description(), [], [])
            terminate_series = True
        elif self.balltracker.yardline >= 100:
            gain_adjustment = 0
            self.balltracker.yardline = 80
            rs.add_stage('DESCRIPTION', 0, '+Touchback.', [], [])
            terminate_series = True

        self.gameclock.advance(play_time, self)
        return terminate_series, rs, gain_adjustment

    def gain_yards_on_scrimmage(self, calc_data, gain, play_time):
        terminate_series = False
        rs = StageSet()

        first_down = self.balltracker.gain_yards(gain)
        gain_adjustment = 0
        if self.balltracker.down > 4:
            self.statemachine.turnover_on_downs_event()
            rs.add_stage('DESCRIPTION', 0, '+Turnover on downs!!', [], [])
            terminate_series = True
        elif self.balltracker.yardline <= 0:
            gain_adjustment = self.balltracker.yardline
            self.statemachine.touchdown_on_scrimmage_event()
            rs.add_stage('DESCRIPTION', 0, '+Touchdown!!', [], [])
            rs.add_stage('DESCRIPTION', 0, self.get_score_description(), [], [])
            terminate_series = True
        elif self.balltracker.yardline >= 100:
            gain_adjustment = self.balltracker.yardline - 100
            self.statemachine.safety_on_scrimmage_event()
            rs.add_stage('DESCRIPTION', 0, '+Safety!!', [], [])
            rs.add_stage('DESCRIPTION', 0, self.get_score_description(), [], [])
            terminate_series = True
        elif first_down:
            rs.add_stage('DESCRIPTION', 0, '+First down!!', [], [])

        self.gameclock.advance(play_time, self)
        return terminate_series, rs, gain_adjustment

    def gain_yards_on_penalty(self, calc_data, gain, play_time, auto_first_down, down_over):
        terminate_series = False
        rs = StageSet()

        first_down = self.balltracker.gain_yards(gain)
        if auto_first_down:
            first_down = 1
            self.balltracker.reset_dandd()
        if down_over and self.balltracker.down > 1:
            self.balltracker.down -= 1

        gain_adjustment = 0
        if self.balltracker.down > 4:
            self.statemachine.turnover_on_downs_event()
            rs.add_stage('DESCRIPTION', 0, '+Turnover on downs!!', [], [])
            terminate_series = True
        elif self.balltracker.yardline <= 0:
            gain_adjustment = self.balltracker.yardline
            self.statemachine.touchdown_on_scrimmage_event()
            rs.add_stage('DESCRIPTION', 0, '+Touchdown!!', [], [])
            rs.add_stage('DESCRIPTION', 0, self.get_score_description(), [], [])
            terminate_series = True
        elif self.balltracker.yardline >= 100:
            gain_adjustment = self.balltracker.yardline - 100
            self.statemachine.safety_on_scrimmage_event()
            rs.add_stage('DESCRIPTION', 0, '+Safety!!', [], [])
            rs.add_stage('DESCRIPTION', 0, self.get_score_description(), [], [])
            terminate_series = True
        elif first_down:
            rs.add_stage('DESCRIPTION', 0, '+First down!!', [], [])

        self.gameclock.advance(play_time, self)
        return terminate_series, rs, gain_adjustment

    def add_score(self, score):
        if self.homepossession:
            self.homescore += score
            if self.gameclock.quarter >= 1 and self.gameclock.quarter <= 5:
                self.homescore_by_quarter[self.gameclock.quarter] += score
        else:
            self.awayscore += score
            if self.gameclock.quarter >= 1 and self.gameclock.quarter <= 5:
                self.awayscore_by_quarter[self.gameclock.quarter] += score

    def get_posteam(self):
        if self.homepossession:
            return self.hometeam
        else:
            return self.awayteam

    def get_defteam(self):
        if self.homepossession:
            return self.awayteam
        else:
            return self.hometeam

    def get_score_diff_post(self):
        if self.homepossession:
            return self.homescore - self.awayscore
        else:
            return self.awayscore - self.homescore

    def print_score(self):
        print (self.get_score_description())

    def get_score_description(self):
        return self.awayteam + ' ' + str(self.awayscore) + ' ' + self.hometeam + ' ' + str(self.homescore)

    def print_score_table(self):
        '''
        +----+----+----+----+----+----+-----+
        |Team| 1st| 2nd| 3rd| 4th| OT |Total|
        +----+----+----+----+----+----+-----+
        | SF |  0 |  0 |  0 |  0 |  0 |   0 |
        |ARI |  0 |  0 |  0 |  0 |  0 |   0 |
        +----+----+----+----+----+----+-----+
        '''
        print ('+----+----+----+----+----+----+-----+')
        print ('|Team| 1st| 2nd| 3rd| 4th| OT |Total|')
        print ('+----+----+----+----+----+----+-----+')

        for x in range(len(self.homescore_by_quarter)):
            if x <= self.gameclock.quarter:
                print("|{:>3s} ".format(str(self.homescore_by_quarter[x])), end='')
            else:
                print("|  - ", end='')
        print("| {:>3s} |".format(str(self.homescore)))

        for x in range(len(self.awayscore_by_quarter)):
            if x <= self.gameclock.quarter:
                print("|{:>3s} ".format(str(self.awayscore_by_quarter[x])), end='')
            else:
                print("|  - ", end='')
        print("| {:>3s} |".format(str(self.awayscore)))

        print ('+----+----+----+----+----+----+-----+')

# [/] PORT
class PlayChooser:
    def __init__(self):
        self.gf2 = GoForTwo()
        self.evc = EVCalc()

    def calc_remaining_possessions(self, gamestate, hometeam):
        if hometeam:
            tos = gamestate.home_to
        else:
            tos = gamestate.away_to

        time_left = gamestate.gameclock.game_seconds_remaining
        if time_left > 120: time_left += 30
        time_left += 30 * tos
        remaining_possessions = time_left / 160
        return remaining_possessions

    # def calc_numbeer_of_scores_lead(self, hometeam):
    #     if hometeam:
    #         return (self.homescore - self.awayscore) / 8.0
    #     else:
    #         return (self.awayscore - self.homescore) / 8.0

    # def calc_onside_kick(self):
    #     remaining_posessions = self.calc_remaining_possessions(not self.homepossession)
    #     number_of_scores = self.calc_numbeer_of_scores_lead(self.homepossession)
    #     return number_of_scores >= remaining_posessions

    # def add_time_of_half_score(self, candidates):
    #     candidates['match_score'] = candidates['match_score'] + 1000.0 / (abs(candidates['half_seconds_remaining'] - self.half_seconds_remaining) + 10)
    #     return candidates

    def use_pos_team(self):
        result = random.choice((True,False))
        return result

    def set_scores(self, candidates, value):
        candidates['match_score'] = value
        return candidates

    def square_scores(self, candidates):
        candidates['match_score'] = candidates['match_score'].apply(square)

    def add_down_match(self, candidates, gamestate, value, clear_if_not_match = False):
        candidates["match_score"] = candidates.apply (lambda row: add_if_match(row["down"],
            gamestate.balltracker.down, row["match_score"], value, clear_if_not_match), axis=1)

    def add_distance_match(self, candidates, gamestate, value, clear_if_not_match = False):
        candidates["match_score"] = candidates.apply (lambda row: add_if_match(row["distance_category"],
            get_distance_category(gamestate.balltracker.distance), row["match_score"], value, clear_if_not_match), axis=1)

    def add_yardline_match(self, candidates, gamestate, value, clear_if_not_match = False):
        candidates["match_score"] = candidates.apply (lambda row: add_if_match(row["yardline_category"],
            get_yardline_category(gamestate.balltracker.yardline), row["match_score"], value, clear_if_not_match), axis=1)

    def add_time_match(self, candidates, gamestate, value, clear_if_not_match = False):
        candidates["match_score"] = candidates.apply (lambda row: add_if_match(row["time_category"],
            get_time_category(gamestate.gameclock.game_seconds_remaining), row["match_score"], value, clear_if_not_match), axis=1)

    def add_score_diff_match(self, candidates, gamestate, value, clear_if_not_match = False):
        candidates["match_score"] = candidates.apply (lambda row: add_if_match(row["score_diff_category"],
            get_score_diff_category(gamestate.get_score_diff_post()), row["match_score"], value, clear_if_not_match), axis=1)

    def mark_as_used(self, ndx):
        game_data["used"] = game_data.apply (lambda row: add_if_match(row["ndx"],
            ndx, row["used"], 1), axis=1)


    # distance
    # yardline
    # time
    # score diff
    def find_candidates(self, gamestate, all_teams):
        candidates = game_data[game_data['used'] == 0]
        if not all_teams:
            if self.use_pos_team():
            # if True:
                candidates = candidates[candidates['posteam'] == gamestate.get_posteam()]
            else:
                candidates = candidates[candidates['defteam'] == gamestate.get_defteam()]

        if gamestate.statemachine.is_ready_for_kickoff_state:
            candidates = candidates[candidates['play_type_nfl'] == 'KICK_OFF']
            self.set_scores(candidates, 1)
            self.add_time_match(candidates, gamestate, 10)
            self.add_score_diff_match(candidates, gamestate, 10)
            self.square_scores(candidates)
        elif gamestate.statemachine.is_ready_for_scrimmage_state:
            choice = 'SCRIMMAGE'
            if gamestate.balltracker.down == 4:
                choice = self.evc.GetBestOption(gamestate.get_posteam(), gamestate.get_defteam(), gamestate.balltracker.yardline, gamestate.balltracker.distance, True)
                if gamestate.balltracker.yardline > 50:
                    choice = 'PUNT'
                
            if choice == 'SCRIMMAGE':
                candidate_list = ['RUSH','PASS','PENALTY']
            else:
                candidate_list = [choice]

            if choice in ['SCRIMMAGE','RUSH','PASS']:
                candidates = candidates[candidates['play_type_nfl'].isin(candidate_list)]
                # candidates = candidates[candidates['play_type_nfl'].isin(["PASS","PENALTY","RUSH","SACK","TIMEOUT","FIELD_GOAL"])]
                self.set_scores(candidates, 1)
                self.add_time_match(candidates, gamestate, 10)
                self.add_score_diff_match(candidates, gamestate, 10)
                self.add_yardline_match(candidates, gamestate, 10)
                self.add_distance_match(candidates, gamestate, 10)
                self.add_down_match(candidates, gamestate, 10)
                self.square_scores(candidates)

            elif choice == 'PUNT':
                candidate_list = ['PUNT']
                candidates = candidates[candidates['play_type_nfl'].isin(candidate_list)]
                self.set_scores(candidates, 1)
                candidates.loc[(candidates['play_type_nfl'] == 'PUNT') & ((candidates['yardline_100'] < gamestate.balltracker.yardline - 10) | (candidates['yardline_100'] > gamestate.balltracker.yardline + 10)), 'match_score'] = 0

            if choice == 'FGA':
                candidate_list = ['FIELD_GOAL']
                candidates = candidates[candidates['play_type_nfl'].isin(candidate_list)]
                self.set_scores(candidates, 1)
                candidates.loc[(candidates['play_type_nfl'] == 'FIELD_GOAL') & ((candidates['yardline_100'] < gamestate.balltracker.yardline - 7) | (candidates['yardline_100'] > gamestate.balltracker.yardline + 8)), 'match_score'] = 0

            self.square_scores(candidates)

        elif gamestate.statemachine.is_ready_for_extra_point_state:
            gf2check = self.gf2.check(gamestate.get_score_diff_post(), self.calc_remaining_possessions(gamestate, gamestate.homepossession))
            if gf2check == 1:
                candidates = candidates[candidates['play_type_nfl'].isin(["PAT2"])]
            else:
                candidates = candidates[candidates['play_type_nfl'].isin(["XP_KICK"])]
            self.set_scores(candidates, 1)
            self.add_time_match(candidates, gamestate, 10)
            self.add_score_diff_match(candidates, gamestate, 10)
            self.square_scores(candidates)
        return candidates

# [ ] PORT
class PlayPrinter:
    def __init__(self, home, away):
        self.home = home
        self.away = away
        self.home_player_names = {}
        self.away_player_names = {}
        self.home_starting_qb = None
        self.away_starting_qb = None

    def get_pos_desc(self, posteam, defteam, yardline):
        if yardline == 50:
            return 'midfield'
        if yardline > 50:
            return 'the ' + posteam + ' ' + str(100 - yardline)
        if yardline < 50:
            return 'the ' + defteam + ' ' + str(yardline)

    def get_yardline_desc(self, posteam, defteam, yardline):
        if yardline == 50:
            return 'at midfield'
        if yardline > 50:
            return 'on ' + posteam + ' ' + str(100 - yardline)
        if yardline < 50:
            return 'on ' + defteam + ' ' + str(yardline)

    def accum_single_set(self, teamname, posteam, category, playtype = None):
        if posteam:
            candidates = game_data[game_data['posteam'] == teamname]
        else:
            candidates = game_data[game_data['defteam'] == teamname]
        if playtype:
            candidates = candidates[candidates['play_type_nfl'] == playtype]

        passers = candidates.groupby(category)
        names = []
        values = []
        for k, gb in passers:
            names.append(str(k))
            values.append(len(gb.index))
        df = pd.DataFrame(list(zip(names, values)),
            columns =['name', 'value'])
        return df

    def accum_player_names(self):
        self.home_player_names['passer_player_name'] = self.accum_single_set(self.home, True, 'passer_player_name')
        self.home_player_names['receiver_player_name'] = self.accum_single_set(self.home, True, 'receiver_player_name')
        self.home_player_names['rusher_player_name'] = self.accum_single_set(self.home, True, 'rusher_player_name')
        self.home_player_names['punter_player_name'] = self.accum_single_set(self.home, True, 'punter_player_name')
        self.home_player_names['kickoff_returner_player_name'] = self.accum_single_set(self.home, True, 'kickoff_returner_player_name')
        self.home_player_names['interception_player_name'] = self.accum_single_set(self.home, False, 'interception_player_name')
        self.home_player_names['punt_returner_player_name'] = self.accum_single_set(self.home, False, 'punt_returner_player_name')
        self.home_player_names['kicker_player_name_KO'] = self.accum_single_set(self.home, False, 'kicker_player_name', 'KICK_OFF')
        self.home_player_names['kicker_player_name_FG'] = self.accum_single_set(self.home, True, 'kicker_player_name', 'FIELD_GOAL')

        self.away_player_names['passer_player_name'] = self.accum_single_set(self.away, True, 'passer_player_name')
        self.away_player_names['receiver_player_name'] = self.accum_single_set(self.away, True, 'receiver_player_name')
        self.away_player_names['rusher_player_name'] = self.accum_single_set(self.away, True, 'rusher_player_name')
        self.away_player_names['punter_player_name'] = self.accum_single_set(self.away, True, 'punter_player_name')
        self.away_player_names['kickoff_returner_player_name'] = self.accum_single_set(self.away, True, 'kickoff_returner_player_name')
        self.away_player_names['interception_player_name'] = self.accum_single_set(self.away, False, 'interception_player_name')
        self.away_player_names['punt_returner_player_name'] = self.accum_single_set(self.away, False, 'punt_returner_player_name')
        self.away_player_names['kicker_player_name_KO'] = self.accum_single_set(self.away, False, 'kicker_player_name', 'KICK_OFF')
        self.away_player_names['kicker_player_name_FG'] = self.accum_single_set(self.away, True, 'kicker_player_name', 'FIELD_GOAL')

        # for key in self.home_player_names.keys():
        #     print (key)
        #     print (self.home_player_names[key])

        # for key in self.away_player_names.keys():
        #     print (key)
        #     print (self.away_player_names[key])

    def get_player_name(self, type, home):
        if home:
            table = self.home_player_names[type]
        else:
            table = self.away_player_names[type]
        player = table.sample(n=1, weights='value')['name'].values[0]
        return player

    def get_or_substitute(self, substitute, subname, home, row, rowname):
        if subname == 'passer_player_name':
            if home:
                if not self.home_starting_qb:
                    self.home_starting_qb = self.get_player_name(subname, home)
                player = self.home_starting_qb
            else:
                if not self.away_starting_qb:
                    self.away_starting_qb = self.get_player_name(subname, home)
                player = self.away_starting_qb
        else:
            if substitute:
                player = self.get_player_name(subname, home)
            else:
                player = row[rowname].values[0]
                if len(str(player)) < 4:
                    player = self.get_player_name(subname, home)

        return str(player)

    def get_starting_qbs(self):
        posplayer = self.get_or_substitute(True, 'passer_player_name', True, None, 'passer_player_name')
        defplayer = self.get_or_substitute(True, 'passer_player_name', False, None, 'passer_player_name')
        print (posplayer + ' is starting at quarterback for ' + self.home)
        print (defplayer + ' is starting at quarterback for ' + self.away)

    def print_state(self, gs):
        if gs.statemachine.is_ready_for_kickoff_state:
            print(gs.get_posteam(), 'receiving kickoff from', gs.get_defteam(), self.get_yardline_desc(gs.get_posteam(), gs.get_defteam(), gs.balltracker.yardline))
        elif gs.statemachine.is_ready_for_scrimmage_state:
            print(gs.get_posteam() + ' has the ball ' + gs.balltracker.get_desc() + ' ' + self.get_yardline_desc(gs.get_posteam(), gs.get_defteam(), gs.balltracker.yardline))
        gs.gameclock.print()

    # def interception_check(self, row, calc_data, interception_player_name):
    #     interception = self.get_int_value(row, 'interception', 0)
    #     CALC_interception_return_yards_PASS = calc_data['CALC_interception_return_yards_PASS']
    #     desc = ''
    #     if interception == 1:
    #         desc += "It's picked off by " + interception_player_name + '! '
    #         if CALC_interception_return_yards_PASS > 0:
    #             desc += 'He returns it for ' + str(CALC_interception_return_yards_PASS) + ' yards.'
    #         elif CALC_interception_return_yards_PASS == 0:
    #             desc += "He's tackled immediately."
    #         else:
    #             desc += "He tries to cut back and loses " + str(- CALC_interception_return_yards_PASS) + ' yards.'
    #         print (desc)
    #     return interception

    # def fumble_check(self, row):
    #     fumble_out_of_bounds = row['fumble_out_of_bounds'].values[0]
    #     fumble = row['fumble'].values[0]
    #     fumble_lost = row['fumble_lost'].values[0]
    #     try:
    #         fumble_recovery_1_yards = int(row['fumble_recovery_1_yards'].values[0])
    #     except:
    #         fumble_recovery_1_yards = 0
    #     desc = ''
    #     if fumble == 1:
    #         desc += 'Fumble!! '
    #         if fumble_out_of_bounds == 1:
    #             desc += 'Out of bounds. '
    #         if fumble_lost == 1:
    #             desc += 'Defense recovers and returns it ' + str(fumble_recovery_1_yards) + ' yards.'
    #         else:
    #             desc += 'Offense maintains possession. '
    #     print (desc)

    def get_int_value(self, row, field, default):
        try:
            value = int(row[field].values[0])
        except:
            value = default
        return value

    def print_stage_set_desc(self, ss, team_data, calc_data):
        for stage in ss.stages:
            # stage.dump()
            desc = stage.description
            for tt in stage.team_tags:
                if tt == '' or tt == 'nan':
                    continue
                desc = desc.replace('['+tt+']', team_data[tt])
            for ct in stage.calc_tags:
                if ct == '' or ct == 'nan':
                    continue
                if len(ct.split('=')) == 2:
                    f, v = ct.split('=')
                    if f == 'get_pos_desc':
                        desc = desc.replace('['+v+']', self.get_pos_desc(team_data['posteamname'], team_data['defteamname'], calc_data[v]))
                else:
                    desc = desc.replace('['+ct+']', str(calc_data[ct]))
            desc = desc.replace('[stage_value]', str(stage.value))
            print (desc)

    def print(self, ss, calc_data, row):
        play_type_nfl = row["play_type_nfl"].values[0]
        if play_type_nfl not in ['KICK_OFF', 'RUSH', 'PASS', 'XP_KICK','PAT2','PUNT','FIELD_GOAL','PENALTY']:
            return

        posteam = row['posteam'].values[0]
        defteam = row['defteam'].values[0]
        # qb_scramble = int(row['qb_scramble'].values[0])

        # CALC_yards_gained = calc_data['CALC_yards_gained']
        # CALC_starting_yardline = calc_data['CALC_starting_yardline']

        substituteposplayers = False
        substitutedefplayers = False
        posishome = False

        if posteam == self.home:
            posishome = True
        elif defteam == self.away:
            posishome = True

        if posteam not in (self.home, self.away):
            substituteposplayers = True
        if defteam not in (self.home, self.away):
            substitutedefplayers = True

        if posteam == self.home:
            posishome = True
        elif defteam == self.away:
            posishome = True

        if posishome:
            posteamname = self.home
            defteamname = self.away
        else:
            posteamname = self.away
            defteamname = self.home

        team_data = {}
        team_data['posteamname'] = posteamname
        team_data['defteamname'] = defteamname

        # ss.dump()
        # print (row['ndx'].values[0], row['desc'].values[0])
        if  play_type_nfl == 'KICK_OFF':
            team_data['kicker_player_name'] = self.get_or_substitute(substitutedefplayers, 'kicker_player_name_KO', not posishome, row, 'kicker_player_name')
            team_data['kickoff_returner_player_name'] = self.get_or_substitute(substitutedefplayers, 'kickoff_returner_player_name', posishome, row, 'kickoff_returner_player_name')
        elif play_type_nfl in ['RUSH']:
            team_data['qb_rusher_player_name'] = self.get_or_substitute(substituteposplayers, 'passer_player_name', posishome, row, 'rusher_player_name')
            team_data['rusher_player_name'] = self.get_or_substitute(substituteposplayers, 'rusher_player_name', posishome, row, 'rusher_player_name')
        elif play_type_nfl in ['PASS']:
            team_data['passer_player_name'] = self.get_or_substitute(substituteposplayers, 'passer_player_name', posishome, row, 'passer_player_name')
            team_data['receiver_player_name'] = self.get_or_substitute(substituteposplayers, 'receiver_player_name', posishome, row, 'receiver_player_name')
        elif play_type_nfl in ['PUNT']:
            team_data['punter_player_name'] = self.get_or_substitute(substituteposplayers, 'punter_player_name', posishome, row, 'punter_player_name')
            team_data['punt_returner_player_name'] = self.get_or_substitute(substituteposplayers, 'punt_returner_player_name', not posishome, row, 'punt_returner_player_name')
        elif play_type_nfl in ['FIELD_GOAL']:
            team_data['kicker_player_name'] = self.get_or_substitute(substitutedefplayers, 'kicker_player_name_FG', posishome, row, 'kicker_player_name')

        # ss.dump()
        # print(calc_data)
        self.print_stage_set_desc(ss, team_data, calc_data)

# [ ] PORT
class PlayEecutor:

    # def get_int_value(self, row, field, default):
    #     try:
    #         value = int(row[field].values[0])
    #     except:
    #         value = default
    #     return value

    def execute(self, gs, row):
        gs.balltracker.start_play()
        calc_data = {}
        ss = StageSet()
        ss.decode(row)
        rs = StageSet()
        posteam = row['posteam'].values[0]

        play_time = int(row['play_time'].values[0])

        CALC_yards_gained = int(row['yards_gained'].values[0])
        if CALC_yards_gained > gs.balltracker.yardline:
            CALC_yards_gained = gs.balltracker.yardline
        calc_data["CALC_yards_gained"] = CALC_yards_gained
        calc_data["CALC_starting_yardline"] = gs.balltracker.yardline
        calc_data["CALC_firstdown"] = 0

        play_type_nfl = row["play_type_nfl"].values[0]
        if  play_type_nfl in ['KICK_OFF', 'RUSH', 'PASS', 'XP_KICK', 'PAT2','PUNT','FIELD_GOAL','PENALTY']:
            # ss.dump()
            for stage in ss.stages:
                if stage.token == 'DESCRIPTION':
                    # [X]
                    rs.stages.append(stage)
                elif stage.token == 'KICK_OFF':
                    # [X]
                    rs.stages.append(stage)
                elif stage.token == 'KICK_OFF_RETURN':
                    # [X]
                    CALC_nextyl_after_KO = stage.value
                    if gs.balltracker.yardline != 35:
                        CALC_nextyl_after_KO = CALC_nextyl_after_KO + gs.balltracker.yardline - 35
                    gs.set_yardline_after_kickoff(CALC_nextyl_after_KO, play_time)
                    calc_data["CALC_nextyl_after_KO"] = CALC_nextyl_after_KO
                    rs.stages.append(stage)
                
                elif stage.token == 'GAIN':
                    # [X]
                    terminate_series, es, gain_adjustment = gs.gain_yards_on_scrimmage(calc_data, stage.value, play_time)
                    stage.value += gain_adjustment
                    rs.stages.append(stage)
                    rs.stages.extend(es.stages)
                    if terminate_series:
                        break
                
                elif stage.token == 'CHANGE_OF_POSSESSION':
                    # [X]
                    rs.stages.append(stage)
                    if stage.value == 1:
                        gs.change_of_possession()
                
                elif stage.token == 'PASS':
                    # [X]
                    calc_data['pass_length'] = 'deep'
                    if gs.balltracker.yardline < 20:
                        calc_data['pass_length'] = 'short'

                    if stage.value > gs.balltracker.yardline:
                        stage.value = gs.balltracker.yardline

                    calc_data["CALC_air_yards_PASS"] = stage.value
                    rs.stages.append(stage)
                
                elif stage.token == 'GAIN_AFTER_CATCH':
                    # [X]
                    CALC_air_yards_PASS = calc_data.get("CALC_air_yards_PASS", 0)
                    terminate_series, es, gain_adjustment = gs.gain_yards_on_scrimmage(calc_data, stage.value, play_time)
                    stage.value += gain_adjustment
                    calc_data['CALC_yards_after_catch'] = stage.value - CALC_air_yards_PASS
                    rs.stages.append(stage)
                    rs.stages.extend(es.stages)
                    if terminate_series:
                        break
                elif stage.token == 'XP_KICK':
                    # [X]
                    if stage.value == 1:
                        gs.statemachine.extra_point_good_event()
                    else:
                        gs.statemachine.extra_point_failed_event()
                    rs.stages.append(stage)
                    rs.add_stage('DESCRIPTION', 0, gs.get_score_description(), [], [])
                elif stage.token == 'FIELD_GOAL':
                    # [X]
                    ball_kicked_from = gs.balltracker.yardline + 8
                    new_position = ball_kicked_from
                    if new_position < 20:
                        new_position = 20
                    calc_data['CALC_field_goal_distane_FG'] = ball_kicked_from + 10
                    if stage.value == 3:
                        gs.statemachine.field_goal_good_event()
                        gs.gameclock.advance(play_time, gs)
                    else:
                        gs.balltracker.yardline = new_position
                        gs.change_of_possession()
                        gs.gameclock.advance(play_time, gs)
                    rs.stages.append(stage)
                    rs.add_stage('DESCRIPTION', 0, gs.get_score_description(), [], [])
                elif stage.token == 'PAT2':
                    # [X]
                    if stage.value == 1:
                        gs.statemachine.extra_point_two_points_good_event()
                    else:
                        gs.statemachine.extra_point_failed_event()
                    rs.stages.append(stage)
                    rs.add_stage('DESCRIPTION', 0, gs.get_score_description(), [], [])
                elif stage.token == 'PUNT':
                    # [/]
                    CALC_kick_distance_PUNT = stage.value
                    if CALC_kick_distance_PUNT > gs.balltracker.yardline:
                        stage.value = CALC_kick_distance_PUNT
                        rs.stages.append(stage)
                        desc = 'It goes into the endzone for a touchback.'
                        rs.add_stage('PUNT_TOUCHBACK', 0, desc, [], [])
                        gs.change_of_possession()
                        gs.balltracker.yardline = 80
                        gs.balltracker.reset_dandd()
                        gs.gameclock.advance(play_time, gs)
                        break
                    else:
                        rs.stages.append(stage)
                        gs.balltracker.yardline -= stage.value
                        gs.change_of_possession()
                        gs.gameclock.advance(play_time, gs)
                elif stage.token == 'RETURN':
                    # [X]
                    terminate_series, es, gain_adjustment = gs.gain_yards_on_return(calc_data, stage.value - calc_data.get("CALC_air_yards_PASS", 0), 0)
                    stage.value += gain_adjustment
                    rs.stages.append(stage)
                    rs.stages.extend(es.stages)
                    if terminate_series:
                        break                    
                elif stage.token == 'PUNT_BLOCKED':
                    # [X]
                    terminate_series, es, gain_adjustment = gs.gain_yards_on_return(calc_data, random.randrange(-10, 30), 0)
                    stage.value += gain_adjustment
                    rs.stages.append(stage)
                    rs.stages.extend(es.stages)
                    if terminate_series:
                        break                    
                elif stage.token == 'PENALTY':
                    # [6]
                    tags = stage.extra_info
                    yards = stage.value
                    if 'ONOFFENSE' in tags:
                        if 'SPOTFOUL' not in tags and gs.balltracker.yardline + 2 * yards >= 100:
                            yards = int((100 - gs.balltracker.yardline) / 2)
                        if 'DOWNOVER' in tags:
                            down_over = True
                        else:
                            down_over = False
                        terminate_series, es, gain_adjustment = gs.gain_yards_on_penalty(calc_data, -yards, play_time, False, down_over)
                    else:
                        if 'SPOTFOUL' not in tags and gs.balltracker.yardline - 2 * yards <= 0:
                            yards = int(gs.balltracker.yardline / 2)
                        if 'SPOTFOUL' in tags and gs.balltracker.yardline - yards <= 0:
                            yards = gs.balltracker.yardline - 1
                        if 'AUTOFIRSTDOWN' in tags:
                            auto_first_down = True
                        else:
                            auto_first_down = False
                        terminate_series, es, gain_adjustment = gs.gain_yards_on_penalty(calc_data, yards, play_time, auto_first_down, False)
                    stage.value = yards
                    rs.stages.append(stage)
                    rs.stages.extend(es.stages)
                elif stage.token == "SACK":
                    # [7]
                    break

        return rs, calc_data

random.seed()
gs = GameState('MIN', 'NO', True)
# gs.print()

game_data = pd.read_csv('game_data.csv')
game_data = game_data[game_data['stage_encoded'] == 1]
# game_data["used"] = 0
# game_data["match_score"] = 0
# game_data["yardline_category"] = game_data.apply (lambda row: get_yardline_category(row["yardline_100"]), axis=1)
# game_data["distance_category"] = game_data.apply (lambda row: get_distance_category(row["ydstogo"]), axis=1)
# game_data["time_category"] = game_data.apply (lambda row: get_time_category(row["game_seconds_remaining"]), axis=1)
# game_data["score_diff_category"] = game_data.apply (lambda row: get_score_diff_category(row["score_differential_post"]), axis=1)

pe = PlayEecutor()

pp = PlayPrinter(gs.hometeam, gs.awayteam)
pp.accum_player_names()
pp.get_starting_qbs()

pc = PlayChooser()

playlist = []
pl_ndx = 0
recordlist = []
starttime = time()
gs.statemachine.coin_flip_event()

# playlist =  [20092, 33248, 36764, 33428, 30888, 36830, 22291, 36723, 30932, 26760, 26769, 806, 33372, 30902, 8478, 10790, 30898, 12781, 8406, 10794, 13288, 768, 13286, 25494, 36703, 36714, 33356, 5235, 16694, 25589, 25512, 13293, 10561, 4828, 36709, 8442, 34023, 27137, 13324, 12784, 36773, 795, 27144, 4831, 13295, 16785, 10878, 26864, 27163, 29602, 763, 2584, 30884, 6883, 20580, 36746, 8407, 36435, 25508, 36759, 22316, 20486, 36797, 27176, 14307, 34058, 20560, 2716, 26153, 36778, 7003, 34177, 34052, 33289, 22363, 25608, 36808, 20429, 29529, 30889, 10918, 4892, 20603, 26763, 33363, 33342, 4925, 34072, 25496, 13358, 896, 26139, 20211, 4930, 36459, 2632, 6925, 20090, 12812, 29523, 26152, 33352, 26897, 5282, 34169, 33281, 10888, 26901, 4900, 25601, 27237, 34142, 36855, 10899, 14302, 2668, 10467, 12744, 4882, 6859, 4808, 858, 36458, 34152, 31004, 20204, 26876, 
# 13379, 14223, 23465, 23427, 23456, 20166, 26824, 16705, 4875, 20425, 10862, 2663, 25610, 12847, 5274, 29549, 27193, 25650, 34050, 25605, 25536, 20571, 26195, 14300, 20622, 33397, 10874, 26169, 4933, 34043]
# gs.homepossession =  True
# gs.homesecondhalfchoice =  False

holdhomepossession = gs.homepossession
holdhomesecondhalfchoice = gs.homesecondhalfchoice
print ('----------------------------------------------------------')
pp.print_state(gs)
print ('----------------------------------------------------------')

interactive = False
play_log = False
for x in range(1000):

    if len(playlist) > pl_ndx:
        choice = game_data[game_data['ndx'] == playlist[pl_ndx]]
        pl_ndx += 1
        skipinput = True
    else:
        candidates = pc.find_candidates(gs, False)
        try:
            choice = candidates.sample(n=1, weights="match_score")
        except:
            print('!!!!!!!!!!!!!!!!!!!!!! ALL TEAMS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            try:
                candidates = pc.find_candidates(gs, True)
                choice = candidates.sample(n=1, weights="match_score")
            except:
                print ('NO CANDIDATES')
                candidates.to_csv('candidates.csv')
                print ('playlist = ', recordlist)
                print ('gs.homepossession = ', holdhomepossession)
                print ('gs.homesecondhalfchoice = ', holdhomesecondhalfchoice)
                break
        skipinput = False
        pc.mark_as_used(choice["ndx"].values[0])

    recordlist.append(choice["ndx"].values[0])
    rs, calc_data = pe.execute(gs, choice)
    if play_log:
        pp.print(rs, calc_data, choice)

    if not skipinput and interactive:
        x = input()
        if x == 's':
            candidates.to_csv('candidates.csv')
        if x == 'p':
            print ('playlist = ', recordlist)
            print ('gs.homepossession = ', holdhomepossession)
            print ('gs.homesecondhalfchoice = ', holdhomesecondhalfchoice)
        if x == 'd':
            print (choice['ndx'].values[0], choice['desc'].values[0])
        if x == 'q':
            break

    if play_log:
        print ('----------------------------------------------------------')
        pp.print_state(gs)
        print ('----------------------------------------------------------')
    if gs.game_over():
        print()
        print ('Final Score:')
        gs.print_score_table()
        break

endtime = time()
print ('playlist = ', recordlist)
print ('gs.homepossession = ', holdhomepossession)
print ('gs.homesecondhalfchoice = ', holdhomesecondhalfchoice)
totaltime = endtime - starttime
num_plays = len(recordlist)
print ('totaltime:', totaltime, 'num_plays', num_plays, 'time_per_play', (totaltime / num_plays))
# pe.print_state(gs)
# candidates.to_csv('candidates.csv')

# game_data.to_csv('game_data.csv')
