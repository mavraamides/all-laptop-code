import pandas as pd 


class GoForTwo:
    def __init__(self):
        self.table = pd.read_csv('gf2_chart.csv')

    def check(self, scoredif, poss_left):
        poss_left = int(poss_left)
        if poss_left < 1:
            poss_left = 1
        if poss_left > 10:
            poss_left = 10
        if scoredif < -16:
            scoredif = -16
        if scoredif > 14:
            scoredif = 14
        row = self.table[self.table['lead'] == scoredif]
        result = row[str(poss_left)]
        return result.values[0]

            



if __name__ == '__main__':
    gf2 = GoForTwo()
    print('      ',end='')
    for poss_left in range(1,11):
        print('{:1d} '.format(poss_left),end='')
    print('')
    for lead in range(-16,15):
        print('{:3d} | '.format(lead),end='')
        for poss_left in range(1,11):
            print (gf2.check(lead, poss_left), '', end='')
        print('')