#include "CompressUtil.h"
#include "BurrowsWheeler.h"
#include "MoveToFront.h"
#include "ArithmeticCoding.h"
#include "LZW.h"

namespace CompressUtil 
{
    bool Uncompress(std::vector<unsigned char>& from, std::vector<unsigned char>& to1)
    {
        auto compressed_size = from.size() * 8;
        auto code = from[0];
        from.erase(from.begin());
        if (code == LZW_CODE)
        {
            cout << "LZW Decode..." << endl;
            if (!LZW::Transform(from, to1, false))
            {
                cout << "LZW transform failed." << endl;
                return false;
            }
        }
        else
        {
            cout << "Arithmetic Coding Decode..." << endl;
            if (!ArithmeticCoding::Transform(from, to1, false))
            {
                cout << "Arithmetic Coding transform failed." << endl;
                return false;
            }
        }
        cout << "Move To Front Decode..." << endl;
        if (!MoveToFront::Transform(to1, from, false))
        {
            cout << "Move To Front transform failed." << endl;
            return false;
        }
        cout << "Burrows Wheeler Decode..." << endl;
        if (!BurrowsWheeler::Transform(from, to1, false))
        {
            cout << "Burrows Wheeler transform failed." << endl;
            return false;
        }
        auto uncompressed_size = to1.size() * 8;
        float pct = (float)compressed_size / (float)uncompressed_size;
        cout << "Total compressed:   " << setw(10) << compressed_size << " uncompressed: " << setw(10) << uncompressed_size << " (" << setw(10) << pct << ")" << endl;
        return true;
    }



    bool Compress(std::vector<unsigned char>& from, std::vector<unsigned char>& to1, std::vector<unsigned char>& to2)
    {
        auto uncompressed_size = from.size() * 8;
        cout << "Burrows Wheeler Encode..." << endl;
        if (!BurrowsWheeler::Transform(from, to1, true))
        {
            cout << "Burrows Wheeler transform failed." << endl;
            return false;
        }
        cout << "Move To Front Encode..." << endl;
        if (!MoveToFront::Transform(to1, from, true))
        {
            cout << "Move To Front transform failed." << endl;
            return false;
        }
        cout << "LZW Encode..." << endl;
        if (!LZW::Transform(from, to1, true, 0xFFFFFFFF))
        {
            cout << "LZW transform failed." << endl;
            return false;
        }
        to1.insert(to1.begin(), LZW_CODE);
        auto compressed_size = to1.size() * 8;
        float pct = (float)compressed_size / (float)uncompressed_size;
        cout << "Total uncompressed: " << setw(10) << uncompressed_size << " compressed:   " << setw(10) << compressed_size << " (" << setw(10) << pct << ")" << endl;
        cout << "Arithmetic Coding Encode..." << endl;
        if (!ArithmeticCoding::Transform(from, to2, true))
        {
            cout << "Arithmetic Coding transform failed." << endl;
            return false;
        }
        to2.insert(to2.begin(), AC_CODE);
        compressed_size = to2.size() * 8;
        pct = (float)compressed_size / (float)uncompressed_size;
        cout << "Total uncompressed: " << setw(10) << uncompressed_size << " compressed:   " << setw(10) << compressed_size << " (" << setw(10) << pct << ")" << endl;
        return true;
    }

}