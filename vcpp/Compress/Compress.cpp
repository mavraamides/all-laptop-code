#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <map>
#include "BurrowsWheeler.h"
#include "MoveToFront.h"
#include "ArithmeticCoding.h"
#include "LZW.h"
#include "CompressUtil.h"
#include "Compress.h"
using namespace std;

void dump_distribution(vector<unsigned char> src)
{
    map<unsigned char, int> distribution;
    for (auto x = 0u; x < 256; x++) distribution[x] = 0;
    for (auto c : src) distribution[c]++;
    for (auto x = 0u; x < 256; x++)
    {
        if (x % 16 == 0) cout << endl;
        cout << setw(8) << distribution[x] << " ";
    }
    cout << endl;
}
int main(int argc, char* argv[])
{

    if (argc != 4)
    {
        cout << "Usage: Compress {source file} {dest file} <1 compress | 2 decompress>" << endl;
        return 1;
    }
    string compress_flag = argv[3];
    bool compressing = (compress_flag.size() > 0 && compress_flag[0] == '1');

    string fromfilename = argv[1];
    string tofilename = argv[2];

    ifstream in(fromfilename, ios::binary);
    if (!in.good()) {
        cout << "Unable to load " << fromfilename << endl;
        return 1;
    }
    vector<unsigned char> from(istreambuf_iterator<char>(in), {});
    vector<unsigned char> to1;
    vector<unsigned char> to2;
    in.close();

    if (compressing)
    {
        bool retflag = CompressUtil::Compress(from, to1, to2);
        if (!retflag) return 1;
		
        ofstream out(tofilename, ios::binary);
		if (out.is_open())
		{
            if (to1.size() < to2.size())
			{
				for (auto c : to1) out << c;
			}
            else
            {
                for (auto c : to2) out << c;
            }
            out.close();
        }
	}
    else
    {
        bool retflag = CompressUtil::Uncompress(from, to1);
        if (!retflag) return 1;
 
        ofstream out(tofilename, ios::binary);
        if (out.is_open())
        {
            for (auto c : to1) out << c;
            out.close();
        }
    }
}

