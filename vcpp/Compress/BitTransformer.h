#pragma once
#include <iostream>
#include <bitset>
#include <vector>
using namespace std;

namespace BitTransformer {
	void copy_int_array_to_bitstream(vector<unsigned int>& int_array, unsigned int src_start, unsigned int max_bits, unsigned char msb, vector<bool>& bitstream, bool clear_first = true);
	void copy_packed_char_array_to_bitstream(vector<unsigned char >& char_array, unsigned int first_bit, unsigned int max_bits, vector<bool>& bitstream);
	void copy_char_array_to_bitstream(vector<unsigned char>& char_array, unsigned int src_start, unsigned int max_bits, unsigned char msb, vector<bool>& bitstream, bool clear_first = true);
	void copy_bitstream_to_char_array(vector<bool>& bitsream, vector<unsigned char>& char_array, unsigned char msb);
	void copy_bitstream_to_int_array(vector<bool>& bitsream, vector<unsigned int>& int_array, unsigned char msb);
	void copy_int_to_char_array(unsigned int src_int, vector<unsigned char>& char_array, unsigned char msb);
	void copy_char_array_to_int(vector<unsigned char>& char_array, unsigned int src_start, unsigned int& target_int, unsigned char msb);
	void test();
}
