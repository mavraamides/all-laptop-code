#include <iostream>
#include <iomanip>
#include "LZW.h"
#include "BitTransformer.h"

namespace LZW
{
	bool debug = false;

	bool Encode(vector<unsigned char>& source, vector<unsigned char>& dest, unsigned long long max_dict)
	{
		map<string, unsigned int> dictionary;
		vector<unsigned int> output;
		vector<unsigned int> bit_size_trigger_ndx;
		unsigned long long nextndx = 256u;

		string s;
		string teststring;
		for (auto c : source)
		{
			teststring = s;
			teststring += c;
			if (dictionary.count(teststring) > 0)
			{
				s = teststring;
			}
			else
			{
				if (dictionary.count(s) > 0) {
					output.push_back(dictionary[s]);
				}
				else {
					for (auto ch : s) output.push_back(ch & 0xFF);
				}
				if (teststring.size() > 1 && nextndx < max_dict)
				{
					dictionary[teststring] = nextndx;
					nextndx++;
				}
				s = c;
			}
		}
		if (dictionary.count(s) > 0) {
			output.push_back(dictionary[s]);
		}
		else {
			for (auto ch : s) output.push_back(ch & 0xFF);
		}

		vector<bool> bits;
		auto bits_needed = 8;
		auto max_value = 0;
		for (auto x = 0u; x < output.size(); x++)
		{
			unsigned int test = (1 << bits_needed) - 1;
			while (output[x] > test)
			{
				bit_size_trigger_ndx.push_back(x);
				bits_needed++;
				test = (1 << bits_needed) - 1;
			}
		}
		dest.push_back((unsigned char)bit_size_trigger_ndx.size());
		BitTransformer::copy_int_array_to_bitstream(bit_size_trigger_ndx, 0, 32 * bit_size_trigger_ndx.size(), 32, bits);

		bits_needed = 8;
		auto bits_needed_ndx = 0;
		for (auto x = 0u; x < output.size(); x++)
		{
			if (bits_needed_ndx < bit_size_trigger_ndx.size() && x >= bit_size_trigger_ndx[bits_needed_ndx])
			{
				bits_needed_ndx++;
				bits_needed++;
			}
			BitTransformer::copy_int_array_to_bitstream(output, x, bits.size() + bits_needed, bits_needed, bits, false);
		}
		BitTransformer::copy_bitstream_to_char_array(bits, dest, 8);

		if (debug)
		{
			cout << "Triggers:" << endl;
			int savings = 0;
			for (auto x : bit_size_trigger_ndx) {
				cout << x << endl;
				savings += (x - 32);
			}
			cout << "Max Bits Used: " << bits_needed << endl;
			cout << "Savings: " << savings << endl;
			for (auto c : output) cout << c << " ";
			cout << endl;
			map<unsigned int, string> rdict;
			for (auto kvp : dictionary) rdict[kvp.second] = kvp.first;
			for (auto c : output) {
				if (rdict.count(c) > 0) cout << "[" << rdict[c] << "]";
				else cout << (unsigned char)c;
			}
			cout << endl;
			//for (auto x = 256u; x < nextndx; x++) {
			//	cout << setw(4) << x << " [" << rdict[x] << "]";
			//	cout << endl;
			//}
			auto maxdict = nextndx - 1;
			for (auto kvp : dictionary) if (kvp.second > maxdict) maxdict = kvp.second;
			float pct = (float)(dest.size() * 8) / (float)(source.size() * 8);
			cout << "Total uncompressed: " << source.size() * 8 << " compressed: " << setw(10) << dest.size() * 8 << " (" << setw(10) << pct << ") maxdict: " << maxdict << " bits: " << bits_needed << endl;
		}

		return true;
	}

	bool Decode(vector<unsigned char>& source, vector<unsigned char>& dest) {

		vector<bool> bits;
		vector<unsigned int> input;
		map<unsigned int, string> rdict;
		vector<unsigned int> bit_size_trigger_ndx;

		for (auto x = 0u; x < 256u; x++) {
			string s = "";
			s += (unsigned char)x;
			rdict[x] = s;

		}

		unsigned int trigger_size = (unsigned int)source[0];
		BitTransformer::copy_char_array_to_bitstream(source, 1, trigger_size * 32, 8, bits);
		BitTransformer::copy_bitstream_to_int_array(bits, bit_size_trigger_ndx, 32);
		if (debug)
		{
			cout << "Triggers:" << endl;
			for (auto x : bit_size_trigger_ndx) {
				cout << x << endl;
			}
		}

		BitTransformer::copy_char_array_to_bitstream(source, 1 + trigger_size * 4, (source.size() - (1 + trigger_size * 4)) * 8, 8, bits);
		auto bits_needed = 8u;
		auto bits_needed_ndx = 0u;
		auto bit_ndx = 0u;
		while (bit_ndx < bits.size())
		{
			if (bits_needed_ndx < bit_size_trigger_ndx.size() && input.size() >= bit_size_trigger_ndx[bits_needed_ndx])
			{
				bits_needed_ndx++;
				bits_needed++;
			}
			unsigned int symbol = 0;
			auto b = 0u;
			for (b = 0u; b < bits_needed; b++)
			{
				if (bit_ndx >= bits.size()) break;
				symbol *= 2;
				symbol += bits[bit_ndx];
				bit_ndx++;
			}
			if (b >= bits_needed) input.push_back(symbol);
		}
		if (debug) for (auto c : input) cout << c << " ";
		if (debug) cout << endl;

		unsigned int next_bit = 0;
		unsigned int old = input[0];
		unsigned int n;
		string s = rdict[old];
		string c = "";
		c += s[0];
		for (auto ch : s) dest.push_back(ch & 0xFF);
		int nextndx = 256;
		for (unsigned int x = 0; x < input.size() - 1; x++) {
			n = input[x + 1];
			if (rdict.find(n) == rdict.end()) {
				s = rdict[old];
				s = s + c;
			}
			else {
				s = rdict[n];
			}
			for (auto ch : s) dest.push_back(ch & 0xFF);
			c = "";
			c += s[0];
			rdict[nextndx] = rdict[old] + c;
			nextndx++;
			old = n;
		}
		if (debug) 
		{
			for (auto c : dest) cout << c;
			cout << endl;
			//for (auto x = 256u; x < nextndx; x++) {
			//	cout << setw(4) << x << " [" << rdict[x] << "]";
			//	cout << endl;
			//}
		}

		return true;
	}
	bool Transform(vector<unsigned char>& source, vector<unsigned char>& dest, bool encode, unsigned long long max_dict)
	{
		dest.clear();

		if (encode) return Encode(source, dest, max_dict);
		else return Decode(source, dest);
	}
}