#pragma once
#include <vector>
using namespace std;

namespace BurrowsWheeler {
	bool Transform(vector<unsigned char>& source, vector<unsigned char>& dest, bool compress);
}