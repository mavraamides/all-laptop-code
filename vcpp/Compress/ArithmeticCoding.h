#pragma once
#include <iostream>
#include <vector>
using namespace std;

namespace ArithmeticCoding 
{
	bool Transform(vector<unsigned char>& source, vector<unsigned char>& dest, bool encode);
	void ReadDataFromSource(std::vector<unsigned char>& source, std::vector<bool>& bitstream);
	void initialize_P(std::vector<unsigned char>& source);
	void WriteDataToDestination(std::vector<unsigned char>& dest, std::vector<bool>& bitstream);
	void DumpState(unsigned long long w, const unsigned char& n, unsigned long long a, unsigned long long b, unsigned int s, std::vector<bool>& bitstream);
}