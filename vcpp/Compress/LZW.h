#pragma once
#include <string>
#include <map>
#include <vector>
using namespace std;

namespace LZW
{
	bool Transform(vector<unsigned char>& source, vector<unsigned char>& dest, bool encode, unsigned long long max_dict = 4096);
}