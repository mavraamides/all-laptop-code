#include <iomanip>
#include <map>
#include <bitset>
#include <algorithm>
#include "ArithmeticCoding.h"
#include "BitTransformer.h"

#define ull unsigned long long

namespace ArithmeticCoding 
{
	bool debug = false;
	const int MaxChar = 256;
	const unsigned int Precision = 34;
	unsigned int MaxFreqPrecision = 0;

	ull Whole = 0;
	ull Divisor = 0;

	unsigned int R;
	unsigned int P[MaxChar];
	unsigned int C[MaxChar];
	unsigned int D[MaxChar];

	void initialize_P(std::vector<unsigned char>& source)
	{
		for (auto x = 0u; x < MaxChar; x++) P[x] = 0u;
		for (auto c : source) P[c]++;
		unsigned int max_count = 0;
		for (auto x = 0u; x < MaxChar; x++) if (P[x] > max_count) max_count = P[x];
		if (debug) for (auto c = 0u; c < MaxChar; c++) if (P[c] > 0) cout << setw(3) << c << " " << P[c] << endl;
		R = (unsigned int)source.size();
		if (debug) cout << "Total Characters: " << R << endl;
		MaxFreqPrecision = max_count == 0 ? 0 : (unsigned int)log2(max_count) + 1;
		if (debug) cout << "Max Count:        " << max_count << " " << MaxFreqPrecision << endl;
	}

	void initialize_tables()
	{
		auto c = 0u;
		auto d = 0u;
		for (auto x = 0u; x < MaxChar; x++)
		{
			if (x > 0) c += P[x - 1];
			C[x] = c;
			d += P[x];
			D[x] = d;
		}
		Whole = 0;
		for (auto x = 0u; x < Precision; x++) {
			Whole *= 2;
			Whole += 1;
		}
		if (debug) cout << Whole << endl;
		Divisor = (Whole + 1) / 2;
		if (debug) for (auto c = 0u; c < MaxChar; c++) if (P[c] > 0) cout << setw(3) << c << " " << setw(3) << C[c] << " " << setw(3) << D[c] << endl;
	}
	bool Encode(vector<unsigned char>& source, vector<unsigned char>& dest)
	{
		bool passed = true;
		initialize_P(source);
		initialize_tables();

		ull a = 0;
		ull b = Whole;
		
		vector<bool> result;
		for (auto c : source)
		{
			ull w = b - a + 1;
			b = a + (w * D[c]) / R - 1;
			a = a + (w * C[c]) / R;
			//cout << setw(4) << a << " " << setw(4) << b << endl;
			while (true) {
				auto adigit = a / Divisor;
				auto bdigit = b / Divisor;
				if (adigit == bdigit)
				{
					result.push_back(adigit & 0x1);
					//cout << "Code: " << adigit << endl;
					a = (a - adigit * Divisor) * 2;
					b = (b - adigit * Divisor) * 2 + 1;
					//cout << setw(4) << a << " " << setw(4) << b << endl;
				}
				else break;
			}
		}

		while (a > 0)
		{
			auto adigit = a / Divisor;
			result.push_back(adigit & 0x1);
			a = (a - adigit * Divisor) * 2;
		}
		if (debug) {
			for (auto c : result) cout << (unsigned int)c;
			cout << endl;
			cout << "Total bits: " << result.size() << endl;
		}

		// 1) Total length of data stream in bits as 32 bit integer
		vector<bool> bits;
		vector<unsigned char> char_array;
		vector<unsigned int> int_array;
		BitTransformer::copy_int_to_char_array(result.size(), char_array, 32);
		BitTransformer::copy_char_array_to_bitstream(char_array, 0, 32, 8, bits);
		if (debug) for (auto b : bits) cout << b;
		if (debug) cout << endl;
		
		// 2) Number of significant bits in count table as char
		char_array.clear();
		char_array.push_back(MaxFreqPrecision);
		BitTransformer::copy_char_array_to_bitstream(char_array, 0, 8 + 32, 8, bits, false);
		if (debug) for (auto b : bits) cout << b;
		if (debug) cout << endl;

		// 3) Count table as bitstream of 256 * significant bits
		int_array.clear();
		for (auto x = 0u; x < MaxChar; x++) int_array.push_back(P[x]);
		BitTransformer::copy_int_array_to_bitstream(int_array, 0, 8 + 32 + MaxChar * MaxFreqPrecision, MaxFreqPrecision, bits, false);
		if (debug) for (auto b : bits) cout << b;
		if (debug) cout << endl;

		// 4) Copy result
		for (auto b : result) bits.push_back(b);

		// 5) Copy to destination
		BitTransformer::copy_bitstream_to_char_array(bits, dest, 8);

		return passed;
	}


	/*
	Plan:
	[X] 1) Get decoder working by just passing the stream of decimal numbers.
	[X] 2) Convert to using previous version of P, C, D calculated in encode.
	[X] 3) Test on big, huge and image.
	[X] 4) Add encoding table and length.
	[X] 5) Convert to binary and do (3) again.
	[X] 6) Port LZW compress
	[X] 7) LZW Decompress
	[ ] 8) Enhance LZW
	[ ] 9) Update Encryption
	[ ] 10) Port Encryption
	[ ] 11) Port to Stegenography
	*/
	bool  Decode(vector<unsigned char>& source, vector<unsigned char>& dest)
	{
		bool passed = true;

		// 1) Total length of data stream in bytes as 32 bit integer
		vector<bool> bits;
		unsigned int length_of_data_stream;
		BitTransformer::copy_char_array_to_int(source, 0, length_of_data_stream, 32);
		if (debug) {
			bitset<32> b32;
			b32 = length_of_data_stream;
			cout << b32 << endl;
			cout << "Total bits: " << length_of_data_stream << endl;
		}
		
		// 2) Number of significant bits in count table as char
		MaxFreqPrecision = source[4];
		if (debug) {
			bitset<8> b8;
			b8 = MaxFreqPrecision;
			cout << b8 << endl;
		}

		// 3) Count table as bitstream of 256 * significant bits
		vector<unsigned char> char_array;
		vector<unsigned int> int_array;
		BitTransformer::copy_packed_char_array_to_bitstream(source, 5 * 8, MaxFreqPrecision * MaxChar, bits);
		BitTransformer::copy_bitstream_to_int_array(bits, int_array, MaxFreqPrecision);
		R = 0;
		for (auto c = 0u; c < MaxChar; c++) {
			P[c] = int_array[c];
			R += P[c];
		}
		if (debug) for (auto c = 0u; c < MaxChar; c++) if (P[c] > 0) cout << setw(3) << c << " " << P[c] << endl;
		if (debug) cout << "Total Characters: " << R << endl;
		initialize_tables();


		// 4) Data stream as bytes
		BitTransformer::copy_packed_char_array_to_bitstream(source, 5 * 8 + MaxFreqPrecision * MaxChar, length_of_data_stream, bits);
		if (debug) for (auto c : bits) cout << c;
		if (debug) cout << endl;

		ull a = 0;
		ull b = Whole;

		ull code = 0;
		unsigned int next_src = 0;
		for (next_src = 0; next_src < Precision; next_src++) {
			code *= 2;
			if (next_src < bits.size())
			{
				code += bits[next_src];
			}
		}
		if (debug) cout << code << endl;

		while (true)
		{
			ull w = b - a + 1;
			ull index = ((code - a + 1) * R - 1) / w;
			
			//cout << "START " << setw(8) << a << " " << setw(8) << code << " " << setw(8) << b << setw(8) << index << endl;
			for (auto c = 0u; c < MaxChar; c++)
			{
				if (C[c] <= index && index < D[c])
				{
					//cout << c << endl;
					dest.push_back(c);
					b = a + w * D[c] / R - 1;
					a = a + w * C[c] / R;
					//cout << "CODE  " << setw(8) << a << " " << setw(8) << code << " " << setw(8) << b << " " << c << endl;
					break;
				}
			}
			if (dest.size() >= R) break;
			while (true)
			{
				auto adigit = a / Divisor;
				auto bdigit = b / Divisor;
				auto cdigit = code / Divisor;
				if (adigit == bdigit)
				{
					a = (a - adigit * Divisor) * 2;
					b = (b - adigit * Divisor) * 2 + 1;
					code = (code - cdigit * Divisor) * 2;
					if (next_src < bits.size()) {
						code += bits[next_src++];
					}
				}
				else {
					//cout << "SCALE " << setw(8) << a << " " << setw(8) << code << " " << setw(8) << b << setw(8) << index << endl;
					break;
				}
			}
		}
		if (debug) for (auto c : dest) cout << c;
		if (debug) cout << endl;
		return passed;
	}
	bool Transform(vector<unsigned char>& source, vector<unsigned char>& dest, bool encode)
	{
		dest.clear();

		if (encode) return Encode(source, dest);
		else return Decode(source, dest);
	}
}