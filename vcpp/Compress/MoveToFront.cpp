#include "MoveToFront.h"

unsigned char buffer[256];

namespace MoveToFront
{
	void initialize_buffer()
	{
		for (auto x = 0u; x < 256; x++) buffer[x] = x;
	}

	unsigned int find_char(unsigned char c)
	{
		unsigned int ndx = 256;
		for (auto x = 0u; x < 256; x++) {
			if (c == buffer[x]) {
				ndx = x;
				break;
			}
		}
		return ndx;
	}

	bool move_to_front(unsigned ndx) {
		if (ndx < 256) {
			auto hold = buffer[ndx];
			for (auto x = ndx; x > 0; x--) buffer[x] = buffer[x - 1];
			buffer[0] = hold;
			return true;
		}
		return false;
	}
	bool Encode(vector<unsigned char>& source, vector<unsigned char>& dest)
	{
		bool passed = true;
		for (auto c : source)
		{
			auto ndx = find_char(c);
			passed = move_to_front(ndx);
			if (!passed) break;
			dest.push_back(ndx);
		}
		return passed;
	}
	bool  Decode(vector<unsigned char>& source, vector<unsigned char>& dest)
	{
		bool passed = true;
		for (auto c : source)
		{
			dest.push_back(buffer[c]);
			passed = move_to_front(c);
			if (!passed) break;
		}
		return passed;
	}
	bool Transform(vector<unsigned char>& source, vector<unsigned char>& dest, bool encode)
	{
		initialize_buffer();
		dest.clear();

		if (encode) return Encode(source, dest);
		else return Decode(source, dest);
	}
}