#pragma once

int Compress(std::vector<unsigned char>& from, std::vector<unsigned char>& to1, bool compressing, std::vector<unsigned char>& to2, bool& retflag);

int Uncompress(std::vector<unsigned char>& from, const unsigned char& LZW_CODE, std::vector<unsigned char>& to1, bool& retflag);
