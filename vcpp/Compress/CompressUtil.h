#pragma once
#include <vector>
#include <iostream>
#include <iomanip>
using namespace std;

namespace CompressUtil {
    const unsigned char LZW_CODE = 0x00;
    const unsigned char AC_CODE = 0x01;

    bool Compress(std::vector<unsigned char>& from, std::vector<unsigned char>& to1, std::vector<unsigned char>& to2);
    bool Uncompress(std::vector<unsigned char>& from, std::vector<unsigned char>& to1);
}