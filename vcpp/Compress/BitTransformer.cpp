#include "BitTransformer.h"

namespace BitTransformer {
	void copy_int_array_to_bitstream(vector<unsigned int>& int_array, unsigned int src_start, unsigned int max_bits, unsigned char msb, vector<bool>& bitstream, bool clear_first)
	{
		if (clear_first) bitstream.clear();
		for (auto x = src_start; bitstream.size() < max_bits && x < int_array.size(); x++)
		{
			auto word = int_array[x];
			unsigned int bit_mask = (1 << (msb - 1));
			while (bitstream.size() < max_bits && bit_mask > 0)
			{
				bitstream.push_back(word & bit_mask);
				bit_mask = bit_mask >> 1;
			}
		}
	}
	/*
	Assumes every bit in char array is relevant.
	first_bit is measured from start of char array
	*/
	void copy_packed_char_array_to_bitstream(vector<unsigned char > & char_array, unsigned int first_bit, unsigned int max_bits, vector<bool>& bitstream)
	{
		unsigned int bit_mask = (1 << 7);
		bit_mask = bit_mask >> (first_bit % 8);
		unsigned int src_start = first_bit / 8;
		bitstream.clear();
		for (auto x = src_start; bitstream.size() < max_bits && x < char_array.size(); x++)
		{
			auto word = char_array[x];
			while (bitstream.size() < max_bits && bit_mask > 0)
			{
				bitstream.push_back(word & bit_mask);
				bit_mask = bit_mask >> 1;
			}
			bit_mask = (1 << 7);
		}
	}
	void copy_char_array_to_bitstream(vector<unsigned char>& char_array, unsigned int src_start, unsigned int max_bits, unsigned char msb, vector<bool>& bitstream, bool clear_first)
	{
		if (clear_first) bitstream.clear();
		for (auto x = src_start; bitstream.size() < max_bits && x < char_array.size(); x++)
		{
			auto word = char_array[x];
			unsigned int bit_mask = (1 << (msb - 1));
			while (bitstream.size() < max_bits && bit_mask > 0)
			{
				bitstream.push_back(word & bit_mask);
				bit_mask = bit_mask >> 1;
			}
		}
	}
	void copy_bitstream_to_char_array(vector<bool>& bitsream, vector<unsigned char>& char_array, unsigned char msb)
	{
		unsigned char word = 0x0;
		unsigned int bit_mask = (1 << (msb-1));
		for (auto b : bitsream)
		{
			if (b) word |= bit_mask;
			bit_mask = bit_mask >> 1;
			if (bit_mask == 0)
			{
				char_array.push_back(word);
				word = 0x0;
				bit_mask = (1 << (msb-1));
			}
		}
		if (bit_mask < (1 << (msb-1))) char_array.push_back(word);
	}
	void copy_bitstream_to_int_array(vector<bool>& bitsream, vector<unsigned int>& int_array, unsigned char msb)
	{
		unsigned int word = 0x0;
		unsigned int bit_mask = (1 << (msb - 1));
		for (auto b : bitsream)
		{
			if (b) word |= bit_mask;
			bit_mask = bit_mask >> 1;
			if (bit_mask == 0)
			{
				int_array.push_back(word);
				word = 0x0;
				bit_mask = (1 << (msb - 1));
			}
		}
		if (bit_mask < (1 << (msb - 1))) int_array.push_back(word);
	}
	void copy_int_to_char_array(unsigned int src_int, vector<unsigned char>& char_array, unsigned char msb)
	{
		vector<unsigned int>source({ src_int });
		vector<bool> bitstream;
		copy_int_array_to_bitstream(source, 0, msb, msb, bitstream);
		copy_bitstream_to_char_array(bitstream, char_array, 8);
	}
	void copy_char_array_to_int(vector<unsigned char>& char_array, unsigned int src_start, unsigned int& target_int, unsigned char msb)
	{
		vector<bool> bitstream;
		vector<unsigned int> int_array;
		copy_char_array_to_bitstream(char_array, src_start, msb, 8, bitstream);
		copy_bitstream_to_int_array(bitstream, int_array, msb);
		if (int_array.size() > 0) target_int = int_array[0];
	}
	void test()
	{
		vector<unsigned int> int_array;
		vector<bool> bitstream;
		vector<unsigned char> char_array;
		bitset<8> b8;
		bitset<9> b9;
		bitset<10> b10;
		bitset<11> b11;
		bitset<12> b12;
		bitset<13> b13;
		bitset<14> b14;
		bitset<15> b15;
		bitset<16> b16;
		for (auto msb = 9; msb <= 16; msb++)
		{
			for (auto x = 1u; x < 10; x++)
			{
				cout << msb << " " << x << endl;
				int_array.clear();
				bitstream.clear();
				char_array.clear();
				for (auto n = 0u; n < x; n++) int_array.push_back(0x16ee);

				BitTransformer::copy_int_array_to_bitstream(int_array, 0, x * msb, msb, bitstream);
				for (auto b : bitstream) cout << b;
				cout << endl;
				BitTransformer::copy_bitstream_to_char_array(bitstream, char_array, 8);
				for (auto b : char_array)
				{
					b8 = b;
					cout << b8;
				}
				cout << endl;
				BitTransformer::copy_char_array_to_bitstream(char_array, 0, x * msb, 8, bitstream);
				for (auto b : bitstream) cout << b;
				cout << endl;
				int_array.clear();
				BitTransformer::copy_bitstream_to_int_array(bitstream, int_array, msb);
				for (auto b : int_array)
				{
					switch (msb)
					{
					case 9:
						b9 = b;
						cout << b9;
						break;
					case 10:
						b10 = b;
						cout << b10;
						break;
					case 11:
						b11 = b;
						cout << b11;
						break;
					case 12:
						b12 = b;
						cout << b12;
						break;
					case 13:
						b13 = b;
						cout << b13;
						break;
					case 14:
						b14 = b;
						cout << b14;
						break;
					case 15:
						b15 = b;
						cout << b15;
						break;
					case 16:
						b16 = b;
						cout << b16;
						break;
					}
				}
				cout << endl;
			}
		}
		char_array.clear();
		BitTransformer::copy_int_to_char_array(0x148841, char_array, 24);
		for (auto b : char_array)
		{
			b8 = b;
			cout << b8;
		}
		cout << endl;
		unsigned int target_int;
		BitTransformer::copy_char_array_to_int(char_array, 0, target_int, 24);
		bitset<24> b24;
		b24 = target_int;
		cout << b24 << endl;

	}
}
