#pragma once
#include <iostream>
#include <vector>
using namespace std;

namespace MoveToFront
{
	bool Transform(vector<unsigned char>& source, vector<unsigned char>& dest, bool encode);
}