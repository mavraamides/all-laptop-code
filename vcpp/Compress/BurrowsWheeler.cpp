#include <iostream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <sstream>
#include <map>
#include "BurrowsWheeler.h"
#include "BitTransformer.h"

#define DCO if (debug) cout


namespace BurrowsWheeler {

	struct last_find {
		unsigned int last_ndx;
		unsigned int last_count;
		bool checked;
	};

	const bool debug = false;
	const int max_buffer = 0xFFFFFF;
	const int final_ndx_size = 4;

	last_find last_find_buffer[256];
	unsigned char buffer[max_buffer * 2];
	unsigned int end_of_buffer = 0;
	unsigned int decode_sort_length = 1;

	const char* get_rotation(unsigned int ndx)
	{
		return (char*)(&buffer[ndx]);
	}
	bool sort_encode(unsigned int A, unsigned int B)
	{
		return (memcmp(get_rotation(A), get_rotation(B), end_of_buffer / 2) < 0);
	}
	bool sort_decode(unsigned char A, unsigned char B)
	{
		return (A < B);
	}
	void dump_rotation_encode(unsigned int ndx)
	{
		auto c = get_rotation(ndx);
		DCO << setw(3) << ndx << " ";
		for (auto y = 0u; y < end_of_buffer / 2; y++)
		{
			DCO << c[y];
		}
		DCO << endl;
	}
	void dump_buffer()
	{
		for (auto x = 0u; x < end_of_buffer; x++) DCO << buffer[x];
		DCO << endl;
	}
	void copy_to_buffer(vector<unsigned char>& source, int final_ndx)
	{
		auto total_chars = 0;
		while (total_chars < max_buffer && final_ndx < source.size() && end_of_buffer < max_buffer * 2) {
			buffer[end_of_buffer++] = source[final_ndx++];
			total_chars++;
		}
	}

	bool Compress(vector<unsigned char>& source, vector<unsigned char>& dest, int start_ndx)
	{
		copy_to_buffer(source, start_ndx);
		copy_to_buffer(source, start_ndx);

		if (debug) dump_buffer();

		vector<int> transform_matrix(end_of_buffer / 2);
		iota(transform_matrix.begin(), transform_matrix.end(), 0);

		sort(transform_matrix.begin(), transform_matrix.end(), sort_encode);

		int final_index = -1;
		for (auto x = 0u; x < end_of_buffer / 2; x++)
		{
			if (transform_matrix[x] == 0) final_index = x;
			DCO << setw(3) << x << " ";
			if (debug) dump_rotation_encode(transform_matrix[x]);
		}
		if (final_index < 0) return false;

		BitTransformer::copy_int_to_char_array(final_index, dest, final_ndx_size * 8);

		for (auto x = 0u; x < end_of_buffer / 2; x++) dest.push_back(get_rotation(transform_matrix[x])[end_of_buffer / 2 - 1]);
		
		if (debug) for (auto c : dest) DCO << c;
		DCO << endl;
		
		return true;
	}

	bool Deompress(vector<unsigned char>& source, vector<unsigned char>& dest, int start_ndx)
	{
		unsigned int final_index = 0;
		BitTransformer::copy_char_array_to_int(source, start_ndx, final_index, final_ndx_size * 8);

		copy_to_buffer(source, start_ndx + final_ndx_size);
		if (debug) for (auto c : source) DCO << c;
		DCO << endl;
		size_t end_of_sort = start_ndx + final_ndx_size + max_buffer;
		if (end_of_sort > source.size()) end_of_sort = source.size();
		sort(source.begin() + start_ndx + final_ndx_size, source.begin() + end_of_sort, sort_decode);
		if (debug) for (auto c : source) DCO << c;
		DCO << endl;
		copy_to_buffer(source, start_ndx + final_ndx_size);
		if (debug) dump_buffer();

		auto old_src = buffer[0];
		auto new_src = buffer[0];
		auto char_count = 1u;
		vector<unsigned int> next;
		for (auto x = 0u; x < 256; x++) last_find_buffer[x].checked = false;
		for (auto ndx = 0u; ndx < end_of_buffer / 2; ndx++)
		{
			old_src = new_src;
			new_src = buffer[ndx + end_of_buffer / 2];
			if (old_src != new_src || ndx == 0) char_count = 1;
			else char_count++;
			DCO << new_src << " " << char_count;

			auto next_count = 0u;
			auto starting_ndx = 0u;
			if (last_find_buffer[new_src].checked) {
				next_count = last_find_buffer[new_src].last_count;
				starting_ndx = last_find_buffer[new_src].last_ndx;
			}
			for (auto n = starting_ndx; n < end_of_buffer / 2; n++)
			{
				if (buffer[n] == new_src) next_count++;
				if (next_count >= char_count) {
					last_find_buffer[new_src].checked = true;
					last_find_buffer[new_src].last_count = next_count;
					last_find_buffer[new_src].last_ndx = n + 1;
					next.push_back(n);
					DCO << " " << n;
					break;
				}
			}
			DCO << endl;
		}
		if (debug) for (auto n : next) DCO << n << " ";
		DCO << endl;

		auto next_ndx = final_index;
		for (auto x = 0u; x < end_of_buffer / 2; x++)
		{
			auto next_char = buffer[next_ndx + end_of_buffer / 2];
			next_ndx = next[next_ndx];
			dest.push_back(next_char);
			DCO << next_char;
		}
		if (debug) {
			DCO << endl;
			for (auto c : dest) DCO << c;
			DCO << endl;
		}
		return true;
	}

	bool Transform(vector<unsigned char>& source, vector<unsigned char>& dest, bool compress)
	{
		bool result = true;
		dest.clear();

		auto start_ndx = 0u;
		while (result && start_ndx < source.size()) {
			end_of_buffer = 0;
			if (compress) {
				result = Compress(source, dest, start_ndx);
				start_ndx += max_buffer;
			}
			else {
				result = Deompress(source, dest, start_ndx);
				start_ndx += max_buffer + final_ndx_size;
			}
		}
		//BitTransformer::test();
		return result;
	}
}