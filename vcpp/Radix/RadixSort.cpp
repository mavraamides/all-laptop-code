#include "RadixSort.h"

namespace RadixSort
{
	auto debug = false;
	unsigned int* workspace[2];
	unsigned int * buckets = nullptr;
	unsigned int bucket_size = 0x8000;
	bool initialized = false;
	unsigned int capacity = 0;
	unsigned int from = 0;
	unsigned int to = 1;

	void InitWorkspace(size_t size)
	{
		if (capacity >= size) return;
		if (initialized)
		{
			delete[] workspace[0];
			delete[] workspace[1];
			delete[] buckets;
		}
		workspace[0] = new unsigned int[size];
		workspace[1] = new unsigned int[size];
		buckets = new unsigned int[bucket_size];
		initialized = true;
		capacity = size;
	}

	void DumpBuckets()
	{
		for (auto x = 0u; x < bucket_size; x++) cout << setw(3) << x << " " << setw(3) << buckets[x] << endl;
	}

	inline unsigned int GetBucket(unsigned int round, unsigned int word)
	{
		for (auto y = 0u; y < round; y++)
		{
			word /= bucket_size;
		}
		word %= bucket_size;
		return word;
	}

	unsigned int* Sort(vector<unsigned int> &v)
	{
		InitWorkspace(v.size());
		unsigned int round = 0;
		auto max = 0;
		auto max_compare = bucket_size;

		for (auto x = 0u; x < v.size(); x++) 
		{
			workspace[from][x] = x;
		}

		while (true)
		{
			for (auto x = 0u; x < bucket_size; x++) buckets[x] = 0;
			for (auto x = 0u; x < v.size(); x++)
			{
				auto value = v[workspace[from][x]];
				if (round == 0 && value > max) max = value;
				auto word = GetBucket(round, value);
				buckets[word] ++;
			}
			if (debug) DumpBuckets();
			auto total = 0u;
			for (auto x = 0u; x < bucket_size; x++)
			{
				total += buckets[x];
				buckets[x] = total;
			}
			if (debug) DumpBuckets();
			auto ndx = v.size() - 1;
			for (auto x = 0u; x < v.size(); x++)
			{
				auto word = GetBucket(round, v[workspace[from][ndx]]);
				buckets[word]--;
				auto loc = buckets[word];
				workspace[to][loc] = workspace[from][ndx];
				ndx--;
			}
			from = 1 - from;
			to = 1 - from;
			round++;

			if (max_compare > max) break;
			max_compare *= bucket_size;
		}
		
		return workspace[from];
	}
}