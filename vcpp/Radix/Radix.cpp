#include <iostream>
#include <iomanip>
#include <random>
#include <time.h>
#include <vector>
#include <algorithm>
#include "RadixSort.h"
#include <chrono>
using namespace std::chrono;
using namespace std;

int main()
{
	srand((unsigned int)time(0));

	unsigned int* result = nullptr;
	vector<unsigned int> v;
	for (auto n = 0u; n < 5; n++)
	{
		v.clear();
		for (auto x = 0u; x < 10000000; x++) v.push_back(rand() % 10000);

		//vector<unsigned int> v2(v.begin(), v.end());
		//high_resolution_clock::time_point t1 = high_resolution_clock::now();
		//sort(v2.begin(), v2.end());
		//high_resolution_clock::time_point t2 = high_resolution_clock::now();
		//duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
		//cout << "Sort time: " << time_span.count() << endl;


		high_resolution_clock::time_point t3 = high_resolution_clock::now();
		result = RadixSort::Sort(v);
		high_resolution_clock::time_point t4 = high_resolution_clock::now();
		duration<double> time_span2 = duration_cast<duration<double>>(t4 - t3);
		cout << "Sort time: " << time_span2.count() << endl;
		//cout << "Ratio: " << time_span.count() / time_span2.count() << endl;
	}
	if (result == nullptr) return 1;
	auto last = 0u;
	for (auto x = 0u; x < v.size(); x++)
	{
		auto word = v[result[x]];
		if (word < last)
		{
			cout << "Error: " << x << " " << last << v[x] << endl;
			break;
		}
		last = word;
	}
}

