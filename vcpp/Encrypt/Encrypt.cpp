// Encrypt.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <iomanip>
#include "AES.h"
#include "keccak.h"
using namespace std;

int main(int argc, char* argv[])
{
    if (argc < 5)
    {
        cout << "Usage: Encrypt {source file} {key file} {dest file} <1 encrypt | 2 decryt> (Optional)<1 - Keccak Encrypt>" << endl;
        return 1;
    }
    bool keccak_enc = false;
    if (argc > 5) {
        string keccak_flag = argv[5];
        keccak_enc = (keccak_flag.size() > 0 && keccak_flag[0] == '1');
    }
    string encrypt_flag = argv[4];
    bool encrypting = (encrypt_flag.size() > 0 && encrypt_flag[0] == '1');

    string sourcefilename = argv[1];
    string keyfilename = argv[2];
    string destfilename = argv[3];

    ifstream in_source(sourcefilename, ios::binary);
    if (!in_source.good()) {
        cout << "Unable to load " << sourcefilename << endl;
        return 1;
    }
    vector<unsigned char> source(istreambuf_iterator<char>(in_source), {});
    in_source.close();

    ifstream in_key(keyfilename, ios::binary);
    if (!in_key.good()) {
        cout << "Unable to load " << keyfilename << endl;
        return 1;
    }
    vector<unsigned char> raw_key(istreambuf_iterator<char>(in_key), {});
    in_key.close();

    vector<unsigned char> dest;

    vector<unsigned char> key(raw_key);
    if (keccak_enc) {
        cout << "Keccak key hash..." << endl;
        KECCAK::get_key_hash(raw_key, key);
    }

    if (encrypting)
    {
        auto uncompressed_size = source.size() * 8;
        cout << "AES Encrypt..." << endl;
        if (!AES::Transform(source, key, dest, encrypting))
        {
            cout << "AES Encrypt failed." << endl;
            return 1;
        }
        if (keccak_enc)
        {
            cout << "Keccak Encrypt..." << endl;
            KECCAK::encrypt(raw_key, dest);
        }
    }
    else
    {
        if (keccak_enc) {
            cout << "Keccak Decrypt..." << endl;
            KECCAK::encrypt(raw_key, source);
        }
        cout << "AES Decrypt..." << endl;
        if (!AES::Transform(source, key, dest, encrypting))
        {
            cout << "AES Decrypt failed." << endl;
            return 1;
        }
    }

    ofstream out(destfilename, ios::binary);
    if (out.is_open())
    {
        for (auto c : dest) out << c;
        out.close();
    }

}
