#pragma once
#include <vector>
using namespace std;


namespace AES 
{
	bool Transform(vector<unsigned char>& source, vector<unsigned char>& key, vector<unsigned char>& dest, bool encode);
}