#pragma once
//
// From Coding Jesus https://youtu.be/LqrF_nygxhY
//
#include <condition_variable>
#include <mutex>
#include <iostream>
#include <queue>
#include <memory>
using namespace std;

class PieceOfWork
{
private:
public:
	bool terminate = false;
	vector<string> inputs;
	vector<string> outputs;
	PieceOfWork()
	{
	};
	~PieceOfWork() {};
	const void dump() {
		for (auto s : outputs)
		{
			cout << s << " ";
		}
		cout << endl;
	};
};

class ThreadSafeQueue
{
private:
	mutable mutex mut;
	queue<PieceOfWork> data_queue;
	condition_variable data_cond;
public:
	explicit ThreadSafeQueue() {};
	explicit ThreadSafeQueue(const ThreadSafeQueue& other)
	{
		lock_guard<mutex> lock(other.mut);
		data_queue = other.data_queue;
	};
	ThreadSafeQueue& operator=(const ThreadSafeQueue& other) = delete;

	void push(PieceOfWork value)
	{
		lock_guard<mutex> lock(mut);
		data_queue.push(value);
		data_cond.notify_one();
	};
	void wait_and_pop(PieceOfWork& value)
	{
		unique_lock<mutex> lock(mut);
		data_cond.wait(lock, [this] {return !data_queue.empty(); });
		value = data_queue.front();
		data_queue.pop();
	};
	shared_ptr<const PieceOfWork> wait_and_pop()
	{
		unique_lock<mutex> lock(mut);
		data_cond.wait(lock, [this] {return !data_queue.empty(); });
		shared_ptr<const PieceOfWork> result(make_shared<const PieceOfWork>(data_queue.front()));
		data_queue.pop();
		return result;
	};
	bool try_pop(PieceOfWork& value)
	{
		lock_guard<mutex> lock(mut);
		if (data_queue.empty())
		{
			return false;
		}
		value = data_queue.front();
		data_queue.pop();
		return true;
	};
	shared_ptr<const PieceOfWork> try_pop()
	{
		lock_guard<mutex> lock(mut);
		if (data_queue.empty())
		{
			return nullptr;
		}
		shared_ptr<const PieceOfWork> result(make_shared<const PieceOfWork>(data_queue.front()));
		data_queue.pop();
		return result;
	};
	bool empty() const
	{
		lock_guard<mutex> lock(mut);
		return data_queue.empty();
	};
};

