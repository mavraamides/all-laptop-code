// ThreadSafeQueue.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <deque>
#include <string>

using namespace std;

#include "ThreadSafeQueue.h"

mutex mu;
condition_variable cond;
deque<int> buffer;
const unsigned int maxBufferSize = 50;

void producer(int val) {
	while (val) {
		unique_lock<mutex> locker(mu);
		cond.wait(locker, [] () {return buffer.size() < maxBufferSize; });
		cout << "Produced: " << val << endl;
		val--;
		locker.unlock();
		cond.notify_all();
	}
	cout << "Producer exiting...";
}

void consumer() {
	auto total = 0;
	while (true) {
		unique_lock<mutex> locker(mu);
		cond.wait(locker, []() { return buffer.size() > 0; });
		int val = buffer.back();
		buffer.pop_back();
		cout << "Consumed: " << val << endl;
		locker.unlock();
		cond.notify_one();
		total++;
		if (total >= 50) break;
	}
}

ThreadSafeQueue message_queue;
ThreadSafeQueue response_queue;

void sim(string name)
{
	auto count = 0;
	auto total = 0;
	while (true) {
		auto w = message_queue.wait_and_pop();
		if (w->terminate) break;
		count++;
		if (w->inputs.size() > 0) total += atoi(w->inputs[0].c_str());
	}
	PieceOfWork r;
	r.outputs.push_back(name);
	r.outputs.push_back(to_string(count));
	r.outputs.push_back(to_string(total));
	response_queue.push(r);
}

int main()
{
	vector<thread> threads;
	for (int x = 0; x < 10; x++)
	{
		threads.push_back(thread(sim, to_string(x)));
	}

	for (int x = 0; x < 100000; x++)
	{
		PieceOfWork w;
		w.inputs.push_back(to_string(x));
		message_queue.push(w);
	}
	
	for (int x = 0; x < threads.size(); x++)
	{
		PieceOfWork w;
		w.terminate = true;
		message_queue.push(w);
	}

	for (int x = 0; x < threads.size(); x++)
	{
		threads[x].join();
	}

	while (!response_queue.empty())
	{
		PieceOfWork r;
		if (response_queue.try_pop(r)) r.dump();
	}
	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
