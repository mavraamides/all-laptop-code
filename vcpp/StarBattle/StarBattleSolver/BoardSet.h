#pragma once


class BoardSet
{
public:
	BoardSet() {};
	~BoardSet() {};
	void PrintLayout(bool sequence = false);
	bool PickNextGuess();
	void MarkBlocked(int col, int row);
	bool MarkUsedSpots();
	bool Backtrack();
	bool IsCompleted();
	void ClearGroups();
	bool CountGroups();
	void PrintGroups();
	void Solve(int block_first = -1);
	void MultiSolve();
	void ClearGuess(int guess);
	void PrintGuesses();
	void CopyBlockList(int from, int to);
};

