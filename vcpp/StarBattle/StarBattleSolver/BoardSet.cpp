#include <iostream>
#include <iomanip>
#include <string>
#include <chrono>
#include <vector>
#include "BoardSet.h"
using namespace std;
using namespace std::chrono;

//static char layout[] =
//{
//	0,0,0,0,0,0,0,0,1,1,
//	0,0,0,2,2,2,0,0,1,1,
//	0,0,2,2,2,3,1,1,1,1,
//	4,4,5,2,2,3,3,3,1,1,
//	4,5,5,5,5,5,5,5,1,1,
//	4,4,5,5,5,5,5,6,6,6,
//	4,4,5,5,5,5,6,6,6,6,
//	4,7,7,8,8,6,6,9,9,9,
//	4,7,7,8,6,6,6,9,9,9,
//	4,7,8,8,8,6,6,9,9,9
//};
//static char layout[] =
//{
//	0,0,1,1,2,2,2,2,2,3,
//	0,0,1,2,2,2,2,4,3,3,
//	0,1,1,1,2,2,2,4,3,3,
//	0,5,5,1,2,2,2,4,4,4,
//	0,5,5,5,5,5,5,4,4,4,
//	0,0,6,6,7,7,7,7,7,7,
//	0,0,6,7,7,8,8,8,7,7,
//	0,6,6,7,8,8,8,8,7,7,
//	0,6,6,6,9,9,8,8,8,7,
//	0,6,9,9,9,8,8,8,8,7
//};
//static char layout[] =
//{
//	0,1,2,2,2,3,3,3,4,4,
//	0,1,2,2,2,3,3,4,4,4,
//	0,1,1,2,2,3,3,4,4,4,
//	1,1,1,2,2,3,3,4,4,4,
//	1,1,1,1,1,3,4,4,4,4,
//	5,1,1,6,6,6,4,4,4,4,
//	5,5,5,6,6,6,6,7,7,7,
//	5,5,5,5,5,6,8,8,8,8,
//	5,5,5,5,5,8,8,8,8,8,
//	5,5,5,9,9,9,8,8,8,8
//};
//static char layout[] =
//{
//	0,0,1,1,1,1,1,2,2,2,
//	0,0,0,0,0,1,1,2,2,2,
//	3,3,3,3,3,3,1,2,2,2,
//	3,3,4,5,5,3,1,1,1,2,
//	3,3,4,5,5,3,6,6,6,2,
//	7,7,4,5,4,4,6,6,6,6,
//	7,7,4,4,4,4,6,6,6,6,
//	7,7,8,8,8,8,9,9,9,9,
//	7,7,8,8,8,8,9,9,9,9,
//	7,7,8,8,8,8,8,8,9,9
//};
//static char layout[] =
//{
//	0,0,0,0,1,2,2,2,2,2,
//	0,0,0,0,1,2,2,2,2,2,
//	3,0,3,0,1,4,4,4,2,2,
//	3,3,3,5,1,4,5,4,2,2,
//	3,6,3,5,1,1,5,4,4,4,
//	6,6,6,5,1,1,5,7,7,7,
//	6,6,6,5,5,5,5,7,9,7,
//	8,6,6,8,9,9,9,7,9,7,
//	8,8,8,8,9,9,9,9,9,9,
//	8,8,8,9,9,9,9,9,9,9
//};
static char layout[] =
{
	0,0,0,1,1,1,1,1,1,2,
	0,0,1,1,1,1,1,1,1,2,
	0,3,3,3,1,1,1,2,2,2,
	0,3,3,3,3,3,3,4,2,9,
	0,0,4,4,4,4,4,4,2,9,
	0,5,4,4,4,4,4,4,2,9,
	5,5,6,6,7,7,7,4,2,9,
	5,5,6,6,7,7,7,7,2,9,
	5,6,6,8,8,8,6,6,6,9,
	5,6,6,6,6,6,6,6,6,9
};
//static char layout[] =
//{
//	 0, 0, 0, 0, 1, 2, 3, 3, 3, 4, 4, 4,
//	 0, 1, 1, 1, 1, 2, 3, 3, 3, 4, 4, 4,
//	 0, 1, 1, 1, 2, 2, 3, 3, 3, 5, 4, 4,
//	 0, 1, 1, 1, 2, 2, 3, 3, 3, 5, 4, 4,
//	 1, 1, 1, 2, 2, 6, 3, 3, 7, 5, 4, 4,
//	 6, 6, 6, 6, 6, 6, 8, 8, 7, 5, 5, 4,
//	 6, 6, 8, 8, 8, 8, 8, 9, 7, 5, 5, 4,
//	 9, 9, 9, 9, 9, 9, 9, 9, 7,10, 5, 4,
//	 9, 7, 7, 7, 7, 7, 7, 7, 7,10, 5, 4,
//	10,10,10,10,10,10,10,10,10,10, 5, 4,
//	11,11,11,11,11,11,11,10, 5, 5, 5, 4,
//	11,11,11,11,11,11,11,11,11,11,11, 4
//};
//
const int board_size = 10;
const int max_guesses = board_size * 2;
const int blocked_size = board_size * board_size;

struct guess {
	unsigned char guess;
	unsigned char blocked[blocked_size];
};

struct group_set {
	unsigned char stars;
	unsigned char open;
};
static group_set group_counts[3][board_size];
static guess guesses[max_guesses + 1];

const int display_width = board_size * 4 + 1;
const int display_height = board_size * 2 + 1;
static char display[display_width * display_height];
static int current_guess;

int col_row_to_ndx(int col, int row) {
	return col + row * board_size;
}
bool is_valid_col_row(int col, int row) {
	return (col >= 0 && col < board_size&& row >= 0 && row < board_size);
}
char get_group(int col, int row)
{
	char group = -1;
	if (is_valid_col_row(col, row)) {
		group = layout[col_row_to_ndx(col, row)];
	}
	return group;
}
void set_location(int x, int y, char value)
{
	if (x >= 0 && x < display_width && y >= 0 && y < display_height)
	{
		auto center = y * display_width + x;
		if (center >= 0 && center < display_width * display_height)
		{
			display[center] = value;
		}
	}
}
char get_location(int x, int y)
{
	auto location = ' ';
	if (x >= 0 && x < display_width && y >= 0 && y < display_height)
	{
		auto center = y * display_width + x;
		if (center >= 0 && center < display_width * display_height)
		{
			location = display[center];
		}
	}
	return location;
}
void add_line(int col, int row, int side)
{
	if (!is_valid_col_row(col, row) || side < 0 || side >= 4) return;
	auto x = col * 4 + 2;
	auto y = row * 2 + 1;
	if (side == 0) {
		x -= 1;
		y -= 1;
		for (int z = 0; z < 3; z++) set_location(x + z, y, '-');
	}
	else if (side == 1) {
		x += 2;
		set_location(x, y, '|');
	}
	else if (side == 2) {
		x -= 1;
		y += 1;
		for (int z = 0; z < 3; z++) set_location(x + z, y, '-');
	}
	else if (side == 3) {
		x -= 2;
		set_location(x, y, '|');
	}
}
void add_center(int col, int row, char value)
{
	if (!is_valid_col_row(col, row)) return;
	auto x = col * 4 + 2;
	auto y = row * 2 + 1;
	set_location(x, y, value);
}
void add_count(int col, int row, int value)
{
	if (!is_valid_col_row(col, row)) return;
	auto label = to_string(value);
	auto x = col * 4 + 2;
	auto y = row * 2 + 1;
	if (label.size() >= 2)
	{
		set_location(x - 1, y, label.c_str()[0]);
		set_location(x, y, label.c_str()[1]);
	}
	else if (label.size() >= 1)
	{
		set_location(x, y, label.c_str()[0]);
	}
}
void set_corner_value(int x, int y)
{
	auto total = 0;
	if (get_location(x, y - 1) != ' ') total += 1;
	if (get_location(x + 1, y) != ' ') total += 2;
	if (get_location(x, y + 1) != ' ') total += 4;
	if (get_location(x - 1, y) != ' ') total += 8;
	switch (total)
	{
	case 3:
	case 6:
	case 9:
	case 12:
	case 15:
		set_location(x, y, '+');
		break;
	case 5:
	case 7:
	case 13:
		set_location(x, y, '|');
		break;
	case 10:
	case 11:
	case 14:
		set_location(x, y, '-');
		break;

	}
}
void add_corner(int col, int row, int ndx)
{
	auto x = col * 4 + 2;
	auto y = row * 2 + 1;
	if (ndx == 0) {
		x -= 2;
		y -= 1;
		set_corner_value(x, y);
	}
	else if(ndx == 1) {
		x += 2;
		y -= 1;
		set_corner_value(x, y);
	}
	else if (ndx == 2) {
		x += 2;
		y += 1;
		set_corner_value(x, y);
	}
	else if (ndx == 3) {
		x -= 2;
		y += 1;
		set_corner_value(x, y);
	}
}
void BoardSet::PrintLayout(bool sequence)
{
	for (int x = 0; x < display_width * display_height; x++) display[x] = ' ';
	for (int row = 0; row < board_size; row++) {
		for (int col = 0; col < board_size; col++) {
			if (!sequence) add_center(col, row, '.');
			if (get_group(col, row) != get_group(col, row - 1)) {
				add_line(col, row, 0);
			}
			if (get_group(col, row) != get_group(col + 1, row)) {
				add_line(col, row, 1);
			}
			if (get_group(col, row) != get_group(col, row + 1)) {
				add_line(col, row, 2);
			}
			if (get_group(col, row) != get_group(col - 1, row )) {
				add_line(col, row, 3);
			}
			for (int ndx = 0; ndx < 4; ndx++) add_corner(col, row, ndx);
		}
	}

	for (int guess = 1; guess <= current_guess; guess++) {
		int x = guesses[guess].guess;
		int row = x / board_size;
		int col = x % board_size;
		if (sequence) {
			add_count(col, row, guess);
		}
		else {
			add_center(col, row, '@');
		}
	}
	for (int x = 0; x < blocked_size; x++)
	{
		int row = x / board_size;
		int col = x % board_size;
		auto code = guesses[current_guess].blocked[x];
		if (code == 1) {
			if (sequence) {
				add_center(col, row, '.');
			}
			else {
				add_center(col, row, 'x');
			}
		}
	}

	for (int y = 0; y < display_height; y++) {
		for (int x = 0; x < display_width; x++) {
			cout << display[y * display_width + x];
		}
		cout << endl;
	}
}
bool BoardSet::PickNextGuess()
{
	bool found_guess = false;

	auto last_guess = current_guess;
	current_guess++;
	CopyBlockList(last_guess, current_guess);

	auto best_group_index = -1;
	auto best_group_score = 11;
	if (CountGroups()) {
		for (int x = 0; x < blocked_size; x++)
		{
			if (guesses[last_guess].blocked[x] != 0) continue;
			if (best_group_index < 0) best_group_index = x;
			auto row = x / board_size;
			auto col = x % board_size;
			auto group = get_group(col, row);
			auto open = group_counts[0][col].open;
			if (group_counts[1][row].open > 0 && group_counts[1][row].open < open) open = group_counts[1][row].open;
			if (group_counts[2][group].open > 0 && group_counts[2][group].open < open) open = group_counts[2][group].open;
			if (open < best_group_score)
			{
				best_group_score = open;
				best_group_index = x;
			}
		}
	}
	if (best_group_index > -1)
	{
		guesses[current_guess].guess = best_group_index;
		guesses[current_guess].blocked[best_group_index] = 2;
		guesses[last_guess].blocked[best_group_index] = 1;
		found_guess = true;
	}

	return found_guess;
}
void BoardSet::MarkBlocked(int col, int row)
{
	if (col >= 0 && col < board_size && row >= 0 && row < board_size && guesses[current_guess].blocked[col + board_size * row] == 0) {
		guesses[current_guess].blocked[col + board_size * row] = 1;
	}
}
bool BoardSet::MarkUsedSpots()
{
	for (int x = 0; x < blocked_size; x++)
	{
		if (guesses[current_guess].blocked[x] == 2)
		{
			int row = x / board_size;
			int col = x % board_size;
			for (int nrow = row - 1; nrow <= row + 1; nrow++)
			{
				for (int ncol = col - 1; ncol <= col + 1; ncol++) MarkBlocked(ncol, nrow);
			}
		}
	}

	if (CountGroups())
	{
		for (int x = 0; x < blocked_size; x++)
		{
			if (guesses[current_guess].blocked[x] == 0)
			{
				int col = x % board_size;
				if (group_counts[0][col].stars == 2) {
					guesses[current_guess].blocked[x] = 1;
					continue;
				}
				int row = x / board_size;
				if (group_counts[1][row].stars == 2) {
					guesses[current_guess].blocked[x] = 1;
					continue;
				}
				int group = get_group(col, row);
				if (group_counts[2][group].stars == 2) {
					guesses[current_guess].blocked[x] = 1;
					continue;
				}
			}
		}
	}
	else {
		return false;
	}
	return CountGroups();
}
bool BoardSet::Backtrack()
{
	auto clear = false;
	while (current_guess >= 1)
	{
		current_guess--;
		clear = CountGroups();
		if (clear) break;
	}
	return clear;
}
bool BoardSet::IsCompleted()
{
	return false;
}
void BoardSet::ClearGroups()
{
	for (int g = 0; g < 3; g++) {
		for (int x = 0; x < board_size; x++) {
			group_counts[g][x].stars = 0;
			group_counts[g][x].open = 0;
		}
	}
}
bool BoardSet::CountGroups()
{
	bool valid = true;
	ClearGroups();
	for (int x = 0; x < blocked_size; x++)
	{
		auto col = x % board_size;
		auto row = x / board_size;
		auto group = get_group(col, row);
		if (guesses[current_guess].blocked[x] == 2) {
			group_counts[0][col].stars++;
			group_counts[1][row].stars++;
			group_counts[2][group].stars++;
		}
		else if (guesses[current_guess].blocked[x] == 0) {
			group_counts[0][col].open++;
			group_counts[1][row].open++;
			group_counts[2][group].open++;
		}
	}

	for (int g = 0; g < 3; g++) {
		for (int x = 0; x < board_size; x++) {
			auto count = (int)group_counts[g][x].open + (int)group_counts[g][x].stars;
			if (count < 2) valid = false;
		}
	}
	return valid;
}
void BoardSet::PrintGroups()
{
	for (int g = 0; g < 3; g++) {
		cout << g << " ";
		for (int x = 0; x < board_size; x++) {
			cout << setw(3) << (int)group_counts[g][x].stars;
		}
		cout << endl;
		cout << "  ";
		for (int x = 0; x < board_size; x++) {
			cout << setw(3) << (int)group_counts[g][x].open;
		}
		cout << endl;
		cout << "  ";
		for (int x = 0; x < board_size; x++) {
			auto count = (int)group_counts[g][x].open + (int)group_counts[g][x].stars;
			cout << setw(3) << count;
		}
		cout << endl;
	}
}
void BoardSet::Solve(int block_first)
{
	current_guess = 0;
	ClearGroups();
	for (int x = 0; x <= max_guesses; x++) {
		ClearGuess(x);
	}
	if (block_first >= 0 && block_first < blocked_size) {
		guesses[0].blocked[block_first] = 1;
		//PrintGuesses();
		//auto c = (char)getchar();
	}
	PrintLayout();
	high_resolution_clock::time_point t1 = high_resolution_clock::now();

	long x = 0;
	auto highest = 0;
	while (true)
	{
		auto valid = PickNextGuess();
		if (valid)
		{
			valid = MarkUsedSpots();
		}

		if (!valid) {
			if (!Backtrack()) break;
		}
		if (current_guess >= max_guesses) break;

		//if (valid) {
		//	PrintLayout();
		//	PrintGuesses();
		//	auto c = (char)getchar();
		//}

		x++;
	}
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
	cout << "Guesses: " << x << " Time: " << time_span.count() << " Guesses per second: " << (x / time_span.count()) << endl;
	PrintLayout();
	PrintLayout(true);
	PrintGuesses();
}

void BoardSet::MultiSolve()
{
	Solve();
	auto c = (char)getchar();
	vector<int> solution;
	for (int x = 1; x <= max_guesses; x++) solution.push_back(guesses[x].guess);
	for (auto s : solution) cout << s << " ";
	cout << endl;
	for (auto s : solution) Solve(s);
}

void BoardSet::ClearGuess(int guess)
{
	guesses[guess].guess = -1;
	for (int c = 0; c < blocked_size; c++) guesses[guess].blocked[c] = 0;
}

void BoardSet::PrintGuesses()
{
	for (int guess = 0; guess <= max_guesses; guess++)
	{
		cout << setw(4) << guess << setw(4) << (int)guesses[guess].guess << " ";
		for (int x = 0; x < blocked_size; x++)
		{
			auto bchar = '.';
			if (guesses[guess].blocked[x] == 1) bchar = 'x';
			else if (guesses[guess].blocked[x] == 2) bchar = '@';
			cout << bchar;
		}
		if (guess == current_guess) cout << "*";
		cout << endl;
	}
}

void BoardSet::CopyBlockList(int from, int to)
{
	for (int x = 0; x < blocked_size; x++)
	{
		guesses[to].blocked[x] = guesses[from].blocked[x];
	}
}
