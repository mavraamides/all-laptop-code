// sfml19.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <SFML/Graphics.hpp>
#include <random>
#include <chrono>
#include <iostream>
#include "FastRand.h"
#include "sfml19.h"
#include "Board.h"

using namespace std;
using namespace std::chrono;

const unsigned int W = 1000;
const unsigned int H = 1100;
sf::Uint8* pixels = new sf::Uint8[W * H * 4];

enum class Commands {
    none,
    toggle_automove,
    toggle_pause,
    toggle_nascar,
    toggle_replay,
    load_replay,
    forward,
    back,
    save,
    load
};
Commands command = Commands::none;

void ClearPixels(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
    for (register int i = 0; i < W * H * 4; i += 4) {
        pixels[i] = r;
        pixels[i + 1] = g;
        pixels[i + 2] = b;
        pixels[i + 3] = a;
    }

}

int main()
{
    srand((unsigned) time(0));
    sf::RenderWindow window(sf::VideoMode(W, H), "SNAKE",sf::Style::Default);


    //window.setFramerateLimit(10);
    //window.setFramerateLimit(60);
    //window.setFramerateLimit(120);
    //window.setFramerateLimit(240);
    //window.setFramerateLimit(2000);
    
    Board board;

    //auto board_size = 6u;
    //auto board_size = 8u;
    //auto board_size = 12u;
    //auto board_size = 20u;
    auto board_size = 30u;
    board.Initialize(board_size, board_size, W, H);
    
    auto direction = SnakeFacingDirection::none;

    bool auto_mode = false;
    bool paused = false;
    bool nascar_mode = false;
    bool replay_mode = false;

    ClearPixels(0, 0, 0, 0);
    for (register int i = 0; i < W * H * 4; i += 4) {
        pixels[i + 3] = rand() % 64;
    }
    unsigned int c = 0;
    unsigned int frame = 0;
    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    while (window.isOpen())
    {
        frame++;
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
                window.close();


            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
                direction = SnakeFacingDirection::up;

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
                direction = SnakeFacingDirection::down;

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
                direction = SnakeFacingDirection::left;

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
                direction = SnakeFacingDirection::right;


            if (sf::Keyboard::isKeyPressed(sf::Keyboard::F))
            {
                command = Commands::forward;
            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::B))
            {
                command = Commands::back;
            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::N))
            {
                command = Commands::toggle_nascar;
            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
            {
                command = Commands::toggle_automove;
            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::G))
            {
                command = Commands::toggle_replay;
            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::H))
            {
                command = Commands::load_replay;
            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
            {
                command = Commands::toggle_pause;
                direction = SnakeFacingDirection::none;
            }
            
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
            {
                command = Commands::save;
            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::L))
            {
                command = Commands::load;
            }


            if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
            {
                board.Reset();
                direction = SnakeFacingDirection::none;
            }

            if (event.type == sf::Event::Closed)
                window.close();
        }

        //if (nascar_mode || frame % 20 == 0)
        if (frame % 20 == 0)
        {
            switch (command)
            {
            case Commands::load_replay:
            {
                board.ReadGameLog();
                board.WriteGameReport();
                break;
            }
            case Commands::toggle_replay:
            {
                replay_mode = !replay_mode;
                board.SetReplayMode(replay_mode);
                break;
            }
            case Commands::toggle_automove:
            {
                auto_mode = !auto_mode;
                board.SetAutoMode(auto_mode);
                direction = SnakeFacingDirection::none;
                break;
            }
            case Commands::toggle_nascar:
            {
                nascar_mode = !nascar_mode;
                break;
            }
            case Commands::toggle_pause:
            {
                paused = !paused;
                break;
            }
            case Commands::save:
            {
                board.Save();
                break;
            }
            case Commands::load:
            {
                board.Load();
                auto_mode = false;
                break;
            }
            case Commands::forward:
            {
                board.ForwardSnakeBackup();
                break;
            }
            case Commands::back:
            {
                board.BackSnakeBackup();
                auto_mode = false;
                break;
            }
            }

            if (!paused && replay_mode)
            {
                board.PlayGameLog();
            }
            else if (!paused && (auto_mode || (direction != SnakeFacingDirection::none)))
            {
                board.MoveSnake(direction);
            }

            command = Commands::none;
        }

        if (!nascar_mode || frame % 10000 == 0)
        {
            window.clear();
            board.Draw(window);

            window.display();
        }
    }
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    cout << "Total time: " << time_span.count() << endl;
    cout << "Frames per second: " << (frame / time_span.count()) << endl;
    board.WriteGameLog();
    return 0;
}

