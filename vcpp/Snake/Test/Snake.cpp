#include "Snake.h"

Snake::Snake()
{
}

Snake::~Snake()
{
}

void Snake::SetRandomPosition(unsigned int w, unsigned int h)
{
	body.clear();
	body.push_back(coord((int)(rand() % w),(int)( rand() % h)));
}

void Snake::Move(SnakeFacingDirection direction)
{	
	if (body.size() > 0)
	{
		for (size_t x = body.size() - 1; x > 0; x--)
		{
			body[x] = body[x - 1];
		}
		body[0] = GetNextHeadPosition(direction);
	}
}

void Snake::Grow(SnakeFacingDirection direction)
{
	body.push_back(coord(0, 0));
	Move(direction);
}

coord& Snake::GetNextHeadPosition(SnakeFacingDirection direction)
{
	return GetPositionOfDirection(direction, body[0].x, body[0].y);
}

coord& Snake::GetPositionOfDirection(SnakeFacingDirection direction, int x, int y)
{
	coord pos(x, y);
	switch (direction)
	{
	case SnakeFacingDirection::up:
		pos.y -= 1;
		break;
	case SnakeFacingDirection::down:
		pos.y += 1;
		break;
	case SnakeFacingDirection::left:
		pos.x -= 1;
		break;
	case SnakeFacingDirection::right:
		pos.x += 1;
		break;
	}
	return pos;
}

snake_path& Snake::GetBody()
{
	return body;
}

bool Snake::SelfCollision(SnakeFacingDirection direction)
{
	auto next_pos = GetNextHeadPosition(direction);
	for (size_t x = 1; x < body.size(); x++)
	{
		if (next_pos == body[x]) return true;
	}
	return false;
}

bool Snake::OutOfBounds()
{
	return false;
}

void Snake::Set(snake_path& b)
{
	body = b;
}
