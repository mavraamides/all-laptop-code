#include <random>
#include <ctime>
#include "FastRand.h"

static int* rand_table;
static unsigned int max_rand = 100000;
static unsigned int cur_rand = 0;
static int countdown = 1000;

int FastRand::get_int()
{
	countdown--;
	if (countdown <= 0)
	{
		countdown += rand();
		countdown = countdown % max_rand;
		cur_rand += rand();
	}
	cur_rand++;
	if (cur_rand >= max_rand) cur_rand = 0;
	return rand_table[cur_rand];
}

FastRand::FastRand()
{
	srand((unsigned)time(0));

	rand_table = new int[max_rand];
	for (auto x = 0u; x < max_rand; x++) rand_table[x] = rand();
}
