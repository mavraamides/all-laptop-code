import glob



def genreports(dir):
    filelist = glob.glob(dir)

    for file in filelist:
        print(file)
        outfilename = file.replace('.txt','.csv')
        outfile = open(outfilename, 'w')
        data = open(file, 'r').read().split('\n')
        current_apple = 1
        current_score = 0
        outfile.writelines(str(current_apple) + "," + str(current_score) + "\n")
        #print (current_apple, ",", current_score)
        for x in range(4, len(data), 2):
            line = data[x]
            if line == "0":
                current_apple += 1
                #print (current_apple, ",", current_score)
                outfile.writelines(str(current_apple) + "," + str(current_score) + "\n")
            elif line == "2":
                current_score += 1
        outfile.close()

#genreports('C:\\Repo\\vcpp\\Snake\\Test\\opening\\*.txt')
#genreports('C:\\Repo\\vcpp\\Snake\\Test\\mg_new\\*.txt')
#genreports('C:\\Repo\\vcpp\\Snake\\Test\\mg_old\\*.txt')
genreports('C:\\Repo\\vcpp\\Snake\\Test\\eg\\*.txt')
