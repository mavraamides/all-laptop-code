#include "Board.h"
#include <iostream>
#include <fstream> 
#include <sstream>
#include <iomanip>
#include <chrono>

using namespace std::chrono;
const auto max_table_size = 900u;
const auto trace = false;
static int flood_fill_apple_dist[max_table_size];
static int flood_fill_table[max_table_size];
static int flood_fill_table_b[max_table_size];
static int fall_back = 1;
static int turns_since_fall_back = 0;

static bool flood_fill_blocked[max_table_size];
static bool flood_fill_blocked_b[max_table_size];

Board::Board()
{
	width = 0;
	height = 0;
	screen_width = 0;
	screen_height = 0;

	if (!font.loadFromFile("zai_CourierPolski1941.ttf"))
	{
		cout << "Unable to load font." << endl;
	}
}

Board::~Board()
{
}

void Board::Initialize(unsigned int w, unsigned int h, unsigned int sw, unsigned int sh)
{
	width = w;
	height = h;
	screen_width = sw;
	screen_height = sh;
	apple_pos.x = 0;
	apple_pos.y = 0;
	auto_mode = false;

	//flood_fill_table.clear();
	for (auto x = 0u; x < w * h; x++) flood_fill_table[x] = -1;
	for (auto x = 0u; x < w * h; x++) flood_fill_table_b[x] = -1;
	//flood_fill_blocked.clear();
	for (auto x = 0u; x < w * h; x++) flood_fill_blocked[x] = false;
	for (auto x = 0u; x < w * h; x++) flood_fill_blocked_b[x] = false;

	Reset();
}

void Board::Reset()
{
	snake_dead = false;
	game_won = false;
	moves_this_game = 0;
	game_log.clear();
	game_log_index = 0;
	replay_mode = false;

	snake.SetRandomPosition(width, height);
	if (!replay_mode) game_log.push_back(log_entry{ log_event::create_snake, snake.GetBody()[0] });

	backup_snakes.clear();
	AddSnakeToBackup();
	SetApplePosition();
}

void Board::Draw(sf::RenderWindow& window)
{
	auto padding = 20u;
	auto max_cell_size_w = (screen_width - 2 * padding) / width;
	auto max_cell_size_h = (screen_height - 2 * padding) / height;
	auto cell_size = max_cell_size_h > max_cell_size_w ? max_cell_size_w : max_cell_size_h;

	DrawBoard(cell_size, padding, window);
	DrawApple(cell_size, padding, window);
	DrawSnake(cell_size, padding, window);
	if (cell_size > 50)
	{
		DrawFloodfill(padding, cell_size, window);
	}
}

void Board::DrawFloodfill(unsigned int padding, unsigned int cell_size, sf::RenderWindow& window)
{
	sf::Text text;
	text.setFont(font);
	text.setString("X");
	text.setCharacterSize(14);
	text.setFillColor(sf::Color::White);

	for (auto x = 0; (x < width); x++)
	{
		for (auto y = 0; (y < height); y++)
		{
			//text.setString(to_string(flood_fill_table[CoordToIndex(coord{ x, y })]));
			text.setString(to_string(CoordToIndex(coord {x, y })));
			text.setPosition((float)(padding + cell_size * x), (float)(padding + cell_size * y));
			window.draw(text);
		}
	}
}

void Board::DrawSnake(unsigned int cell_size, unsigned int padding, sf::RenderWindow& window)
{
	auto snake_padding = 6;
	auto snakesize = cell_size - snake_padding;

	sf::CircleShape snakehead((float)(snakesize / 2));
	sf::CircleShape snakebody((float)snakesize / 2.5f);
	snakebody.setOrigin((float)snakesize / 2.5f, (float)snakesize / 2.5f);
	sf::RectangleShape snakesfillH(sf::Vector2f((float)(snakesize / 1.1f), (float)(snakesize / 1.3f)));
	sf::RectangleShape snakesfillV(sf::Vector2f((float)(snakesize / 1.3f), (float)(snakesize / 1.1f)));
	snakesfillH.setOrigin((float)(snakesize / 2.2f), (float)(snakesize / 2.6f));
	snakesfillV.setOrigin((float)(snakesize / 2.6f), (float)(snakesize / 2.2f));
	sf::CircleShape autodot(3.0f);
	autodot.setOrigin(3.0f, 3.0f);
	if (replay_mode)
	{
		autodot.setFillColor(sf::Color::Black);
	}
	if (auto_mode && GetSearchStage() == search_stage::middlegame)
	{
		autodot.setFillColor(sf::Color::Yellow);
	}
	else if (auto_mode && GetSearchStage() == search_stage::endgame)
	{
		autodot.setFillColor(sf::Color::Blue);
	}
	auto snake_body = snake.GetBody();
	if (snake_dead)
	{
		snakehead.setFillColor(sf::Color::Yellow);
		snakebody.setFillColor(sf::Color::Yellow);
		snakesfillH.setFillColor(sf::Color::Yellow);
		snakesfillV.setFillColor(sf::Color::Yellow);
	}
	else if (game_won)
	{
		snakehead.setFillColor(sf::Color::Cyan);
		snakebody.setFillColor(sf::Color::Cyan);
		snakesfillH.setFillColor(sf::Color::Cyan);
		snakesfillV.setFillColor(sf::Color::Cyan);
	}
	else
	{
		snakehead.setFillColor(sf::Color::Green);
		snakebody.setFillColor(sf::Color::Green);
		snakesfillH.setFillColor(sf::Color::Green);
		snakesfillV.setFillColor(sf::Color::Green);
	}

	for (auto x = 0u; x < snake_body.size() - 1; x++)
	{
		//rectangle.setPosition((float)(padding + x * cell_size), (float)(padding + y * cell_size));
		auto x1pos = padding + snake_body[x].x * cell_size + cell_size / 2;
		auto y1pos = padding + snake_body[x].y * cell_size + cell_size / 2;
		auto x2pos = padding + snake_body[x + 1].x * cell_size + cell_size / 2;
		auto y2pos = padding + snake_body[x + 1].y * cell_size + cell_size / 2;
		if (snake_body[x].y == snake_body[x + 1].y)
		{
			snakesfillH.setPosition((float)(x1pos + x2pos) / 2.0f, (float)(y1pos + y2pos) / 2.0f);
			window.draw(snakesfillH);
		}
		else
		{
			snakesfillV.setPosition((float)(x1pos + x2pos) / 2.0f, (float)(y1pos + y2pos) / 2.0f);
			window.draw(snakesfillV);
		}
	}
	bool head = true;
	for (auto part : snake_body)
	{
		if (head)
		{
			snakehead.setPosition((float)(padding + snake_padding / 2 + part.x * cell_size), (float)(padding + snake_padding / 2 + part.y * cell_size));
			window.draw(snakehead);
		}
		else
		{
			auto x1pos = padding + part.x * cell_size + cell_size / 2;
			auto y1pos = padding + part.y * cell_size + cell_size / 2;
			snakebody.setPosition((float)(x1pos), (float)(y1pos));
			window.draw(snakebody);
		}
		if (auto_mode || replay_mode)
		{
			auto x1pos = padding + part.x * cell_size + cell_size / 2;
			auto y1pos = padding + part.y * cell_size + cell_size / 2;
			autodot.setPosition((float)(x1pos), (float)(y1pos));
			window.draw(autodot);
		}
		head = false;
	}
}

bool Board::SetApplePosition()
{
	SetOccupied();
	auto snake_body = snake.GetBody();
	auto number_of_open_spaces = width * height - (unsigned int) snake_body.size();

	if (number_of_open_spaces == 0)
	{
		return true;
	}
	auto position_choice = rand() % number_of_open_spaces;

	auto current_pos = 0u;
	auto done = false;
	for (auto x = 0; (x < width) & !done; x++)
	{
		for (auto y = 0; (y < height) &!done; y++)
		{
			if (occupied.count(CoordToIndex(coord{ x, y })) == 0)
			{
				if (current_pos == position_choice)
				{
					apple_pos.x = x;
					apple_pos.y = y;
					done = true;
					break;
				}
				current_pos++;
			}
		}
	}
	if (!replay_mode) game_log.push_back(log_entry{ log_event::create_apple, apple_pos });
	return false;
}

void Board::DrawApple(unsigned int cell_size, unsigned int padding, sf::RenderWindow& window)
{
	auto apple_padding = 10u;
	sf::CircleShape apple((float)((cell_size - apple_padding) / 2));
	apple.setPosition((float)(padding + apple_padding / 2 + apple_pos.x * cell_size), (float)(padding + apple_padding / 2 + apple_pos.y * cell_size));
	apple.setFillColor(sf::Color::Red);

	window.draw(apple);
}

void Board::DrawBoard(unsigned int cell_size, unsigned int padding, sf::RenderWindow& window)
{
	sf::RectangleShape rectangle(sf::Vector2f((float)cell_size, (float)cell_size));
	rectangle.setOutlineColor(sf::Color::Black);
	rectangle.setOutlineThickness(1.0f);

	set<int> master_path_set;
	if (!replay_mode && master_path.size() > 0)
	{
		for (auto p : master_path) master_path_set.insert(CoordToIndex(p));
	}

	for (auto x = 0; x < width; x++)
	{
		for (auto y = 0; y < height; y++)
		{
			if (master_path_set.count(CoordToIndex(coord{ x, y })) > 0)
			{
				rectangle.setFillColor(sf::Color(150u, 0u, 0u, 255u));
			}
			else
			{
				rectangle.setFillColor(sf::Color(50u, 50u, 50u, 255u));
			}
			rectangle.setPosition((float)(padding + x * cell_size), (float)(padding + y * cell_size));

			window.draw(rectangle);
		}
	}

	sf::Text text;
	text.setFont(font);
	text.setCharacterSize(14);
	text.setFillColor(sf::Color::White);

	auto nl = 14.0f;
	auto linepos = sf::Vector2f((float)(padding), (float)(padding + height * cell_size));
	text.setPosition(linepos);

	string col1 = "Turns: ";
	col1 += to_string(moves_this_game);
	text.setString(col1);
	window.draw(text);

	linepos.y += nl;
	text.setPosition(linepos);
	col1 = "Score: ";
	col1 += to_string(snake.GetBody().size());
	col1 += " of ";
	col1 += to_string(width * height);
	text.setString(col1);
	window.draw(text);

	linepos.y += nl;
	text.setPosition(linepos);
	col1 = "Projected: ";
	col1 += to_string((moves_this_game * width * height) / snake.GetBody().size());
	text.setString(col1);
	window.draw(text);

	//linepos = sf::Vector2f((float)(padding) + 200.0f, (float)(padding + height * cell_size));
	//text.setPosition(linepos);
	//col1 = "Distance to Tail: ";
	//col1 += to_string(GetDistanceToTail());
	//text.setString(col1);
	//window.draw(text);

	//auto turns_till_tail_visible = 0;
	//auto path_length = 0;
	//GetTurnsTillTailVisibleAndPathLength(turns_till_tail_visible, path_length);

	//linepos.y += nl;
	//text.setPosition(linepos);
	//col1 = "Turns Tail Vis: ";
	//col1 += to_string(turns_till_tail_visible);
	//text.setString(col1);
	//window.draw(text);

	//linepos.y += nl;
	//text.setPosition(linepos);
	//col1 = "Distance from Tail: ";
	//col1 += to_string(path_length);
	//text.setString(col1);
	//window.draw(text);

	//linepos.y += nl;
	//text.setPosition(linepos);
	//col1 = "Cost to Follow Tail: ";
	//col1 += to_string(TotalDistanceToFollowTail());
	//text.setString(col1);
	//window.draw(text);

	//linepos = sf::Vector2f((float)(padding)+400.0f, (float)(padding + height * cell_size));
	//text.setPosition(linepos);
	//col1 = "Half Space: ";
	//col1 += to_string(GetHalfOpenSpace());
	//text.setString(col1);
	//window.draw(text);

	//linepos.y += nl;
	//text.setPosition(linepos);
	//col1 = EndGame() ? "END GMME" : AvoidTail() ? "AVOID TAIL" : "FOLLOW TAIL";
	//text.setString(col1);
	//window.draw(text);

	//snake_path path;
	//linepos = sf::Vector2f((float)(padding)+600.0f, (float)(padding + height * cell_size));
	//text.setPosition(linepos);
	//col1 = "Expected Cost: ";
	//col1 += to_string(CalcHoleScore(path));
	//text.setString(col1);
	//window.draw(text);

}

void Board::SetOccupied()
{
	occupied.clear();
	auto snake_body = snake.GetBody();
	for (auto part : snake_body)
	{
		occupied.insert(CoordToIndex(part));
	}
}

int Board::CoordToIndex(coord c)
{
	if (!IsValidCoord(c)) return -1;

	return c.y * width + c.x;
}

coord Board::IndexToCoord(int ndx)
{
	coord result(-1, -1);

	if (IsValidIndex(ndx))
	{
		result.x = ndx % width;
		result.y = ndx / width;
	}
	return result;
}

bool Board::IsValidCoord(coord c)
{
	return (c.x >= 0 && c.x < (int)width && c.y >= 0 && c.y < (int)height);
}

bool Board::IsValidIndex(int ndx)
{
	return (ndx >= 0 && ndx < (int)(width * height));
}

coord Board::GetNeighborFill(coord c, SnakeFacingDirection d)
{
	coord testc({ -1, -1 });

	if (IsValidCoord(c))
	{
		switch (d)
		{
		case SnakeFacingDirection::up:
			if (c.x % 2 == 1)
			{
				testc.x = c.x;
				testc.y = c.y - 1;
			}
			break;
		case SnakeFacingDirection::right:
			if (c.y % 2 == 1)
			{
				testc.x = c.x + 1;
				testc.y = c.y;
			}
			break;
		case SnakeFacingDirection::down:
			if (c.x % 2 == 0)
			{
				testc.x = c.x;
				testc.y = c.y + 1;
			}
			break;
		case SnakeFacingDirection::left:
			if (c.y % 2 == 0)
			{
				testc.x = c.x - 1;
				testc.y = c.y;
			}
			break;
		}
	}

	return testc;
}

coord Board::GetNeighborFollow(coord c, SnakeFacingDirection d)
{
	coord testc({ -1, -1 });

	if (IsValidCoord(c))
	{
		switch (d)
		{
		case SnakeFacingDirection::up:
			if (c.x % 2 == 0)
			{
				testc.x = c.x;
				testc.y = c.y - 1;
			}
			break;
		case SnakeFacingDirection::right:
			if (c.y % 2 == 0)
			{
				testc.x = c.x + 1;
				testc.y = c.y;
			}
			break;
		case SnakeFacingDirection::down:
			if (c.x % 2 == 1)
			{
				testc.x = c.x;
				testc.y = c.y + 1;
			}
			break;
		case SnakeFacingDirection::left:
			if (c.y % 2 == 1)
			{
				testc.x = c.x - 1;
				testc.y = c.y;
			}
			break;
		}
	}

	return testc;
}

void Board::InitializeFloodFill(snake_path proposed_path)
{
	memcpy(flood_fill_table, flood_fill_table_b, sizeof(flood_fill_table));
	memcpy(flood_fill_blocked, flood_fill_blocked_b, sizeof(flood_fill_blocked));
	//for (auto x = 0u; x < width * height; x++) flood_fill_table[x] = -1;
	//for (auto x = 0u; x < width * height; x++) flood_fill_blocked[x] = false;
	auto snake_body = snake.GetBody();
	for (auto part : proposed_path) flood_fill_blocked[CoordToIndex(part)] = true;
	int length_of_snake_to_block = (int)snake_body.size() - (int)proposed_path.size() + 1;
	for (auto x = 0; x < length_of_snake_to_block && x < (int)snake_body.size(); x++) flood_fill_blocked[CoordToIndex(snake_body[x])] = true;
	

}
void Board::FloodFill(coord target, snake_path proposed_path, bool reverse, int add_on, bool initialize)
{
	if (initialize)
	{
		InitializeFloodFill(proposed_path);
	}

	vector<SnakeFacingDirection> direction;
	direction = { SnakeFacingDirection::left, SnakeFacingDirection::down, SnakeFacingDirection::right, SnakeFacingDirection::up };

	snake_path open;
	snake_path new_open;

	flood_fill_table[CoordToIndex(target)] = add_on;
	open.push_back(target);

	while (open.size() > 0)
	{
		new_open.clear();
		for (auto v : open)
		{
			auto distance = flood_fill_table[CoordToIndex(v)];
			distance++;
			for (auto d : direction)
			{
				auto neighbor = GetNeighborFill(v, d);
				if (reverse) neighbor = GetNeighborFollow(v, d);
				if (IsValidCoord(neighbor) && 
					flood_fill_table[CoordToIndex(neighbor)] == -1 && 
					flood_fill_blocked[CoordToIndex(neighbor)] == false)
				{
					flood_fill_table[CoordToIndex(neighbor)] = distance;
					new_open.push_back(neighbor);
				}
			}
		}
		open.clear();
		open = new_open;
	}
}

string Board::SnakePathToId(snake_path path)
{
	string id;
	stringstream stream;
	for (auto c : path)
	{
		stream << setfill('0') << setw(2) << CoordToIndex(c);
	}
	id += stream.str();
	return id;
}

snake_path Board::IdToSnakePath(string id)
{
	if (trace) cout << id << endl;
	for (auto x = 0; x < id.size(); x += 2)
	{
		auto ss = id.substr(x, 2);
		if (trace) cout << stoi(ss) << " ";
	}
	if (trace) cout << endl;
	return snake_path();
}

void Board::DumpFloodFill(string label)
{
	cout << "Dump Flood Fill: " << label << endl;
	stringstream stream;
	for (auto y = 0; y < (int)width; y++)
	{
		for (auto x = 0; x < (int)width; x++)
		{
			if (flood_fill_blocked[CoordToIndex(coord{ x,y })])
			{
				stream << " X ";
			}
			else
			{
				stream << setw(3) << flood_fill_table[CoordToIndex(coord{ x,y })];
			}
		}
		stream << endl;
	}
	cout << stream.str() << endl;
}

snake_path Board::GetPathFrom(coord from, bool runaway, bool reverse, int max_length)
{
	snake_path path;
	coord current_cell(from);
	coord next_cell(from);
	vector<SnakeFacingDirection> direction;
	if (reverse)
	{
		direction = { SnakeFacingDirection::left, SnakeFacingDirection::down, SnakeFacingDirection::right, SnakeFacingDirection::up };
	}
	else
	{
		direction = { SnakeFacingDirection::up, SnakeFacingDirection::right, SnakeFacingDirection::down, SnakeFacingDirection::left };
	}

	auto found_cell = false;
	do
	{
		auto best_direction_score = flood_fill_table[CoordToIndex(current_cell)];
		if (flood_fill_blocked[CoordToIndex(current_cell)])
		{
			if (runaway)
			{
				best_direction_score = -1000;
			}
			else
			{
				best_direction_score = 1000;
			}
		}
		found_cell = false;

		SnakeFacingDirection best_direction = SnakeFacingDirection::none;
		for (auto d: direction)
		{
			auto test_pos = GetNeighborFollow(current_cell, d);
			if (IsValidCoord(test_pos))
			{
				auto score = flood_fill_table[CoordToIndex(test_pos)];
				auto blocked = flood_fill_blocked[CoordToIndex(test_pos)];
				if (runaway)
				{

					if (!blocked && score >= 0 && score > best_direction_score)
					{
						best_direction_score = score;
						next_cell.x = test_pos.x;
						next_cell.y = test_pos.y;
						found_cell = true;
					}
				}
				else
				{
					if (!blocked && score >= 0 && score < best_direction_score)
					{
						best_direction_score = score;
						next_cell.x = test_pos.x;
						next_cell.y = test_pos.y;
						found_cell = true;
					}
				}
			}
		}
		if (found_cell) path.push_back(next_cell);
		if (path.size() >= max_length) break;
		current_cell = next_cell;

	} while (found_cell);


	return path;
}


SnakeFacingDirection Board::GetAutoMove()
{
	snake_path best_path;
	snake_path test_path;
	snake_path vpath;
	auto snake_body = snake.GetBody();

	if (master_path.size() > 0)
	{
		best_path = master_path;
		master_path.erase(master_path.begin());
	}
	else
	{
		FloodFill(apple_pos, vpath);
		memcpy(flood_fill_apple_dist, flood_fill_table, sizeof(flood_fill_table));

		auto test_score = 10000000.0f;


		best_path = FindBestPath(apple_pos);
		if (best_path.size() > 0)
		{
			// special case where snake can get stuck
			if (best_path.size() == 1)
			{
				FloodFill(snake_body[snake_body.size() - 1], vpath);
				vpath = GetPathFrom(snake_body[0]);
				if (vpath.size() > 0 && vpath[0].x == apple_pos.x && vpath[0].y == apple_pos.y)
				{
					if (GetDistanceToTail() == 1)
					{
						best_path.clear();
						FloodFill(snake_body[snake_body.size() - 1], best_path);
						best_path = GetPathFrom(snake_body[0], true);
					}
				}
			}
			test_score = CalcHoleScore(best_path);
		}

		if (GetSearchStage() == search_stage::opening)
		{
			if (best_path.size() == 0)
			{
				FloodFill(snake_body[snake_body.size() - 1], best_path);
				best_path = GetPathFrom(snake_body[0], false);
			}
		}
		else
		{
			FloodFill(snake_body[snake_body.size() - 1], test_path);
			test_path = GetPathFrom(snake_body[0], true);
			if (CalcHoleScore(test_path) < test_score)
			{
				best_path = test_path;

				if (best_path.size() > 0)
				{
					auto apple_dist = flood_fill_apple_dist[CoordToIndex(best_path[best_path.size() - 1])];

					test_path.clear();
					FloodFill(snake_body[snake_body.size() - 1], test_path);
					test_path = GetPathFrom(snake_body[0], true, true);
					if (CalcHoleScore(test_path) && flood_fill_apple_dist[CoordToIndex(test_path[test_path.size() - 1])] < apple_dist)
					{
						cout << "Using reverse runaway." << endl;
						best_path = test_path;
					}
				}
			}
			else
			{
				fall_back = 1;
				turns_since_fall_back = 0;
			}
		}
	}
	//master_path = best_path;

	//if (best_path.size() == 0) best_path = FindBestPath(snake_body[snake_body.size() - 1], true);

	auto result = SnakeFacingDirection::none;



	//snake_path path;
	//snake_path vpath;



	//auto current_cost = CalcHoleScore(path);
	//CostOfMoveToApple = 1000000000.0f;
	//CostOfFollowTail = 1000000000.0f;
	//CostOfRunFromTail = 1000000000.0f;

	////if (!EndGame())
	////{
	//	FloodFill(apple_pos.x, apple_pos.y, path);
	//	path = GetPathFrom(snake_body[0].x, snake_body[0].y);
	//	// special case where we are 'right on the tail'

	////}

	//if (path.size() > 0)
	//{
	//	vpath.clear();
	//	vpath.push_back(path[0]);
	//	CostOfMoveToApple = CalcHoleScore(vpath);
	//	if (CostOfMoveToApple <= current_cost)
	//	{
	//		best_path = path;
	//	}
	//}

	//if (best_path.size() == 0)
	//{
	//	path.clear();
	//	FloodFill(snake_body[snake_body.size() - 1].x, snake_body[snake_body.size() - 1].y, path);
	//	path = GetPathFrom(snake_body[0].x, snake_body[0].y, true);

	//	if (path.size() > 0)
	//	{
	//		vpath.clear();
	//		vpath.push_back(path[0]);
	//		CostOfRunFromTail = CalcHoleScore(vpath);
	//		if (CostOfRunFromTail < current_cost)
	//		{
	//			best_path = path;
	//		}
	//	}
	//}

	//if (best_path.size() == 0)
	//{
	//	best_path.clear();
	//	FloodFill(snake_body[snake_body.size() - 1], path);
	//	path = GetPathFrom(snake_body[0].x, snake_body[0].y);

	//	if (path.size() > 0)
	//	{
	//		CostOfFollowTail = CalcHoleScore(path);
	//		best_path = path;
	//	}
	//}

	if (best_path.size() > 0)
	{
		auto path_ndx = CoordToIndex(best_path[0]);
		result = GetFacingForCoord(snake_body[0], path_ndx);
	}

	//auto_mode = false;
	return result;
}

SnakeFacingDirection Board::GetFacingForCoord(coord head, int path_ndx)
{
	SnakeFacingDirection result = SnakeFacingDirection::none;
	vector<SnakeFacingDirection> direction;
	direction = { SnakeFacingDirection::left, SnakeFacingDirection::down, SnakeFacingDirection::right, SnakeFacingDirection::up };
	for (auto d : direction)
	{
		auto test_pos = snake.GetPositionOfDirection((SnakeFacingDirection)d, head.x, head.y);
		if (IsValidCoord(test_pos))
		{
			if (CoordToIndex(test_pos) == path_ndx)
			{
				result = (SnakeFacingDirection)d;
				break;
			}
		}
	}
	return result;
}

snake_path Board::FindBestPath(coord target)
{
	InitializeFloodFill(snake_path());
	auto starting_score = CalcHoleScore(snake_path());

	snake_path best_path;
	auto snake_body = snake.GetBody();

	FloodFill(target, best_path);
	best_path = GetPathFrom(snake_body[0]);
	auto best_score = CalcHoleScore(best_path);

	if (best_score > starting_score)
	{
		FloodFill(target, snake_path());
		auto test_path = GetPathFrom(snake_body[0], false, true);
		if (test_path.size() > 0)
		{
			auto test_score = CalcHoleScore(test_path);
			if (test_score < best_score)
			{
				cout << "Using reverse: " << test_score << " < " << best_score << endl;
				best_score = test_score;
				best_path = test_path;
			}
		}
	}

	if (GetSearchStage() == search_stage::middlegame && best_score > starting_score)
	{
		if (fall_back < turns_since_fall_back)
		{
			auto test_path = SearchAllPaths(target, starting_score, (int)best_path.size());
			if (test_path.size() > 0)
			{
				auto test_score = CalcHoleScore(test_path);
				if (test_score < best_score)
				{
					best_path = test_path;
					master_path = best_path;
					cout << "Using SearchAllPaths: " << test_score << " < " << best_score << endl;
				}
			}
			turns_since_fall_back = 0;
			if (fall_back < 64) fall_back *= 2;
		}
		else
		{
			turns_since_fall_back++;
		}
	}
		
	return best_path;
}

snake_path Board::SearchAllPaths(coord target, float target_score, int distance_to_target)
{
	auto direction = { SnakeFacingDirection::left, SnakeFacingDirection::down, SnakeFacingDirection::right, SnakeFacingDirection::up };
	vector<snake_path> open_paths;
	vector<snake_path> new_paths;
	snake_path best_path;
	float best_score = 1000000.0f;

	auto max_time = 0.05f * float(fall_back);
	auto longest_path_tested = 0;
	auto best_apple_dist = 10000;

	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	open_paths.push_back(snake_path({ snake.GetBody()[0] }));
	while (open_paths.size() > 0 && (best_score > target_score || GetSearchStage() == search_stage::middlegame))
	{
		for (auto path : open_paths)
		{
			if (path.size() == 0) continue;
			InitializeFloodFill(path);
			for (auto d : direction)
			{
				auto c = GetNeighborFollow(path[path.size() - 1], d);
				if (!IsValidCoord(c)) continue;

				auto ndx = CoordToIndex(c);
				if (flood_fill_blocked[ndx]) continue;

				auto test_path = path;
				test_path.push_back(c);
				if (test_path.size() > longest_path_tested) longest_path_tested = test_path.size();

				if (trace) cout << SnakePathToId(test_path) << endl;
				auto test_score = CalcHoleScore(test_path);
				if (test_score > best_score) continue;

				if (GetSearchStage() == search_stage::middlegame)
				{
					if (test_score < best_score)
					{
						best_score = test_score;
						best_path = test_path;
						best_apple_dist = 10000;
					}
					else
					{
						auto apple_dist = flood_fill_apple_dist[CoordToIndex(test_path[test_path.size() - 1])];
						if (apple_dist < best_apple_dist)
						{
							cout << "New best apple score. " << apple_dist << " < " << best_apple_dist << endl;
							best_score = test_score;
							best_path = test_path;
							best_apple_dist = apple_dist;
						}
					}
				}

				if (ndx == CoordToIndex(target))
				{
					if (trace) cout << "Hit Target: " << test_score << endl;
					best_score = test_score;
					best_path = test_path;
					continue;
				}

				new_paths.push_back(test_path);
			}
		}
		open_paths = new_paths;
		high_resolution_clock::time_point t2 = high_resolution_clock::now();
		duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
		if (time_span.count() > max_time)
		{
			//cout << "Hit Threshhold. Target: " << target_score << " Best: " << best_score << endl;
			cout << "Longest path tested: " << longest_path_tested;
			cout << " Distance to target: " << distance_to_target;
			auto wait_turns = distance_to_target > longest_path_tested ? distance_to_target - longest_path_tested : longest_path_tested;
			cout << " Wait turns: " << wait_turns << endl;
			break;
		}
		new_paths.clear();
	}

	if (best_path.size() > 0) best_path.erase(best_path.begin());
	return best_path;
}

unsigned int Board::GetNumberOfUncheckedCells()
{
	auto unchecked_cells = 0u;

	for (auto x = 0u; x < (width * height); x++)
	{
		if (flood_fill_blocked[x]) continue;
		if (flood_fill_table[x] == -1) unchecked_cells++;
	}

	return unchecked_cells;
}

void Board::MoveSnake(SnakeFacingDirection dir)
{
	
	if (auto_mode) dir = GetAutoMove();
	if (snake_dead || game_won || dir == SnakeFacingDirection::none) return;
	if (snake.SelfCollision(dir)) {
		//snake_dead = true;
		return;
	}
	auto next_pos = snake.GetNextHeadPosition(dir);
	if (next_pos.x < 0 || next_pos.x >= (int) width || next_pos.y < 0 || next_pos.y >= (int) height)
	{
		//snake_dead = true;
		return;
	}
	if (!replay_mode) game_log.push_back(log_entry{ log_event::move_snake, next_pos });

	if (next_pos.x == apple_pos.x && next_pos.y == apple_pos.y)
	{
		snake.Grow(dir);
		if (!replay_mode) game_won = SetApplePosition();
	}
	else
	{
		snake.Move(dir);
	}

	AddSnakeToBackup();
	moves_this_game++;
}

void Board::WriteGameLog()
{
	WriteGameLogByName(logfilename);
	auto moves = 0u;
	for (auto le : game_log)
	{
		if (le.le == log_event::move_snake) moves++;
	}
	WriteGameLogByName(GetSaveName(moves, ".txt"));
}

string Board::GetSaveName(unsigned int moves, string extension)
{
	stringstream savename;

	savename << width << "_" << height << " _" << moves;
	savename << "_" << duration_cast<seconds>(system_clock::now().time_since_epoch()).count() << extension;

	return savename.str();
}

void Board::WriteGameReport()
{
	auto apple_count = 0u;
	auto moves = 0u;
	auto current_move = 0;

	for (auto le : game_log)
	{
		switch (le.le)
		{
		case log_event::move_snake:
			moves++;
			break;
		}
	}

	ofstream fileSink(GetSaveName(moves, ".csv"));

	if (!fileSink) {
		cerr << "Canot open " << GetSaveName(moves, ".csv") << endl;
		return;
	}

	for (auto le : game_log)
	{
		switch (le.le)
		{
		case log_event::create_apple:
			apple_count++;
			fileSink << apple_count;
			fileSink << ", ";
			fileSink << current_move;
			fileSink << endl;
			break;
		case log_event::move_snake:
			current_move++;
			break;
		}
	}

	fileSink.close();

}

void Board::WriteGameLogByName(string name)
{
	ofstream fileSink(name);

	if (!fileSink) {
		cerr << "Canot open " << name << endl;
		return;
	}

	fileSink << width << endl;
	fileSink << height << endl;

	for (auto le : game_log)
	{
		fileSink << (int)(le.le) << endl;
		fileSink << CoordToIndex(le.c) << endl;
	}

	fileSink.close();
}

void Board::ReadGameLog()
{
	ifstream fileSource(logfilename); // Creates an input file stream

	if (!fileSource) {
		cerr << "Canot open " << logfilename << endl;
		return;
	}
	else {
		// Intermediate buffer
		string buffer;
		game_log.clear();

		auto w = 0u;
		auto h = 0u;

		fileSource >> w;
		fileSource >> h;

		Initialize(w, h, screen_width, screen_height);
		
		int entry;
		int ndx;
		while (!fileSource.eof())
		{
			fileSource >> entry;
			fileSource >> ndx;
			switch (entry)
			{
			case (int)log_event::create_apple:
				game_log.push_back(log_entry({ log_event::create_apple, IndexToCoord(ndx) }));
				break;
			case (int)log_event::create_snake:
				game_log.push_back(log_entry({ log_event::create_snake, IndexToCoord(ndx) }));
				break;
			case (int)log_event::move_snake:
				game_log.push_back(log_entry({ log_event::move_snake, IndexToCoord(ndx) }));
				break;
			}

		}
	}

	fileSource.close();
}

bool Board::PlayGameLog()
{
	if (game_log_index == 0)
	{
		snake_dead = false;
		game_won = false;
		moves_this_game = 0;
		replay_mode = true;
	}
	snake_path path;

	if (game_log_index >= 0 && game_log_index < game_log.size())
	{
		auto le = game_log[game_log_index];

		switch (le.le)
		{
		case log_event::create_apple:
			apple_pos = le.c;
			break;
		case log_event::create_snake:
			path.push_back(le.c);
			snake.Set(path);
			backup_snakes.clear();
			AddSnakeToBackup();
			break;
		case log_event::move_snake:
			MoveSnake(GetFacingForCoord(snake.GetBody()[0], CoordToIndex(le.c)));
			break;
		}
		game_log_index++;
		return true;
	}
	else
	{
		game_won = true;
		return true;
	}
}


void Board::Save()
{
	ofstream fileSink(savefilename); 

	if (!fileSink) {
		cerr << "Canot open " << savefilename << endl;
		return;
	}

	fileSink << width << endl;
	fileSink << height << endl;
	fileSink << apple_pos.x << endl;
	fileSink << apple_pos.y << endl;

	auto snake_body = snake.GetBody();

	fileSink << snake_body.size() << endl;
	for (auto p : snake_body)
	{
		fileSink << p.x << endl;
		fileSink << p.y << endl;
	}

	fileSink.close();
}

void Board::Load()
{
	ifstream fileSource(savefilename); // Creates an input file stream

	if (!fileSource) {
		cerr << "Canot open " << savefilename << endl;
		return;
	}
	else {
		// Intermediate buffer
		string buffer;

		// By default, the >> operator reads word by workd (till whitespace)
		auto w = 0u;
		auto h = 0u;

		fileSource >> w;
		fileSource >> h;

		Initialize(w, h, screen_width, screen_height);

		fileSource >> apple_pos.x;
		fileSource >> apple_pos.y;

		auto snake_size = 0u;
		auto b_x = 0;
		auto b_y = 0;
		snake_path b;

		fileSource >> snake_size;
		for (auto x = 0u; x < snake_size; x++)
		{
			fileSource >> b_x;
			fileSource >> b_y;
			b.push_back(coord(b_x, b_y));
		}
		snake.Set(b);
	}

	fileSource.close();
	backup_snakes.clear();
	AddSnakeToBackup();
}

bool Board::AvoidTail()
{
	return ((int)GetHalfOpenSpace() < TotalDistanceToFollowTail());
}

search_stage Board::GetSearchStage()
{
	//if (snake.GetBody().size() < (width * height) * 35 / 90) return search_stage::opening;
	if (snake.GetBody().size() < (width * height) * 26 / 90) return search_stage::opening;
	//if (snake.GetBody().size() < (width * height) * 126 / 900) return search_stage::opening;
	if (snake.GetBody().size() < (width * height) * 62 / 90) return search_stage::middlegame;
	//return search_stage::opening;
	//return search_stage::middlegame;
	return search_stage::endgame;
}

bool Board::EndGame()
{
	//auto remaining_space = width * height - (unsigned int)snake.GetBody().size();
	//auto total_space = width * height;
	//return remaining_space < total_space / 3;
	//return 
	return true;
}

unsigned int Board::GetHalfOpenSpace()
{
	return (width * height - (unsigned int) snake.GetBody().size()) / 2;
}

int Board::GetDistanceToTail()
{
	auto snake_body = snake.GetBody();
	snake_path path;
	FloodFill(snake_body[snake_body.size() - 1], path);
	path = GetPathFrom(snake_body[0], false, false, 1);
	if (path.size() > 0) return flood_fill_table[CoordToIndex(path[0])];
	return -1;
}

void Board::GetTurnsTillTailVisibleAndPathLength(int& turns, int& path_length)
{
	auto snake_body = snake.GetBody();
	snake_path path;
	FloodFill(apple_pos, path);
	turns = 0;
	for (int x = (int)(snake_body.size() - 1); x >= 0; x--)
	{
		path = GetPathFrom(snake_body[x], false, false, 1);
		if (path.size() > 0)
		{
			path_length = flood_fill_table[CoordToIndex(path[0])];
			break;
		}
		turns++;
	}
}

int Board::TotalDistanceToFollowTail()
{
	auto turns_till_tail_visible = 0;
	auto path_length = 0;
	GetTurnsTillTailVisibleAndPathLength(turns_till_tail_visible, path_length);
	return turns_till_tail_visible + path_length + GetDistanceToTail();
}

float Board::CalcHoleScore(snake_path path)
{
	auto total_cost = 0.0f;
	InitializeFloodFill(snake_path());
	if (trace) DumpFloodFill("CHS Before Path");

	auto starting_open_cells = GetNumberOfUncheckedCells();
	//for (auto c : path)
	//{
	//	if (!flood_fill_blocked[CoordToIndex(c)]) starting_open_cells--;
	//}
	auto orig_open_cells = starting_open_cells;
	auto snake_body = snake.GetBody();

	auto distance_to_tail = GetDistanceToTail();
	auto startinc_c = snake_body[0];
	if (path.size() > 0) startinc_c = path[path.size() - 1];
	FloodFill(startinc_c, path, true);
	if (trace) DumpFloodFill("CHS First Path");

	auto current_open_cells = GetNumberOfUncheckedCells();
	auto cell_size = starting_open_cells - current_open_cells;
	starting_open_cells = current_open_cells;

	if (cell_size > 0) total_cost += (float)(orig_open_cells) / float(cell_size);

	for (int x = snake_body.size() - 1; x >= 0 && current_open_cells > 0; x--)
	{
		FloodFill(snake_body[x], path, true, distance_to_tail, false);
		current_open_cells = GetNumberOfUncheckedCells();
		cell_size = starting_open_cells - current_open_cells;
		starting_open_cells = current_open_cells;

		if (cell_size > 0) total_cost += (float)(orig_open_cells) / float(cell_size);
	}

	return total_cost;
}

void Board::AddSnakeToBackup()
{
	if (backup_snakes.size() < max_backup_snake)
	{
		backup_snakes.push_back(backup_set{ apple_pos, snake.GetBody() });
		current_snake = (unsigned int)backup_snakes.size() - 1;
	}
	else
	{
		current_snake++;
		if (current_snake >= max_backup_snake) current_snake = 0;
		backup_snakes[current_snake] = backup_set{ apple_pos, snake.GetBody() };
	}
}

void Board::BackSnakeBackup()
{
	if (current_snake == 0) current_snake = (unsigned int)backup_snakes.size();
	current_snake--;
	snake.Set(backup_snakes[current_snake].snake);
	apple_pos = backup_snakes[current_snake].apple;
}

void Board::ForwardSnakeBackup()
{
	current_snake++;
	if (current_snake >= backup_snakes.size()) current_snake = 0;
	snake.Set(backup_snakes[current_snake].snake);
	apple_pos = backup_snakes[current_snake].apple;
}

//
//void Board::UpdateHolesAndBestPath(std::snake_path& path, int& holes, int& best_hole_count, std::snake_path& best_path)
//{
//	if (path.size() > 0)
//	{
//		holes = CountHoles(path);
//		if (holes < best_hole_count)
//		{
//			best_hole_count = holes;
//			best_path = path;
//		}
//	}
//}
//
//void Board::FindPathWithFewestHoles(int x, int y, snake_path& path, bool runaway, bool verify, int max_length)
//{
//	snake_path test_path;
//	GetPathUnverified(x, y, path, runaway, false, max_length);
//	if (verify) VerifyPath(path);
//	auto holes = CountHoles(path);
//
//	GetPathUnverified(x, y, test_path, runaway, true, max_length);
//	VerifyPath(test_path);
//	if (path.size() == 0 || 
//		(test_path.size() > 0 && CountHoles(test_path) < holes)) path = test_path;
//}
//
//void Board::GetPathUnverified(int fromx, int fromy, std::snake_path& path, bool runaway, bool reverse, int max_length)
//{
//	auto snake_body = snake.GetBody();
//
//	FloodFill(fromx, fromy, path);
//	path = GetPathFrom(snake_body[0].x, snake_body[0].y, runaway, reverse, max_length);
//}
//
//void Board::VerifyPath(std::snake_path& path)
//{
//	snake_path vpath;
//	auto snake_body = snake.GetBody();
//
//	if (path.size() > 0)
//	{
//		// Now verify that we could still get to tail after path
//		FloodFill(snake_body[snake_body.size() - 1].x, snake_body[snake_body.size() - 1].y, path);
//		vpath = GetPathFrom(path[path.size() - 1].x, path[path.size() - 1].y);
//		if (vpath.size() == 0) path.clear();
//	}
//}

//snake_path Board::GetPathFrom(int x, int y, bool runaway, bool reverse, int max_length)
//{
//	snake_path path;
//	coord current_cell(x, y);
//	coord next_cell(x, y);
//	vector<SnakeFacingDirection> direction;
//	if (reverse)
//	{
//		direction = { SnakeFacingDirection::left, SnakeFacingDirection::down, SnakeFacingDirection::right, SnakeFacingDirection::up };
//	}
//	else
//	{
//		direction = { SnakeFacingDirection::up, SnakeFacingDirection::right, SnakeFacingDirection::down, SnakeFacingDirection::left };
//	}
//
//	auto found_cell = false;
//	do
//	{
//		auto best_direction_score = flood_fill_table[CoordToIndex(current_cell.x, current_cell.y)];
//		if (flood_fill_blocked[CoordToIndex(current_cell.x, current_cell.y)])
//		{
//			if (runaway)
//			{
//				best_direction_score = -1000;
//			}
//			else
//			{
//				best_direction_score = 1000;
//			}
//		}
//		found_cell = false;
//
//		SnakeFacingDirection best_direction = SnakeFacingDirection::none;
//		for (auto d: direction)
//		{
//			auto test_pos = snake.GetPositionOfDirection(d, current_cell.x, current_cell.y);
//			if (test_pos.x >= 0 && test_pos.x < width && test_pos.y >= 0 && test_pos.y < height)
//			{
//				auto score = flood_fill_table[CoordToIndex(test_pos.x, test_pos.y)];
//				auto blocked = flood_fill_blocked[CoordToIndex(test_pos.x, test_pos.y)];
//				if (runaway)
//				{
//
//					if (!blocked && score >= 0 && score > best_direction_score)
//					{
//						best_direction_score = score;
//						next_cell.x = test_pos.x;
//						next_cell.y = test_pos.y;
//						found_cell = true;
//					}
//				}
//				else
//				{
//					if (!blocked && score >= 0 && score < best_direction_score)
//					{
//						best_direction_score = score;
//						next_cell.x = test_pos.x;
//						next_cell.y = test_pos.y;
//						found_cell = true;
//					}
//				}
//			}
//		}
//		if (found_cell) path.push_back(next_cell);
//		if (path.size() >= max_length) break;
//		current_cell = next_cell;
//
//	} while (found_cell);
//
//
//	return path;
//}
//
//bool Board::GetFloodFillBlockedValue(int x, int y)
//{
//	int returnValue = true;
//	if (x >= 0 && x < width && y >= 0 && y < height)
//	{
//		returnValue = flood_fill_blocked[CoordToIndex(x, y)];
//	}
//	return returnValue;
//}
//
//bool Board::GetNeighborFloodFillBlockedValue(int x, int y,SnakeFacingDirection d)
//{
//	int returnValue = true;
//	switch (d)
//	{
//	case SnakeFacingDirection::up:
//	returnValue = GetFloodFillBlockedValue(x, y - 1);
//	break;
//	case SnakeFacingDirection::right:
//	returnValue = GetFloodFillBlockedValue(x + 1, y);
//	break;
//	case SnakeFacingDirection::down:
//	returnValue = GetFloodFillBlockedValue(x, y + 1);
//	break;
//	case SnakeFacingDirection::left:
//	returnValue = GetFloodFillBlockedValue(x - 1, y);
//	break;
//	}
//	return returnValue;
//}

//unsigned int Board::CountHoles(snake_path path)
//{
//	unsigned int holes = 0;
//	vector<SnakeFacingDirection> direction;
//	direction = { SnakeFacingDirection::left, SnakeFacingDirection::down, SnakeFacingDirection::right, SnakeFacingDirection::up };
//	
//	for (auto x = 0u; x < flood_fill_table.size(); x++) flood_fill_table[x] = -1;
//	for (auto x = 0u; x < flood_fill_blocked.size(); x++) flood_fill_blocked[x] = false;
//	auto snake_body = snake.GetBody();
//	for (auto part : snake_body) flood_fill_blocked[CoordToIndex(part.x, part.y)] = true;
//	for (auto part : path) flood_fill_blocked[CoordToIndex(part.x, part.y)] = true;
//
//	for (auto x = 0u; x < width; x++)
//	{
//		for (auto y = 0u; y < height; y++)
//		{
//			if (GetFloodFillBlockedValue(x, y) == false)
//			{
//				unsigned int open_neighbors = 0;
//				for (auto d : direction)
//				{
//					if (GetNeighborFloodFillBlockedValue(x, y, d) == false) open_neighbors++;
//				}
//				if (open_neighbors < 2) holes++;
//			}
//		}
//	}
//	return holes;
//}
//
//void Board::FloodFillTestDirection(sf::Vector2u& v, int xoffset, int yoffset, int distance, vector<sf::Vector2u>& new_open)
//{
//
//	if (flood_fill_table[CoordToIndex(v.x + xoffset, v.y + yoffset)] == -1 && !flood_fill_blocked[CoordToIndex(v.x + xoffset, v.y + yoffset)]) {
//		flood_fill_table[CoordToIndex(v.x + xoffset, v.y + yoffset)] = distance;
//		new_open.push_back(sf::Vector2u(v.x + xoffset, v.y + yoffset));
//	}
//}

