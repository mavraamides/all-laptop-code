#pragma once
#include <SFML/Graphics.hpp>
#include "Snake.h"
#include <set>
#include <vector>
#include <string>
#include <map>

using namespace std;

enum class search_stage
{
	opening,
	middlegame,
	endgame
};

enum class log_event
{
	create_apple,
	create_snake,
	move_snake
};

class log_entry
{
public:
	log_event le;
	coord c;
};
class backup_set
{
public:
	coord apple;
	snake_path snake;
};

class Board
{
private:
	const string savefilename = "snakesave.txt";
	const string logfilename = "snakelog.txt";
	const unsigned int max_backup_snake = 100u;
	snake_path master_path;
	int game_log_index;

	set<unsigned int> occupied;
	//vector<int> flood_fill_table;
	//vector<bool> flood_fill_blocked;
	vector<backup_set> backup_snakes;
	unsigned int current_snake;
	vector<log_entry> game_log;

	unsigned int moves_this_game;
	unsigned int width;
	unsigned int height;
	unsigned int screen_width;
	unsigned int screen_height;
	bool snake_dead = false;
	bool game_won = false;
	bool auto_mode = false;
	bool replay_mode = false;
	Snake snake;

	coord apple_pos;
	sf::Font font;

public:
	Board();
	~Board();
	void Initialize(unsigned int w, unsigned int h, unsigned int sw, unsigned int sh);
	void Reset();
	void Draw(sf::RenderWindow & window);
	void DrawFloodfill(unsigned int padding, unsigned int cell_size, sf::RenderWindow& window);
	void DrawSnake(unsigned int cell_size, unsigned int padding, sf::RenderWindow& window);
	bool SetApplePosition();
	void DrawApple(unsigned int cell_size, unsigned int padding, sf::RenderWindow& window);
	void DrawBoard(unsigned int cell_size, unsigned int padding, sf::RenderWindow& window);
	bool AvoidTail();
	search_stage GetSearchStage();
	bool EndGame();
	unsigned int GetHalfOpenSpace();
	int GetDistanceToTail();
	void GetTurnsTillTailVisibleAndPathLength(int & turns, int & path_length);
	int TotalDistanceToFollowTail();


	void SetAutoMode(bool v) { auto_mode = v; }
	void SetReplayMode(bool v) { replay_mode = v; }
	void Save();
	void Load();
	void WriteGameLog();
	string GetSaveName(unsigned int moves, string extension);
	void WriteGameReport();
	void WriteGameLogByName(string name);
	void ReadGameLog();
	bool PlayGameLog();
	void AddSnakeToBackup();
	void BackSnakeBackup();
	void ForwardSnakeBackup();
	void MoveSnake(SnakeFacingDirection dir);

	SnakeFacingDirection GetAutoMove();
	SnakeFacingDirection GetFacingForCoord(coord head, int path_ndx);
	snake_path FindBestPath(coord target);
	snake_path SearchAllPaths(coord target, float target_score, int distance_to_target);
	//snake_path FindADifferentPath(coord target, map<string, float> previous_paths, int max_length);
	float CalcHoleScore(snake_path path);
	unsigned int GetNumberOfUncheckedCells();
	void InitializeFloodFill(snake_path proposed_path);
	void FloodFill(coord target, snake_path proposed_path, bool reverse = false, int add_on = 0, bool initialize = true);
	string SnakePathToId(snake_path path);
	snake_path IdToSnakePath(string id);

	void DumpFloodFill(string label);

	void SetOccupied();
	int CoordToIndex(coord c);
	coord IndexToCoord(int ndx);
	bool IsValidCoord(coord c);
	bool IsValidIndex(int ndx);
	coord GetNeighborFill(coord c, SnakeFacingDirection d);
	coord GetNeighborFollow(coord c, SnakeFacingDirection d);
	snake_path GetPathFrom(coord from, bool runaway = false, bool reverse = false, int max_length = 1000000);

	//unsigned int CountHoles(snake_path path);
	//void FloodFillTestDirection(sf::Vector2u& v, int xoffset, int yoffset, int distance, vector<sf::Vector2u>& new_open);
	//void UpdateHolesAndBestPath(std::snake_path& path, int& holes, int& best_hole_count, std::snake_path& best_path);
	//void FindPathWithFewestHoles(int x, int y, snake_path& path, bool runaway = false, bool verify = false, int max_length = 1000000);
	//void GetPathUnverified(int fromx, int fromy, std::snake_path& path, bool runaway = false, bool reverse = false, int max_length = 1000000);
	//void VerifyPath(std::snake_path& path);
	//bool GetFloodFillBlockedValue(int x, int y);
	//bool GetNeighborFloodFillBlockedValue(int x, int y, SnakeFacingDirection d);
	
};

