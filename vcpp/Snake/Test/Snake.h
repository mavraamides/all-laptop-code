#pragma once
#include <SFML/Graphics.hpp>
#include <vector>

using namespace std;
typedef sf::Vector2i coord;
typedef vector<coord> snake_path;


enum class SnakeFacingDirection
{
	none,
	up,
	right,
	down,
	left
};

class Snake
{
	snake_path body;
public:
	Snake();
	~Snake();
	void SetRandomPosition(unsigned int w, unsigned int h);
	void Move(SnakeFacingDirection direction);
	void Grow(SnakeFacingDirection direction);
	coord& GetNextHeadPosition(SnakeFacingDirection direction);
	coord& GetPositionOfDirection(SnakeFacingDirection direction, int x, int y);
	snake_path& GetBody();
	bool SelfCollision(SnakeFacingDirection direction);
	bool OutOfBounds();
	void Set(snake_path& b);
};

