#pragma once
#include <SFML/Graphics.hpp>

enum kUStates {
    looking_for_food,
    headed_home
};

struct unit {
    unsigned int pos;
    char last_dir;
    kUStates state;
};

enum kUPathType {
    no_path,
    wander_path,
    food_path
};

enum kSearchType {
    no_path_st,
    path_st,
    any_path_st
};
struct phermone {
    int home;
    int last_visit_frame;
    kUPathType path_type;
};
class Units {
public:
    static Units& getInstance() {
        static Units instance;
        return instance;
    }

    void Dump();
    bool AddUnit(unsigned int xpos, unsigned int ypos);
    void UpdateTarget(unsigned int xpos, unsigned int ypos);
    unsigned int GetPathAge(unsigned int ndx);
    int GetNeighbor(unsigned int pos, unsigned char nndx);
    void PaintPhermoneTrails(sf::Uint8* pixels);
    int FindOpenDirection(unsigned int pos, char last_dir, kSearchType search_type);
    void UpdatePhermonePath(unsigned int from, unsigned int to, kUPathType path_type);
    void Update();
    void Paint(sf::Uint8* pixels);
    void SetScreenSize(unsigned int W, unsigned int H);
    
    static unsigned int screen_width;
    static unsigned int screen_height;
private:
    unsigned int frame;
    Units();
    ~Units() = default;
    Units(const Units&) = delete;
    Units& operator=(const Units&) = delete;
  
    int group_countdown;
};
