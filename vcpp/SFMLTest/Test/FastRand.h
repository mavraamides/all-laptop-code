#pragma once
/*
class MySingleton {
public:
    static MySingleton& getInstance() {
        static MySingleton instance;
        // volatile int dummy{};
        return instance;
    }
private:
    MySingleton(){};
    ~MySingleton() = default;
    MySingleton(const MySingleton&) = delete;
    MySingleton& operator=(const MySingleton&) = delete;

};
*/

#define FR FastRand::getInstance()

class FastRand {
public:
    static FastRand& getInstance() {
        static FastRand instance;
        // volatile int dummy{};
        return instance;
    }

    int get_int();
private:
    FastRand();
    ~FastRand() = default;
    FastRand(const FastRand&) = delete;
    FastRand& operator=(const FastRand&) = delete;

};

