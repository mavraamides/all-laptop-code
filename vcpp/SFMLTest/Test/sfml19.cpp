// sfml19.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <SFML/Graphics.hpp>
#include <random>
#include <chrono>
#include <iostream>
#include "FastRand.h"
#include "Units.h"
#include "sfml19.h"

using namespace std;
using namespace std::chrono;

const unsigned int W = 1920;
const unsigned int H = 1080;
sf::Uint8* pixels = new sf::Uint8[W * H * 4];

void ClearPixels(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
    for (register int i = 0; i < W * H * 4; i += 4) {
        pixels[i] = r;
        pixels[i + 1] = g;
        pixels[i + 2] = b;
        pixels[i + 3] = a;
    }

}

int main()
{
    Units::getInstance().SetScreenSize(W, H);
    //while (Units::getInstance().AddUnit((float)(rand() % W), (float)(rand() % H)));
    //while (Units::getInstance().AddUnit((float)(W / 2), (float)(H / 2)));
    //Units::getInstance().AddUnit((W / 2), (H / 2));

    srand((unsigned) time(0));
    sf::RenderWindow window(sf::VideoMode(W, H), "Ants... Ants.. ANTS!!!",sf::Style::Fullscreen);
    window.setFramerateLimit(60);

    static unsigned char rands[10000];
    for (int x = 0; x < 10000; x++) rands[x] = rand() % 255;

    sf::Texture unit_texture;
    unit_texture.create(W, H);

    sf::Texture grass_texture;
    grass_texture.create(W, H);

    sf::Texture overlay_texture;
    overlay_texture.create(W, H);

    sf::Sprite unit_sprite(unit_texture); // needed to draw the texture on screen
    sf::Sprite grass_sprite(grass_texture);
    sf::Sprite overlay_sprite(overlay_texture);


    ClearPixels(0, 0, 0, 0);
    for (register int i = 0; i < W * H * 4; i += 4) {
        pixels[i + 3] = rand() % 64;
    }
    overlay_texture.update(pixels);
    
    float x = 0.0f;
    float y = 0.0f;
    unsigned int c = 0;
    unsigned int frame = 0;
    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    while (window.isOpen())
    {
        //Units::getInstance().AddUnit((float) (W/2), (float) (H /2));
        //Units::getInstance().AddUnit((float)(rand() % W), (float)(rand() % H));
        frame++;
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            if (event.type == sf::Event::MouseButtonPressed)
            {
                sf::Vector2i position = sf::Mouse::getPosition(window);
            }
        }
        Units::getInstance().AddUnit((W / 2), (H / 2));

        Units::getInstance().Update();
        ClearPixels(0,0,0,0);       
        Units::getInstance().Paint(pixels);
        unit_texture.update(pixels);

        window.clear(sf::Color(158, 145, 109, 255));
        //window.clear(sf::Color(80,57,42,255));
        ClearPixels(35, 128, 31, 255);
        Units::getInstance().PaintPhermoneTrails(pixels);
        grass_texture.update(pixels);
        window.draw(grass_sprite);
        window.draw(unit_sprite);
        //window.draw(overlay_sprite);
        window.display();
    }
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    cout << "Frames per second: " << (frame / time_span.count()) << endl;

    return 0;
}

