#include <iostream>
#include <random>
#include "Units.h"
#include "FastRand.h"

using namespace std;

const int group_size = 10;

static unit* unit_table = nullptr;
static unsigned int num_units = 0;
static unsigned int max_unit = 100000;
unsigned int Units::screen_width = 0;
unsigned int Units::screen_height = 0;

static phermone* phermone_table = nullptr;
static unsigned int max_phermone = 0;
struct phermone_work {
	int target;
	int score;
};
static phermone_work phermone_work_table[8];
const int max_path_age = 255;

//struct nav_element {
//	phermone* neighbors[8];
//};
//static nav_element* nav_table = nullptr;

inline unsigned int xy_to_pos(int x, int y)
{
	return x + Units::screen_width * y;
}

inline void scale_vector(float& x, float& y, float velocity)
{
	auto magnitude = sqrtf((x * x) + (y * y));
	if (magnitude != 0.0f)
	{
		x /= magnitude;
		y /= magnitude;
	}
	x *= velocity;
	y *= velocity;
}

void Units::Dump()
{
	cout << "My dog has no nose.";
}

bool Units::AddUnit(unsigned int xpos, unsigned int ypos)
{
	auto added_unit = false;

	if (num_units < max_unit)
	{
		unit_table[num_units].pos = xy_to_pos(xpos, ypos);
		unit_table[num_units].last_dir = FR.get_int() % 8;
		unit_table[num_units].state = kUStates::looking_for_food;
		num_units++;
		added_unit = true;
	}
	return added_unit;
}

inline unsigned int Units::GetPathAge(unsigned int ndx)
{
	auto path_age = max_path_age;

	if (ndx < max_phermone)
	{
		if (phermone_table[ndx].path_type != kUPathType::no_path)
		{
			path_age = (frame - phermone_table[ndx].last_visit_frame);
		}
		if (path_age > max_path_age) path_age = max_path_age;
	}

	return path_age;
}

inline int Units::GetNeighbor(unsigned int pos, unsigned char nndx)
{
	int neighbor = -1;
	auto valid = false;

	auto xpos = pos % screen_width;
	auto ypos = pos / screen_width;
	auto nxpos = xpos;
	auto nypos = ypos;
	switch (nndx)
	{
	case 0:
		if (ypos > 0)
		{
			nypos--;
			valid = true;
		}
		break;
	case 1:
		if (ypos > 0 && xpos < screen_width - 1)
		{
			nxpos++;
			nypos--;
			valid = true;
		}
		break;
	case 2:
		if (xpos < screen_width - 1)
		{
			nxpos++;
			valid = true;
		}
		break;
	case 3:
		if (ypos < screen_height - 1 && xpos < screen_width - 1)
		{
			nxpos++;
			nypos++;
			valid = true;
		}
		break;
	case 4:
		if (ypos < screen_height - 1)
		{
			nypos++;
			valid = true;
		}
		break;
	case 5:
		if (ypos < screen_height - 1 && xpos > 0)
		{
			nxpos--;
			nypos++;
			valid = true;
		}
		break;
	case 6:
		if (xpos > 0)
		{
			nxpos--;
			valid = true;
		}
		break;
	case 7:
		if (ypos > 0 && xpos > 0)
		{
			nxpos--;
			nypos--;
			valid = true;
		}
		break;
	}
	if (valid) neighbor = nxpos + nypos * screen_width;

	return neighbor;
}

void Units::PaintPhermoneTrails(sf::Uint8* pixels)
{
	for (register auto ndx = 0u; ndx < max_phermone; ndx++)
	{
		auto age = GetPathAge(ndx);
		if (age < max_path_age)
		{
			auto offset = ndx * 4;
			pixels[offset + 3] = age * 255 / max_path_age;
		}
	}
}

inline int Units::FindOpenDirection(unsigned int pos, char last_dir, kSearchType search_type)
{
	int direction = -1;

	unsigned char path_choices[14][7] = {
		0,1,7,2,6,3,5,
		0,7,1,6,2,5,3,
		0,1,7,2,6,3,5,
		0,7,1,6,2,5,3,
		0,1,7,2,6,3,5,
		0,7,1,6,2,5,3,
		0,1,7,2,6,3,5,
		0,7,1,6,2,5,3,
		0,1,7,2,6,3,5,
		0,7,1,6,2,5,3,
		0,1,7,2,6,3,5,
		0,7,1,6,2,5,3,
		1,7,2,6,0,3,5,
		7,1,6,2,0,5,3
	};

	if (last_dir == -1) last_dir = FR.get_int() % 8;

	auto option = FR.get_int() % 14;
	for (auto x = 0u; x < 7; x++)
	{
		auto n = (path_choices[option][x] + last_dir) % 8;
		auto target = GetNeighbor(pos, n);
		if (target > 0) {
			if (search_type == kSearchType::path_st)
			{
				if (phermone_table[target].home == pos)
				{
					direction = n;
					break;
				}
			}
			else if (search_type == kSearchType::no_path_st)
			{
				auto path_age = GetPathAge(target);
				if (path_age >= max_path_age)
				{
					direction = n;
					break;
				}
			}
			else {
				direction = n;
				break;
			}
		}
	}

	return direction;
}

void Units::UpdatePhermonePath(unsigned int from, unsigned int to, kUPathType path_type)
{
	phermone_table[from].last_visit_frame = frame;
	phermone_table[from].path_type = kUPathType::wander_path;
	//if (phermone_table[to].home < 0) phermone_table[to].home = from;
	phermone_table[to].home = from;
	phermone_table[to].last_visit_frame = frame;
	phermone_table[to].path_type = kUPathType::wander_path;
}

void Units::Update()
{
	frame++;
	group_countdown++;
	group_countdown = group_countdown % group_size;
	for (auto n = 0u; n < num_units; n++)
	{
		if (unit_table[n].state == kUStates::looking_for_food)
		{
			auto direction = FindOpenDirection(unit_table[n].pos, unit_table[n].last_dir, kSearchType::no_path_st);
			if (direction < 0)
			{
				direction = FindOpenDirection(unit_table[n].pos, unit_table[n].last_dir, kSearchType::path_st);
			}
			if (direction < 0)
			{
				direction = FindOpenDirection(unit_table[n].pos, unit_table[n].last_dir, kSearchType::any_path_st);
			}
			if (direction > 0) {
				auto path = GetNeighbor(unit_table[n].pos, direction);
				UpdatePhermonePath(unit_table[n].pos, path, kUPathType::wander_path);
				unit_table[n].pos = path;
				unit_table[n].last_dir = direction;
			}
		}
	}
}

void Units::Paint(sf::Uint8* pixels)
{
	if (!pixels) return;

	for (auto n = 0u; n < num_units; n++)
	{
		auto ndx = unit_table[n].pos;
		if (ndx < screen_width * screen_height)
		{
			auto offset = ndx * 4;
			pixels[offset] = 64;
			pixels[offset + 1] = 0;
			pixels[offset + 2] = 0;

			auto alpha = pixels[offset + 3];
			if (alpha == 0) {
				pixels[offset + 3] = 255;
			}
			else
			{
				auto remaining = 255 - alpha;
				if (remaining > 20) remaining = 20;
				pixels[offset + 3] += remaining;
			}
			//pixels[offset + 3] = 255;
		}
	}
}

void Units::SetScreenSize(unsigned int W, unsigned int H)
{
	if (phermone_table == nullptr)
	{
		screen_width = W;
		screen_height = H;
		max_phermone = W * H;
		phermone_table = new phermone[max_phermone];
		for (auto n = 0u; n < max_phermone; n++) {
			phermone_table[n].home = -1;
			phermone_table[n].last_visit_frame = -1;
			phermone_table[n].path_type = kUPathType::no_path;
		}
	}
}

Units::Units()
{
	unit_table = new unit[max_unit];
	group_countdown = 0;
	frame = 0;
}
