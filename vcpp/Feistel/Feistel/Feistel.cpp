// Feistel.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <iostream>
#include "FeistelEncrypt.h"
using namespace std;

int main(int argc, char* argv[])
{
    if (argc != 5)
    {
        cout << "Usage: Feistel {message filename} {key filename} {target_file} {1 - encrypt | 2 decrypt}" << endl;
        return 1;
    }
    string msgfilename = argv[1];
    string encfilename = argv[2];
    string targetfilename = argv[3];
    string encode = argv[4];
    bool encrypting = (encode.size() > 0 && encode.c_str()[0] == '1');

    FeistelEncryp(msgfilename, encfilename, targetfilename, encrypting);
}
