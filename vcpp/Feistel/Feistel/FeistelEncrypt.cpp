#include <string>
#include <iomanip>
#include <vector>
#include <fstream>
#include <iostream>
using namespace std;

const int max_key = 4;
unsigned char* left_side = nullptr;
unsigned char* right_side = nullptr;
unsigned char* work_side = nullptr;
unsigned char* keys[max_key];
unsigned long long key_len = 0l;
unsigned long long side_len = 0l;

void dump_hex(unsigned char side[], unsigned long long len)
{
    for (auto x = 0u; x < len; x++) {
        if (x % 40 == 0) cout << endl;
        if (x % 8 == 0) cout << " ";
        cout << hex << setw(2) << setfill('0') << right << (unsigned int)side[x] << " ";
    }
    cout << endl;
}
void x_or(unsigned char target[], unsigned char key[], unsigned long long len)
{
    for (auto x = 0u; x < len; x++) {
        target[x] = (target[x] ^ key[x]);
    }
}
void f(unsigned char side[], unsigned long long len, unsigned char key[], unsigned long long key_len)
{
    for (auto x = 0u; x < len; x++) {
        side[x] = (side[x] + key[x % key_len]) % 0x256;
    }
}

void swap(unsigned char left[], unsigned char right[], unsigned long long len)
{
    memcpy(work_side, right, len);
    memcpy(right, left, len);
    memcpy(left, work_side, len);

}
void cycle(unsigned char left[], unsigned char right[], unsigned long long len, unsigned char key[], unsigned long long key_len)
{
    memcpy(work_side, right, len);
    f(work_side, len, key, key_len);
    x_or(work_side, left, len);
    memcpy(left, right, len);
    memcpy(right, work_side, len);
}

bool write_result(string filename, unsigned char left[], unsigned char right[], unsigned long long len, bool encrypting)
{
    bool result = false;

    ofstream out(filename, ios::binary);
    if (out.is_open())
    {
        bool skip_last_character = false;

        for (auto x = 0u; x < len; x++)
        {
            if (x == 0u)
            {
                if (!encrypting)
                {
                    if (left[x] == '1') skip_last_character = true;
                    continue;
                }
            }
            out << left[x];

        }
        auto right_end = len;
        if (skip_last_character) right_end--;
        for (auto x = 0u; x < right_end; x++) out << right[x];
        out.close();
        result = true;
    }

    return result;
}
bool read_keys(string filename)
{
    string line;

    ifstream in(filename, ios::binary);
    if (!in.good()) return false;
    vector<unsigned char> buffer(istreambuf_iterator<char>(in), {});
    in.close();

    auto len = buffer.size();
    key_len = len / max_key;

    for (auto k = 0u; k < max_key; k++)
    {
        keys[k] = new unsigned char[key_len];
    }

    for (auto x = 0u; x < key_len; x++)
    {
        for (auto k = 0u; k < max_key; k++)
        {
            keys[k][x] = buffer[x + k * key_len];
        }
    }
    //for (auto k = 0u; k < max_key; k++)
    //{
    //    for (auto x = 0u; x < key_len; x++) cout << keys[k][x];
    //    cout << endl;
    //}

    return true;
}

bool read_file(string filename, bool encrypting)
{
    string line;

    ifstream in(filename, ios::binary);
    if (!in.good()) return false;
    vector<unsigned char> buffer(istreambuf_iterator<char>(in), {});
    if (encrypting) buffer.insert(buffer.begin(), '0');
    in.close();

    auto len = buffer.size();
    side_len = len / 2;
    if (2 * side_len < len) {
        side_len++;
        buffer[0] = '1';
    }

    left_side = new unsigned char[side_len];
    right_side = new unsigned char[side_len];
    work_side = new unsigned char[side_len];
    for (auto x = 0u; x < side_len; x++)
    {
        left_side[x] = buffer[x];
        if (buffer.size() > x + side_len) right_side[x] = buffer[x + side_len];
        else right_side[x] = '@';
    }
    //for (auto x = 0u; x < side_len; x++) cout << left_side[x];
    //for (auto x = 0u; x < side_len; x++) cout << right_side[x];
    cout << endl;

    return true;
}

bool FeistelEncryp(string msgfilename, string encfilename, string targetfilename, bool encrypting)
{
    if (!read_file(msgfilename, encrypting))
    {
        cout << "Unable to read " << msgfilename << endl;
        return false;
    }

    if (!read_keys(encfilename))
    {
        cout << "Unable to read " << encfilename << endl;
        return false;
    }

    if (encrypting)
    {
        for (auto k = 0u; k < max_key; k++) cycle(left_side, right_side, side_len, keys[k], key_len);
    }
    else
    {
        for (unsigned int k = max_key; k > 0u; k--) cycle(left_side, right_side, side_len, keys[k - 1], key_len);
    }

    swap(left_side, right_side, side_len);

    if (!write_result(targetfilename, left_side, right_side, side_len, encrypting))
    {
        cout << "Unable to write to " << targetfilename << endl;
        return false;
    }

    return true;
}
