#include "EncodeLZW.h"
#include <iostream>
#include <iomanip>

bool EncodeLZW(vector<unsigned char>& source, vector<unsigned char>& dest, unsigned int max_dict)
{
	map<string, unsigned int> dictionary;
	vector<unsigned int> output;
	auto nextndx = 256u;

	string s;
	string teststring;
	for (auto c : source)
	{
		teststring = s;
		teststring += c;
		if (dictionary.count(teststring) > 0)
		{
			s = teststring;
		}
		else
		{
			if (dictionary.count(s) > 0) {
				output.push_back(dictionary[s]);
			}
			else {
				for (auto ch: s) output.push_back(ch);
			}
			if (teststring.size() > 1 && nextndx < max_dict) dictionary[teststring] = nextndx++;
			s = c;
		}
	}
	if (dictionary.count(s) > 0) {
		output.push_back(dictionary[s]);
	}
	else {
		for (auto ch : s) output.push_back(ch);
	}

	//for (auto c : output) cout << c << " ";
	//cout << endl;
	//map<unsigned int, string> rdict;
	//for (auto kvp : dictionary) rdict[kvp.second] = kvp.first;
	//for (auto c : output) {
	//	if (rdict.count(c) > 0) cout << "[" << rdict[c] << "]";
	//	else cout << (unsigned char)c;
	//}
	//cout << endl;
	auto maxdict = 0;
	for (auto kvp : dictionary) if (kvp.second > maxdict) maxdict = kvp.second;
	auto bitsneeded = 0;
	auto testmaxdict = maxdict;
	while (testmaxdict > 0) {
		testmaxdict /= 2;
		bitsneeded++;
	}
	float pct = (float)(output.size() * bitsneeded) / (float)(source.size() * 8);
	cout << "Total uncompressed: " << source.size() * 8 << " compressed: " << setw(10) << output.size() * bitsneeded <<  " (" << setw(10) << pct << ") maxdict: " << maxdict << " bits: " << bitsneeded << endl;
	return true;
}