#pragma once
#include <string>
#include <map>
#include <vector>
using namespace std;

bool EncodeLZW(vector<unsigned char>& source, vector<unsigned char>& dest, unsigned int max_dict);
