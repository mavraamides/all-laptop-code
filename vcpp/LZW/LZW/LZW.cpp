// LZW.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <bitset>
#include <iomanip>
#include "EncodeLZW.h"
using namespace std;

void copy_down(vector<unsigned char>& target, vector<unsigned int> source, unsigned int bits)
{
    
    if (source.size() == 0) return;

    unsigned char work_byte = 0x0;
    unsigned int work_ptr = 7;
    
    unsigned int source_ptr = bits - 1;
    unsigned int ndx = 0u;
    unsigned work_source = source[ndx];

    while (ndx < source.size())
    {
        bool bit = work_source & (1 << source_ptr);
        if (bit) work_byte = work_byte | (1 << work_ptr);

        if (work_ptr == 0) {
            target.push_back(work_byte);
            work_byte = 0x0;
            work_ptr = 7;
        }
        else {
            work_ptr--;
        }

        if (source_ptr == 0)
        {
            ndx++;
            if (ndx < source.size()) work_source = source[ndx];
            source_ptr = bits - 1;
        }
        else {
            source_ptr--;
        }
    }
    bitset<8> b8;
    if (work_ptr < 7) target.push_back(work_byte);
    for (auto w : target) {
        b8 = w;
        cout << b8;
    }
    cout << endl;
    bitset <13> b13;
    for (auto w : source) {
        b13 = w;
        cout << b13;
    }
    cout << endl;
}
int main(int argc, char* argv[])
{
    if (argc != 4)
    {
        cout << "Usage: LZW {source file} {dest file} <1 compress | 2 decompress>" << endl;
        return 1;
    }
    string compress = argv[3];
    bool compressing = (compress.size() > 0 && compress[0] == '1');

    string fromfilename = argv[1];
    string tofilename = argv[2];


    vector<unsigned char> target;
    vector<unsigned int> source;
    source.push_back(0x16ee);
    source.push_back(0x16ee);
    source.push_back(0x16ee);
    source.push_back(0x16ee);
    copy_down(target, source, 13);
    if (compressing)
    {
        ifstream in(fromfilename, ios::binary);
        if (!in.good()) {
            cout << "Unable to load " << fromfilename << endl;
            return 1;
        }
        vector<unsigned char> fromfile(istreambuf_iterator<char>(in), {});
        in.close();

        vector<unsigned char> tofile;

        auto max_size = 512;
        for (auto x = 0u; x < 16; x++)
        {
            if (!EncodeLZW(fromfile, tofile, max_size))
            {
                cout << "Unable to compress " << fromfilename << endl;
                return 1;
            }
            max_size *= 2;
        }
    }

}
