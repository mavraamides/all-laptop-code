#pragma once
#include<iostream>
#include<vector>
#include<iomanip>
#include <SFML/Graphics.hpp>

using namespace std;

namespace Bots
{
	struct bot_s {
		sf::Vector2f position;
		sf::Vector2f velocity;
		sf::Vector2f acceleration;
		float max_speed;
		float max_force;
		sf::Vector2f target;
		bool colided;
		unsigned int collision_check_index;
	};

	void Initialize(unsigned int screenw, unsigned int screenh);
	void Update();
	sf::Vector2f & Arrive(unsigned int ndx);
	void Paint(sf::RenderWindow& window);
}