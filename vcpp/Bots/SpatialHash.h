#pragma once
#include <unordered_map>
#include <vector>
using namespace std;

class SpatialHash
{
	unordered_multimap<unsigned int, unsigned int> grid;
	int grid_size = 0;
public:
	SpatialHash(unsigned int size);
	~SpatialHash();
	void Clear();
	void Add(unsigned int x, unsigned int y, unsigned int id);
	void Check(unsigned int x, unsigned int y, vector<unsigned int>& result);
	void CheckRange(unsigned int x, unsigned int y, unsigned int w, unsigned int h, vector<unsigned int> & result);
	unsigned int GetKey(unsigned int x, unsigned int y);
};