#include <SFML/Graphics.hpp>
#include <iostream>
#include <chrono>
#include "Bots.h"

using namespace std;
using namespace std::chrono;

int main()
{
    auto W = 1024;
    auto H = 768;
    sf::RenderWindow window(sf::VideoMode(W, H), "Bots");
    window.setFramerateLimit(60);

    Bots::Initialize(W, H);

    unsigned int frame = 0;
    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    while (window.isOpen())
    {
        //if (frame % 60 == 0) Bots::AddPulse(0, 5.f);
        Bots::Update();

        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::KeyPressed) {
                //cout << event.key.code << endl;
                switch (event.key.code)
                {
                case sf::Keyboard::Escape:
                    window.close();
                    break;
                }
            }
            if (event.type == sf::Event::Closed)
                window.close();
            if (event.type == sf::Event::MouseButtonPressed)
            {
            }
        }


        window.clear(sf::Color::White);

        Bots::Paint(window);

        window.display();

        frame++;
    }
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    cout << "Frames per second: " << (frame / time_span.count()) << endl;
}
