#include "Bots.h"
#include <cmath>
#include <set>
#include "../SFMLTest/Test/FastRand.h"
#include "SpatialHash.h"

const float pi = 3.14159265f;
const float deg_to_rad = pi / 180.f;

namespace Bots
{
	vector<bot_s> bot_list;
	sf::Vector2u screen;
	auto botsize = 10.0f;
	SpatialHash * grid = nullptr;

	float Distance(sf::Vector2f a, sf::Vector2f b)
	{
		return sqrtf((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
	}
	bool BotCollision(float x, float y, unsigned int id)
	{
		bool result = false;
		vector<unsigned int> collisions;
		auto collision_check_index = FR.get_int();
		auto starty = y - botsize * 2;
		for (auto r = 0; r < 3 && !result; r++)
		{
			auto startx = x - botsize * 2;
			for (auto c = 0; c < 3 && !result; c++)
			{
				collisions.clear();
				grid->Check((unsigned int)startx, (unsigned int)starty, collisions);
				for (auto ndx : collisions)
				{
					if (ndx == id) continue;
					if (bot_list[ndx].collision_check_index == collision_check_index) continue;
					bot_list[ndx].collision_check_index = collision_check_index;

					if (Distance(bot_list[id].position, bot_list[ndx].position) < botsize * 2)
					{
						result = true;
						break;
					}
				}

				startx += botsize * 2;
			}
			starty += botsize * 2;
		}
		return result;
	}
	void GetNeighborsInRange(unsigned int id, float range, vector<unsigned int> &results)
	{
		vector<unsigned int> neighbors;

		auto ulx = bot_list[id].position.x - range;
		if (ulx < 0) ulx = 0;
		auto uly = bot_list[id].position.y - range;
		if (uly < 0) uly = 0;
		grid->CheckRange(ulx, uly, range * 2, range * 2, neighbors);

		auto collision_check_index = FR.get_int();
		for (auto ndx : neighbors)
		{
			if (ndx == id) continue;
			if (bot_list[ndx].collision_check_index == collision_check_index) continue;
			bot_list[ndx].collision_check_index = collision_check_index;

			if (Distance(bot_list[ndx].position, bot_list[id].position) < range) results.push_back(ndx);
		}
	}
	float GetMagnitude(sf::Vector2f& v)
	{
		return sqrtf(v.x * v.x + v.y * v.y);
	}
	void Normalize(sf::Vector2f& v)
	{
		auto magnitude = sqrtf(v.x * v.x + v.y * v.y);
		if (magnitude > 0.f)
		{
			v /= magnitude;
		}
	}
	void SetMagnitude(sf::Vector2f& v, float magnitude)
	{
		Normalize(v);
		v *= magnitude;
	}
	void Limit(sf::Vector2f& v, float magnitude)
	{
		if (GetMagnitude(v) > magnitude)
		{
			Normalize(v);
			v *= magnitude;
		}
	}
	
	void Initialize(unsigned int screenw, unsigned int screenh)
	{
		FastRand::getInstance();
		screen = { screenw, screenh };
		grid = new SpatialHash((int)(botsize * 2));
		//for (auto x = 0u; x < 10000; x++)
		for (auto x = 0u; x < 50; x++)
		{
			bot_s new_bot;
			
			new_bot.velocity = { 
				(float)(50 - FR.get_int() % 100),
				(float)(50 - FR.get_int() % 100)
			};
			Normalize(new_bot.velocity);
			new_bot.velocity *= botsize;
			new_bot.position = { 
				(float)botsize + (FR.get_int() % (screen.x - 2 * (int)botsize)),
				(float)botsize + (FR.get_int() % (screen.y - 2 * (int)botsize))
			};
			new_bot.acceleration = { 0.0f, 0.0f };
			new_bot.target = {
				(float)botsize + (FR.get_int() % (screen.x - 2 * (int)botsize)),
				(float)botsize + (FR.get_int() % (screen.y - 2 * (int)botsize))
			};
			new_bot.max_speed = 1.0f;
			new_bot.max_force = 0.05f;
			new_bot.colided = false;
			new_bot.collision_check_index = 0;
			bot_list.push_back(new_bot);
		}
	}

	sf::Vector2f& Arrive(unsigned int ndx)
	{
		sf::Vector2f target = bot_list[ndx].target - bot_list[ndx].position;
		auto distance = GetMagnitude(target);

		auto velocity = bot_list[ndx].max_speed;
		if (distance < bot_list[ndx].max_speed * 40) {
			velocity = distance / 60;
		}

		SetMagnitude(target, velocity);

		sf::Vector2f desired = target - bot_list[ndx].velocity;
		return desired;
	}

	sf::Vector2f& StayInBounds(unsigned int ndx)
	{
		sf::Vector2f v({ 0.f, 0.f });

		auto futurepos = bot_list[ndx].position + bot_list[ndx].velocity * botsize;
		if (futurepos.x < 0.f) v.x += 1.f;
		if (futurepos.x > screen.x) v.x -= 1.f;
		if (futurepos.y < 0.f) v.y += 1.f;
		if (futurepos.y > screen.y) v.y -= 1.f;
		
		return v;
	}
	sf::Vector2f& RepulseFromNeighbors(unsigned int ndx)
	{
		sf::Vector2f v({ 0.f, 0.f });

		vector<unsigned int> neighbors;
		GetNeighborsInRange(ndx, botsize * 4, neighbors);

		for (auto n : neighbors)
		{
			auto repulse = (bot_list[ndx].position - bot_list[n].position);
			auto distance = Distance(bot_list[ndx].position,  bot_list[n].position);
			repulse *= (botsize * 4 - distance);
			//Limit(repulse, bot_list[ndx].max_force);
			v += repulse;
		}
		if (neighbors.size() > 0) v = v / (float)neighbors.size();

		return v;
	}
	void Update()
	{
		grid->Clear();
		for (auto ndx = 0u; ndx < bot_list.size(); ndx++) grid->Add((unsigned int)bot_list[ndx].position.x, (unsigned int)bot_list[ndx].position.y, ndx);
		for (auto ndx = 0u; ndx < bot_list.size(); ndx++)
		{
			bot_list[ndx].colided = BotCollision(bot_list[ndx].position.x, bot_list[ndx].position.y, ndx);

			bot_list[ndx].acceleration = { 0.f, 0.f };
			bot_list[ndx].acceleration += Arrive(ndx);
			bot_list[ndx].acceleration += StayInBounds(ndx);
			bot_list[ndx].acceleration += RepulseFromNeighbors(ndx) * 5.0f;

			if (Distance(bot_list[ndx].position, bot_list[ndx].target) < botsize / 5.0f) {
				bot_list[ndx].target = {
					(float)botsize + (FR.get_int() % (screen.x - 2 * (int)botsize)),
					(float)botsize + (FR.get_int() % (screen.y - 2 * (int)botsize))
				};
			}
		}
		for (auto ndx = 0u; ndx < bot_list.size(); ndx++)
		{
			Limit(bot_list[ndx].acceleration, bot_list[ndx].max_force);
			bot_list[ndx].velocity += bot_list[ndx].acceleration;
			Limit(bot_list[ndx].velocity, bot_list[ndx].max_speed);

			bot_list[ndx].position += bot_list[ndx].velocity;
		}
	}


	void Paint(sf::RenderWindow &window)
	{
		//sf::RectangleShape g({ botsize * 2, botsize * 2 });
		//g.setOutlineColor(sf::Color::Black);
		//g.setOutlineThickness(1.0f);
		//
		//for (auto gx = 0.f; gx < screen.x; gx += botsize * 2)
		//{
		//	for (auto gy = 0.f; gy < screen.y; gy += botsize * 2)
		//	{
		//		vector<unsigned int> results;
		//		results.clear();
		//		grid->Check((unsigned int)(gx + botsize), (unsigned int)(gy + botsize), results);
		//		if (results.size() > 0)
		//		{
		//			g.setFillColor(sf::Color(0, 255/ results.size(), 0));
		//		}
		//		else
		//		{
		//			g.setFillColor(sf::Color::White);
		//		}
		//		g.setPosition(gx, gy);
		//		window.draw(g);
		//	}
		//}

		vector<unsigned int> neighbors;
		GetNeighborsInRange(0, botsize * 4, neighbors);
		set<unsigned int>nset(neighbors.begin(), neighbors.end());

		sf::CircleShape range(3 * botsize);
		range.setOrigin(3 * botsize, 3 * botsize);
		range.setOutlineColor(sf::Color::Blue);
		range.setOutlineThickness(1.0f);

		sf::CircleShape bot(botsize);
		bot.setFillColor(sf::Color::Red);
		bot.setOrigin(botsize, botsize);

		sf::CircleShape dot(5.0f);
		dot.setFillColor(sf::Color::Green);
		dot.setOrigin(2.5f, 2.5f);

		sf::Vertex line[2];
		for (auto ndx = 0u; ndx < bot_list.size(); ndx++)
		{
			bot.setPosition(bot_list[ndx].position);
			if (bot_list[ndx].colided) {
				bot.setFillColor(sf::Color::Red);
			}
			else {
				bot.setFillColor(sf::Color::Red);
			}
			if (ndx == 0) {
				bot.setFillColor(sf::Color::Blue);
				range.setPosition(bot_list[ndx].position);
				window.draw(range);
			}
			if (nset.count(ndx) > 0)
			{
				bot.setFillColor(sf::Color::Yellow);
			}
			dot.setPosition(bot_list[ndx].target);
			line[0].position = bot_list[ndx].position;
			line[0].color = sf::Color::Black;
			line[1].position = bot_list[ndx].position + bot_list[ndx].velocity * botsize;
			line[1].color = sf::Color::Black;


			window.draw(bot);
			window.draw(line, 2, sf::Lines);
			window.draw(dot);
		}
	}
}