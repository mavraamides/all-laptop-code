#include "SpatialHash.h"

SpatialHash::SpatialHash(unsigned int size)
{
	grid_size = size;
}

SpatialHash::~SpatialHash()
{
	grid.clear();
}

void SpatialHash::Clear()
{
	grid.clear();
}

void SpatialHash::Add(unsigned int x, unsigned int y, unsigned int id)
{
	auto key = GetKey(x, y);
	grid.insert(make_pair(key, id));
}

void SpatialHash::Check(unsigned int x, unsigned int y, vector<unsigned int>& result)
{
	auto key = GetKey(x, y);
	auto its = grid.equal_range(key);
	for (auto it = its.first; it != its.second; ++it)
	{
		result.push_back(it->second);
	}
}

void SpatialHash::CheckRange(unsigned int x, unsigned int y, unsigned int w, unsigned int h, vector<unsigned int>& result)
{
	for (auto testy = y; testy <= y + h; testy += grid_size)
	{
		for (auto testx = x; testx <= x + w; testx += grid_size)
		{
			Check(testx, testy, result);
		}
	}
}

unsigned int SpatialHash::GetKey(unsigned int x, unsigned int y)
{
	auto key = x / grid_size;
	key = key << 16;
	key += y / grid_size;
	return key;
}
