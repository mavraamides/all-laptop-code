// sfml19.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <SFML/Graphics.hpp>
#include <random>
#include <chrono>
#include <iostream>
#include "DisplayBoard.h"


using namespace std;
using namespace std::chrono;

const unsigned int W = 1024;
const unsigned int H = 768;

int main()
{
    sf::RenderWindow window(sf::VideoMode(W, H), "Star Battles");
    window.setFramerateLimit(60);
 
    DisplayBoard db(10, 10);

    unsigned int frame = 0;
    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    while (window.isOpen())
    {
        frame++;
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            if (event.type == sf::Event::MouseButtonPressed)
            {
                sf::Vector2i position = sf::Mouse::getPosition(window);
            }
        }

        window.clear(sf::Color::White);
        db.Draw(window);
        window.display();
    }
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    cout << "Frames per second: " << (frame / time_span.count()) << endl;

    return 0;
}

