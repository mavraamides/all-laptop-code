#include "DisplayBoard.h"

DisplayBoard::DisplayBoard(int w, int h)
{
	width = w;
	height = h;
}

void DisplayBoard::Draw(sf::RenderWindow& window)
{
	auto length = 40.f;

	sf::RectangleShape thick_line(sf::Vector2f(length, 5.f));
	thick_line.setOrigin(0.f, 2.5f);
	thick_line.setFillColor(sf::Color::Black);
	thick_line.setOutlineColor(sf::Color::Black);
	thick_line.setOutlineThickness(1.0f);

	sf::RectangleShape thin_line(sf::Vector2f(length, 2.f));
	thin_line.setFillColor(sf::Color::Black);
	thin_line.setOrigin(0.f, 1.f);

	auto total_width = width * length;
	auto ulx = 50.0f;
	auto uly = 50.0f;
	auto x = ulx;
	auto y = uly;
	for (auto n = 0; n < width; n++)
	{
		thick_line.setPosition(x, y);
		window.draw(thick_line);
		thick_line.setPosition(x, y + total_width);
		window.draw(thick_line);

		for (auto m = 1; m < width; m++)
		{
			thin_line.setPosition(x, y + length * m);
			window.draw(thin_line);
		}
		x += length;
	}
	thin_line.setRotation(90.f);
	thick_line.setRotation(90.f);
	x = ulx;
	y = uly;
	for (auto n = 0; n < width; n++)
	{
		thick_line.setPosition(x, y);
		window.draw(thick_line);
		thick_line.setPosition(x + total_width, y);
		window.draw(thick_line);
		for (auto m = 1; m < width; m++)
		{
			thin_line.setPosition(x + length * m, y);
			window.draw(thin_line);
		}
		y += length;
	}
}
