#pragma once
#include <SFML/Graphics.hpp>

class DisplayBoard
{
	int width;
	int height;
public:
	DisplayBoard(int w, int h);
	~DisplayBoard() {};

	void Draw(sf::RenderWindow &window);
	void HandleInput();
};

