#include <SFML/Graphics.hpp>
#include <random>
#include <chrono>
#include <vector>
#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include "..\..\SFMLTest\Test\FastRand.h"
#include "..\..\Compress\CompressUtil.h"
#include "..\..\Encrypt\AES.h"
#include "..\..\Encrypt\keccak.h"
#include "..\..\HashShuffle\Shuffle.h"


using namespace std;
using namespace std::chrono;
sf::Uint8* pixels = nullptr;
unsigned int W = 0;
unsigned int H = 0;

void get_bitstream_from_message(vector<unsigned char> v, vector<bool>& bitstream)
{
    for (auto c : v) {
        auto mask = 0x80;
        for (auto x = 0; x < 8; x++) {
            bitstream.push_back(c & mask);
            mask = mask >> 1;
        }
    }
}
void put_color_stream(sf::Image& image, vector<unsigned char>& color_stream)
{
    auto size = image.getSize();
    auto ix = 0u;
    auto iy = 0u;
    auto ndx = 0u;
    while (ndx + 3 < color_stream.size()) {
        if (ix >= size.x) {
            ix = 0;
            iy++;
        }
        if (iy >= size.y) break;
        image.setPixel(ix, iy, sf::Color(color_stream[ndx], color_stream[ndx + 1], color_stream[ndx + 2], color_stream[ndx + 3]));
        ix++;
        ndx += 4;
    }
}
bool get_color_stream(sf::Image& image, vector<unsigned char>& color_stream)
{
    auto size = image.getSize();
    auto ix = 0u;
    auto iy = 0u;
    while (true) {
        if (ix >= size.x) {
            ix = 0;
            iy++;
        }
        if (iy >= size.y) {
            break;
        }
        auto pixel = image.getPixel(ix, iy);
        color_stream.push_back(pixel.r);
        color_stream.push_back(pixel.g);
        color_stream.push_back(pixel.b);
        color_stream.push_back(pixel.a);
        ix++;
    }
    return true;
}
void get_bitstream_from_colorstream(vector<bool> &bitstream, vector<unsigned char> colorstream)
{
    for (auto c : colorstream)
    {
        bool b = c & 0x01;
        //cout << hex << setfill('0') << right << (unsigned int)b << " ";
        bitstream.push_back(b);
    }
    //cout << endl;
}

void get_bytestrea_from_bitstream(vector<bool>& bitstream, vector<unsigned char>& bytestream)
{
    unsigned char byte = 0x00;
    unsigned char mask = 0x80;
    for (auto b : bitstream) {
        if (b) byte = byte | mask;
        mask = mask >> 1;
        if (mask <= 0) {
            bytestream.push_back(byte);
            byte = 0x00;
            mask = 0x80;
        }
    }
}
void get_message(vector<unsigned char>& message, sf::Image& image, vector<unsigned char> key)
{
    vector<unsigned char> color_stream;
    get_color_stream(image, color_stream);

    vector <unsigned int> shuffle_vector;
    HashShuffle::GetShuffleVectorFromHash(key, shuffle_vector, color_stream.size());

    unsigned int length = 0;
    unsigned char bytes[sizeof(length)];
 
    vector<unsigned char> shuffled_color_stream;
    for (auto x = 0u; x < 32; x++)
    {
        shuffled_color_stream.push_back(color_stream[shuffle_vector[x]]);
    }

    vector<bool> bitstream;
    get_bitstream_from_colorstream(bitstream, shuffled_color_stream);
    
    vector<unsigned char> bytestream;
    get_bytestrea_from_bitstream(bitstream, bytestream);
    
    for (auto x = 0u; x < bytestream.size() && x < sizeof(length); x++) bytes[x] = bytestream[x] ^ key[x];
    memcpy(&length, bytes, sizeof(length));
    cout << "Length: " << length << endl;

    shuffled_color_stream.clear();
    for (auto x = 32; x < 32 + 8 * length; x++)
    {
        shuffled_color_stream.push_back(color_stream[shuffle_vector[x]]);
    }

    bitstream.clear();
    get_bitstream_from_colorstream(bitstream, shuffled_color_stream);

    bytestream.clear();
    get_bytestrea_from_bitstream(bitstream, bytestream);

    for (auto x = 0u; x < bytestream.size(); x++) message.push_back(bytestream[x]);
}
bool set_message(vector<unsigned char> message, sf::Image& image, vector<unsigned char> key)
{
    unsigned int length = message.size();
    cout << "Setting length to: " << length << endl;
    unsigned char bytes[sizeof(length)];
    memcpy(bytes, &length, sizeof(length));
    for (auto x = 0u; x < sizeof(length); x++) message.insert(message.begin(), 0x00);
    for (auto x = 0u; x < sizeof(length); x++) message[x] = bytes[x] ^ key[x];
    cout.fill('0');
    for (auto x = 0u; x < sizeof(length); x++) cout << setw(2) << hex << (unsigned int)message[x] << " ";
    cout << endl;

    vector<bool> bitstream;
    get_bitstream_from_message(message, bitstream);
    vector<unsigned char> color_stream;
    if (!get_color_stream(image, color_stream)) return false;

    vector <unsigned int> shuffle_vector;
    HashShuffle::GetShuffleVectorFromHash(key, shuffle_vector, color_stream.size());
    for (auto x = 0u; x < bitstream.size(); x++)
    {
        bool b = bitstream[x];
        if (x >= color_stream.size()) continue;
        auto c = color_stream[shuffle_vector[x]];
        if (b) c = c | 0x01;
        else c = c & 0xfe;
        color_stream[shuffle_vector[x]] = c;
    }

    put_color_stream(image, color_stream);
    return true;
}

void print_usage()
{
        cout << "Usage:" << endl;
        cout << "Embedding:  ImageText 1 {message filename} {image filename} {target filename} {key filename}" << endl;
        cout << "Retrieving: ImageText 2 {image filename} {target filename} {key filename}" << endl;
        cout << "Viewing:    ImageText 3 {image filename} {target filename} {key filename}" << endl;
}

void ClearPixels(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
    if (!pixels) return;
    for (register int i = 0; i < W * H * 4; i += 4) {
        pixels[i] = r;
        pixels[i + 1] = g;
        pixels[i + 2] = b;
        pixels[i + 3] = a;
    }
}
void ScramblePixels(unsigned char a)
{
    if (!pixels) return;
    for (register int i = 0; i < W * H * 4; i += 4) {
        auto c = FR.get_int() % 256;
        pixels[i] = c;
        pixels[i + 1] = c;
        pixels[i + 2] = c;
        pixels[i + 3] = a;
    }
}

void get_document(vector<string>& document, vector<unsigned char>& message, int line_width)
{
    string line = "";
    string word = "";
    vector<string> words;
    for (auto c : message)
    {
        if (c == ' ' || c == '\n' || c == '\r')
        {
            words.push_back(word);
            word = "";
            continue;
        }
        word += c;
    }
    words.push_back(word);
    bool last_line_blank = false;
    for (auto w : words)
    {
        if (w.size() == 0)
        {
            if (line.size() > 0) document.push_back(line);
            if (!last_line_blank) document.push_back(w);
            last_line_blank = true;
            line = w;
            continue;
        }
        if (line.size() + w.size() + 1 >= line_width)
        {
            document.push_back(line);
            last_line_blank = false;
            line = "";
        }
        line += w;
        line += " ";
    }
    document.push_back(line);
    //for (auto l : document) cout << '[' << l << ']' << endl;
}

int main(int argc, char* argv[])
{
    if (argc < 3)
    {
        print_usage();
        return 1;
    }

    string option = argv[1];
    bool embedding = (option.size() > 0 && option.c_str()[0] == '1');
    bool viewing = (option.size() > 0 && option.c_str()[0] == '3');

	if (embedding && argc < 6) {
		print_usage();
		return 1;
	}
	else if (argc < 5) {
		print_usage();
		return 1;
	}

    string msgfilename = "";
    string imagefilename = "";
    string targetfilename = "";
    string keyfilename = "";
    vector<unsigned char> message;

    if (embedding)
    {
        msgfilename = argv[2];
        imagefilename = argv[3];
        targetfilename = argv[4];
        keyfilename = argv[5];
    }
    else
    {
        imagefilename = argv[2];
        targetfilename = argv[3];
        keyfilename = argv[4];
    }

    sf::Image image;
    if (!image.loadFromFile(imagefilename))
    {
        cout << "Unable to load " << imagefilename << endl;
        return 1;
    }

    ifstream in_key(keyfilename, ios::binary);
    if (!in_key.good()) {
        cout << "Unable to load " << keyfilename << endl;
        return 1;
    }
    vector<unsigned char> raw_key(istreambuf_iterator<char>(in_key), {});
    in_key.close();
    vector<unsigned char> key(raw_key);
    vector<unsigned char> dest;

    cout << "Keccak key hash..." << endl;
    KECCAK::get_key_hash(raw_key, key);

    if (embedding)
    {
        ifstream in(msgfilename, ios::binary);
        if (!in.good()) {
            cout << "Unable to load " << msgfilename << endl;
            return 1;
        }
        vector<unsigned char> from(istreambuf_iterator<char>(in), {});
        in.close();
        vector<unsigned char> to1;
        vector<unsigned char> to2;
        CompressUtil::Compress(from, to1, to2);

        cout << "AES Encrypt..." << endl;
        if (to1.size() < to2.size())
        {
            if (!AES::Transform(to1, key, dest, true))
            {
                cout << "AES Encrypt failed." << endl;
                return 1;
            }
        }
        else
        {
            if (!AES::Transform(to2, key, dest, true))
            {
                cout << "AES Encrypt failed." << endl;
                return 1;
            }
        }
        cout << "Keccak Encrypt..." << endl;
        KECCAK::encrypt(raw_key, dest);
        if (!set_message(dest, image, key)) return 1;

        image.saveToFile(targetfilename);
    }
    else
    {
        vector<unsigned char> from;
        get_message(from, image, key);

        cout << "Keccak Decrypt..." << endl;
        KECCAK::encrypt(raw_key, from);
        cout << "AES Decrypt..." << endl;
        if (!AES::Transform(from, key, dest, false))
        {
            cout << "AES Decrypt failed." << endl;
            return 1;
        }
        CompressUtil::Uncompress(dest, message);

        ofstream out(targetfilename, ios::binary);
        if (!out.is_open())
        {
            cout << "Unable to save " << targetfilename << endl;
            return 1;
        }
        for (auto c : message)
        {
            out << c;
        }
        out.close();
    }

    if (!viewing) return 0;
    sf::Font font;
    string fontname = "courbd.ttf";
    if (!font.loadFromFile(fontname))
    {
        cout << "Unable to load " << fontname << endl;
        return 1;
    }
    sf::Texture image_tex;
    if (!image_tex.loadFromFile(imagefilename)) {
        cout << "Unable to load " << msgfilename << endl;
        return 1;
    }
    W = image_tex.getSize().x;
    H = image_tex.getSize().y;
    auto desktop_mode = sf::VideoMode::getDesktopMode();
    auto screen_W = desktop_mode.width;
    auto screen_H = desktop_mode.height;
    auto text_W = W < screen_W ? W : screen_W;
    auto text_H = H < screen_H ? H : screen_H;
    auto text_x = 0;
    auto text_y = 0;
    if (W > screen_W) text_x = (W - screen_W) / 2;
    if (H > screen_H) text_y = (H - screen_H) / 2;

    sf::Text text;
    auto margin = 5;
    text.setFont(font);
    text.setCharacterSize(14);
    text.setFillColor(sf::Color::Black);
    text.setPosition(text_x + margin, text_y + margin);
    string test_text = "";
    while (true) {
        text.setString(test_text);
        auto width = text.getLocalBounds().width;
        if (width > text_W - 2 * margin) {
            break;
        }
        test_text += "W";
    }
    auto max_text_width = test_text.size() - 1;
    auto text_display_height = text.getLocalBounds().height + 2;
    auto page_lines = text_H / text_display_height;
    vector<string> document;
    get_document(document, message, max_text_width);

    sf::RenderWindow window(sf::VideoMode(W, H), "Image Text");
    window.setFramerateLimit(60);

    pixels = new sf::Uint8[W * H * 4];
    ClearPixels(0, 0, 0, 0);

    sf::Sprite image_sprite;
    image_sprite.setTexture(image_tex);
    image_sprite.setPosition(0, 0);

    sf::Texture overlay_tex;
    overlay_tex.create(W, H);
    sf::Sprite overlay_sprite;
    overlay_sprite.setTexture(overlay_tex);
    overlay_sprite.setPosition(0, 0);

    unsigned int frame = 0;
    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    unsigned int alpha = 0;
    bool begin_sequence = false;
    bool alpha_ascending = true;
    bool show_image = true;
    int current_line = 0u;
    while (window.isOpen())
    {
        frame++;
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::KeyPressed) {
                //cout << event.key.code << endl;
                switch (event.key.code)
                {
                case sf::Keyboard::Escape:
                    window.close();
                    break;
                case sf::Keyboard::Up:
                    current_line--;
                    break;
                case sf::Keyboard::Down:
                    current_line++;
                    break;
                case sf::Keyboard::PageUp:
                    current_line -= page_lines;
                    break;
                case sf::Keyboard::PageDown:
                    current_line += page_lines;
                    break;
                }
                if (current_line < 0) current_line = 0;
                if (current_line > document.size() - 1) current_line = document.size() - 1;
            }
            if (event.type == sf::Event::Closed)
                window.close();
            if (event.type == sf::Event::MouseButtonPressed)
            {
                sf::Vector2i position = sf::Mouse::getPosition(window);
                begin_sequence = true;
            }
        }
        if (alpha > 255) ScramblePixels(255);
        else ScramblePixels(alpha);
        overlay_tex.update(pixels);

        window.clear(sf::Color::White);
        auto display_line = margin + text_y;
        auto document_line = current_line;
        while (display_line + text_display_height < H - margin && document_line < document.size())
        {
            text.setPosition(margin + text_x, display_line);
            text.setString(document[document_line]);
            window.draw(text);
            display_line += text_display_height;
            document_line += 1;
        }
        if (show_image) window.draw(image_sprite);
        window.draw(overlay_sprite);
        window.display();

        if (begin_sequence)
        {
            if (alpha_ascending) {
                alpha++;
                if (alpha > 300) {
                    alpha_ascending = false;
                    alpha = 300;
                    show_image = !show_image;
                }
            }
            else if (alpha > 0) {
                alpha--;
            }
            else
			{
				alpha_ascending = true;
				alpha = 0;
                begin_sequence = false;
			}
		}
    }
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    cout << "Frames per second: " << (frame / time_span.count()) << endl;

    return 0;
}

