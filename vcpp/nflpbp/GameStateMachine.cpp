#include "GameStateMachine.h"
#include <iostream>
#include <stdlib.h>
#include "GameState.h"

extern bool global_silent_mode;

void GameStateMachine::coin_flip_event()
{
	state = kGSMState::ready_for_kickoff;
	if (!global_silent_mode) std::cout << "Coin flip!" << std::endl;
	bool heads_chosen = rand() % 2 == 0;
	if (!global_silent_mode) std::cout << gamestate->awayteam << " chooses " << (heads_chosen ? "Heads" : "Tails") << std::endl;
	bool heads_tossed = rand() % 2 == 0;
	if (!global_silent_mode) std::cout << "Coin comes up " << (heads_tossed ? "Heads" : "Tails") << std::endl;
	if (heads_chosen == heads_tossed)
	{
		if (!global_silent_mode) std::cout << gamestate->awayteam << " wins the flip and defers." << std::endl;
		gamestate->homechoicesecondhalf = false;
		gamestate->homepossession = true;
	}
	else
	{
		if (!global_silent_mode) std::cout << gamestate->hometeam << " wins the flip and defers." << std::endl;
		gamestate->homechoicesecondhalf = true;
		gamestate->homepossession = false;
	}
}

void GameStateMachine::kickoff_received_event()
{
	state = kGSMState::ready_for_scrimmage;
	gamestate->gameclock.stop_clock();
}

void GameStateMachine::own_kickoff_recovered_event()
{
	state = kGSMState::ready_for_scrimmage;
	if (!global_silent_mode) std::cout << "Onside Kick Recovered By Kicking Team!" << std::endl;
	gamestate->gameclock.stop_clock();
}

void GameStateMachine::touchdown_on_kickoff_return_event()
{
	state = kGSMState::ready_for_extra_point;
	gamestate->add_score(6);
	gamestate->balltracker.set_yardline(35);
	gamestate->gameclock.stop_clock();
}

void GameStateMachine::turnover_on_downs_event()
{
	state = kGSMState::ready_for_scrimmage;
	gamestate->change_of_possession();
	gamestate->gameclock.stop_clock();
}

void GameStateMachine::touchdown_on_scrimmage_event()
{
	state = kGSMState::ready_for_extra_point;
	gamestate->add_score(6);
	gamestate->balltracker.set_yardline(35);
	gamestate->gameclock.stop_clock();
}

void GameStateMachine::safety_on_scrimmage_event()
{
	state = kGSMState::ready_for_kickoff;
	gamestate->change_of_possession();
	gamestate->add_score(2);
	gamestate->balltracker.set_yardline(20);
	gamestate->balltracker.reset_dandd();
	gamestate->gameclock.stop_clock();
}

void GameStateMachine::seond_half_kickoff_event()
{
	state = kGSMState::ready_for_kickoff;
	gamestate->gameclock.stop_clock();
}

void GameStateMachine::field_goal_good_event()
{
	state = kGSMState::ready_for_kickoff;
	gamestate->add_score(3);
	gamestate->change_of_possession();
	gamestate->balltracker.set_yardline(35);
	gamestate->gameclock.stop_clock();
}

void GameStateMachine::extra_point_good_event()
{
	state = kGSMState::ready_for_kickoff;
	gamestate->add_score(1);
	gamestate->change_of_possession();
	gamestate->balltracker.set_yardline(35);
	gamestate->gameclock.stop_clock();
}

void GameStateMachine::extra_point_two_points_good_event()
{
	state = kGSMState::ready_for_kickoff;
	gamestate->add_score(2);
	gamestate->change_of_possession();
	gamestate->balltracker.set_yardline(35);
	gamestate->gameclock.stop_clock();
}

void GameStateMachine::extra_point_failed_event()
{
	state = kGSMState::ready_for_kickoff;
	gamestate->change_of_possession();
	gamestate->balltracker.set_yardline(35);
	gamestate->gameclock.stop_clock();
}

bool GameStateMachine::is_start()
{
	return state == kGSMState::start;
}

bool GameStateMachine::is_ready_for_kickoff()
{
	return state == kGSMState::ready_for_kickoff;
}

bool GameStateMachine::is_ready_for_scrimmage()
{
	return state == kGSMState::ready_for_scrimmage;
}

bool GameStateMachine::is_ready_for_extra_point()
{
	return state == kGSMState::ready_for_extra_point;
}
