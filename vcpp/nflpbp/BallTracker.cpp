#include "BallTracker.h"
#include <sstream>
#include <iomanip>

void BallTracker::DeepCopy(BallTracker& other)
{
	already_advanced_down = other.already_advanced_down;
	down = other.down;
	distance = other.distance;
	yardline = other.yardline;
}

void BallTracker::reset()
{
	yardline = 35;
	already_advanced_down = false;
	down = 1;
	distance = 10;
}

void BallTracker::start_play()
{
	already_advanced_down = false;
}

void BallTracker::reset_dandd()
{
	down = 1;
	distance = 10;
	if (distance > yardline) distance = yardline;
}

bool BallTracker::gain_yards(int yards)
{
	auto firstdown = false;
	distance -= yards;
	if (distance <= 0) {
		reset_dandd();
		firstdown = true;
	}
	else {
		if (!already_advanced_down) {
			down += 1;
			already_advanced_down = true;
		}
	}
	yardline -= yards;
	if (yardline < 0) yardline = 0;
	if (distance > yardline) distance = yardline;
	return firstdown;
}

void BallTracker::change_of_possession()
{
	yardline = 100 - yardline;
	reset_dandd();
}

void BallTracker::set_yardline(int yline)
{
	yardline = yline;
}

string BallTracker::get_down_desc()
{
	string desc = "";

	switch (down)
	{
	case 1:
		desc += "1st";
		break;
	case 2:
		desc += "2nd";
		break;
	case 3:
		desc += "3rd";
		break;
	default:
		desc += "4th";
		break;
	}

	return desc;
}

string BallTracker::get_desc()
{
	string desc = get_down_desc();

	if (distance >= yardline) {
		desc += " and goal to go";
	}
	else {
		desc += " and ";
		desc += to_string(distance);
	}

	return desc;
}

vector<string> BallTracker::GetMiniDesc(bool homepos)
{
	vector<string> desc;
	stringstream sstr;
	auto display_dist = to_string(distance);
	if (distance >= yardline) display_dist = "GL";
	sstr << get_down_desc() << " " << setw(2) << right << display_dist << "|";
	desc.push_back(sstr.str());
	sstr.str("");
	if (homepos)
	{
		if (yardline > 50) {
			sstr << "  <<" << setw(2) << right << (100 - yardline);
		}
		else if (yardline < 50) {
			sstr << setw(2) << right << yardline << "<<  ";
		}
		else {
			sstr << " <50< ";
		}
	}
	else
	{
		if (yardline > 50) {
			sstr << setw(2) << right << (100 - yardline) << ">>  ";
		}
		else if (yardline < 50) {
			sstr << "  >>" << setw(2) << right << yardline;
		}
		else {
			sstr << " >50> ";
		}
	}
	sstr << "|";
	desc.push_back(sstr.str());

	return desc;
}

