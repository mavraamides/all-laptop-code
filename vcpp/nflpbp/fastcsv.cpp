#include <iostream>
#include <fstream>

static char line[4096];

bool fast_csv_check(const char* filename, int& max_len, int& max_row, int& max_size)
{
    std::ifstream in(filename);
    if (!in.good()) return false;
    max_len = 0;
    max_row = 0;
    while (in.getline(line, 4095, '\n')) {
        if (strlen(line) > max_len)
        {
            max_len = strlen(line);
        }
        max_row++;
    }
    in.close();
    max_len++;
    max_size = max_row * max_len;
    max_row--;
    return true;
}

void fast_csv_load(const char* filename, char* table, int max_len, int max_size)
{
    auto ptr = 0l;
    std::ifstream in2(filename);

    while (in2.getline(table + ptr, max_len)) {
        ptr += max_len;
        if (ptr + max_len > max_size) break;
    }

    in2.close();
}

int fast_csv_count_fields(const char* data, char delim)
{
    int count = 1;
    char* p = (char *) data;
    while (*p != '\0')
    {
        if (*p == delim) count++;
        p++;
    }
    return count;
}

char * fast_csv_get_next_field(const char* data, char * buffer, char delim)
{
    buffer[0] = '\0';

    char* p = (char *) data;
    char* q = buffer;
    while (*p != '\0')
    {
        if (*p == delim)
        {
            p++;
            break;
        }
        *q = *p;
        q++;
        p++;
    }
    *q = '\0';

    return p;
}

char* fast_csv_get(char* table, int max_len, int max_row, int row, int col)
{
    line[0] = '\0';
    auto ndx = 0;
    auto file_row = row + 1;
    auto in_quote = false;
    if (file_row >= 1 && file_row <= max_row)
    {
        long ptr = file_row * max_len;
        auto cur_col = 0;
        for (int x = ptr; x < ptr + max_len; x++)
        {
            if (table[x] == '"')
            {
                in_quote = !in_quote;
                continue;
            }
            if (!in_quote && table[x] == ',')
            {
                cur_col++;
                continue;
            }
            if (cur_col == col)
            {
                line[ndx++] = table[x];
            }
            if (cur_col > col) {
                break;
            }
        }
        line[ndx] = '\0';
    }
    return line;

}
