// nflpbp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <iomanip>
#include <stdio.h>
#include <chrono>
#include <thread>
#include "BallTracker.h"
#include "GameClock.h"
#include "GameState.h"
#include "GameStateMachine.h"
#include "StageSet.h"
#include "GameData.h"
#include "PlayChooser.h"
#include "PlayExecutor.h"
#include "PlayPrinter.h"
#include "PlayerUsage.h"
#include "WeightedChoice.h"
#include "fastcsv.h"
#include "FastStageSet.h"
#include "EVCalc.h"
#include "GoForTwo.h"
#include "StatAccumulator.h"

using namespace std;
using namespace std::chrono;

bool global_silent_mode = true;

void TestBT()
{
    auto bt = BallTracker();
    cout << bt.get_desc() << endl;
    bt.start_play();
    bt.gain_yards(3);
    cout << bt.get_desc() << endl;
    bt.start_play();
    bt.gain_yards(3);
    cout << bt.get_desc() << endl;
    bt.start_play();
    bt.gain_yards(3);
    cout << bt.get_desc() << endl;
    bt.start_play();
    bt.gain_yards(3);
    cout << bt.get_desc() << endl;
}
void TestGC()
{
    GameState gs("ARI", "SF", true);
    gs.gameclock.print();
    while (gs.gameclock.get_game_seconds_remaining() > 0)
    {
        gs.gameclock.advance(37);
        gs.add_score(1);
        gs.gameclock.print();
    }
}
void TestGD()
{
    GDI->print_stats();
    for (int x = 0; x < 30; x++)
    {
        cout << GDI->get(30978, x) << endl;
    }
    cout << time(NULL) << endl;
    auto t = time(NULL);
    for (int x = 0; x < 10000000; x++)
    {
        auto test = GDI->get(rand() % 30000, rand() % 20);
        auto t = test[0];
        if (x % 1000000 == 0) cout << t;
    }
    cout << endl;
    cout << time(NULL) - t << endl;
}
void TestChoiceScorer()
{
    auto hometeam = "ARI";
    auto awayteam = "SF";
    GameState gs(hometeam, awayteam, true);
    PlayChooser::getInstance()->SetTeams(hometeam, awayteam);
    cout << "Start...";
    auto t = time(NULL);
    for (int x = 0; x < 1000000; x++)
    {
        //auto ndx = PlayChooser::getInstance()->get_match(kPCPlayType::scrimmage, kPCMatchup::home_is_pos, 1, 10, 75, 2000, 0);
        auto ndx = PlayChooser::getInstance()->get_play_ndx(gs);

        //if (ndx < 0) break;
        //string posteam = GDI->get(ndx, KDCol::posteam);
        //string defteam = GDI->get(ndx, KDCol::defteam);
        //string down = GDI->get(ndx, KDCol::down);
        //string togo = GDI->get(ndx, KDCol::ydstogo);
        //string yardline = GDI->get(ndx, KDCol::yardline_100);
        //string time = GDI->get(ndx, KDCol::game_seconds_remaining);
        //string sd = GDI->get(ndx, KDCol::score_differential_post);
        //string ptnfl = GDI->get(ndx, KDCol::play_type_nfl);
        //cout << ndx << " " << posteam << " " << defteam;
        //cout << " " << down;
        //cout << " " << togo;
        //cout << " " << yardline;
        //cout << " " << time;
        //cout << " " << sd;
        //cout << " " << ptnfl;
        //cout << endl;
    }
    cout << time(NULL) - t << endl;
    cout << "Done." << endl;
}
void TestFCSV()
{
    char buffer[] = "xyz,abc,123";
    char result[80];

    cout << fast_csv_count_fields("xyz,abc,123", ',') << endl;
    cout << fast_csv_count_fields("", ',') << endl;
    
    char* p = buffer;
    auto count = fast_csv_count_fields(buffer, ',');
    for (int x = 0; x < count; x++)
    {
        p = fast_csv_get_next_field(p, result, ',');
        cout << result << endl;
    }
}
void TestWeightedChoice()
{
    WeightedChoice wc;

    wc.add_element(0, 1);
    wc.add_element(1, 2);
    wc.add_element(2, 3);
    wc.add_element(3, 4);
    wc.add_element(4, 5);
    wc.add_element(5, 6);
    wc.add_element(6, 7);
    wc.add_element(7, 8);
    wc.add_element(8, 9);
    wc.add_element(9, 10);

    map<int, int> totals;

    for (int x = 0; x < 55000; x++)
    {
        auto ndx = wc.chooese();
        if (totals.count(ndx) == 0) totals[ndx] = 0;
        totals[ndx] ++;
    }

    for (auto kvp : totals)
    {
        cout << setw(5) << kvp.first << " " << kvp.second << endl;
    }

    PlayerUsage* pu = PlayerUsage::getInstance();

    map<string, int> receivers;
    for (int x = 0; x < 1000; x++)
    {
        auto player_name = pu->get_player_name("MIN", kPUPlayerCategory::rusher_player_name_cat);
        if (receivers.count(player_name) == 0) receivers[player_name] = 0;
        receivers[player_name] ++;
    }

    for (auto kvp : receivers)
    {
        cout << setw(20) << left << kvp.first << " " << kvp.second << endl;
    }
}
void TestFSS()
{
    FastStageSet* fss = FastStageSet::getInstance();
    fss->dump(20);

    for (int ndx = 0; ndx < 20; ndx++)
    {
        cout << ndx << ":" << endl;
        auto endx = 0;
        while (true) {
            auto e = fss->get_element(ndx, endx);
            if (e)
            {
                cout << " " << setw(5) << e->count;
                cout << " " << setw(5) << e->token;
                cout << " " << setw(5) << e->value;
                cout << endl;
            }
            else
            {
                break;
            }
            endx++;
        }
    }

    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    for (int ndx = 0; ndx < GDI->get_max_row(); ndx++)
    {
        StageSet ss;
        ss.decode(ndx);
    }
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    cout << "Time: " << time_span.count() << endl;

    t1 = high_resolution_clock::now();
    for (int ndx = 0; ndx < GDI->get_max_row(); ndx++)
    {
        auto e = fss->get_element(ndx, 0);
    }
    t2 = high_resolution_clock::now();
    duration<double> time_span2 = duration_cast<duration<double>>(t2 - t1);
    cout << "Time: " << time_span2.count() << endl;
    cout << "Ratio: " << time_span.count() / time_span2.count() << endl;
}
void TestEV()
{
    vector<string> teams({ "ARI","ATL","BAL","BUF","CAR","CHI","CIN","CLE","DAL","DEN","DET","GB","HOU","IND","JAX","KC","LA","LAC","LV","MIA","MIN","NE","NO","NYG","NYJ","PHI","PIT","SEA","SF","TB","TEN","WAS" });
    for (auto team : teams) {
        cout << "Longest FG " << team << " " << EVCalc::getInstance()->GetLongestFG(team) << endl;
    }
    cout << EVCalc::getInstance()->GetStat("SF", kEVColumn::pos_EV_yl_ev, 13) << endl;
    cout << EVCalc::getInstance()->GetAvg("SF", "ARI", kEVColumn::pos_EV_yl_ev, kEVColumn::def_EV_yl_ev, 13) << endl;
    cout << EVCalc::getInstance()->GetAvg("SF", "ARI", kEVColumn::pos_EV_yl_ev, kEVColumn::def_EV_yl_ev, 80) << endl;
    auto home = "BUF";
    auto away = "BAL";
    auto yl = 35;
    auto togo = 3;
    cout << EVCalc::getInstance()->CalcPuntEV(home, away, yl) << endl;
    cout << EVCalc::getInstance()->CalcFGEV(home, away, yl) << endl;
    cout << EVCalc::getInstance()->CalcRushEV(home, away, yl, togo) << endl;
    cout << EVCalc::getInstance()->CalcPassEV(home, away, yl, togo) << endl;
    cout << EVCalc::EVChoiceToString(EVCalc::getInstance()->GetBestOption(home, away, yl, togo, false)) << endl;
    EVCalc::getInstance()->MakeChart(home, away, true);
    EVCalc::getInstance()->MakeChart(away, home, false);
    EVCalc::getInstance()->PrintChart(true);
    EVCalc::getInstance()->PrintChart(false);
}
void TestGS()
{
    GameState gamestate("SF", "ATL", true);
    gamestate.save("testgs.txt");
    GameState gs_test("", "", false);
    gs_test.load("testgs.txt");
    gs_test.save("testgs2.txt");
}

void PlaySingleGameInteractive(string hometeam, string awayteam, GameStateLogger* logger)
{
    global_silent_mode = false;
    GameState gamestate(hometeam, awayteam, true);
    GameState print_gs("", "", false);
    FastStageSet::getInstance();
    PlayChooser* pc = PlayChooser::getInstance();
    PlayExecutor pe;
    PlayPrinter pp(hometeam, awayteam);
    PlayerUsage* pu = PlayerUsage::getInstance();
    calc_data_t calc_data;
    StageSet ss;
    pc->SetTeams(hometeam, awayteam);

    int ndx = -1;
    int gs_num = 0;

    gamestate.statemachine->coin_flip_event();
    gamestate.save("state", gs_num++);
    if (logger) logger->log(ndx, gamestate);

    //gamestate.load("./teststates/test.txt");
    pp.print_state(gamestate);
    while (!gamestate.game_over())
    {
        ndx = pc->get_play_ndx_monte_carlo(gamestate);
        
        print_gs.deep_copy(gamestate);
        gamestate.save("state", gs_num++);

        pe.execute(gamestate, ndx, calc_data, ss);
        if (logger) logger->log(ndx, gamestate);
        pp.print(print_gs, ndx, ss, calc_data);
        gamestate.print_score_table();

        //if (gamestate.gameclock.get_game_seconds_remaining() > 1920) continue;
        if (gamestate.gameclock.get_game_seconds_remaining() > 1840) continue;
        auto c = (char)getchar();
        if (c == 'q')
        {
            break;
        }
    }
    gamestate.print_score_table();

}
void ReplaySingleGame(string hometeam, string awayteam, GameStateLogger& replay_logger, GameStateLogger* logger, bool interactive_onreplay, bool interactive_onplaychooser, bool quit_on_end_interactive)
{
    if (replay_logger.gamelog.size() == 0) return;
    global_silent_mode = false;

    GameState gamestate(hometeam, awayteam, true);
    GameState print_gs("", "", false);
    FastStageSet::getInstance();
    PlayChooser* pc = PlayChooser::getInstance();
    PlayExecutor pe;
    PlayPrinter pp(hometeam, awayteam);
    PlayerUsage* pu = PlayerUsage::getInstance();
    calc_data_t calc_data;
    StageSet ss;

    pc->SetTeams(hometeam, awayteam);

    int ndx = -1;
    int gs_num = 0;

    gamestate.statemachine->coin_flip_event();
    gamestate.homepossession = replay_logger.gamelog[0].homepossession;
    gamestate.homechoicesecondhalf = replay_logger.gamelog[0].homechoicesecondhalf;
    if (logger) logger->log(ndx, gamestate);

    pp.print_state(gamestate);

    int x = 1;
    bool on_replay = true;
    while (!gamestate.game_over())
    {
        if (x < replay_logger.gamelog.size()) {
            ndx = replay_logger.gamelog[x].ndx;
            x++;
            if (x >= replay_logger.gamelog.size()) {
                on_replay = false;
                if (quit_on_end_interactive) break;
            }
        }
        else {
            ndx = pc->get_play_ndx_monte_carlo(gamestate);
            on_replay = false;
        }

        print_gs.deep_copy(gamestate);
        //gamestate.gameclock.dump_clock_stopped("Main - before execute");
        pe.execute(gamestate, ndx, calc_data, ss);
        //gamestate.gameclock.dump_clock_stopped("Main - after execute");
        if (logger) logger->log(ndx, gamestate);
        pp.print(print_gs, ndx, ss, calc_data);
        gamestate.print_score_table();

        if ((interactive_onreplay && on_replay) ||
            (interactive_onplaychooser && !on_replay))
        {
            auto c = (char)getchar();
            if (c == 'q')
            {
                break;
            }
        }
    }
}
void PlaySingleGameInteractiveFastExecute(string hometeam, string awayteam, GameStateLogger* logger)
{
    FSS;
    global_silent_mode = false;

    GameState gamestate(hometeam, awayteam, true);
    PlayChooser* pc = PlayChooser::getInstance();
    PlayExecutor pe;

    pc->SetTeams(hometeam, awayteam);

    int ndx = -1;
    gamestate.statemachine->coin_flip_event();
    if (logger) logger->log(ndx, gamestate);
    if (logger) logger->print_last_log_entry();

    while (!gamestate.game_over())
    {
        ndx = pc->get_play_ndx(gamestate);

        pe.fast_execute(gamestate, ndx);
        if (logger) logger->log(ndx, gamestate);
        if (logger) logger->print_last_log_entry();

        auto c = (char)getchar();
        if (c == 'q')
        {
            break;
        }
    }
}
void PlayMultipleGamesSilent(string hometeam, string awayteam, int num_games, GameStateLogger* logger)
{
    global_silent_mode = true;

    GameState gamestate(hometeam, awayteam, true);
    PlayChooser* pc = PlayChooser::getInstance();
    PlayExecutor pe;
    PlayerUsage* pu = PlayerUsage::getInstance();
    calc_data_t calc_data;
    StageSet ss;

    int total_home_score = 0;
    int total_away_score = 0;
    int total_home_wins = 0;
    int total_home_losses = 0;
    int total_ties = 0;
    int total_games = 0;
    pc->SetTeams(hometeam, awayteam);
    EVCalc::getInstance()->MakeCharts(hometeam, awayteam);

    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    for (int x = 0; x < num_games; x++) {
        int ndx = -1;
        gamestate.statemachine->coin_flip_event();
        if (logger) logger->log(ndx, gamestate);

        while (!gamestate.game_over())
        {
            ndx = pc->get_play_ndx(gamestate);
            pe.execute(gamestate, ndx, calc_data, ss);
            if (logger) logger->log(ndx, gamestate);
        }
        total_home_score += gamestate.homescore;
        total_away_score += gamestate.awayscore;
        if (gamestate.homescore + 1.89f > gamestate.awayscore) {
            total_home_wins++;
        }
        else if (gamestate.homescore + 1.89f < gamestate.awayscore) {
            total_home_losses++;
        }
        else {
            total_ties++;
        }
        if (total_games % 1000 == 0) cout << endl;
        if (total_games % 100 == 0) cout << ".";
        total_games += 1;
        gamestate.reset();
    }
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    cout << endl;
    cout << hometeam << ": " << total_home_score << " " << awayteam << ": " << total_away_score << " games: " << total_games << endl;
    cout << "Average score: " << (float)total_home_score / (float)total_games << " - " << (float)total_away_score / (float)total_games << endl;
    cout << hometeam << " goes: " << total_home_wins << "-" << total_home_losses << "-" << total_ties << " (" << (((float)total_home_wins + ((float)total_ties / 2.0f)) / (float)total_games) << ")" << endl;
    cout << "Prediction: " << hometeam << " " << ((((float)total_away_score - (float)total_home_score) / (float)total_games) - 1.89f) << endl;
    cout << "Time: " << time_span.count() << endl;
    cout << "Games per second: " << (total_games / time_span.count()) << endl;
}
void PlayMultipleGamesSilentFastExecute(string hometeam, string awayteam, int num_games, GameStateLogger* logger, StatAccumulator* sa, float hfa, float spread = 0.0f)
{
    global_silent_mode = true;

    GameState gamestate(hometeam, awayteam, true);
    PlayChooser* pc = PlayChooser::getInstance();
    PlayExecutor pe;
    FastStageSet::getInstance();

    float total_home_score = 0;
    float total_away_score = 0;
    int total_home_wins = 0;
    int total_home_losses = 0;
    int total_ties = 0;
    int total_games = 0;
    pc->SetTeams(hometeam, awayteam);
    EVCalc::getInstance()->MakeCharts(hometeam, awayteam);

    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    for (int x = 0; x < num_games; x++) {
        int ndx = -1;
        gamestate.statemachine->coin_flip_event();
        if (logger) logger->log(ndx, gamestate);

        while (!gamestate.game_over())
        {
            ndx = pc->get_play_ndx(gamestate);
            pe.fast_execute(gamestate, ndx);
            if (logger) logger->log(ndx, gamestate);
        }
        total_home_score += gamestate.homescore;
        total_away_score += gamestate.awayscore;
        if (gamestate.homescore + hfa + spread > gamestate.awayscore) {
            total_home_wins++;
        }
        else if (gamestate.homescore + hfa + spread < gamestate.awayscore) {
            total_home_losses++;
        }
        else {
            total_ties++;
        }
        if (total_games % 5000 == 0) cout << endl;
        if (total_games % 100 == 0) cout << ".";
        total_games += 1;
        gamestate.reset();
    }
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    if (sa) sa->AddGame(hometeam, awayteam, total_home_score, total_away_score, total_home_wins, total_home_losses, total_ties);
    cout << endl;
    cout << hometeam << ": " << total_home_score << " " << awayteam << ": " << total_away_score << " games: " << total_games << endl;
    cout << "Average score: " << (float)total_home_score / (float)total_games << " - " << (float)total_away_score / (float)total_games << endl;
    cout << hometeam << " goes: " << total_home_wins << "-" << total_home_losses << "-" << total_ties << " (" << (((float)total_home_wins + ((float)total_ties / 2.0f)) / (float)total_games) << ")" << endl;
    cout << "Prediction: " << hometeam << " " << ((((float)total_away_score - (float)total_home_score) / (float)total_games) - hfa) << endl;
    cout << "Time: " << time_span.count() << endl;
    cout << "Games per second: " << (total_games / time_span.count()) << endl;
}
void PlayMultipleGames_thread(string hometeam, string awayteam, int num_games, StatAccumulator * sa, float hfa, char name)
{
    // thread section
    PlayChooser* pc = PlayChooser::getInstance();
    PlayExecutor pe;
    GameState gamestate(hometeam, awayteam, true);
    float total_home_score = 0;
    float total_away_score = 0;
    int total_home_wins = 0;
    int total_home_losses = 0;
    int total_ties = 0;
    int total_games = 0;

    for (int x = 0; x < num_games; x++) {
        int ndx = -1;
        gamestate.statemachine->coin_flip_event();

        while (!gamestate.game_over())
        {
            ndx = pc->get_play_ndx(gamestate);
            pe.fast_execute(gamestate, ndx);
        }
        total_home_score += gamestate.homescore;
        total_away_score += gamestate.awayscore;
        if (gamestate.homescore + hfa > gamestate.awayscore) {
            total_home_wins++;
        }
        else if (gamestate.homescore + hfa < gamestate.awayscore) {
            total_home_losses++;
        }
        else {
            total_ties++;
        }
        if (total_games % 100 == 0) cout << name;
        total_games += 1;
        gamestate.reset();
    }
    if (sa) sa->AddGame(hometeam, awayteam, total_home_score, total_away_score, total_home_wins, total_home_losses, total_ties);

}
void PlayMultipleGamesSilentFastThreaded(string hometeam, string awayteam, int num_games, int num_threads, float hfa)
{
    global_silent_mode = true;
    PlayChooser* pc = PlayChooser::getInstance();
    pc->SetTeams(hometeam, awayteam);
    FastStageSet::getInstance();
    EVCalc::getInstance()->MakeCharts(hometeam, awayteam);
    StatAccumulator * sa = new StatAccumulator[num_threads];

    high_resolution_clock::time_point t1 = high_resolution_clock::now();

    vector<thread> threads;
    for (int x = 0; x < num_threads; x++)
    {
        threads.push_back(thread(PlayMultipleGames_thread, hometeam, awayteam, num_games, &sa[x], hfa, (char)('a' + x)));
    }
    for (int x = 0; x < threads.size(); x++)
    {
        threads[x].join();
        cout << " [" << (char)('a' + x) << "] ";
    }

    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    cout << endl;
 
    StatAccumulator totals;
    for (int x = 0; x < num_threads; x ++)
    {
        sa[x].Dump();
        totals.Merge(sa[x]);
    }
    cout << "------------------------------------------------------------------------" << endl;
    totals.Dump();
    auto total_games = num_games * num_threads;
    cout << "Total Games:      " << total_games << endl;
    cout << "Total Time:       " << time_span.count() << endl;
    cout << "Games per second: " << (total_games / time_span.count()) << endl;
    delete[] sa;
}
void RunRoundRobin(int games)
{
    vector<string> teams({ "ARI","ATL","BAL","BUF","CAR","CHI","CIN","CLE","DAL","DEN","DET","GB","HOU","IND","JAX","KC","LA","LAC","LV","MIA","MIN","NE","NO","NYG","NYJ","PHI","PIT","SEA","SF","TB","TEN","WAS" });

    StatAccumulator sa;
    for (int team1ndx = 0; team1ndx < teams.size() - 1; team1ndx++) {
        auto team1 = teams[team1ndx];
        for (int team2ndx = team1ndx + 1; team2ndx < teams.size(); team2ndx++) {
            auto team2 = teams[team2ndx];
            cout << team1 << " vs " << team2 << endl;
            PlayMultipleGamesSilentFastExecute(team1, team2, games, nullptr, &sa, 0.0f);
        }
    }
    sa.Dump();
    sa.Write("ratings.csv");
}

int main()
{
    cout << "Main loop started..." << endl;
    string hometeam = "GB";
    string awayteam = "TB";
    srand((unsigned int) time(NULL));
    if (!GDI->data_loaded) return 1;
    if (!EVCalc::getInstance()->data_loaded) return 1;
    if (!GoForTwo::getInstance()->data_loaded) return 1;
    GameStateLogger logger;
    GameStateLogger logger2;
    
    //TestEV();
    auto play_type = 4;
    if (play_type == 0)
    {
        PlaySingleGameInteractive(hometeam, awayteam, &logger);
        logger.save("lastgame.csv");
    }
    else if (play_type == 1)
    {
        PlaySingleGameInteractiveFastExecute(hometeam, awayteam, &logger);
        logger.save("lastgame.csv");
        ReplaySingleGame(hometeam, awayteam, logger, &logger2, false, true, true);
        logger.compare(logger2, true);
    }
    else if (play_type == 2)
    {
        logger.load("lastgame.csv");
        //logger.load("teststates/safety.csv");
        ReplaySingleGame(hometeam, awayteam, logger, &logger2, false, true, false);
    }
    else if (play_type == 3)
    {
        //PlayMultipleGamesSilent(hometeam, awayteam, 10000, nullptr);
        PlayMultipleGamesSilentFastExecute(hometeam, awayteam, 10000, nullptr, nullptr, 0.0f);
    }
    else if (play_type == 4)
    {
        StatAccumulator sa;

        PlayMultipleGamesSilentFastExecute("KC", "TB", 100000, nullptr, &sa, 0.0f, -3.5f);

        sa.Dump();
        sa.Write("superbowl.csv");
    }
    else if (play_type == 5)
    {
        high_resolution_clock::time_point t1 = high_resolution_clock::now();

        RunRoundRobin(10000);

        high_resolution_clock::time_point t2 = high_resolution_clock::now();
        duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

        cout << "Total Time:       " << time_span.count() << endl;
    }
    else if (play_type == 6)
    {
        PlayMultipleGamesSilentFastThreaded(hometeam, awayteam, 20000, 20, 0.0f);
    }
    //cout << PlayChooser::getInstance()->max_hash << endl;
 }

// ADD_PLAY_TYPE
