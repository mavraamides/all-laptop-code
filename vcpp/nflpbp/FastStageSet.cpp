#include "FastStageSet.h"
#include "StageSet.h"
#include "GameData.h"
#include <iostream>
#include <iomanip>

static FastStageSetElement* stage_set_data = nullptr;
static StageSetIndex * stage_set_ndx = nullptr;
FastStageSet* FastStageSet::instance = nullptr;


FastStageSet::FastStageSet()
{
	cout << "Initializing fast stage set...";
	max_element = 0;

	StageSet ss;
	max_index = GDI->get_max_row();
	for (int ndx = 0; ndx < max_index; ndx++)
	{
		ss.decode(ndx);
		for (auto s : ss.stages)
		{
			if (get_token(s.token) != kFSSTokens::no_token_fss)
			{
				max_element++;
			}
		}
	}
	stage_set_data = new FastStageSetElement[max_element];
	stage_set_ndx = new StageSetIndex[max_index];

	int cur_stage_set_ndx = 0;
	int working_stage_set_ndx = 0;
	for (int ndx = 0; ndx < max_index; ndx++)
	{
		ss.decode(ndx);
		bool first = true;
		stage_set_ndx[ndx].index = -1;
		for (auto s : ss.stages)
		{
			stage_set_data[cur_stage_set_ndx].count = 0;
			auto token = get_token(s.token);
			if (token != kFSSTokens::no_token_fss)
			{
				if (first)
				{
					stage_set_ndx[ndx].index = cur_stage_set_ndx;
					working_stage_set_ndx = cur_stage_set_ndx;
					stage_set_ndx[ndx].play_time = GDI->get_int(ndx, KDCol::play_time);
					stage_set_ndx[ndx].play_time_run_clock = GDI->get_int(ndx, KDCol::play_time_run_clock);
					stage_set_ndx[ndx].play_time_hurry_up = GDI->get_int(ndx, KDCol::play_time_hurry_up);
					stage_set_ndx[ndx].yards_gained = GDI->get_int(ndx, KDCol::yards_gained);
				}
				first = false;
				stage_set_data[working_stage_set_ndx].count++;
				stage_set_data[cur_stage_set_ndx].token = token;
				stage_set_data[cur_stage_set_ndx].value = s.value;
				if (token == kFSSTokens::penalty_fss)
				{
					stage_set_data[cur_stage_set_ndx].penalty_flags = 0x0;
					for (auto ei : s.extra_info)
					{
						if (ei == "ONOFFENSE") stage_set_data[cur_stage_set_ndx].penalty_flags |= 0x1;
						else if (ei == "ONDEFENSE") stage_set_data[cur_stage_set_ndx].penalty_flags |= 0x2;
						else if (ei == "AUTOFIRSTDOWN") stage_set_data[cur_stage_set_ndx].penalty_flags |= 0x4;
						else if (ei == "SPOTFOUL") stage_set_data[cur_stage_set_ndx].penalty_flags |= 0x8;
						else if (ei == "DOWNOVER") stage_set_data[cur_stage_set_ndx].penalty_flags |= 0x10;
						else if (ei == "ADDTOYARDAGE") stage_set_data[cur_stage_set_ndx].penalty_flags |= 0x20;
						else {
							cout << "!!! UNKNOWN TAG !!! " << ei << endl;
						}
					}
				}
				cur_stage_set_ndx++;
				if (cur_stage_set_ndx >= max_element) break;
			}
		}
		if (cur_stage_set_ndx >= max_element) break;
	}
	cout << "Done." << endl;
}

kFSSTokens FastStageSet::get_token(string desc)
{
	kFSSTokens retVal = kFSSTokens::no_token_fss;

	// ADD_PLAY_TYPE_STAGE
	if (desc == "KICK_OFF")	{
		retVal = kFSSTokens::kick_off_fss;
	}
	else if (desc == "KICK_OFF_RETURN") {
		retVal = kFSSTokens::kick_off_return_fss;
	}
	else if (desc == "GAIN") {
		retVal = kFSSTokens::gain_fss;
	}
	else if (desc == "CHANGE_OF_POSSESSION") {
		retVal = kFSSTokens::change_of_possession_ffs;
	}
	else if (desc == "XP_KICK") {
		retVal = kFSSTokens::xp_kick_fss;
	}
	else if (desc == "FIELD_GOAL") {
		retVal = kFSSTokens::field_goal_fss;
	}
	else if (desc == "PUNT") {
		retVal = kFSSTokens::punt_fss;
	}
	else if (desc == "RETURN") {
		retVal = kFSSTokens::return_fss;
	}
	else if (desc == "PUNT_BLOCKED") {
		retVal = kFSSTokens::punt_blocked_fss;
	}
	else if (desc == "PASS") {
		retVal = kFSSTokens::pass_fss;
	}
	else if (desc == "GAIN_AFTER_CATCH") {
		retVal = kFSSTokens::gain_after_catch_fss;
	}
	else if (desc == "PAT2") {
		retVal = kFSSTokens::two_point_conv_fss;
	}
	else if (desc == "PENALTY") {
		retVal = kFSSTokens::penalty_fss;
	}

	return retVal;
}

void FastStageSet::dump(int num)
{
	for (int ndx = 0; ndx < num; ndx++)
	{
		auto ssndx = stage_set_ndx[ndx].index;
		cout << setw(5) << ndx << " " << setw(5) << ssndx;
		if (ssndx >= 0 && ssndx < max_element) {
			for (int endx = 0; endx < stage_set_data[ssndx].count; endx++) {
				if (endx > 0) cout << "           ";
				dump_element(ssndx + endx);
			}
		}
		else {
			cout << endl;
		}
	}
}

void FastStageSet::dump_element(int ssndx)
{
	if (ssndx >= 0 && ssndx < max_element) {
		cout << " " << setw(5) << stage_set_data[ssndx].count;
		cout << " " << setw(5) << stage_set_data[ssndx].token;
		cout << " " << setw(5) << stage_set_data[ssndx].value;
		cout << endl;
	}
}

FastStageSetElement* FastStageSet::get_element(int ndx, int endx)
{
	FastStageSetElement * element = nullptr;

	if (ndx >= 0 && ndx < max_index)
	{
		auto ssndx = stage_set_ndx[ndx].index;
		if (ssndx >= 0 && ssndx < max_element && endx < stage_set_data[ssndx].count)
		{
			element = &(stage_set_data[ssndx + endx]);
		}
	}

	return element;
}

StageSetIndex* FastStageSet::get_index(int ndx)
{
	StageSetIndex* index = nullptr;

	if (ndx >= 0 && ndx < max_index)
	{
		index = &(stage_set_ndx[ndx]);
	}

	return index;
}
