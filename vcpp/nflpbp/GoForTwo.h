#pragma once
#include <string>
using namespace std;


class GoForTwo
{
	static GoForTwo* instance;
	int max_len = 0;
	int max_row = 0;
	int max_size = 0;

	GoForTwo();
public:
	bool data_loaded = false;
	static GoForTwo* getInstance()
	{
		if (instance == nullptr)
		{
			instance = new GoForTwo();
		}
		return instance;
	}
	int check(int scoredif, int poss_left);
	void print();
	~GoForTwo();
};

