#include "PlayPrinter.h"
#include "GameState.h"
#include <iostream>
#include <set>
#include "PlayerUsage.h"

extern set<string> valid_play_types;

PlayPrinter::PlayPrinter(string hometeam, string awayteam)
{
	home = hometeam;
	away = awayteam;
}

string PlayPrinter::get_pos_desc(string posteam, string defteam, int yardline)
{
	string desc;
	if (yardline == 50) {
		desc = "midfield.";
	}
	else if (yardline > 50)
	{
		desc = "the " + posteam + " " + to_string(100 - yardline);
	}
	else
	{
		desc = "the " + defteam + " " + to_string(yardline);
	}
	return desc;
}

string PlayPrinter::get_yardline_desc(string posteam, string defteam, int yardline)
{
	string desc;
	if (yardline == 50) {
		desc = "at midfield.";
	}
	else if (yardline > 50)
	{
		desc = "on " + posteam + " " + to_string(100 - yardline);
	}
	else
	{
		desc = "on " + defteam + " " + to_string(yardline);
	}
	return desc;
}

void PlayPrinter::accum_single_set(string teamname, bool posteam, string category, kPCPlayType playtype)
{
}

void PlayPrinter::accum_player_names()
{
}

string PlayPrinter::get_player_name(string type, bool home)
{
	return string();
}

string PlayPrinter::get_or_substitute(bool substitute, string subname, bool home, int ndx, KDCol column)
{
	return string();
}

void PlayPrinter::get_starting_qbs()
{
}

void PlayPrinter::print_state(GameState &gamestate)
{
	sep();
	if (gamestate.statemachine->is_ready_for_kickoff())
	{
		cout << gamestate.get_posteam() << " receiving kickoff from ";
		cout << gamestate.get_defteam() << " " << get_yardline_desc(gamestate.get_posteam(), gamestate.get_defteam(), gamestate.balltracker.yardline);
	}
	else
	{
		cout << gamestate.get_posteam();
		if (gamestate.get_clock_mode_by_pos(true) == kGSClockMode::hurry_up_cm) cout << "+";
		else if (gamestate.get_clock_mode_by_pos(true) == kGSClockMode::run_clock_cm) cout << "-";
		cout << " has the ball ";
		cout << gamestate.balltracker.get_desc() << " " << get_yardline_desc(gamestate.get_posteam(), gamestate.get_defteam(), gamestate.balltracker.yardline);
	}
	cout << endl;
	gamestate.gameclock.print();
	sep();
}

int PlayPrinter::get_int_value(int ndx, KDCol col, int def_value)
{
	return 0;
}

bool PlayPrinter::replace(std::string& str, const std::string& from, const std::string& to) {
	size_t start_pos = str.find(from);
	if (start_pos == std::string::npos)
		return false;
	str.replace(start_pos, from.length(), to);
	return true;
}
/*
+-----+---------------+------+---------------------------------------------------------------------------
|1st Q|TTT <B> TTT    |1st 13|
|99:99|00  *** 00  ***|TTT 37|
+-----+---------------+------+---------------------------------------------------------------------------

*/
void PlayPrinter::print_stage_set_desc(GameState& gamestate, StageSet &ss, map<string, string> &team_data, calc_data_t &calc_data, int ndx)
{
	vector<string> play_desc;
	bool first = true;
	for (auto stage : ss.stages)
	{
		auto desc = stage.description;
		if (first)
		{
			desc = "(" + to_string(ndx) + ") " + stage.description;
			first = false;
		}
		for (auto tt : stage.team_tags)
		{
			auto replace_key = "[" + tt + "]";
			replace(desc, replace_key, team_data[tt]);
		}

		for (auto ct : stage.calc_tags)
		{
			if (replace(ct, "get_pos_desc=", "")) {
				auto replace_key = "[" + ct + "]";
				replace(desc, replace_key, get_pos_desc(team_data["posteamname"], team_data["defteamname"], atoi(calc_data[ct].c_str())));
			}
			else {
				auto replace_key = "[" + ct + "]";
				replace(desc, replace_key, calc_data[ct]);
			}
		}

		replace(desc, "[stage_value]", to_string(stage.value));
		play_desc.push_back(desc);
	}

	auto mini_clock = gamestate.gameclock.GetMiniDesc();
	auto mini_score = gamestate.get_mini_desc();
	auto mini_ball = gamestate.balltracker.GetMiniDesc(gamestate.homepossession);
	auto line = 0;
	cout << "+-----+---------------+------+---------------------------------------------------------------------------" << endl;
	while (true)
	{
		if (line >= mini_clock.size() && line >= mini_score.size() && line >= mini_ball.size() && line >= play_desc.size()) break;

		auto printed_line = false;
		if (line < mini_clock.size())
		{
			cout << mini_clock[line];
			printed_line = true;
		}
		else
		{
			cout << "|     |";
		}

		if (line < mini_score.size())
		{
			cout << mini_score[line];
			printed_line = true;
		}
		else
		{
			cout << "               |";
		}

		if (line < mini_ball.size())
		{
			cout << mini_ball[line];
			printed_line = true;
		}
		else
		{
			cout << "      |";
		}

		if (line < play_desc.size())
		{
			cout << play_desc[line];
			printed_line = true;
		}

		line++;
		if (!printed_line) break;
		cout << endl;
	}
	cout << "+-----+---------------+------+---------------------------------------------------------------------------" << endl;
}

void PlayPrinter::print(GameState& gamestate, int ndx, StageSet &ss, calc_data_t &calc_data)
{
	string play_type_nfl = GDI->get(ndx, KDCol::play_type_nfl);
	if (valid_play_types.count(play_type_nfl) == 0) return;

	string posteam = GDI->get(ndx, KDCol::posteam);
	string defteam = GDI->get(ndx, KDCol::defteam);

	auto posishome = false;

	if (posteam == home) {
		posishome = true;
	}
	else if(defteam == away) {
		posishome = true;
	}

	auto posteamname = home;
	auto defteamname = away;
	if (!posishome) {
		posteamname = away;
		defteamname = home;
	}

	map<string, string> team_data;
	team_data["posteamname"] = posteamname;
	team_data["defteamname"] = defteamname;
	if (play_type_nfl == "KICK_OFF")
	{
		team_data["kicker_player_name"] = PlayerUsage::getInstance()->get_player_name(defteamname, kPUPlayerCategory::kicker_player_name_KO_cat);
		team_data["kickoff_returner_player_name"] = PlayerUsage::getInstance()->get_player_name(posteamname, kPUPlayerCategory::kickoff_returner_player_name_cat);
	}
	else if (play_type_nfl == "RUSH")
	{
		team_data["qb_rusher_player_name"] = PlayerUsage::getInstance()->get_player_name(posteamname, kPUPlayerCategory::passer_player_name_cat);
		team_data["rusher_player_name"] = PlayerUsage::getInstance()->get_player_name(posteamname, kPUPlayerCategory::rusher_player_name_cat);
	}
	else if (play_type_nfl == "FIELD_GOAL") {
		team_data["kicker_player_name"] = PlayerUsage::getInstance()->get_player_name(posteamname, kPUPlayerCategory::kicker_player_name_FG_cat);
	}
	else if (play_type_nfl == "PUNT") {
		team_data["punter_player_name"] = PlayerUsage::getInstance()->get_player_name(posteamname, kPUPlayerCategory::punter_player_name_cat);
		team_data["punt_returner_player_name"] = PlayerUsage::getInstance()->get_player_name(posteamname, kPUPlayerCategory::punt_returner_player_name_cat);
	}
	else if (play_type_nfl == "PASS") {
		team_data["passer_player_name"] = PlayerUsage::getInstance()->get_player_name(posteamname, kPUPlayerCategory::passer_player_name_cat);
		team_data["receiver_player_name"] = PlayerUsage::getInstance()->get_player_name(posteamname, kPUPlayerCategory::receiver_player_name_cat);
	}
	else if (play_type_nfl == "SACK") {
		team_data["passer_player_name"] = PlayerUsage::getInstance()->get_player_name(posteamname, kPUPlayerCategory::passer_player_name_cat);
	}

	print_stage_set_desc(gamestate, ss, team_data, calc_data, ndx);
}

void PlayPrinter::sep()
{
	cout << "--------------------------------------------------------------------------------" << endl;
}
