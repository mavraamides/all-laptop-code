#pragma once
#include <string>
#include <vector>

typedef std::vector<std::string> vstr_t;

class Stage
{
public:
	std::string token;
	int value = 0;
	std::string description;
	vstr_t team_tags;
	vstr_t calc_tags;
	vstr_t extra_info;

	Stage() {
		clear();
	}
	~Stage() {
		team_tags.clear();
		calc_tags.clear();
		extra_info.clear();
	}
	void clear();
	void set(std::string t, int v, std::string d, vstr_t tt, vstr_t ct, vstr_t ei);
	void dump();
};

class StageSet
{	
public:
	std::vector<Stage> stages;

	StageSet() {};
	~StageSet() {
		clear();
	};

	void add_stage(std::string t, int v, std::string d,	vstr_t tt, vstr_t ct, vstr_t ei);
	void clear();
	void decode(int ndx);
	void dump();

};

