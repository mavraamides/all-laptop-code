#pragma once

class GameState;

enum kGSMState {
	start,
	ready_for_kickoff,
	ready_for_scrimmage,
	ready_for_extra_point
};

class GameStateMachine
{
	kGSMState state = kGSMState::start;
	GameState* gamestate = nullptr;
public:
	friend class GameState;
	GameStateMachine(GameState* gs) {
		gamestate = gs;
	};
	~GameStateMachine() {};
	void reset() {
		state = kGSMState::start;
	};
	void DeepCopy(GameStateMachine& other) {
		state = other.state;
	}
	kGSMState get_state() { return state; }
	void coin_flip_event();
	
	void kickoff_received_event();
	void own_kickoff_recovered_event();
	void touchdown_on_kickoff_return_event();
	
	void turnover_on_downs_event();
	void touchdown_on_scrimmage_event();
	void safety_on_scrimmage_event();
	void seond_half_kickoff_event();
	void field_goal_good_event();
	
	void extra_point_good_event();
	void extra_point_two_points_good_event();
	void extra_point_failed_event();

	bool is_start();
	bool is_ready_for_kickoff();
	bool is_ready_for_scrimmage();
	bool is_ready_for_extra_point();
};

