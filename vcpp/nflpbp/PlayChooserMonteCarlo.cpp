#include "PlayChooserMonteCarlo.h"
#include "PlayChooser.h"
#include "PlayExecutor.h"
#include "PlayPrinter.h"
#include "PlayerUsage.h"
#include <iostream>
#include <chrono>
using namespace std::chrono;

extern bool global_silent_mode;

bool PlayChooserMonteCarlo::get_best_clock_mode(GameState& gamestate, vector< kGSClockMode> &options, kGSClockMode& clock_mode)
{
    if (options.size() == 0) return false;
    double time_per_choice = 1.00 / (double)options.size();

    bool hold_global_silent = global_silent_mode;

    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    global_silent_mode = true;

    bool found_one = false;
    bool found_best_score = false;
    bool found_best_pct = false;
    float best_score = -1000.0f;
    float best_pct = 0.0f;
    bool best_win_pct_tied = false;

    kGSClockMode best_clock_mode_score = options[0];
    kGSClockMode best_clock_mode_pct = options[0];

    GameState gs("", "", false);
    for (auto cm : options)
    {
        cout << "Option: " << cm;
        auto score = 0.0f;
        auto win_pct = 0.0f;
        gs.deep_copy(gamestate);
        gs.set_clock_mode_by_pos(cm, true);
        if (get_score_for_option(gs, kPCPlayType::scrimmage, score, win_pct, time_per_choice))
        {
            if (score > best_score)
            {
                best_score = score;
                best_clock_mode_score = cm;
                found_best_score = true;
            }
            if (win_pct > best_pct)
            {
                best_pct = win_pct;
                best_clock_mode_pct = cm;
                found_best_pct = true;
                best_win_pct_tied = false;
            }
            else if (win_pct == best_pct)
            {
                best_win_pct_tied = true;
            }
            cout << " score: " << score << " win pct: " << win_pct;;
        }
        cout << endl;
    }

    if (found_best_pct)
    {
        if (!best_win_pct_tied)
        {
            clock_mode = best_clock_mode_pct;
            found_one = true;
        }
        else if (found_best_score)
        {
            clock_mode = best_clock_mode_score;
            found_one = true;
        }
    }

    global_silent_mode = hold_global_silent;
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    cout << "Total Time:       " << time_span.count() << endl;

    return found_one;
}

bool PlayChooserMonteCarlo::get_best_option(GameState& gamestate, vector<kPCPlayType> options, kPCPlayType& play_type)
{
    if (options.size() == 0) return false;
    double time_per_choice = 1.00 / (double)options.size();
    bool hold_global_silent = global_silent_mode;

    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    global_silent_mode = true;

    bool found_one = false;
    bool found_best_score = false;
    bool found_best_pct = false;
    float best_score = -1000.0f;
    float best_pct = 0.0f;
    bool best_win_pct_tied = false;
    kPCPlayType best_play_type_score = options[0];
    kPCPlayType best_play_type_pct = options[0];

    for (auto pt : options)
    {
        cout << "Option: " << pt;
        auto score = 0.0f;
        auto win_pct = 0.0f;
        if (get_score_for_option(gamestate, pt, score, win_pct, time_per_choice))
        {
            if (score > best_score)
            {
                best_score = score;
                best_play_type_score = pt;
                found_best_score = true;
            }
            if (win_pct > best_pct)
            {
                best_pct = win_pct;
                best_play_type_pct = pt;
                found_best_pct = true;
                best_win_pct_tied = false;
            }
            else if (win_pct == best_pct)
            {
                best_win_pct_tied = true;
            }
            cout << " score: " << score << " win pct: " << win_pct;;
        }
        cout << endl;
    }

    if (found_best_pct && found_best_score)
    {
        if (gamestate.gameclock.get_half() == 2 && !best_win_pct_tied)
        {
            play_type = best_play_type_pct;
            found_one = true;
        }
        else
        {
            play_type = best_play_type_score;
            found_one = true;
        }
    }
    else if (found_best_pct)
    {
        play_type = best_play_type_pct;
        found_one = true;
    }
    else if (found_best_pct)
    {
        play_type = best_play_type_score;
        found_one = true;
    }

    global_silent_mode = hold_global_silent;
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    cout << "Total Time:       " << time_span.count() << endl;

    return found_one;
}

bool PlayChooserMonteCarlo::get_timeout_spike_option(GameState& gamestate, vector<kPCMCTimeoutSpike> options, vector<int> play_times, kPCMCTimeoutSpike& timeout_spike_choice, bool defense_check)
{
    if (options.size() == 0) return false;
    if (options.size() > play_times.size()) return false;
    double time_per_choice = 1.00 / (double)options.size();
    bool hold_global_silent = global_silent_mode;

    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    global_silent_mode = true;

    float first_option_bonus = 1.01f;
    bool found_one = false;
    bool found_best_score = false;
    bool found_best_pct = false;
    float best_score = -1000.0f;
    float best_pct = 0.0f;
    bool best_win_pct_tied = false;
    kPCMCTimeoutSpike best_timeout_spike_score = options[0];
    kPCMCTimeoutSpike best_timeout_spike_pct = options[0];

    GameState gs("", "", false);
    auto play_time_ndx = 0;
    for (auto pt : options)
    {
        cout << "Option: " << pt;
        auto score = 0.0f;
        auto win_pct = 0.0f;
        gs.deep_copy(gamestate);
        gs.gameclock.advance(play_times[play_time_ndx]);
        if (pt == kPCMCTimeoutSpike::spike_pcmp) {
            gs.balltracker.down++;
        }
        if (get_score_for_option(gs, kPCPlayType::scrimmage, score, win_pct, time_per_choice))
        {
            if (defense_check) {
                score = -score;
                win_pct = 1.0f - win_pct;
            }
            if (score > best_score)
            {
                best_score = score;
                best_timeout_spike_score = pt;
                found_best_score = true;
            }
            if (win_pct > best_pct)
            {
                best_pct = win_pct;
                best_timeout_spike_pct = pt;
                found_best_pct = true;
                best_win_pct_tied = false;
            }
            else if (win_pct == best_pct)
            {
                best_win_pct_tied = true;
            }
            cout << " score: " << score << " win pct: " << win_pct;;
        }
        cout << endl;
    }

    if (found_best_pct && found_best_score)
    {
        if (gamestate.gameclock.get_half() == 2 && !best_win_pct_tied)
        {
            timeout_spike_choice = best_timeout_spike_pct;
            found_one = true;
        }
        else
        {
            timeout_spike_choice = best_timeout_spike_score;
            found_one = true;
        }
    }
    else if (found_best_pct)
    {
        timeout_spike_choice = best_timeout_spike_pct;
        found_one = true;
    }
    else if (found_best_pct)
    {
        timeout_spike_choice = best_timeout_spike_score;
        found_one = true;
    }

    global_silent_mode = hold_global_silent;
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    cout << "Total Time:       " << time_span.count() << endl;

    return found_one;
}

bool PlayChooserMonteCarlo::get_score_for_option(GameState& gamestate, kPCPlayType play_type, float &score, float& win_pct, double max_time)
{

    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    bool hold_global_silent = global_silent_mode;
    global_silent_mode = true;

    PlayChooser* pc = PlayChooser::getInstance();
    PlayExecutor pe;

    int num_games = 50000;
    score = 0.0f;
    win_pct = 0.0f;
    auto wins = 0.0f;
    auto starting_score = (float)gamestate.get_score_diff_post(gamestate.homepossession);
    GameState gs("", "", false);

    int game_num = 1;
    for (game_num = 1; game_num < num_games; game_num++)
    {
        gs.deep_copy(gamestate);
        auto ndx = pc->get_play_ndx_force_play_type(gs, play_type);
        if (ndx == -1)
        {
            score = -100.0f;
            win_pct = 0.0f;
            global_silent_mode = hold_global_silent;
            return false;
        }
        pe.fast_execute(gs, ndx);
        while (gs.gameclock.get_half() == gamestate.gameclock.get_half() && !gs.game_over())
        {
            ndx = pc->get_play_ndx(gs);
            pe.fast_execute(gs, ndx);
        }
        auto game_score = gs.get_score_diff_post(gamestate.homepossession);
        score += (float)(game_score - starting_score);
        if (game_score > 0) wins += 1.0f;
        else if (game_score == 0) wins += 0.5f;

        if (game_num % 100 == 0)
        {
            high_resolution_clock::time_point t3 = high_resolution_clock::now();
            duration<double> time_span = duration_cast<duration<double>>(t3 - t1);
            if (time_span.count() > max_time) break;
        }
    }

    score /= game_num;
    win_pct = wins / game_num;
    global_silent_mode = hold_global_silent;

    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    cout << " Total Time: " << time_span.count() << " games " << game_num;

    return true;
}
