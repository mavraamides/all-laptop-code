#pragma once
#include <string>
#include <vector>
using namespace std;
class GameState;

class GameClock
{
	int quarter = 1;
	int game_seconds_remaining = 3600;
	GameState* gamestate = nullptr;
	bool clock_running_start;
	bool clock_running_end;
public:
	
	friend class GameState;
	GameClock(GameState * gs) {
		gamestate = gs;
		reset();
	};
	~GameClock() {};
	void DeepCopy(GameClock& other);
	void reset();
	string get_quarter_desc();
	int get_half();
	int get_quarter_seconds_remaining();
	int get_half_seconds_remaining();
	int get_game_seconds_remaining();
	int get_quarter();
	string get_clock_desc();
	void print();
	void advance(int seconds);
	string get_mini_quarter_desc();
	vector<string> GetMiniDesc();
	void stop_clock();
	void dump_clock_stopped(string label);

};

