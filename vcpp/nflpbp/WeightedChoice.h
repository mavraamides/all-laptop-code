#pragma once
#include <vector>
using namespace std;

struct WeightedChoiceElement
{
	int ndx;
	int value;
};
class WeightedChoice
{
	vector<WeightedChoiceElement> choices;
	int total;
public:
	WeightedChoice() {}
	~WeightedChoice() {}

	void reset();
	void add_element(int ndx, int value);
	int chooese();
};

