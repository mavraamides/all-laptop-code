#include "WeightedChoice.h"

void WeightedChoice::reset()
{
	choices.clear();
	total = 0;
}

void WeightedChoice::add_element(int ndx, int value)
{
	WeightedChoiceElement wce;
	wce.ndx = ndx;
	wce.value = value;
	choices.push_back(wce);
	total += value;
}

int WeightedChoice::chooese()
{
	auto ndx = -1;
	if (total <= 0) return ndx;

	auto random_choie = rand() % total;
	auto total_choice = 0;

	for (auto wce : choices)
	{
		total_choice += wce.value;
		if (total_choice > random_choie)
		{
			ndx = wce.ndx;
			break;
		}
	}

	return ndx;
}
