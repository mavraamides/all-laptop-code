#include "GameState.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include "fastcsv.h"

GameState::GameState(std::string hteam, std::string ateam, bool hpossession)
{
	hometeam = hteam;
	awayteam = ateam;
	homescore = 0;
	awayscore = 0;
	home_to = 3;
	away_to = 3;
	home_cm = kGSClockMode::normal_cm;
	away_cm = kGSClockMode::normal_cm;
	clock_mode_checked_this_possession = false;
	homepossession = hpossession;
	homechoicesecondhalf = false;
	statemachine = new GameStateMachine(this);
}

GameState::~GameState()
{
	if (statemachine)
	{
		delete statemachine;
		statemachine = nullptr;
	}
}

void GameState::reset()
{
	homescore = 0;
	awayscore = 0;
	home_to = 3;
	away_to = 3;
	home_cm = kGSClockMode::normal_cm;
	away_cm = kGSClockMode::normal_cm;
	clock_mode_checked_this_possession = false;
	homepossession = false;
	homechoicesecondhalf = false;
	for (int x = 0; x < home_score_by_quarter.size(); x++) home_score_by_quarter[x] = 0;
	for (int x = 0; x < away_score_by_quarter.size(); x++) away_score_by_quarter[x] = 0;
	if (statemachine) statemachine->reset();
	balltracker.reset();
	gameclock.reset();
	box_score = "";
	box_score_ready = false;
}

bool GameState::game_over()
{
	return gameclock.get_game_seconds_remaining() <= 0;
}

void GameState::change_of_possession()
{
	homepossession = !homepossession;
	balltracker.change_of_possession();
}

void GameState::set_yardline_after_kickoff(int yardline, int play_time)
{
	balltracker.set_yardline(yardline);
	balltracker.reset_dandd();
	if (yardline <= 0) {
		statemachine->touchdown_on_kickoff_return_event();
	}
	else {
		statemachine->kickoff_received_event();
	}
	//gameclock.advance(play_time, this);
}

void GameState::set_yardline_after_punt(calc_data_t& calc_data, int yardline, int play_time)
{
	balltracker.set_yardline(yardline);
	if (yardline <= 0)
	{
		statemachine->touchdown_on_scrimmage_event();
		calc_data["CALC_touchdonw_PUNT"] = 1;
	}
	else if (yardline >= 100)
	{
		balltracker.set_yardline(80);
		balltracker.reset_dandd();
	}
	else {
		balltracker.reset_dandd();
	}
	//gameclock.advance(play_time, this);
}

GameStateReturnSet GameState::gain_yards_on_return(calc_data_t& calc_data, int gain, int play_time)
{
	auto gsrs = GameStateReturnSet();

	balltracker.yardline -= gain;
	balltracker.reset_dandd();
	if (balltracker.yardline <= 0) {
		gsrs.gain_adjust = balltracker.yardline;
		statemachine->touchdown_on_scrimmage_event();
		gsrs.ss.add_stage("DESCRIPTION", 0, "Touchdown!!", vstr_t(), vstr_t(), vstr_t());
		gsrs.terminate_series = true;
	}
	else if (balltracker.yardline >= 100) {
		gsrs.gain_adjust = 0;
		balltracker.yardline = 80;
		gsrs.ss.add_stage("DESCRIPTION", 0, "Touchback.", vstr_t(), vstr_t(), vstr_t());
		gsrs.terminate_series = true;
	}

	//gameclock.advance(play_time, this);
	return gsrs;
}

GameStateReturnSet GameState::gain_yards_on_scrimmage(calc_data_t& calc_data, int gain, int play_time)
{
	auto gsrs = GameStateReturnSet();

	auto yardline = balltracker.yardline;
	auto first_down = balltracker.gain_yards(gain);
	if (balltracker.down > 4) {
		clock_mode_checked_this_possession = false;
		statemachine->turnover_on_downs_event();
		gsrs.ss.add_stage("DESCRIPTION", 0, "Turnover on downs!!", vstr_t(), vstr_t(), vstr_t());
		gsrs.terminate_series = true;
	}
	else if (balltracker.yardline <= 0) {
		gsrs.gain_adjust = yardline - gain;
		statemachine->touchdown_on_scrimmage_event();
		gsrs.ss.add_stage("DESCRIPTION", 0, "Touchdown!!", vstr_t(), vstr_t(), vstr_t());
		gsrs.terminate_series = true;
	}
	else if (balltracker.yardline >= 100) {
		gsrs.gain_adjust = balltracker.yardline - 100;
		statemachine->safety_on_scrimmage_event();
		gsrs.ss.add_stage("DESCRIPTION", 0, "Safety!!", vstr_t(), vstr_t(), vstr_t());
		gsrs.terminate_series = true;
	}
	else if (first_down) {
		gsrs.ss.add_stage("DESCRIPTION", 0, "First Down!!", vstr_t(), vstr_t(), vstr_t());
	}
	//gameclock.advance(play_time, this);
	return gsrs;
}

GameStateReturnSet GameState::gain_yards_on_penalty(calc_data_t& calc_data, int gain, int play_time, bool auto_first_down, bool down_over)
{
	auto gsrs = GameStateReturnSet();
	
	auto first_down = balltracker.gain_yards(gain);
	if (auto_first_down) {
		first_down = true;
		balltracker.reset_dandd();
	}
	if (down_over && balltracker.down > 1) {
		balltracker.down--;
	}

	if (balltracker.down > 4) {
		clock_mode_checked_this_possession = false;
		statemachine->turnover_on_downs_event();
		gsrs.ss.add_stage("DESCRIPTION", 0, "Turnover on downs!!", vstr_t(), vstr_t(), vstr_t());
		gsrs.terminate_series = true;
	}
	else if (balltracker.yardline <= 0) {
		gsrs.gain_adjust = balltracker.yardline;
		statemachine->touchdown_on_scrimmage_event();
		gsrs.ss.add_stage("DESCRIPTION", 0, "Touchdown!!", vstr_t(), vstr_t(), vstr_t());
		gsrs.terminate_series = true;
	}
	else if (balltracker.yardline >= 100) {
		gsrs.gain_adjust = balltracker.yardline - 100;
		statemachine->safety_on_scrimmage_event();
		gsrs.ss.add_stage("DESCRIPTION", 0, "Safety!!", vstr_t(), vstr_t(), vstr_t());
		gsrs.terminate_series = true;
	}
	else if (first_down) {
		gsrs.ss.add_stage("DESCRIPTION", 0, "First Down!!", vstr_t(), vstr_t(), vstr_t());
	}
	//gameclock.advance(play_time, this);

	return gsrs;
}

bool GameState::fast_gain_yards_on_scrimmage(int gain, int play_time)
{
	auto terminate_series = false;

	auto first_down = balltracker.gain_yards(gain);
	if (balltracker.down > 4) {
		statemachine->turnover_on_downs_event();
		terminate_series = true;
	}
	else if (balltracker.yardline <= 0) {
		statemachine->touchdown_on_scrimmage_event();
		terminate_series = true;
	}
	else if (balltracker.yardline >= 100) {
		statemachine->safety_on_scrimmage_event();
		terminate_series = true;
	}
	gameclock.advance(play_time);
	
	return terminate_series;
}

bool GameState::fast_gain_yards_on_return(int gain, int play_time)
{
	auto terminate_series = false;

	balltracker.yardline -= gain;
	balltracker.reset_dandd();
	if (balltracker.yardline <= 0) {
		statemachine->touchdown_on_scrimmage_event();
		terminate_series = true;
	}
	else if (balltracker.yardline >= 100) {
		balltracker.yardline = 80;
		terminate_series = true;
	}
	gameclock.advance(play_time);

	return terminate_series;
}

bool GameState::fast_gain_yards_on_penalty(int gain, int play_time, bool auto_first_down, bool down_over)
{
	auto terminate_series = false;

	auto first_down = balltracker.gain_yards(gain);
	if (auto_first_down) {
		first_down = true;
		balltracker.reset_dandd();
	}
	if (down_over && balltracker.down > 1) {
		balltracker.down--;
	}

	if (balltracker.down > 4) {
		statemachine->turnover_on_downs_event();
		terminate_series = true;
	}
	else if (balltracker.yardline <= 0) {
		statemachine->touchdown_on_scrimmage_event();
		terminate_series = true;
	}
	else if (balltracker.yardline >= 100) {
		statemachine->safety_on_scrimmage_event();
		terminate_series = true;
	}
	gameclock.advance(play_time);

	return terminate_series;
}

void GameState::add_score(int score)
{
	auto ndx = gameclock.get_quarter() - 1;
	if (homepossession) {
		homescore += score;
		if (ndx >= 0 && ndx < home_score_by_quarter.size()) {
			home_score_by_quarter[ndx] += score;
		}
	}
	else {
		awayscore += score;
		if (ndx >= 0 && ndx < away_score_by_quarter.size()) {
			away_score_by_quarter[ndx] += score;
		}
	}
}

std::string GameState::get_posteam()
{
	if (homepossession) {
		return hometeam;
	}
	else {
		return awayteam;
	}
}

std::string GameState::get_defteam()
{
	if (homepossession) {
		return awayteam;
	}
	else {
		return hometeam;
	}
}

int GameState::get_score_diff_post()
{
	if (homepossession) {
		return homescore - awayscore;
	}
	else {
		return awayscore - homescore;
	}
}

int GameState::get_score_diff_post(bool homepos)
{
	if (homepos) {
		return homescore - awayscore;
	}
	else {
		return awayscore - homescore;
	}
}

void GameState::print_score()
{
	std::cout << get_score_description() << std::endl;
}

std::string GameState::get_score_description()
{
	std::string desc;
	desc = awayteam + " " + std::to_string(awayscore) + " " + hometeam + " " + std::to_string(homescore);
	return desc;
}

void GameState::build_score_table()
{
	stringstream sstr;
	sstr << "End of " << gameclock.get_quarter_desc() << endl;

	sstr << "+----+----+----+----+----+----+-----+" << std::endl;
	sstr << "|Team| 1st| 2nd| 3rd| 4th| OT |Total|" << std::endl;
	sstr << "+----+----+----+----+----+----+-----+" << std::endl;

	sstr << "| " << std::left << std::setw(3) << hometeam;
	for (int x = 0; x < home_score_by_quarter.size(); x++) {
		if (x >= gameclock.get_quarter()) {
			sstr << "| " << std::right << std::setw(2) << " - ";
		}
		else {
			sstr << "| " << std::right << std::setw(2) << home_score_by_quarter[x] << " ";
		}
	}
	sstr << "| " << std::right << std::setw(3) << homescore << " |" << std::endl;

	sstr << "| " << std::left << std::setw(3) << awayteam;
	for (int x = 0; x < away_score_by_quarter.size(); x++) {
		if (x >= gameclock.get_quarter()) {
			sstr << "| " << std::right << std::setw(2) << " - ";
		}
		else {
			sstr << "| " << std::right << std::setw(2) << away_score_by_quarter[x] << " ";
		}
	}
	sstr << "| " << std::right << std::setw(3) << awayscore << " |" << std::endl;

	sstr << "+----+----+----+----+----+----+-----+" << std::endl;
	box_score = sstr.str();
	box_score_ready = true;
}

void GameState::print_score_table()
{
	if (box_score_ready)
	{
		box_score_ready = false;
		cout << box_score;
	}
}

void GameState::set_to_second_half_choice()
{
	homepossession = homechoicesecondhalf;
}
/*
	vector<int> home_score_by_quarter = { 0,0,0,0,0 };
	vector<int> away_score_by_quarter = { 0,0,0,0,0 };
public:
	int homescore = 0;
	int awayscore = 0;
	int home_to = 3;
	int away_to = 3;
	bool homepossession = false;
	bool homechoicesecondhalf = false;
	string hometeam;
	string awayteam;
	GameStateMachine * statemachine = nullptr;
	BallTracker balltracker = BallTracker();
	GameClock gameclock = GameClock();

*/
void GameState::deep_copy(GameState& other)
{
	home_score_by_quarter = other.home_score_by_quarter;
	away_score_by_quarter = other.away_score_by_quarter;
	homescore = other.homescore;
	awayscore = other.awayscore;
	home_to = other.home_to;
	away_to = other.away_to;
	home_cm = other.home_cm;
	away_cm = other.away_cm;
	clock_mode_checked_this_possession = other.clock_mode_checked_this_possession;
	homepossession = other.homepossession;
	homechoicesecondhalf = other.homechoicesecondhalf;
	hometeam = other.hometeam;
	awayteam = other.awayteam;
	if (statemachine && other.statemachine)
	{
		statemachine->DeepCopy(*other.statemachine);
	}
	balltracker.DeepCopy(other.balltracker);
	gameclock.DeepCopy(other.gameclock);
	box_score = (other.box_score);
}

kGSClockMode GameState::get_clock_mode_by_pos(bool pos)
{
	if (pos == homepossession)
	{
		return home_cm;
	}
	else
	{
		return away_cm;
	}
	return kGSClockMode();
}

void GameState::set_clock_mode_by_pos(kGSClockMode clock_mode, bool pos)
{
	if (pos == homepossession)
	{
		home_cm = clock_mode;
	}
	else
	{
		away_cm = clock_mode;
	}
}

int GameState::get_timeouts_remaining_by_pos(bool pos)
{
	if (pos == homepossession)
	{
		return home_to;
	}
	else
	{
		return away_to;
	}
}

void GameState::use_timeout_by_pos(bool pos)
{
	if (pos == homepossession)
	{
		if (home_to > 0) {
			home_to--;
		}
	}
	else
	{
		if (away_to > 0) {
			away_to--;
		}
	}
}

vector<string> GameState::get_mini_desc()
{
	vector<string> desc;
	stringstream sstr;
	sstr << setw(3) << left << awayteam;
	if (get_clock_mode_by_pos(true) == kGSClockMode::hurry_up_cm) sstr << "+";
	else if (get_clock_mode_by_pos(true) == kGSClockMode::run_clock_cm) sstr << "-";
	else sstr << " ";
	if (homepossession) {
		sstr << "    ";
	}
	else {
		sstr << "<O> ";
	}
	sstr << setw(3) << left << hometeam;
	if (get_clock_mode_by_pos(false) == kGSClockMode::hurry_up_cm) sstr << "+";
	else if (get_clock_mode_by_pos(false) == kGSClockMode::run_clock_cm) sstr << "-";
	else sstr << " ";
	if (homepossession) {
		sstr << "<O>|";
	}
	else {
		sstr << "   |";
	}
	desc.push_back(sstr.str());
	sstr.str("");
	sstr << setw(2) << right << awayscore << "  ";
	for (int x = 0; x < 3; x++) if (x < away_to) sstr << "*"; else sstr << " ";
	sstr << " ";
	sstr << setw(2) << right << homescore << "  ";
	for (int x = 0; x < 3; x++) if (x < home_to) sstr << "*"; else sstr << " ";
	sstr << "|";
	desc.push_back(sstr.str());
	return desc;
}

/*
	vector<int> home_score_by_quarter = { 0,0,0,0,0 };
	vector<int> away_score_by_quarter = { 0,0,0,0,0 };
public:
	int homescore = 0;
	int awayscore = 0;
	int home_to = 3;
	int away_to = 3;
	kGSClockMode home_cm;
	kGSClockMode away_cm;
	bool clock_mode_checked_this_possession;
	bool homepossession = false;
	bool homechoicesecondhalf = false;
	string hometeam;
	string awayteam;
	GameStateMachine * statemachine = nullptr;
	BallTracker balltracker = BallTracker();
	GameClock gameclock = GameClock();

*/
void GameState::save(string filename, int filenum)
{
	stringstream sstr;
	sstr << "./gamestates/" << filename << "_" << setw(3) << setfill('0') << filenum << ".txt";
	save(sstr.str());
}

void GameState::save(string filename)
{
	ofstream out(filename);
	if (out.is_open())
	{
		for (auto score : home_score_by_quarter) out << score << "\n";
		for (auto score : away_score_by_quarter) out << score << "\n";
		out << homescore << "\n";
		out << awayscore << "\n";
		out << home_to << "\n";
		out << away_to << "\n";
		out << home_cm << "\n";
		out << away_cm << "\n";
		out << clock_mode_checked_this_possession << "\n";
		out << homepossession << "\n";
		out << homechoicesecondhalf << "\n";
		out << hometeam << "\n";
		out << awayteam << "\n";
		out << statemachine->state << "\n";
		out << balltracker.down << "\n";
		out << balltracker.distance << "\n";
		out << balltracker.yardline << "\n";
		out << balltracker.already_advanced_down << "\n";
		out << gameclock.quarter << "\n";
		out << gameclock.game_seconds_remaining << "\n";
		out.close();
	}
}

bool GameState::load(string filename)
{
	char line[80];
	std::ifstream in(filename);
	if (!in.good()) return false;

	vector<string> lines;
	while (in.getline(line, 79, '\n')) {
		lines.push_back(line);
	}
	in.close();

	if (lines.size() < 28) return false;
	auto line_num = 0;
	home_score_by_quarter.clear();
	for (int x = 0; x < 5; x++) home_score_by_quarter.push_back(atoi(lines[line_num++].c_str()));
	away_score_by_quarter.clear();
	for (int x = 0; x < 5; x++) away_score_by_quarter.push_back(atoi(lines[line_num++].c_str()));
	homescore = atoi(lines[line_num++].c_str());
	awayscore = atoi(lines[line_num++].c_str());
	home_to = atoi(lines[line_num++].c_str());
	away_to = atoi(lines[line_num++].c_str());
	home_cm = (kGSClockMode) atoi(lines[line_num++].c_str());
	away_cm = (kGSClockMode) atoi(lines[line_num++].c_str());
	clock_mode_checked_this_possession = atoi(lines[line_num++].c_str());
	homepossession = atoi(lines[line_num++].c_str());
	homechoicesecondhalf = atoi(lines[line_num++].c_str());
	hometeam = lines[line_num++];
	awayteam = lines[line_num++];
	statemachine->state = (kGSMState) atoi(lines[line_num++].c_str());
	balltracker.down = atoi(lines[line_num++].c_str());
	balltracker.distance = atoi(lines[line_num++].c_str());
	balltracker.yardline = atoi(lines[line_num++].c_str());
	balltracker.already_advanced_down = atoi(lines[line_num++].c_str());
	gameclock.quarter = atoi(lines[line_num++].c_str());
	gameclock.game_seconds_remaining = atoi(lines[line_num++].c_str());

	return true;
}

void GameStateLogger::log(int playndx, GameState& gamestate)
{
	GameStateLogEntry gsle;
	gsle.ndx = playndx;
	gsle.homescore = gamestate.homescore;
	gsle.awayscore = gamestate.awayscore;
	gsle.home_to = gamestate.home_to;
	gsle.away_to = gamestate.away_to;
	gsle.state = gamestate.statemachine->get_state();
	gsle.down = gamestate.balltracker.down;
	gsle.distance = gamestate.balltracker.distance;
	gsle.yardline = gamestate.balltracker.yardline;
	gsle.game_seconds_remaining = gamestate.gameclock.get_game_seconds_remaining();
	gsle.homepossession = gamestate.homepossession;
	gsle.homechoicesecondhalf = gamestate.homechoicesecondhalf;
	gamelog.push_back(gsle);
}

void GameStateLogger::print_last_log_entry()
{
	if (gamelog.size() > 0) {
		print_log_entry(gamelog.size() - 1);
		cout << endl;
	}
}
void GameStateLogger::print_log_entry(int x)
{
	cout << "|";
	cout << setw(5) << x;
	cout << "|";
	cout << (gamelog[x].homepossession ? 'H' : 'A');
	cout << (gamelog[x].homechoicesecondhalf ? 'T' : 'F');
	cout << "|";
	cout << setw(5) << gamelog[x].ndx;
	cout << "|";
	cout << setw(3) << gamelog[x].homescore;
	cout << "|";
	cout << setw(3) << gamelog[x].awayscore;
	cout << "|";
	cout << setw(3) << gamelog[x].home_to;
	cout << "|";
	cout << setw(3) << gamelog[x].away_to;
	cout << "|";
	cout << setw(3) << gamelog[x].state;
	cout << "|";
	cout << setw(3) << gamelog[x].down;
	cout << "|";
	cout << setw(3) << gamelog[x].distance;
	cout << "|";
	cout << setw(3) << gamelog[x].yardline;
	cout << "|";
	cout << setw(6) << gamelog[x].game_seconds_remaining;
	cout << "|";
}
void GameStateLogger::print_log()
{
	for (int x = 0; x < gamelog.size(); x++)
	{
		if (x % 20 == 0) {
			cout << "+-----+--+-----+---+---+---+---+---+---+---+---+------+" << endl;
			cout << "|  row|P2|  ndx|hsc|asc|hto|ato|sta|dwn|tgo|ydl|  time|" << endl;
			cout << "+-----+--+-----+---+---+---+---+---+---+---+---+------+" << endl;
		}
		print_log_entry(x);
		cout << endl;
	}
}

void GameStateLogger::clear()
{
	if (gamelog.size()) gamelog.clear();
}

void GameStateLogger::save(string filename)
{
	ofstream out(filename);
	if (out.is_open())
	{
		out << "ndx,homescore,awayscore,home_to,away_to,state,down,distance,yardline,game_seconds_remaining,homepossession,homechoicesecondhalf\n";
		for (auto log_entry : gamelog)
		{
			out << log_entry.ndx << ",";
			out << log_entry.homescore << ",";
			out << log_entry.awayscore << ",";
			out << log_entry.home_to << ",";
			out << log_entry.away_to << ",";
			out << log_entry.state << ",";
			out << log_entry.down << ",";
			out << log_entry.distance << ",";
			out << log_entry.yardline << ",";
			out << log_entry.game_seconds_remaining << ",";
			out << log_entry.homepossession << ",";
			out << log_entry.homechoicesecondhalf;
			out << "\n";
		}
		out.close();
	}
}

void GameStateLogger::load(string filename)
{
	int max_len;
	int max_row;
	int max_size;
	gamelog.clear();
	if (fast_csv_check(filename.c_str(), max_len, max_row, max_size))
	{
		auto data = new char[max_size];
		fast_csv_load(filename.c_str(), data, max_len, max_size);
		for (int row = 0; row < max_row; row++)
		{
			GameStateLogEntry gsle;
			gsle.ndx = atoi(fast_csv_get(data, max_len, max_row, row, 0));
			gsle.homescore = atoi(fast_csv_get(data, max_len, max_row, row, 1));
			gsle.awayscore = atoi(fast_csv_get(data, max_len, max_row, row, 2));
			gsle.home_to = atoi(fast_csv_get(data, max_len, max_row, row, 3));
			gsle.away_to = atoi(fast_csv_get(data, max_len, max_row, row, 4));
			gsle.state = (kGSMState) atoi(fast_csv_get(data, max_len, max_row, row, 5));
			gsle.down = atoi(fast_csv_get(data, max_len, max_row, row, 6));
			gsle.distance = atoi(fast_csv_get(data, max_len, max_row, row, 7));
			gsle.yardline = atoi(fast_csv_get(data, max_len, max_row, row, 8));
			gsle.game_seconds_remaining = atoi(fast_csv_get(data, max_len, max_row, row, 9));
			gsle.homepossession = atoi(fast_csv_get(data, max_len, max_row, row, 10));
			gsle.homechoicesecondhalf = atoi(fast_csv_get(data, max_len, max_row, row, 11));
			gamelog.push_back(gsle);
		}
	}
}

bool GameStateLogger::compare(GameStateLogger & other, bool display)
{
	bool retVal = true;
	auto num_own_entries = gamelog.size();
	auto num_other_entries = other.gamelog.size();
	auto max_entries = num_own_entries > num_other_entries ? num_own_entries : num_other_entries;
	cout << endl << "LOG MATCH:" << endl;
	if (num_own_entries != num_other_entries) {
		retVal = false;
		if (display) {
			cout << "This size: " << num_own_entries << " other size: " << num_other_entries << endl;
		}
	}
	for (int x = 0; x < max_entries; x++)
	{
		bool line_match = false;
		if (x < num_own_entries && x < num_other_entries)
		{
			line_match = (gamelog[x] == other.gamelog[x]);
			retVal = retVal && line_match;
		}
		if (display)
		{
			if (x < num_own_entries)
			{
				print_log_entry(x);
			}
			else
			{
				cout << "+-----+--+-----+---+---+---+---+---+---+---+---+------+";
			}
			if (line_match)
			{
				cout << " ";
			}
			else
			{
				cout << "*";
			}
			if (x < num_other_entries)
			{
				other.print_log_entry(x);
			}
			else
			{
				cout << "+-----+--+-----+---+---+---+---+---+---+---+---+------+";
			}
			cout << endl;
		}
	}
	if (display) cout << (retVal ? "PASS" : "FAIL") << endl;

	return retVal;
}

/*
	gsle.ndx = playndx;
	gsle.homescore = gamestate.homescore;
	gsle.awayscore = gamestate.awayscore;
	gsle.home_to = gamestate.home_to;
	gsle.away_to = gamestate.away_to;
	gsle.state = gamestate.statemachine->get_state();
	gsle.down = gamestate.balltracker.down;
	gsle.distance = gamestate.balltracker.distance;
	gsle.yardline = gamestate.balltracker.yardline;
	gsle.game_seconds_remaining = gamestate.gameclock.get_game_seconds_remaining();
	gsle.homepossession = gamestate.homepossession;
	gsle.homechoicesecondhalf = gamestate.homechoicesecondhalf;

*/

bool GameStateLogEntry::operator==(const GameStateLogEntry& other)
{
	return 
		ndx == other.ndx &&
		homescore == other.homescore &&
		awayscore == other.awayscore &&
		home_to == other.home_to &&
		away_to == other.away_to &&
		state == other.state &&
		down == other.down &&
		distance == other.distance &&
		yardline == other.yardline &&
		game_seconds_remaining == other.game_seconds_remaining &&
		homepossession == other.homepossession &&
		homechoicesecondhalf == other.homechoicesecondhalf
		;
}
