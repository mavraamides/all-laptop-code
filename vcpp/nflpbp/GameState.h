#pragma once
#include <string>
#include <vector>
#include <map>
#include "BallTracker.h"
#include "GameClock.h"
#include "GameStateMachine.h"
#include "StageSet.h"
using namespace std;

typedef map<string, string> calc_data_t;

enum kGSClockMode {
	normal_cm,
	run_clock_cm,
	hurry_up_cm,
};

struct GameStateLogEntry
{
	int ndx;
	int homescore;
	int awayscore;
	int home_to;
	int away_to;
	kGSMState state;
	int down;
	int distance;
	int yardline;
	int game_seconds_remaining;
	bool homepossession;
	bool homechoicesecondhalf;
	bool operator==(const GameStateLogEntry& other);
};
class GameStateReturnSet
{
public:
	bool terminate_series = false;
	StageSet ss = StageSet();
	int gain_adjust = 0;

	GameStateReturnSet() {};
	~GameStateReturnSet() {};
};
class GameStateLogger
{
public:
	vector<GameStateLogEntry> gamelog;
	GameStateLogger() {};
	~GameStateLogger() {};
	void log(int playndx, GameState & gamestate);
	void print_last_log_entry();
	void print_log_entry(int ndx);
	void print_log();
	void clear();
	void save(string filename);
	void load(string filename);
	bool compare(GameStateLogger & other, bool display);
};
class GameState
{
	// not serialized
	string box_score;
	bool box_score_ready;

	vector<int> home_score_by_quarter = { 0,0,0,0,0 };
	vector<int> away_score_by_quarter = { 0,0,0,0,0 };
public:
	int homescore = 0;
	int awayscore = 0;
	int home_to = 3;
	int away_to = 3;
	kGSClockMode home_cm;
	kGSClockMode away_cm;
	bool clock_mode_checked_this_possession;
	bool homepossession = false;
	bool homechoicesecondhalf = false;
	string hometeam;
	string awayteam;
	GameStateMachine * statemachine = nullptr;
	BallTracker balltracker = BallTracker();
	GameClock gameclock = GameClock(this);
	GameState(string hteam, string ateam, bool hpossession);
	~GameState();

	void reset();
	bool game_over();
	void change_of_possession();
	void set_yardline_after_kickoff(int yardline, int play_time);
	void set_yardline_after_punt(calc_data_t&calc_data, int yardline, int play_time);
	GameStateReturnSet gain_yards_on_return(calc_data_t& calc_data, int gain, int play_time);
	GameStateReturnSet gain_yards_on_scrimmage(calc_data_t& calc_data, int gain, int play_time);
	GameStateReturnSet gain_yards_on_penalty(calc_data_t& calc_data, int gain, int play_time, bool auto_first_down, bool down_over);
	bool fast_gain_yards_on_scrimmage(int gain, int play_time);
	bool fast_gain_yards_on_return(int gain, int play_time);
	bool fast_gain_yards_on_penalty(int gain, int play_time, bool auto_first_down, bool down_over);
	void add_score(int score);
	string get_posteam();
	string get_defteam();
	int get_score_diff_post();
	int get_score_diff_post(bool homepos);
	void print_score();
	string get_score_description();
	void build_score_table();
	void print_score_table();
	void set_to_second_half_choice();
	void deep_copy(GameState& other);
	kGSClockMode get_clock_mode_by_pos(bool pos);
	void set_clock_mode_by_pos(kGSClockMode clock_mode, bool pos);
	int get_timeouts_remaining_by_pos(bool pos);
	void use_timeout_by_pos(bool pos);
	vector<string> get_mini_desc();
	void save(string filename, int filenum);
	void save(string filename);
	bool load(string filename);
};

