#pragma once
#include <string>
#include <vector>
using namespace std;

class GameState;

enum kPCMatchup {
	home_is_pos,
	home_is_def,
	away_is_pos,
	away_is_def,
	max_match_type
};

// ADD_PLAY_TYPE_PLAY
enum kPCPlayType {
	scrimmage,
	kickoff,
	field_goal,
	extra_point,
	two_point_conv,
	punt,
	timeout,
	spike,
	no_play
};

struct MatchSetIndex {
	char posteam[4];
	char defteam[4];
	unsigned char down;
	unsigned char play_type;
	unsigned char pos_is_home;
	unsigned char encoded;
};

struct MatchSetElement
{
	int ndx;
	unsigned char yardline_category;
	unsigned char distance_category;
	unsigned char time_category;
	char score_diff_category;
	unsigned char yardline;
	unsigned char sort_key;
};

struct MatchSet {
	MatchSetIndex index;
	MatchSetElement element;
};
const int max_start = kPCPlayType::no_play + 5;

class PlayChooser
{
	//unsigned char play_counter = 0;
	static PlayChooser* instance;
	
	PlayChooser();
	void initialize_league_data();
public:
	unsigned int max_hash;
	static PlayChooser* getInstance()
	{
		if (instance == nullptr)
		{
			instance = new PlayChooser();
		}
		return instance;
	}
	~PlayChooser() {};
	void SetTeams(string hometeam, string awayteam);
	int score_options(int play_type, int match_type, int down, int yardline_category, int distance_category, int time_category, int score_diff_category, int yardline, int stop_at_choice);
	int get_match(int play_type, int match_type, int down, int togo, int yardline, int game_seconds_remaining, int score_diff);
	int get_yardline_category(int yardline);
	int get_distance_category(int distance);
	int get_time_category(int time);
	int get_score_diff_category(int score_diff);
	float calc_remaining_posessions(GameState & gamestate, bool hometeam);
	bool use_pos_team();
	void GetMatchType(GameState& gamestate, kPCMatchup& match_type);
	int get_play_ndx_monte_carlo(GameState& gamestate);
	int get_play_ndx(GameState& gamestate);
	int get_play_ndx_force_play_type(GameState& gamestate, kPCPlayType play_type);
	void dump();
	void dump_match_type(kPCMatchup match_type);
};

