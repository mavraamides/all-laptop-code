#pragma once
#include "GameState.h"

class PlayExecutor
{
public:
	PlayExecutor() {};
	~PlayExecutor() {};
	void execute(GameState& gamestate, int ndx, calc_data_t& calc_data, StageSet& rs, bool use_monte_carlo = false);
	void fast_execute(GameState& gamestate, int ndx);
};

