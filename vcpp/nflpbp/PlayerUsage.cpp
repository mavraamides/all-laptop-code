#include "PlayerUsage.h"
#include <map>
#include <iostream>
#include <fstream>
#include "GameData.h"
#include "fastcsv.h"

static map<string, PlayerSet> player_data_map;
static char* player_data = nullptr;
PlayerUsage* PlayerUsage::instance = nullptr;

PlayerUsage::PlayerUsage()
{
	cout << "Initializing player data...";

	auto filename = "player_data.csv";
	if (!fast_csv_check(filename, max_len, max_row, max_size))
	{
		initialize_player_data();
		write_player_data(filename);
	}
	fast_csv_check(filename, max_len, max_row, max_size);
	player_data = new char[max_size];
	fast_csv_load(filename, player_data, max_len, max_size);

	cout << "Done." << endl;
}

void PlayerUsage::add_player(string team, kPUPlayerCategory category, string name)
{
	if (name.size() == 0) return;
	string key = team + "_" + to_string(category) + "_" + name;
	if (player_data_map.count(key) == 0)
	{
		PlayerSet ps;
		strncpy_s(ps.teamname, 4, team.c_str(), 4);
		ps.category = category;
		strncpy_s(ps.playername, 80, name.c_str(), 79);
		ps.count = 0;
		player_data_map[key] = ps;
	}
	player_data_map[key].count++;
}

void PlayerUsage::initialize_player_data()
{
	auto max_row = GDI->get_max_row();

	for (int ndx = 0; ndx < max_row; ndx++)
	{
		string posteam = GDI->get(ndx, KDCol::posteam);
		string defteam = GDI->get(ndx, KDCol::defteam);

		string passer_player_name = GDI->get(ndx, KDCol::passer_player_name);
		add_player(posteam, kPUPlayerCategory::passer_player_name_cat, passer_player_name);

		string receiver_player_name = GDI->get(ndx, KDCol::receiver_player_name);
		add_player(posteam, kPUPlayerCategory::receiver_player_name_cat, receiver_player_name);

		string rusher_player_name = GDI->get(ndx, KDCol::rusher_player_name);
		add_player(posteam, kPUPlayerCategory::rusher_player_name_cat, rusher_player_name);

		string punter_player_name = GDI->get(ndx, KDCol::punter_player_name);
		add_player(posteam, kPUPlayerCategory::punter_player_name_cat, punter_player_name);

		string kickoff_returner_player_name = GDI->get(ndx, KDCol::kickoff_returner_player_name);
		add_player(posteam, kPUPlayerCategory::kickoff_returner_player_name_cat, kickoff_returner_player_name);

		string interception_player_name = GDI->get(ndx, KDCol::interception_player_name);
		add_player(defteam, kPUPlayerCategory::interception_player_name_cat, interception_player_name);

		string punt_returner_player_name = GDI->get(ndx, KDCol::punt_returner_player_name);
		add_player(defteam, kPUPlayerCategory::punt_returner_player_name_cat, punt_returner_player_name);

		string kicker_player_name = GDI->get(ndx, KDCol::kicker_player_name);
		if (strcmp(GDI->get(ndx, KDCol::play_type_nfl), "KICK_OFF") == 0) {
			add_player(defteam, kPUPlayerCategory::kicker_player_name_KO_cat, kicker_player_name);
		}
		else if (strcmp(GDI->get(ndx, KDCol::play_type_nfl), "FIELD_GOAL") == 0) {
			add_player(posteam, kPUPlayerCategory::kicker_player_name_FG_cat, kicker_player_name);
		}
	}
}

void PlayerUsage::write_player_data(string filename)
{
	map<string, PlayerSet> most_used_qb;
	for (auto kvp : player_data_map)
	{
		if (kvp.second.category != kPUPlayerCategory::passer_player_name_cat) continue;
		if (most_used_qb.count(kvp.second.teamname) == 0) most_used_qb[kvp.second.teamname] = kvp.second;
		if (most_used_qb[kvp.second.teamname].count < kvp.second.count) most_used_qb[kvp.second.teamname] = kvp.second;
	}
	for (auto kvp : most_used_qb)
	{
		cout << kvp.first << " " << kvp.second.playername << " " << kvp.second.count << endl;
	}
	ofstream out(filename);
	if (out.is_open())
	{
		out << "Team,category,playername,count\n";
		for (auto kvp : player_data_map)
		{
			if (kvp.second.category == kPUPlayerCategory::passer_player_name_cat)
			{
				string test_player_name = kvp.second.playername;
				string most_used_player_name = most_used_qb[kvp.second.teamname].playername;
				if (test_player_name != most_used_player_name) continue;
			}
			out << kvp.second.teamname << ",";
			out << kvp.second.category << ",";
			out << kvp.second.playername << ",";
			out << kvp.second.count << ",";
			out << "\n";
		}
		out.close();
	}
}

string PlayerUsage::get_player_name(string team, kPUPlayerCategory category)
{
	weighted_choice.reset();

	for (int row = 0; row < max_row; row++)
	{
		string test_team = fast_csv_get(player_data, max_len, max_row, row, 0);
		int test_cat = atoi(fast_csv_get(player_data, max_len, max_row, row, 1));
		if (test_team == team && test_cat == category)
		{
			int count = atoi(fast_csv_get(player_data, max_len, max_row, row, 3));
			weighted_choice.add_element(row, count);
		}
	}

	auto ndx = weighted_choice.chooese();
	string result = fast_csv_get(player_data, max_len, max_row, ndx, 2);

	return result;
}
