#pragma once
#include <string>
using namespace std;

#define FSS FastStageSet::getInstance()

// ADD_PLAY_TYPE_STAGE
enum kFSSTokens {
	no_token_fss,
	kick_off_fss,
	kick_off_return_fss,
	gain_fss,
	change_of_possession_ffs,
	xp_kick_fss,
	field_goal_fss,
	punt_fss,
	return_fss,
	punt_blocked_fss,
	pass_fss,
	gain_after_catch_fss,
	two_point_conv_fss,
	penalty_fss,
};

struct StageSetIndex {
	int index;
	int play_time;
	int play_time_run_clock;
	int play_time_hurry_up;
	int yards_gained;
};
struct FastStageSetElement
{
	int count;
	int token;
	int value;
	unsigned char penalty_flags;
};

class FastStageSet
{
	static FastStageSet* instance;
	FastStageSet();
	~FastStageSet() {}
	int max_element;
	int max_index;
public:
	static FastStageSet* getInstance()
	{
		if (instance == nullptr)
		{
			instance = new FastStageSet();
		}
		return instance;
	}
	static kFSSTokens get_token(string desc);
	void dump(int num);
	void dump_element(int ssndx);
	FastStageSetElement* get_element(int ndx, int endx);
	StageSetIndex* get_index(int ndx);
};

