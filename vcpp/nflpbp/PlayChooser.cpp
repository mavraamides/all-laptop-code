#include "PlayChooser.h"
#include "GameData.h"
#include "GameState.h"
#include "EVCalc.h"
#include "GoForTwo.h"
#include "PlayChooserMonteCarlo.h"
#include <string>
#include <map>
#include <iostream>
#include <stdio.h>
#include <algorithm>

extern bool global_silent_mode;
#define max_yl_cat 12
#define max_d_cat 6
#define max_t_cat 8
#define max_sd_cat 11
#define max_dwn_cat 4
#define max_hash (max_yl_cat * max_d_cat * max_t_cat * max_sd_cat * max_dwn_cat)

int max_total_matchup_row = 0;
int start_match[kPCMatchup::max_match_type][max_start];
int end_match[kPCMatchup::max_match_type][max_start];
MatchSetElement* matchup_data[kPCMatchup::max_match_type];
int* match_totals[kPCMatchup::max_match_type];
MatchSet* league_data = nullptr;
int max_current_matchup_row[kPCMatchup::max_match_type];
int max_total_league_row = 0;

PlayChooser* PlayChooser::instance = nullptr;

unsigned int get_hash(int yardline_category, int distance_category, int time_category, int score_diff_category, int down)
{
	unsigned int hash = yardline_category;
	hash *= max_d_cat;
	hash += distance_category;
	hash *= max_t_cat;
	hash += time_category;
	hash *= max_sd_cat;
	hash += score_diff_category + 5;
	hash *= max_dwn_cat;
	hash += down - 1;

	auto test_hash = hash;
	auto test_down = (test_hash % max_dwn_cat) + 1;
	test_hash /= max_dwn_cat;
	int test_sdc = (test_hash % max_sd_cat) - 5;
	test_hash /= max_sd_cat;
	int test_tc = (test_hash % max_t_cat);
	test_hash /= max_t_cat;
	int test_dc = (test_hash % max_d_cat);
	test_hash /= max_d_cat;
	int test_ylc = test_hash;

	if (test_ylc != yardline_category ||
		test_dc != distance_category ||
		test_tc != time_category ||
		test_sdc != score_diff_category ||
		test_down != down) {
		cout << "ERROR: hash";
	}
	return hash;
}

bool sort_matchup(MatchSetElement a, MatchSetElement b)
{
	if (a.sort_key == b.sort_key)
	{
		auto ah = get_hash(a.yardline_category, a.distance_category, a.time_category, a.score_diff_category, 1);
		auto bh = get_hash(b.yardline_category, b.distance_category, b.time_category, b.score_diff_category, 1);
		return (ah < bh);
	}
	return (a.sort_key < b.sort_key);
}

int get_sort_key(int play_type, int down)
{
	if (play_type == 0) {
		return down;
	}
	else {
		return play_type + 4;
	}
}

PlayChooser::PlayChooser()
{
	map<string, int> team_counts;

	max_total_league_row = GDI->get_max_row();
	//cout << max_total_league_row << endl;
	for (int row = 0; row < max_total_league_row; row++)
	{
		auto posteam = GDI->get(row, KDCol::posteam);
		if (team_counts.count(posteam) == 0) team_counts[posteam] = 0;
		team_counts[posteam] ++;
	}
	for (int row = 0; row < max_total_league_row; row++)
	{
		auto defteam = GDI->get(row, KDCol::defteam);
		if (team_counts.count(defteam) == 0) team_counts[defteam] = 0;
		team_counts[defteam] ++;
	}

	//cout << "Start" << endl;
	auto total = 0;
	max_total_matchup_row = 0;
	for (auto kvp : team_counts)
	{
		//cout << "[" << kvp.first << "]" << " " << kvp.second << endl;
		total += kvp.second;
		if (kvp.first.size() > 0 && kvp.second > max_total_matchup_row) max_total_matchup_row = kvp.second;
	}
	max_total_matchup_row *= 2;
	//cout << "Total: " << total << " Max: " << max_total_matchup_row << endl;
	matchup_data[kPCMatchup::home_is_pos] = new MatchSetElement[max_total_matchup_row];
	matchup_data[kPCMatchup::home_is_def] = new MatchSetElement[max_total_matchup_row];
	matchup_data[kPCMatchup::away_is_pos] = new MatchSetElement[max_total_matchup_row];
	matchup_data[kPCMatchup::away_is_def] = new MatchSetElement[max_total_matchup_row];
	match_totals[kPCMatchup::home_is_pos] = new int[max_hash];
	match_totals[kPCMatchup::home_is_def] = new int[max_hash];
	match_totals[kPCMatchup::away_is_pos] = new int[max_hash];
	match_totals[kPCMatchup::away_is_def] = new int[max_hash];
	initialize_league_data();
}

void PlayChooser::initialize_league_data()
{
	league_data = new MatchSet[max_total_league_row];

	cout << "Initializing league data...";
	for (int row = 0; row < max_total_league_row; row++)
	{
		strcpy_s(league_data[row].index.posteam, 4, GDI->get(row, KDCol::posteam));
		strcpy_s(league_data[row].index.defteam, 4, GDI->get(row, KDCol::defteam));
		if (strcmp(league_data[row].index.posteam, GDI->get(row, KDCol::home_team)) == 0) {
			league_data[row].index.pos_is_home = 1;
		}
		else {
			league_data[row].index.pos_is_home = 0;
		}

		string play_type_nfl = GDI->get(row, KDCol::play_type_nfl);
		auto play_type = kPCPlayType::no_play;
		if (play_type_nfl == "RUSH" || play_type_nfl == "PASS" || play_type_nfl == "PENALTY" || play_type_nfl == "SACK")
		{
			play_type = kPCPlayType::scrimmage;
		}
		else if (play_type_nfl == "KICK_OFF")
		{
			play_type = kPCPlayType::kickoff;
		}
		else if (play_type_nfl == "XP_KICK")
		{
			play_type = kPCPlayType::extra_point;
		}
		else if (play_type_nfl == "FIELD_GOAL")
		{
			play_type = kPCPlayType::field_goal;
		}
		else if (play_type_nfl == "PUNT")
		{
			play_type = kPCPlayType::punt;
		}
		else if (play_type_nfl == "PAT2")
		{
			play_type = kPCPlayType::two_point_conv;
		}
		league_data[row].index.play_type = play_type;
		league_data[row].element.ndx = row;
		league_data[row].index.down = atoi(GDI->get(row, KDCol::down));
		league_data[row].element.yardline_category = atoi(GDI->get(row, KDCol::yardline_category));
		league_data[row].element.distance_category = atoi(GDI->get(row, KDCol::distance_category));
		league_data[row].element.time_category = atoi(GDI->get(row, KDCol::time_category));
		league_data[row].element.score_diff_category = atoi(GDI->get(row, KDCol::score_diff_category));
		league_data[row].element.yardline = atoi(GDI->get(row, KDCol::yardline_100));
		league_data[row].index.encoded = atoi(GDI->get(row, KDCol::stage_encoded));
		league_data[row].element.sort_key = get_sort_key(play_type, league_data[row].index.down);
	}
	cout << "Done." << endl;
}

void PlayChooser::SetTeams(string hometeam, string awayteam)
{
	cout << "Initializing matchup data for " << awayteam << " at " << hometeam << "...";
	max_current_matchup_row[kPCMatchup::home_is_pos] = 0;
	max_current_matchup_row[kPCMatchup::home_is_def] = 0;
	max_current_matchup_row[kPCMatchup::away_is_pos] = 0;
	max_current_matchup_row[kPCMatchup::away_is_def] = 0;
	char hometeamstr[4];
	char awayteamstr[4];
	strcpy_s(hometeamstr, 4, hometeam.c_str());
	strcpy_s(awayteamstr, 4, awayteam.c_str());

	for (int row = 0; row < max_total_league_row; row++)
	{
		auto match_type = kPCMatchup::max_match_type;
		if (strcmp(league_data[row].index.posteam, hometeamstr) == 0) {
			match_type = kPCMatchup::home_is_pos;
		}
		else if (strcmp(league_data[row].index.posteam, awayteamstr) == 0) {
			match_type = kPCMatchup::away_is_pos;
		}
		else if (strcmp(league_data[row].index.defteam, hometeamstr) == 0) {
			match_type = kPCMatchup::home_is_def;
		}
		else if (strcmp(league_data[row].index.defteam, awayteamstr) == 0) {
			match_type = kPCMatchup::away_is_def;
		}

		if (match_type != kPCMatchup::max_match_type && league_data[row].index.encoded == 1)
		{
			memcpy_s(&matchup_data[match_type][max_current_matchup_row[match_type]], sizeof(MatchSetElement), &league_data[row].element, sizeof(MatchSetElement));
			max_current_matchup_row[match_type]++;
		}
	}
	auto match_type = kPCMatchup::home_is_pos;
	for (int match_type = home_is_pos; match_type < kPCMatchup::max_match_type; match_type++)
	{
		sort(matchup_data[match_type], matchup_data[match_type] + max_current_matchup_row[match_type], sort_matchup);
		for (int x = 0; x < max_start; x++)
		{
			start_match[match_type][x] = -1;
			end_match[match_type][x] = -1;
		}
		for (int row = 0; row < max_current_matchup_row[match_type]; row++) {
			int key = matchup_data[match_type][row].sort_key;
			if (start_match[match_type][key] == -1) start_match[match_type][key] = row;
			end_match[match_type][key] = row;
		}
		for (int hash = 0; hash < max_hash; hash++)
		{
			auto test_hash = hash;
			auto test_down = (test_hash % max_dwn_cat) + 1;
			test_hash /= max_dwn_cat;
			int test_sdc = (test_hash % max_sd_cat) - 5;
			test_hash /= max_sd_cat;
			int test_tc = (test_hash % max_t_cat);
			test_hash /= max_t_cat;
			int test_dc = (test_hash % max_d_cat);
			test_hash /= max_d_cat;
			int test_ylc = test_hash;

			match_totals[match_type][hash] = score_options(kPCPlayType::scrimmage, match_type, test_down, test_ylc, test_dc, test_tc, test_sdc, 50, -1);
		}
	}
	cout << "Done." << endl;
}

int PlayChooser::score_options(int play_type, int match_type, int down, int yardline_category, int distance_category, int time_category, int score_diff_category, int yardline, int stop_at_choice)
{
	int ret_value = -1;
	int total_score = 0;
	int min_yl = 1;
	int max_yl = 99;
	bool yardline_constraint = false;
	if (play_type == kPCPlayType::field_goal) {
		min_yl = yardline - 7;
		max_yl = yardline + 8;
		yardline_constraint = true;
	}
	else if (play_type == kPCPlayType::punt) {
		min_yl = yardline - 10;
		max_yl = yardline + 10;
		yardline_constraint = true;
	}
	auto sort_key = get_sort_key(play_type, down);
	int start_row = 0;
	int end_row = max_current_matchup_row[match_type];
	if (sort_key >= 0 && sort_key < max_start)
	{
		start_row = start_match[match_type][sort_key];
		if (start_row < 0) start_row = 0;
		end_row = end_match[match_type][sort_key] + 1;
		if (end_row > max_current_matchup_row[match_type]) end_row = max_current_matchup_row[match_type];
	}
	for (int row = start_row; row < end_row; row++)
	{
		auto score = 1;
		if (yardline_constraint)
		{
			if (matchup_data[match_type][row].yardline >= min_yl && matchup_data[match_type][row].yardline <= max_yl) score += 10;
		}
		if (matchup_data[match_type][row].yardline_category == yardline_category) score++;
		if (matchup_data[match_type][row].distance_category == distance_category) score++;
		if (matchup_data[match_type][row].time_category == time_category) score++;
		if (score_diff_category == score_diff_category) score++;
		total_score += score * score;

		if (stop_at_choice > -1 && total_score > stop_at_choice)
		{
			ret_value = matchup_data[match_type][row].ndx;
			break;
		}
	}

	if (stop_at_choice == -1) {
		ret_value = total_score;
	}
	return ret_value;
}

int PlayChooser::get_match(int play_type, int match_type, int down, int togo, int yardline, int game_seconds_remaining, int score_diff)
{
	int ndx = -1;

	auto yardline_category = get_yardline_category(yardline);
	auto distance_category = get_distance_category(togo);
	auto time_category = get_time_category(game_seconds_remaining);
	auto score_diff_category = get_score_diff_category(score_diff);
	auto hash = get_hash(yardline_category, distance_category, time_category, score_diff_category, down);
	if (hash > max_hash)
	{
		cout << "ERROR" << endl;
	}
	auto total_score = 0;
	if (play_type == 0)	total_score = match_totals[match_type][hash];
	if (total_score <= 0)
	{
		total_score = score_options(play_type, match_type, down, yardline_category, distance_category, time_category, score_diff_category, yardline, -1);
	}

	//auto total_score = 0;
	if (total_score <= 0)
	{
		switch (match_type)
		{
		case kPCMatchup::home_is_pos:
			match_type = kPCMatchup::away_is_def;
			break;
		case kPCMatchup::away_is_def:
			match_type = kPCMatchup::home_is_pos;
			break;
		case kPCMatchup::home_is_def:
			match_type = kPCMatchup::away_is_pos;
			break;
		case kPCMatchup::away_is_pos:
			match_type = kPCMatchup::home_is_def;
			break;
		}
		if (play_type == 0)	total_score = match_totals[match_type][hash];
		if (total_score <= 0)
		{
			total_score = score_options(play_type, match_type, down, yardline_category, distance_category, time_category, score_diff_category, yardline, -1);
		}
	}

	if (total_score > 0)
	{
		//if (play_type == 0)	match_totals[match_type][hash] = total_score;
		auto guess = rand() % total_score;
		ndx = score_options(play_type, match_type, down, yardline_category, distance_category, time_category, score_diff_category, yardline, guess);
	}

	return ndx;
}

int PlayChooser::get_yardline_category(int yardline)
{
	int category = 0;

	if (yardline >= 99) category = 0;
	else if (yardline >= 98) category = 1;
	else if (yardline >= 95) category = 2;
	else if (yardline >= 90) category = 3;
	else if (yardline >= 80) category = 4;
	else if (yardline >= 50) category = 5;
	else if (yardline >= 30) category = 6;
	else if (yardline >= 20) category = 7;
	else if (yardline >= 10) category = 8;
	else if (yardline >= 5) category = 9;
	else if (yardline >= 2) category = 10;
	else category = 11;
	
	return category;
}

int PlayChooser::get_distance_category(int distance)
{
	int category = 0;

	if (distance >= 20) category = 0;
	else if (distance >= 15) category = 1;
	else if (distance >= 10) category = 2;
	else if (distance >= 5) category = 3;
	else if (distance >= 2) category = 4;
	else category = 5;

	return category;
}

int PlayChooser::get_time_category(int time)
{
	int category = 0;

	if (time >= 1800 + 240) category = 0;
	else if (time > 1800 + 120) category = 1;
	else if (time > 1800 + 60) category = 2;
	else if (time > 1800) category = 3;
	else if (time > 240) category = 4;
	else if (time > 120) category = 5;
	else if (time > 60) category = 6;
	else category = 7;

	return category;
}

int PlayChooser::get_score_diff_category(int score_diff)
{
	int test_score = abs(score_diff);
	int category = 0;

	if (test_score ==0) category = 0;
	else if (test_score <= 3) category = 1;
	else if (test_score <= 6) category = 2;
	else if (test_score <= 8) category = 3;
	else if (test_score <= 16) category = 4;
	else category = 5;

	if (score_diff < 0) category *= -1;

	return category;
}


float PlayChooser::calc_remaining_posessions(GameState& gamestate, bool hometeam)
{
	auto tos = 0;
	if (hometeam) tos = gamestate.home_to;
	else tos = gamestate.away_to;

	auto time_left = gamestate.gameclock.get_game_seconds_remaining();
	if (time_left > 120) time_left += 30;
	time_left += 30 * tos;

	return time_left / 160.0f;
}

bool PlayChooser::use_pos_team()
{
	return (rand() % 2 == 0);
}

void PlayChooser::GetMatchType(GameState& gamestate, kPCMatchup& match_type)
{
	if (use_pos_team())
	{
		if (gamestate.homepossession)
		{
			match_type = kPCMatchup::home_is_pos;
		}
		else
		{
			match_type = kPCMatchup::away_is_pos;
		}
	}
	else
	{
		if (gamestate.homepossession)
		{
			match_type = kPCMatchup::away_is_def;
		}
		else
		{
			match_type = kPCMatchup::home_is_def;
		}
	}
}

int PlayChooser::get_play_ndx_monte_carlo(GameState& gamestate)
{
	auto ndx = -1;

	auto match_type = kPCMatchup::max_match_type;
	GetMatchType(gamestate, match_type);
	bool timeout_check = false;
	bool critical_time = false;
	bool play_type_is_scrimmage = false;
	if (gamestate.gameclock.get_half_seconds_remaining() <= 120) {
		critical_time = true;
	}

	if (gamestate.statemachine->is_ready_for_scrimmage())
	{
		auto in_fg_range = EVCalc::getInstance()->GetLongestFG(gamestate.get_posteam()) >= gamestate.balltracker.yardline;
		auto too_close_to_punt = EVCalc::getInstance()->GetLongestFG(gamestate.get_posteam()) - 5 >= gamestate.balltracker.yardline;
		if (gamestate.balltracker.down == 4 ||
			(gamestate.gameclock.get_half_seconds_remaining() < 25 && in_fg_range))
		{
			if (gamestate.balltracker.yardline < 60)
			{
				kPCPlayType play_type = kPCPlayType::scrimmage;
				gamestate.print_score();

				vector<kPCPlayType> options({ kPCPlayType::scrimmage });
				if (in_fg_range) options.push_back(kPCPlayType::field_goal);
				if (!too_close_to_punt) options.push_back(kPCPlayType::punt);

				auto found_one = PlayChooserMonteCarlo::get_best_option(gamestate, options, play_type);
				cout << "play_type: " << to_string(play_type) << endl;

				if (found_one) {
					ndx = get_match(play_type, match_type,
						gamestate.balltracker.down,
						gamestate.balltracker.yardline,
						gamestate.balltracker.distance,
						gamestate.gameclock.get_game_seconds_remaining(),
						gamestate.get_score_diff_post());
					if (play_type == kPCPlayType::scrimmage) play_type_is_scrimmage = true;
				}
			}
		}
		if (ndx == -1 || play_type_is_scrimmage)
		{
			auto do_mc_test = false;
			if (critical_time) {
				do_mc_test = true;
			}
			else if (gamestate.clock_mode_checked_this_possession) {
				do_mc_test = false;
			}
			else if (gamestate.gameclock.get_half_seconds_remaining() < 900) {
				do_mc_test = true;
			}

			//do_mc_test = true;
			if (do_mc_test)
			{
				cout << endl << endl << "Checking clock mode." << endl << endl;
				gamestate.print_score();

				vector< kGSClockMode> options({ kGSClockMode::normal_cm ,kGSClockMode::run_clock_cm,kGSClockMode::hurry_up_cm });

				kGSClockMode clock_mode = gamestate.get_clock_mode_by_pos(true);
				auto found_one = PlayChooserMonteCarlo::get_best_clock_mode(gamestate, options, clock_mode);
				cout << "clock_mode: " << to_string(clock_mode) << endl;

				gamestate.clock_mode_checked_this_possession = true;
				if (found_one)
				{
					gamestate.set_clock_mode_by_pos(clock_mode, true);
					cout << gamestate.get_posteam() << " is ";
					switch (gamestate.get_clock_mode_by_pos(true))
					{
					case kGSClockMode::normal_cm:
						cout << "starting their drive.";
						break;
					case kGSClockMode::run_clock_cm:
						cout << "letting the play clock wind down.";
						break;
					case kGSClockMode::hurry_up_cm:
						cout << "running quickly up to the line of scrimage.";
						timeout_check = true;
						break;
					}
					cout << endl;
				}
			}
		}
	}
	else if (gamestate.statemachine->is_ready_for_extra_point())
	{
		kPCPlayType play_type = kPCPlayType::extra_point;

		vector<kPCPlayType> options({ kPCPlayType::extra_point, kPCPlayType::two_point_conv });

		auto found_one = PlayChooserMonteCarlo::get_best_option(gamestate, options, play_type);
		cout << "play_type: " << to_string(play_type) << endl;

		if (found_one) {
			ndx = get_match(play_type, match_type,
				gamestate.balltracker.down,
				gamestate.balltracker.distance,
				gamestate.balltracker.yardline,
				gamestate.gameclock.get_game_seconds_remaining(),
				gamestate.get_score_diff_post());
		}
	}

	if (ndx == -1) ndx = get_play_ndx(gamestate);
	
	//if (timeout_check && critical_time && GDI->get_int(ndx, KDCol::play_time) > 5)
	//{
	//	int clock_choice = 1;
	//	auto found_one = PlayChooserMonteCarlo::get_timeout_check(gamestate, GDI->get_int(ndx, KDCol::play_time_hurry_up), clock_choice);
	//	if (found_one && clock_choice == 0)
	//	{
	//		gamestate.UseTimeout(true);
	//		cout << gamestate.get_posteam() << " calls Timeout! They have " << gamestate.GetTimeoutsRemaining(true) << " remaining." << endl;
	//	}
	//}

	return ndx;
}

int PlayChooser::get_play_ndx(GameState& gamestate)
{
	auto ndx = -1;

	auto match_type = kPCMatchup::max_match_type;
	GetMatchType(gamestate, match_type);

	if (gamestate.statemachine->is_ready_for_kickoff())
	{
		ndx = get_match(kPCPlayType::kickoff, match_type,
			gamestate.balltracker.down,
			gamestate.balltracker.distance,
			gamestate.balltracker.yardline,
			gamestate.gameclock.get_game_seconds_remaining(),
			gamestate.get_score_diff_post());
		//ndx = 11784; penalty starts from 6
	}
	else if (gamestate.statemachine->is_ready_for_scrimmage())
	{
		if (gamestate.balltracker.down == 4)
		{
			kEVChoice play_choice = kEVChoice::punt_ev;
			if (gamestate.balltracker.yardline < 60)
			{
				if (global_silent_mode) {
					play_choice = EVCalc::getInstance()->GetBestOptionFast(gamestate.homepossession, gamestate.balltracker.yardline, gamestate.balltracker.distance);
				}
				else
				{
					play_choice = EVCalc::getInstance()->GetBestOption(gamestate.hometeam,
						gamestate.awayteam,
						gamestate.balltracker.yardline,
						gamestate.balltracker.distance,
						false);
				}
			}
			if (play_choice == kEVChoice::fg_ev)
			{
				ndx = get_match(kPCPlayType::field_goal, match_type,
					gamestate.balltracker.down,
					gamestate.balltracker.distance,
					gamestate.balltracker.yardline,
					gamestate.gameclock.get_game_seconds_remaining(),
					gamestate.get_score_diff_post());
			}
			else if (play_choice == kEVChoice::punt_ev)
			{
				ndx = get_match(kPCPlayType::punt, match_type,
					gamestate.balltracker.down,
					gamestate.balltracker.distance,
					gamestate.balltracker.yardline,
					gamestate.gameclock.get_game_seconds_remaining(),
					gamestate.get_score_diff_post());
			}
		}
		if (ndx == -1)
		{
			ndx = get_match(kPCPlayType::scrimmage, match_type,
				gamestate.balltracker.down,
				gamestate.balltracker.distance,
				gamestate.balltracker.yardline,
				gamestate.gameclock.get_game_seconds_remaining(),
				gamestate.get_score_diff_post());
		}
	}
	else if (gamestate.statemachine->is_ready_for_extra_point())
	{
		ndx = -1;
		if (GoForTwo::getInstance()->check(gamestate.get_score_diff_post(), (int) calc_remaining_posessions(gamestate, gamestate.homepossession)) == 1)
		{
			ndx = get_match(kPCPlayType::two_point_conv, match_type,
				gamestate.balltracker.down,
				gamestate.balltracker.distance,
				gamestate.balltracker.yardline,
				gamestate.gameclock.get_game_seconds_remaining(),
				gamestate.get_score_diff_post());
		}
		if (ndx == -1)
		{
			ndx = get_match(kPCPlayType::extra_point, match_type,
				gamestate.balltracker.down,
				gamestate.balltracker.distance,
				gamestate.balltracker.yardline,
				gamestate.gameclock.get_game_seconds_remaining(),
				gamestate.get_score_diff_post());
		}
	}

	return ndx;
}

int PlayChooser::get_play_ndx_force_play_type(GameState& gamestate, kPCPlayType play_type)
{
	auto match_type = kPCMatchup::max_match_type;
	GetMatchType(gamestate, match_type);
	return get_match(play_type, match_type,
		gamestate.balltracker.down,
		gamestate.balltracker.distance,
		gamestate.balltracker.yardline,
		gamestate.gameclock.get_game_seconds_remaining(),
		gamestate.get_score_diff_post());
}


void PlayChooser::dump_match_type(kPCMatchup match_type)
{
	//map<int, int> play_types;
	//for (int row = 0; row < max_current_matchup_row[match_type]; row++)
	//{
	//	int hash = get_hash(matchup_data[match_type][row].yardline_category, matchup_data[match_type][row].distance_category, matchup_data[match_type][row].time_category, matchup_data[match_type][row].score_diff_category, down);
	//	cout << row << " " << (int)matchup_data[match_type][row].sort_key << " " << hash << endl;
	//	int key = matchup_data[match_type][row].sort_key;
	//	if (play_types.count(key) == 0) play_types[key] = 0;
	//	play_types[key]++;
	//}

	//for (auto kvp : play_types)
	//{
	//	cout << kvp.first << " " << kvp.second << endl;
	//}
	//for (int x = 0; x < max_start; x++)
	//{
	//	cout << x << " " << start_match[match_type][x] << " " << end_match[match_type][x] << endl;
	//}
}
