#include "GoForTwo.h"
#include <iostream>
#include <iomanip>
#include "fastcsv.h"

static char* gf2_data = nullptr;
GoForTwo* GoForTwo::instance = nullptr;

GoForTwo::GoForTwo()
{
    if (gf2_data == nullptr) {
        auto filename = "gf2_chart.csv";
        cout << "Loading go for 2 data...";

        if (fast_csv_check(filename, max_len, max_row, max_size))
        {
            gf2_data = new char[max_size];
            fast_csv_load(filename, gf2_data, max_len, max_size);
            cout << "Done." << endl;
            data_loaded = true;
        }
        else
        {
            cout << "Unable to load " << filename << " game can not continue." << endl;
        }
    }
}

int GoForTwo::check(int scoredif, int poss_left)
{
    int ndx = scoredif + 16;
    if (scoredif < 0) scoredif = 0;
    if (scoredif > 30) scoredif = 30;
    int col = poss_left;
    if (col < 0) col = 0;
    if (col > 10) col = 10;
    return atoi(fast_csv_get(gf2_data, max_len, max_row, ndx, col));
}

void GoForTwo::print()
{
    cout << "      ";
    for (int pos_left = 1; pos_left < 11; pos_left++) cout << pos_left << " ";
    cout << endl;
    for (int lead = -16; lead < 15; lead++) {
        cout << setw(3) << lead << " | ";
        for (int pos_left = 1; pos_left < 11; pos_left++) cout << check(lead, pos_left) << " ";
        cout << endl;
    }
}
