#pragma once
#include <vector>
#include "GameState.h"
#include "PlayChooser.h"
#include "EVCalc.h"

using namespace std;

enum kPCMCTimeoutSpike {
	normal_pcmp,
	timeout_pcmp,
	spike_pcmp
};
class PlayChooserMonteCarlo
{
public:
	PlayChooserMonteCarlo() {};
	~PlayChooserMonteCarlo() {};
	static bool get_best_clock_mode(GameState& gamestate, vector< kGSClockMode> &options, kGSClockMode& clock_mode);
	static bool get_best_option(GameState& gamestate, vector<kPCPlayType> options, kPCPlayType& play_type);
	static bool get_timeout_spike_option(GameState& gamestate, vector<kPCMCTimeoutSpike> options, vector<int> play_times, kPCMCTimeoutSpike& timeout_spike_choice, bool defense_check);
	static bool get_score_for_option(GameState& gamestate, kPCPlayType play_type, float &score, float &win_pct, double max_time);
};

