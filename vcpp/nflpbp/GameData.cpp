#include "GameData.h"
#include <iostream>
#include <fstream>
#include "fastcsv.h"

static char* game_data = nullptr;
GameData* GameData::instance = nullptr;

bool GameData::load()
{
    if (game_data == nullptr) {
        auto filename = "game_data.csv";
        cout << "Loading game data...";

        if (!fast_csv_check(filename, max_len, max_row, max_size))
        {
            cout << "Unable to load " << filename << " game can not continue." << endl;
            return false;
        }
        game_data = new char[max_size];
        fast_csv_load(filename, game_data, max_len, max_size);
        cout << "Done." << endl;
    }
    return true;
}

char * GameData::get(int row, int col)
{
    return fast_csv_get(game_data, max_len, max_row, row, col);
}

int GameData::get_int(int row, int col)
{
    return atoi(get(row, col));
}

void GameData::print_stats()
{
    cout << max_len << " " << max_row << " " << max_row * max_len << endl;
}
