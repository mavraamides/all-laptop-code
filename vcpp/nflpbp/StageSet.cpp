#include "StageSet.h"
#include "GameData.h"
#include "fastcsv.h"
#include <iostream>

void Stage::clear()
{
	token = "NONE";
	value = 0;
	team_tags.clear();
	calc_tags.clear();
	extra_info.clear();
}

void Stage::set(std::string t, int v, std::string d, vstr_t tt, vstr_t ct, vstr_t ei)
{
	token = t;
	value = v;
	description = d;
	team_tags = tt;
	calc_tags = ct;
	extra_info = ei;
}

void Stage::dump()
{
	std::cout << token << ",";
	std::cout << std::to_string(value) << ",";
	std::cout << description << ",";
	for (auto tt : team_tags)
	{
		std::cout << tt << ":";
	}
	std::cout << ",";
	for (auto ct : calc_tags)
	{
		std::cout << ct << ":";
	}
	std::cout << ",";
	for (auto ei : extra_info)
	{
		std::cout << ei << ":";
	}
	std::cout << std::endl;
}

void StageSet::add_stage(std::string t, int v, std::string d, vstr_t tt, vstr_t ct, vstr_t ei)
{
	auto s = Stage();
	s.set(t, v, d, tt, ct, ei);
	stages.push_back(s);
}

void StageSet::clear()
{
	stages.clear();
}

void StageSet::decode(int ndx)
{
	stages.clear();
	char buffer[1024];
	char buffer2[1024];

	char* p = GDI->get(ndx, KDCol::stage_tokens);
	auto count = fast_csv_count_fields(p, ':');

	vstr_t token_list;
	for (int x = 0; x < count; x++)
	{
		p = fast_csv_get_next_field(p, buffer, ':');
		token_list.push_back(buffer);
	}

	p = GDI->get(ndx, KDCol::stage_values);
	vstr_t value_list;
	for (int x = 0; x < count; x++)
	{
		p = fast_csv_get_next_field(p, buffer, ':');
		value_list.push_back(buffer);
	}

	p = GDI->get(ndx, KDCol::stage_descriptions);
	vstr_t description_list;
	for (int x = 0; x < count; x++)
	{
		p = fast_csv_get_next_field(p, buffer, ':');
		description_list.push_back(buffer);
	}

	p = GDI->get(ndx, KDCol::team_tags);
	std::vector<vstr_t> team_tag_list;
	for (int x = 0; x < count; x++)
	{
		p = fast_csv_get_next_field(p, buffer, ':');

		auto fcount = fast_csv_count_fields(buffer, ',');
		vstr_t work_list;
		work_list.clear();

		char* q = buffer;
		for (int y = 0; y < fcount; y++)
		{
			q = fast_csv_get_next_field(q, buffer2, ',');
			work_list.push_back(buffer2);
		}
		team_tag_list.push_back(work_list);
	}

	p = GDI->get(ndx, KDCol::calc_tags);
	std::vector<vstr_t> calc_tags_list;
	for (int x = 0; x < count; x++)
	{
		p = fast_csv_get_next_field(p, buffer, ':');

		auto fcount = fast_csv_count_fields(buffer, ',');
		vstr_t work_list;
		work_list.clear();

		char* q = buffer;
		for (int y = 0; y < fcount; y++)
		{
			q = fast_csv_get_next_field(q, buffer2, ',');
			work_list.push_back(buffer2);
		}
		calc_tags_list.push_back(work_list);
	}

	p = GDI->get(ndx, KDCol::stage_extra_info);
	std::vector<vstr_t> extra_info_list;
	for (int x = 0; x < count; x++)
	{
		p = fast_csv_get_next_field(p, buffer, ':');

		auto fcount = fast_csv_count_fields(buffer, ',');
		vstr_t work_list;
		work_list.clear();

		char* q = buffer;
		for (int y = 0; y < fcount; y++)
		{
			q = fast_csv_get_next_field(q, buffer2, ',');
			work_list.push_back(buffer2);
		}
		extra_info_list.push_back(work_list);
	}

	for (int x = 0; x < count; x++)
	{
		add_stage(
			token_list[x],
			atoi(value_list[x].c_str()),
			description_list[x],
			team_tag_list[x],
			calc_tags_list[x],
			extra_info_list[x]
		);
	}
	//cout << GDI->get(ndx, KDCol::desc);
	//dump();
}

void StageSet::dump()
{
	for (auto s : stages)
	{
		s.dump();
	}
}
