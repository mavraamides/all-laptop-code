#pragma once
#include <string>
#include "PlayChooser.h"
#include "GameData.h"
#include "GameState.h"
#include "StageSet.h"
#include <map>
using namespace std;

class PlayPrinter
{
public:
	string home;
	string away;

	PlayPrinter(string hometeam, string awayteam);
	~PlayPrinter() {};

	string get_pos_desc(string posteam, string defteam, int yardline);
	string get_yardline_desc(string posteam, string defteam, int yardline);
	void accum_single_set(string teamname, bool posteam, string category, kPCPlayType playtype);
	void accum_player_names();
	string get_player_name(string type, bool home);
	string get_or_substitute(bool substitute, string subname, bool home, int ndx, KDCol column);
	void get_starting_qbs();
	void print_state(GameState &gamestate);
	int get_int_value(int ndx, KDCol col, int def_value);
	bool replace(std::string& str, const std::string& from, const std::string& to);
	void print_stage_set_desc(GameState& gamestate, StageSet &ss, map<string, string> &team_data, calc_data_t &calc_data, int ndx);
	void print(GameState& gamestate, int ndx, StageSet &ss, calc_data_t &calc_data);
	void sep();
};

