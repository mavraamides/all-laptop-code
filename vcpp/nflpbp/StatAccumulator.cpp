#include "StatAccumulator.h"
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <vector>
#include <fstream>

float calc_rating(Stat a)
{
	float total_games = (float)(a.wins + a.losses + a.ties);
	auto rating = (a.own_points - a.opp_points) / total_games + 100.0f;
	return rating;
}

bool sort_rating(StatWithName a, StatWithName b)
{
	return (calc_rating(a.s) > calc_rating(b.s));
}
StatAccumulator::StatAccumulator()
{
	stats.clear();
}

void StatAccumulator::AddGame(string team1, string team2, float t1pts, float t2pts, int t1wins, int t2wins, int ties)
{
	if (stats.count(team1) == 0) stats[team1] = Stat({ 0.0f, 0.0f, 0,0,0 });
	if (stats.count(team2) == 0) stats[team2] = Stat({ 0.0f, 0.0f, 0,0,0 });
	
	stats[team1].own_points += t1pts;
	stats[team1].opp_points += t2pts;
	stats[team1].wins += t1wins;
	stats[team1].losses += t2wins;
	stats[team1].ties += ties;
	stats[team2].own_points += t2pts;
	stats[team2].opp_points += t1pts;
	stats[team2].wins += t2wins;
	stats[team2].losses += t1wins;
	stats[team2].ties += ties;
}

void StatAccumulator::Dump()
{
	vector<StatWithName> ratings;
	for (auto kvp : stats)
	{
		ratings.push_back({ kvp.first, kvp.second });
	}

	sort(ratings.begin(), ratings.end(), sort_rating);

	int rank = 1;
	for (auto r : ratings)
	{
		float total_games = (float)(r.s.wins + r.s.losses + r.s.ties);

		cout << setw(3) << right << rank << ") ";
		cout << setw(4) << left << r.name << " ";
		cout << setw(5) << r.s.wins << " ";
		cout << setw(5) << r.s.losses << " ";
		cout << setw(5) << r.s.ties << " ";
		cout << setw(10) << right << fixed << setprecision(3) << r.s.own_points / total_games << " ";
		cout << setw(10) << right << fixed << setprecision(3) << r.s.opp_points / total_games << " ";

		auto rating = (r.s.own_points - r.s.opp_points) / total_games + 100.0f;
		cout << setw(10) << right << fixed << setprecision(3) << rating << endl;
		rank++;
	}
}

void StatAccumulator::Write(string filename)
{
	ofstream out(filename);
	if (out.is_open())
	{
		out << "Team,Points,OpPoints,Wins,Losses,Ties,Rating\n";
		for (auto kvp : stats)
		{
			out << kvp.first << ",";
			out << kvp.second.own_points << ",";
			out << kvp.second.opp_points << ",";
			out << kvp.second.wins << ",";
			out << kvp.second.losses << ",";
			out << kvp.second.ties << ",";
			out << calc_rating(kvp.second);
			out << "\n";
		}
		out.close();
	}
}

void StatAccumulator::Merge(StatAccumulator& other)
{
	for (auto kvp : other.stats)
	{
		if (stats.count(kvp.first) == 0) stats[kvp.first] = Stat({ 0.0f, 0.0f, 0,0,0 });
		
		stats[kvp.first].own_points += other.stats[kvp.first].own_points;
		stats[kvp.first].opp_points += other.stats[kvp.first].opp_points;
		stats[kvp.first].wins += other.stats[kvp.first].wins;
		stats[kvp.first].losses += other.stats[kvp.first].losses;
		stats[kvp.first].ties += other.stats[kvp.first].ties;
	}
}
