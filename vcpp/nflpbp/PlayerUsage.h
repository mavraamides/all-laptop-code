#pragma once
#include <string>
#include "WeightedChoice.h"
using namespace std;

enum kPUPlayerCategory {
	passer_player_name_cat,
	receiver_player_name_cat,
	rusher_player_name_cat,
	punter_player_name_cat,
	kickoff_returner_player_name_cat,
	interception_player_name_cat,
	punt_returner_player_name_cat,
	kicker_player_name_KO_cat,
	kicker_player_name_FG_cat,
};

struct PlayerSet
{
	char teamname[4];
	int category;
	char playername[80];
	int count;
};

class PlayerUsage
{
	static PlayerUsage* instance;
	WeightedChoice weighted_choice;

	int max_len = 0;
	int max_row = 0;
	int max_size = 0;

	PlayerUsage();
	~PlayerUsage();
	void add_player(string team, kPUPlayerCategory category, string name);
	void initialize_player_data();
	void write_player_data(string filename);
public:
	static PlayerUsage* getInstance()
	{
		if (instance == nullptr)
		{
			instance = new PlayerUsage();
		}
		return instance;
	}
	string get_player_name(string team, kPUPlayerCategory category);
};

