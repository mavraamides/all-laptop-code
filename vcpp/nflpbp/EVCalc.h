#pragma once
#include <string>
#include <map>
using namespace std;

enum kEVColumn {
	autondx_ev,
	Team_ev,
	yardline_100_ev,
	pos_yl_after_punt_ev,
	pos_p_FG_ev,
	pos_EV_yl_ev,
	pos_p_Run_ev,
	pos_gain_when_Run_made_ev,
	pos_gain_when_Run_fail_ev,
	pos_p_Pass_ev,
	pos_gain_when_Pass_made_ev,
	pos_gain_when_Pass_fail_ev,
	def_yl_after_punt_ev,
	def_p_FG_ev,
	def_EV_yl_ev,
	def_p_Run_ev,
	def_gain_when_Run_made_ev,
	def_gain_when_Run_fail_ev,
	def_p_Pass_ev,
	def_gain_when_Pass_made_ev,
	def_gain_when_Pass_fail
};

enum kEVChoice {
	punt_ev,
	fg_ev,
	rush_ev,
	pass_ev
};

class EVCalc
{
	static EVCalc* instance;
	int max_len = 0;
	int max_row = 0;
	int max_size = 0;
	map<string, int> starting_lines;
	map<string, int> longest_fg;

	EVCalc();
public:
	bool data_loaded = false;

	static EVCalc* getInstance()
	{
		if (instance == nullptr)
		{
			instance = new EVCalc();
		}
		return instance;
	}
	~EVCalc() {};

	int GetLongestFG(string team);
	float GetStat(string team, kEVColumn stat, int ndx);
	float GetAvg(string posteam, string defteam, kEVColumn posstat, kEVColumn defstat, int ndx);
	float CalcPuntEV(string posteam, string defteam, int yardline);
	float CalcFGEV(string posteam, string defteam, int yardline);
	float CalcRushEV(string posteam, string defteam, int yardline, int togo);
	float CalcPassEV(string posteam, string defteam, int yardline, int togo);
	kEVChoice GetBestOption(string posteam, string defteam, int yardline, int togo, bool suppress_print);
	kEVChoice GetBestOptionFast(bool home, int yardline, int togo);
	void MakeChart(string posteam, string defteam, bool home);
	void MakeCharts(string hometeam, string awayteam);
	void PrintChart(bool home);
	static string EVChoiceToString(kEVChoice choice);

};

