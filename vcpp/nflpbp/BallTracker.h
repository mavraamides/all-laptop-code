#pragma once
#include <string>
#include <vector>
using namespace std;

class BallTracker
{
private:
	bool already_advanced_down = false;

public:
	friend class GameState;
	int down = 1;
	int distance = 10;
	int yardline = 35;

	BallTracker() {};
	~BallTracker() {};

	void DeepCopy(BallTracker& other);
	void reset();
	void start_play();
	void reset_dandd();
	bool gain_yards(int yards);
	void change_of_possession();
	void set_yardline(int yline);
	string get_down_desc();
	string get_desc();
	vector<string> GetMiniDesc(bool homepos);
};

