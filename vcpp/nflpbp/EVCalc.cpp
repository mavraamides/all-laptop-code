#include "EVCalc.h"
#include "fastcsv.h"
#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <string>
#include <map>

static char* ev_data = nullptr;
static kEVChoice home_play_chart[100][21];
static kEVChoice away_play_chart[100][21];
EVCalc* EVCalc::instance = nullptr;

EVCalc::EVCalc()
{
    if (ev_data == nullptr) {
        auto filename = "evs.csv";
        cout << "Loading expected value data...";

        if (!fast_csv_check(filename, max_len, max_row, max_size))
        {
            cout << "Unable to load " << filename << " game can not continue." << endl;
         }
        ev_data = new char[max_size];
        fast_csv_load(filename, ev_data, max_len, max_size);
        data_loaded = true;

        for (int row = 0; row < max_row; row++) {
            string team = fast_csv_get(ev_data, max_len, max_row, row, kEVColumn::Team_ev);
            if (starting_lines.count(team) == 0) {
                starting_lines[team] = row;
            }
            auto fg_p = atof(fast_csv_get(ev_data, max_len, max_row, row, kEVColumn::pos_p_FG_ev));
            if (fg_p > 0.0f)
            {
                longest_fg[team] = atoi(fast_csv_get(ev_data, max_len, max_row, row, kEVColumn::yardline_100_ev));
            }
        }
        cout << "Done." << endl;
    }

}

int EVCalc::GetLongestFG(string team)
{
    if (longest_fg.count(team) > 0) return longest_fg[team];
    return 30;
}

float EVCalc::GetStat(string team, kEVColumn stat, int ndx)
{
    float return_stat = 0.0f;

    int starting_row = 0;
    int ending_row = max_row;
    bool skip_tests = false;
    if (starting_lines.count(team) > 0) {
        starting_row = starting_lines[team];
        if (ndx >= 0 && ndx < 100)
        {
            starting_row += ndx - 1;
            ending_row = starting_row + 1;
            skip_tests = true;
        }
    }
    if (skip_tests) {
        return_stat = (float)atof(fast_csv_get(ev_data, max_len, max_row, starting_row, stat));
    }
    else {
        for (int row = starting_row; row < ending_row; row++)
        {
            string test_team = fast_csv_get(ev_data, max_len, max_row, row, kEVColumn::Team_ev);
            if (team == test_team && atoi(fast_csv_get(ev_data, max_len, max_row, row, kEVColumn::yardline_100_ev)) == ndx)
            {
                return_stat = (float)atof(fast_csv_get(ev_data, max_len, max_row, row, stat));
                break;
            }
        }

    }

    return return_stat;
}

float EVCalc::GetAvg(string posteam, string defteam, kEVColumn posstat, kEVColumn defstat, int ndx)
{
    float total = 0.0f;
    if (ndx < 1) ndx = 1;
    if (ndx > 99) ndx = 99;
    total += GetStat(posteam, posstat, ndx);
    total += GetStat(defteam, defstat, ndx);
    return total / 2.0f;
}

float EVCalc::CalcPuntEV(string posteam, string defteam, int yardline)
{
    auto expected_yl = int(GetAvg(posteam, defteam, kEVColumn::pos_yl_after_punt_ev,
        kEVColumn::def_yl_after_punt_ev, yardline) + 0.5f);
    auto ev = -1.0f * GetAvg(posteam, defteam, kEVColumn::pos_EV_yl_ev, kEVColumn::def_EV_yl_ev, expected_yl);
    return ev;
}
float EVCalc::CalcFGEV(string posteam, string defteam, int yardline)
{
    if (yardline > GetLongestFG(posteam)) return -100.0f;
    auto pMake = GetAvg(posteam, defteam, kEVColumn::pos_p_FG_ev, kEVColumn::def_p_FG_ev, yardline);
    auto ylv_after_KO = GetAvg(posteam, defteam, kEVColumn::pos_EV_yl_ev, kEVColumn::def_EV_yl_ev, 75);
    auto new_yl = 100 - yardline - 8;
    auto ylv_after_Miss = GetAvg(posteam, defteam, kEVColumn::pos_EV_yl_ev, kEVColumn::def_EV_yl_ev, new_yl);
    auto ev = pMake * (3.0f - ylv_after_KO) - (1.0f - pMake) * ylv_after_Miss;
    return ev;
}


float EVCalc::CalcRushEV(string posteam, string defteam, int yardline, int togo)
{
    auto pMake = GetAvg(posteam, defteam, kEVColumn::pos_p_Run_ev, kEVColumn::def_p_Run_ev, togo);
    auto yl_after_make = yardline - int(GetAvg(posteam, defteam, kEVColumn::pos_gain_when_Run_made_ev, kEVColumn::def_gain_when_Run_made_ev, togo) + 0.5f);
    auto ylv__make = GetAvg(posteam, defteam, kEVColumn::pos_EV_yl_ev, kEVColumn::def_EV_yl_ev, yl_after_make);
    auto yl_after_miss = 100 - (yardline - int(GetAvg(posteam, defteam, kEVColumn::pos_gain_when_Run_fail_ev, kEVColumn::def_gain_when_Run_fail_ev, togo) + 0.5f));
    auto ylv_miss = GetAvg(posteam, defteam, kEVColumn::pos_EV_yl_ev, kEVColumn::def_EV_yl_ev, yl_after_miss);
    auto ylv_after_KO = GetAvg(posteam, defteam, kEVColumn::pos_EV_yl_ev, kEVColumn::def_EV_yl_ev, 75);
    auto ev = pMake * ylv__make - (1.0f - pMake) * ylv_miss - ylv_after_KO;
    return ev;
}
/*
        pMake = self.GetAvg(posteam, defteam, 'p_Pass', togo)
        yl_after_make = yardline - self.fround(self.GetAvg(posteam, defteam, 'gain_when_Pass_made', togo))
        ylv_make = self.GetAvg(posteam, defteam, 'EV_yl', yl_after_make)
        yl_after_miss = 100 - (yardline - self.fround(self.GetAvg(posteam, defteam, 'gain_when_Pass_fail', togo)))
        ylv_miss = self.GetAvg(posteam, defteam, 'EV_yl', yl_after_miss)
        ylv_after_KO = self.GetAvg(posteam, defteam, 'EV_yl', 75)
        # print(pMake, yl_after_make, ylv_make, yl_after_miss, ylv_miss)
        ev = pMake * ylv_make - (1 - pMake) * ylv_miss - ylv_after_KO
*/
float EVCalc::CalcPassEV(string posteam, string defteam, int yardline, int togo)
{
    auto pMake = GetAvg(posteam, defteam, kEVColumn::pos_p_Pass_ev, kEVColumn::def_p_Pass_ev, togo);
    auto yl_after_make = yardline - int(GetAvg(posteam, defteam, kEVColumn::pos_gain_when_Pass_made_ev, kEVColumn::def_gain_when_Pass_made_ev, togo) + 0.5f);
    auto ylv__make = GetAvg(posteam, defteam, kEVColumn::pos_EV_yl_ev, kEVColumn::def_EV_yl_ev, yl_after_make);
    auto yl_after_miss = 100 - (yardline - int(GetAvg(posteam, defteam, kEVColumn::pos_gain_when_Pass_fail_ev, kEVColumn::pos_gain_when_Pass_fail_ev, togo) + 0.5f));
    auto ylv_miss = GetAvg(posteam, defteam, kEVColumn::pos_EV_yl_ev, kEVColumn::def_EV_yl_ev, yl_after_miss);
    auto ylv_after_KO = GetAvg(posteam, defteam, kEVColumn::pos_EV_yl_ev, kEVColumn::def_EV_yl_ev, 75);
    auto ev = pMake * ylv__make - (1.0f - pMake) * ylv_miss - ylv_after_KO;
    return ev;
}

kEVChoice EVCalc::GetBestOption(string posteam, string defteam, int yardline, int togo, bool suppress_print)
{
    auto best_ev = CalcPuntEV(posteam, defteam, yardline);
    auto best_option = kEVChoice::punt_ev;
    if (!suppress_print) cout << EVChoiceToString(kEVChoice::punt_ev) << ": " << best_ev << endl;

    auto ev = CalcFGEV(posteam, defteam, yardline);
    if (!suppress_print) cout << EVChoiceToString(kEVChoice::fg_ev) << ": " << ev << endl;
    if (ev > best_ev) {
        best_ev = ev;
        best_option = kEVChoice::fg_ev;
    }

    ev = CalcRushEV(posteam, defteam, yardline, togo);
    if (!suppress_print) cout << EVChoiceToString(kEVChoice::rush_ev) << ": " << ev << endl;
    if (ev > best_ev) {
        best_ev = ev;
        best_option = kEVChoice::rush_ev;
    }

    ev = CalcPassEV(posteam, defteam, yardline, togo);
    if (!suppress_print) cout << EVChoiceToString(kEVChoice::pass_ev) << ": " << ev << endl;
    if (ev > best_ev) {
        best_ev = ev;
        best_option = kEVChoice::pass_ev;
    }

    return best_option;
}

kEVChoice EVCalc::GetBestOptionFast(bool home, int yardline, int togo)
{
    kEVChoice result = kEVChoice::punt_ev;
    if (yardline >= 1 && yardline < 100 && togo >= 1)
    {
        if (togo > 20) togo = 20;
        auto play_chart = home_play_chart;
        if (!home) play_chart = away_play_chart;
        result = play_chart[yardline][togo];
    }
    return result;
}

void EVCalc::MakeChart(string posteam, string defteam, bool home)
{
    cout << "Building play chart (" << posteam << ") (" << defteam << ")...";
    auto play_chart = home_play_chart;
    if (!home) play_chart = away_play_chart;
    for (int yardline = 1; yardline < 100; yardline++) {
        for (int togo = 1; togo < 21; togo++) {
            play_chart[yardline][togo] = GetBestOption(posteam, defteam, yardline, togo, true);
        }
    }
    cout << "Done." << endl;
}

void EVCalc::MakeCharts(string hometeam, string awayteam)
{
    MakeChart(hometeam, awayteam, true);
    MakeChart(awayteam, hometeam, false);
}

void EVCalc::PrintChart(bool home)
{
    auto skip = 0;
    auto play_chart = home_play_chart;
    if (!home) play_chart = away_play_chart;
    for (int yardline = 1; yardline < 100; yardline++) {
        if (skip <= 0) {
            cout << "  ";
            for (int togo = 1; togo < 21; togo++) {
                cout << setw(4) << togo << " ";
            }
            cout << endl;
            skip = 10;
        }
        cout << setw(2) << yardline << " ";
        for (int togo = 1; togo < 21; togo++) {
            auto targetline = yardline - togo;
            if (targetline >= 90) {
                cout << "---- ";
            }
            else {
                cout << setw(4) << EVChoiceToString(play_chart[yardline][togo]) << " ";
            }
        }
        cout << endl;
        skip -= 1;
    }
}

string EVCalc::EVChoiceToString(kEVChoice choice)
{
    string result = "UNKW";

    switch (choice) {
    case kEVChoice::fg_ev:
        result = "FGA ";
        break;
    case kEVChoice::pass_ev:
        result = "PASS";
        break;
    case kEVChoice::rush_ev:
        result = "RUSH";
        break;
    case kEVChoice::punt_ev:
        result = "PUNT";
        break;
    }
    return result;
}

