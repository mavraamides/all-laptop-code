#pragma once
#include <string>
using namespace std;

#define GDI GameData::getInstance()

enum KDCol {
	autondx,
	ndx,
	down,
	ydstogo,
	yardline_100,
	yards_gained,
	home_team,
	posteam,
	defteam,
	game_seconds_remaining,
	score_differential_post,
	passer_player_name,
	receiver_player_name,
	rusher_player_name,
	interception_player_name,
	punt_returner_player_name,
	kickoff_returner_player_name,
	punter_player_name,
	kicker_player_name,
	desc,
	play_type_nfl,
	incomplete_pass,
	stage_tokens,
	stage_values,
	stage_descriptions,
	team_tags,
	calc_tags,
	stage_encoded,
	stage_extra_info,
	play_time,
	play_time_run_clock,
	play_time_hurry_up,
	used,
	match_score,
	yardline_category,
	distance_category,
	time_category,
	score_diff_category
};


class GameData
{
	static GameData* instance;

	int max_len = 0;
	int max_row = 0;
	int max_size = 0;
	GameData() {
		data_loaded = load();
	};
	bool load();
public:
	bool data_loaded = false;
	static GameData* getInstance()
	{
		if (instance == nullptr)
		{
			instance = new GameData();
		}
		return instance;
	}
	~GameData() {};
	char* get(int row, int col);
	int get_int(int row, int col);
	void print_stats();
	int get_max_row() { return max_row; }
};

