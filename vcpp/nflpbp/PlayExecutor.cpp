#include "PlayExecutor.h"
#include "StageSet.h"
#include "GameData.h"
#include "GameState.h"
#include "FastStageSet.h"
#include "PlayChooserMonteCarlo.h"
#include "EVCalc.h"
#include <iostream>
#include <set>
// ADD_PLAY_TYPE_PLAY
std::set<std::string> valid_play_types({ "KICK_OFF", "RUSH" , "XP_KICK", "FIELD_GOAL","PUNT", "PASS", "PAT2", "PENALTY", "SACK"});

void PlayExecutor::execute(GameState& gamestate, int ndx, calc_data_t& calc_data, StageSet& rs, bool use_monte_carlo)
{
	gamestate.balltracker.start_play();
	calc_data.clear();
	StageSet ss;
	ss.decode(ndx);
	rs.clear();

	//cout << GDI->get_int(ndx, KDCol::play_time) << " ";
	//cout << GDI->get_int(ndx, KDCol::play_time_run_clock) << " ";
	//cout << GDI->get_int(ndx, KDCol::play_time_hurry_up) << " ";
	//cout << clock_mode << " " << play_time << endl;


	auto CALC_yards_gained = GDI->get_int(ndx, KDCol::yards_gained);
	if (CALC_yards_gained > gamestate.balltracker.yardline) {
		CALC_yards_gained = gamestate.balltracker.yardline;
	}
	calc_data["CALC_yards_gained"] = to_string(CALC_yards_gained);
	calc_data["CALC_starting_yardline"] = to_string(gamestate.balltracker.yardline);
	calc_data["CALC_firstdown"] = to_string(0);

	std::string play_type_nfl = GDI->get(ndx, KDCol::play_type_nfl);
	if (valid_play_types.count(play_type_nfl) == 0) return;

	bool timeout_spike_check = false;
	bool clock_could_be_running = false;
	for (auto stage : ss.stages)
	{
		if (stage.token == "DESCRIPTION")
		{
			rs.stages.push_back(stage);
		}
		else if (stage.token == "KICK_OFF")
		{
			gamestate.clock_mode_checked_this_possession = false;
			rs.stages.push_back(stage);
		}
		else if (stage.token == "KICK_OFF_RETURN")
		{
			auto CALC_nextyl_after_KO = stage.value;
			if (gamestate.balltracker.yardline != 35)
			{
				CALC_nextyl_after_KO = CALC_nextyl_after_KO + gamestate.balltracker.yardline - 35;
				stage.description = "It is returned by [posteamname] to [CALC_nextyl_after_KO].";
			}
			gamestate.set_yardline_after_kickoff(CALC_nextyl_after_KO, play_time);
			calc_data["CALC_nextyl_after_KO"] = to_string(CALC_nextyl_after_KO);
			rs.stages.push_back(stage);
		}
		else if (stage.token == "GAIN")
		{
			auto gsrs = gamestate.gain_yards_on_scrimmage(calc_data, stage.value, play_time);
			stage.value += gsrs.gain_adjust;
			rs.stages.push_back(stage);
			for (auto s : gsrs.ss.stages) rs.stages.push_back(s);
			if (gsrs.terminate_series) break;
			clock_could_be_running = true;
			timeout_spike_check = true; 
		}
		else if (stage.token == "CHANGE_OF_POSSESSION")
		{
			gamestate.clock_mode_checked_this_possession = false;
			rs.stages.push_back(stage);
			if (stage.value == 1) gamestate.change_of_possession();
		}
		else if (stage.token == "XP_KICK")
		{
			if (stage.value == 1)
			{
				gamestate.statemachine->extra_point_good_event();
			}
			else
			{
				gamestate.statemachine->extra_point_failed_event();
			}
			rs.stages.push_back(stage);
		}
		else if (stage.token == "PAT2")
		{
			if (stage.value == 1)
			{
				gamestate.statemachine->extra_point_two_points_good_event();
			}
			else
			{
				gamestate.statemachine->extra_point_failed_event();
			}
			rs.stages.push_back(stage);
		}
		else if (stage.token == "FIELD_GOAL")
		{
			gamestate.clock_mode_checked_this_possession = false;
			auto ball_kicked_from = gamestate.balltracker.yardline + 8;
			auto new_position = ball_kicked_from;
			if (new_position < 20) new_position = 20;
			calc_data["CALC_field_goal_distane_FG"] = to_string(ball_kicked_from + 10);
			if (EVCalc::getInstance()->GetLongestFG(gamestate.get_posteam()) < gamestate.balltracker.yardline) {
				stage.value = 0;
				stage.description = "[kicker_player_name] ([posteamname]) lines up for a [CALC_field_goal_distane_FG] yard field goal attempt. It's no good!";
			}
			if (stage.value == 3) {
				gamestate.statemachine->field_goal_good_event();
				//gamestate.gameclock.advance(play_time);
			}
			else {
				gamestate.balltracker.yardline = new_position;
				gamestate.change_of_possession();
				//gamestate.gameclock.advance(play_time);
			}
			rs.stages.push_back(stage);
		}
		else if (stage.token == "PUNT")
		{
			gamestate.clock_mode_checked_this_possession = false;
			auto CALC_kick_distance_PUNT = stage.value;
			if (CALC_kick_distance_PUNT > gamestate.balltracker.yardline) {
				stage.value = CALC_kick_distance_PUNT;
				rs.stages.push_back(stage);
				rs.add_stage("DESCRIPTION", 0, "It goes into the endzone for a touchback.", vstr_t(), vstr_t(), vstr_t());
				gamestate.change_of_possession();
				gamestate.balltracker.yardline = 80;
				gamestate.balltracker.reset_dandd();
				//gamestate.gameclock.advance(play_time);
				break;
			}
			else
			{
				rs.stages.push_back(stage);
				gamestate.balltracker.yardline -= stage.value;
				gamestate.change_of_possession();
				//gamestate.gameclock.advance(play_time);
			}
		}
		else if (stage.token == "RETURN")
		{
			auto gsrs = gamestate.gain_yards_on_return(calc_data, stage.value, play_time);
			stage.value += gsrs.gain_adjust;
			rs.stages.push_back(stage);
			for (auto s : gsrs.ss.stages) rs.stages.push_back(s);
			if (gsrs.terminate_series) break;
		}
		else if (stage.token == "PUNT_BLOCKED")
		{
			gamestate.clock_mode_checked_this_possession = false;
			gamestate.change_of_possession();
			auto gsrs = gamestate.gain_yards_on_return(calc_data, (rand() % 41) - 10, play_time);
			stage.value += gsrs.gain_adjust;
			rs.stages.push_back(stage);
			for (auto s : gsrs.ss.stages) rs.stages.push_back(s);
			if (gsrs.terminate_series) break;
		}
		else 
		if (stage.token == "PASS")
		{
			calc_data["pass_length"] = "deep";
			if (gamestate.balltracker.yardline < 20) calc_data["pass_length"] = "short";
			if (stage.value > gamestate.balltracker.yardline) stage.value = gamestate.balltracker.yardline;
			//auto gsrs = gamestate.gain_yards_on_scrimmage(calc_data, stage.value, play_time);
			calc_data["CALC_air_yards_PASS"] = to_string(stage.value);
			rs.stages.push_back(stage);
			//for (auto s : gsrs.ss.stages) rs.stages.push_back(s);
			//if (gsrs.terminate_series) break;
			//timeout_spike_check = true;
		}
		else if (stage.token == "GAIN_AFTER_CATCH")
		{
			auto CALC_air_yards_PASS = 0;
			cout << "Air yards: " << CALC_air_yards_PASS << endl;
			if (GDI->get_int(ndx, KDCol::incomplete_pass) != 1) {
				if (calc_data.count("CALC_air_yards_PASS") > 0) CALC_air_yards_PASS = atoi(calc_data["CALC_air_yards_PASS"].c_str());
				cout << "Air yards: " << CALC_air_yards_PASS << endl;
				clock_could_be_running = true;
			}
			auto CALC_yards_after_catch = stage.value - CALC_air_yards_PASS;
			cout << "Yards after catch: " << CALC_yards_after_catch << endl;
			auto gsrs = gamestate.gain_yards_on_scrimmage(calc_data, stage.value, play_time);
			CALC_yards_after_catch += gsrs.gain_adjust;
			cout << "Yards after catch: " << CALC_yards_after_catch << endl;
			stage.value = CALC_yards_after_catch + CALC_air_yards_PASS;
			cout << "Gain: " << stage.value << endl;
			calc_data["CALC_yards_after_catch"] = to_string(CALC_yards_after_catch);
			rs.stages.push_back(stage);
			for (auto s : gsrs.ss.stages) rs.stages.push_back(s);
			if (gsrs.terminate_series) break;
			timeout_spike_check = true;
		}
		else if (stage.token == "PENALTY")
		{
			//cout << "!!!Penalty!!!" ;
			//for (auto ei : stage.extra_info) cout << ei << ":" << endl;
			auto on_offense = false;
			auto on_defense = false;
			auto auto_first_down = false;
			auto spotfoul = false;
			auto down_over = false;
			auto add_to_yardage = false;
			for (auto ei : stage.extra_info)
			{
				if (ei == "ONOFFENSE") on_offense = true;
				else if (ei == "ONDEFENSE") on_defense = true;
				else if (ei == "AUTOFIRSTDOWN") auto_first_down = true;
				else if (ei == "SPOTFOUL") spotfoul = true;
				else if (ei == "DOWNOVER") down_over = true;
				else if (ei == "ADDTOYARDAGE") add_to_yardage = true;
				else {
					cout << "!!! UNKNOWN TAG !!! " << ei << endl;
				}
			}
			if (on_defense && !auto_first_down) down_over = true;
			auto yards = stage.value;
			GameStateReturnSet gsrs;
			if (on_offense) {
				if (!spotfoul && gamestate.balltracker.yardline + 2 * yards >= 100) {
					yards = (99 - gamestate.balltracker.yardline) / 2;
				}
				gsrs = gamestate.gain_yards_on_penalty(calc_data, -yards, play_time, auto_first_down, down_over);
			}
			else {
				if (!spotfoul && gamestate.balltracker.yardline - 2 * yards <= 0) {
					yards = (gamestate.balltracker.yardline + 1) / 2;
				}
				if (spotfoul && gamestate.balltracker.yardline - yards <= 0) {
					yards = gamestate.balltracker.yardline - 1;
				}
				gsrs = gamestate.gain_yards_on_penalty(calc_data, yards, play_time, auto_first_down, down_over);

			}
			rs.stages.push_back(stage);
			for (auto s : gsrs.ss.stages) rs.stages.push_back(s);
			if (gsrs.terminate_series) break;
		}
		else {
			cout << "UNAHNDLED STAGE: " << ndx << " " << stage.description << endl;
		}
	}

	auto play_time = GDI->get_int(ndx, KDCol::play_time);
	auto clock_mode = gamestate.get_clock_mode_by_pos(true);
	if (clock_mode == kGSClockMode::run_clock_cm)
	{
		play_time = GDI->get_int(ndx, KDCol::play_time_run_clock);
	}
	else if (clock_mode == kGSClockMode::hurry_up_cm)
	{
		play_time = GDI->get_int(ndx, KDCol::play_time_hurry_up);
	}

	if (timeout_spike_check && gamestate.gameclock.get_half_seconds_remaining() < 120 && GDI->get_int(ndx,KDCol::incomplete_pass) != 1)
	{
		cout << endl << "TIMEOUT SPIKE CHECK" << endl << endl;

		vector<kPCMCTimeoutSpike> options({ kPCMCTimeoutSpike::normal_pcmp });
		vector<int> play_times({ play_time });
		kPCMCTimeoutSpike timeout_spike_choice = kPCMCTimeoutSpike::normal_pcmp;
		if (gamestate.balltracker.down <= 2 ||
			(gamestate.balltracker.down <= 3 && gamestate.get_timeouts_remaining_by_pos(true) == 0))
		{
			options.push_back(kPCMCTimeoutSpike::spike_pcmp);
			play_times.push_back(8);
		}
		if (gamestate.get_timeouts_remaining_by_pos(true) > 0)
		{
			options.push_back(kPCMCTimeoutSpike::timeout_pcmp);
			play_times.push_back(5);
		}
		auto found_one = PlayChooserMonteCarlo::get_timeout_spike_option(gamestate, options, play_times, timeout_spike_choice, false);
		cout << "timeout_spike_choice: " << to_string(timeout_spike_choice) << endl;
		if (found_one)
		{
			if (timeout_spike_choice == kPCMCTimeoutSpike::timeout_pcmp)
			{
				gamestate.use_timeout_by_pos(true);
				play_time = 5;
				rs.add_stage("DESCRIPTION", 0, "[posteamname] calls Timeout! They have " + to_string(gamestate.get_timeouts_remaining_by_pos(true)) + " remaining.", vstr_t({ "posteamname" }), vstr_t(), vstr_t());
			}
			else if (timeout_spike_choice == kPCMCTimeoutSpike::spike_pcmp)
			{
				gamestate.balltracker.down++;
				play_time = 8;
				rs.add_stage("DESCRIPTION", 0, "[posteamname] spikes the ball!", vstr_t({ "posteamname" }), vstr_t(), vstr_t());
			}
			else if (gamestate.get_timeouts_remaining_by_pos(false) > 0)
			{
				cout << endl << "DEFENSE TIMEOUT CHECK" << endl << endl;

				options.clear();
				play_times.clear();
				timeout_spike_choice = kPCMCTimeoutSpike::normal_pcmp;
				options.push_back(kPCMCTimeoutSpike::normal_pcmp);
				options.push_back(kPCMCTimeoutSpike::timeout_pcmp);
				play_times.push_back(play_time);
				play_times.push_back(5);
				auto found_one = PlayChooserMonteCarlo::get_timeout_spike_option(gamestate, options, play_times, timeout_spike_choice, true);

				if (found_one && timeout_spike_choice == kPCMCTimeoutSpike::timeout_pcmp) 
				{
					gamestate.use_timeout_by_pos(false);
					play_time = 5;
					rs.add_stage("DESCRIPTION", 0, "[defteamname] calls a defensive Timeout! They have " + to_string(gamestate.get_timeouts_remaining_by_pos(false)) + " remaining.", vstr_t({ "defteamname" }), vstr_t(), vstr_t());
				}

				cout << "timeout_spike_choice: " << to_string(timeout_spike_choice) << endl;
				//options.push_back
			}
		}
	}
	
	if (!clock_could_be_running) gamestate.gameclock.stop_clock();
	gamestate.gameclock.advance(play_time);

	// ADD_PLAY_TYPE_STAGE
}

void PlayExecutor::fast_execute(GameState& gamestate, int ndx)
{
	gamestate.balltracker.start_play();

	auto stage_set_index = FSS->get_index(ndx);
	auto play_time = stage_set_index->play_time;
	auto clock_mode = gamestate.get_clock_mode_by_pos(true);

	if (clock_mode == kGSClockMode::run_clock_cm)
	{
		play_time = stage_set_index->play_time_run_clock;
	}
	else if (clock_mode == kGSClockMode::hurry_up_cm)
	{
		play_time = stage_set_index->play_time_hurry_up;
	}

	auto CALC_yards_gained = stage_set_index->yards_gained;

	if (CALC_yards_gained > gamestate.balltracker.yardline) {
		CALC_yards_gained = gamestate.balltracker.yardline;
	}
	auto CALC_air_yards_PASS = 0;
	
	auto stage_ndx = 0;
	while (true)
	{
		auto stage = FSS->get_element(ndx, stage_ndx);
		if (!stage) break;
		stage_ndx++;
		if (stage->token == kFSSTokens::kick_off_return_fss)
		{
			auto CALC_nextyl_after_KO = stage->value;
			if (gamestate.balltracker.yardline != 35)
			{
				CALC_nextyl_after_KO = CALC_nextyl_after_KO + gamestate.balltracker.yardline - 35;
			}
			gamestate.set_yardline_after_kickoff(CALC_nextyl_after_KO, play_time);
		}
		else if (stage->token == kFSSTokens::gain_fss)
		{
			auto terminate_series = gamestate.fast_gain_yards_on_scrimmage(stage->value, play_time);
			if (terminate_series) break;
		}
		else if (stage->token == kFSSTokens::change_of_possession_ffs)
		{
			if (stage->value == 1) gamestate.change_of_possession();
		}
		else if (stage->token == kFSSTokens::xp_kick_fss)
		{
			if (stage->value == 1)
			{
				gamestate.statemachine->extra_point_good_event();
			}
			else
			{
				gamestate.statemachine->extra_point_failed_event();
			}
		}
		else if (stage->token == kFSSTokens::two_point_conv_fss)
		{
			if (stage->value == 1)
			{
				gamestate.statemachine->extra_point_two_points_good_event();
			}
			else
			{
				gamestate.statemachine->extra_point_failed_event();
			}
		}
		else if (stage->token == kFSSTokens::field_goal_fss)
		{
			auto ball_kicked_from = gamestate.balltracker.yardline + 8;
			auto new_position = ball_kicked_from;
			if (new_position < 20) new_position = 20;
			auto result = stage->value;
			if (EVCalc::getInstance()->GetLongestFG(gamestate.get_posteam()) < gamestate.balltracker.yardline) result = 0;
			if (result == 3) {
				gamestate.statemachine->field_goal_good_event();
				gamestate.gameclock.advance(play_time);
			}
			else {
				gamestate.balltracker.yardline = new_position;
				gamestate.change_of_possession();
				gamestate.gameclock.advance(play_time);
			}
		}
		else if (stage->token == kFSSTokens::punt_fss)
		{
			auto CALC_kick_distance_PUNT = stage->value;
			if (CALC_kick_distance_PUNT > gamestate.balltracker.yardline) {
				gamestate.change_of_possession();
				gamestate.balltracker.yardline = 80;
				gamestate.balltracker.reset_dandd();
				gamestate.gameclock.advance(play_time);
				break;
			}
			else
			{
				gamestate.balltracker.yardline -= stage->value;
				gamestate.change_of_possession();
				gamestate.gameclock.advance(play_time);
			}
		}
		else if (stage->token == kFSSTokens::return_fss)
		{
			auto terminate_series = gamestate.fast_gain_yards_on_return(stage->value, play_time);
			if (terminate_series) break;
		}
		else if (stage->token == kFSSTokens::punt_blocked_fss)
		{
			gamestate.change_of_possession();
			auto terminate_series = gamestate.fast_gain_yards_on_return((rand() % 41) - 10, play_time);
			if (terminate_series) break;
		}
		else if (stage->token == kFSSTokens::pass_fss)
		{
			CALC_air_yards_PASS = stage->value;
			if (CALC_air_yards_PASS > gamestate.balltracker.yardline) CALC_air_yards_PASS = gamestate.balltracker.yardline;
		}
		else if (stage->token == kFSSTokens::gain_after_catch_fss)
		{
			auto terminate_series = gamestate.fast_gain_yards_on_scrimmage(stage->value, play_time);
			if (terminate_series) break;
		}
		else if (stage->token == kFSSTokens::penalty_fss)
		{
			//cout << "!!!Penalty!!!" ;
			bool on_offense = stage->penalty_flags & 0x1;
			bool on_defense = stage->penalty_flags & 0x2;
			bool auto_first_down = stage->penalty_flags & 0x4;
			bool spotfoul = stage->penalty_flags & 0x8;
			bool down_over = stage->penalty_flags & 0x10;
			bool add_to_yardage = stage->penalty_flags & 0x20;
			auto yards = stage->value;
			bool terminate_series = false;
			if (on_defense && !auto_first_down) down_over = true;
			if (on_offense) {
				if (!spotfoul && gamestate.balltracker.yardline + 2 * yards >= 100) {
					yards = (99 - gamestate.balltracker.yardline) / 2;
				}
				terminate_series = gamestate.fast_gain_yards_on_penalty(-yards, play_time, false, down_over);
			}
			else {
				if (!spotfoul && gamestate.balltracker.yardline - 2 * yards <= 0) {
					yards = (gamestate.balltracker.yardline + 1) / 2;
				}
				if (spotfoul && gamestate.balltracker.yardline - yards <= 0) {
					yards = gamestate.balltracker.yardline - 1;
				}
				terminate_series = gamestate.fast_gain_yards_on_penalty(yards, play_time, auto_first_down, down_over);

			}
			if (terminate_series) break;
		}
	}
	// ADD_PLAY_TYPE_STAGE
}

