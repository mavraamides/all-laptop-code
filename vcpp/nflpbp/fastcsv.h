extern bool fast_csv_check(const char* filename, int& max_len, int& max_row, int& max_size);
extern void fast_csv_load(const char* filename, char* table, int max_len, int max_size);
extern char* fast_csv_get(char* table, int max_len, int max_row, int row, int col);
extern int fast_csv_count_fields(const char* data, char delim);
extern char* fast_csv_get_next_field(const char* data, char* buffer, char delim);
