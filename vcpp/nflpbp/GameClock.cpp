#include "GameClock.h"
#include <sstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include "GameState.h"

extern bool global_silent_mode;

void GameClock::DeepCopy(GameClock& other)
{
	quarter = other.quarter;
	game_seconds_remaining = other.game_seconds_remaining;
	clock_running_start = other.clock_running_start;
	clock_running_end = other.clock_running_end;
}

void GameClock::reset()
{
	quarter = 1;
	game_seconds_remaining = 3600;
	clock_running_start = false;
	clock_running_end = false;
}

string GameClock::get_quarter_desc()
{
	string desc;
	switch (quarter)
	{
	case 1:
		desc += "first quarter";
		break;
	case 2:
		desc += "second quarter";
		break;
	case 3:
		desc += "third quarter";
		break;
	case 4:
		desc += "fourth quarter";
		break;
	default:
		desc += "overtime";
		break;
	}

	return desc;
}

int GameClock::get_half()
{
	if (quarter == 1 || quarter == 2) return 1;
	else return 2;
}

int GameClock::get_quarter_seconds_remaining()
{
	auto retVal = game_seconds_remaining;

	switch (quarter)
	{
	case 1:
		retVal -= 2700;
		break;
	case 2:
		retVal -= 1800;
		break;
	case 3:
		retVal -= 900;
		break;
	default:
		break;
	}

	return retVal;
}

int GameClock::get_half_seconds_remaining()
{
	auto retVal = game_seconds_remaining;

	switch (quarter)
	{
	case 1:
	case 2:
		retVal -= 1800;
		break;
	default:
		break;
	}

	return retVal;
}

int GameClock::get_game_seconds_remaining()
{
	return game_seconds_remaining;
}

int GameClock::get_quarter()
{
	return quarter;
}

string GameClock::get_clock_desc()
{
	auto minutes = get_quarter_seconds_remaining() / 60;
	auto seconds = get_quarter_seconds_remaining() % 60;
	ostringstream oss;
	oss << minutes;
	oss << ":";
	oss << setfill('0') << setw(2) << seconds;

	return oss.str();
}

void GameClock::print()
{
	cout << get_clock_desc();
	cout << " left in ";
	cout << get_quarter_desc();
	cout << endl;
}

void GameClock::advance(int seconds)
{
	if (!clock_running_start && seconds > 10) seconds = 10;
	if (get_half_seconds_remaining() > 120 && seconds >= get_half_seconds_remaining() - 120) {
		if (!global_silent_mode) cout << "Two minute warning!" << endl;
		seconds = get_half_seconds_remaining() - 120;
	}

	if (seconds >= get_quarter_seconds_remaining()) {
		seconds = get_quarter_seconds_remaining();
		if (!global_silent_mode) {
			gamestate->build_score_table();
			gamestate->home_cm = kGSClockMode::normal_cm;
			gamestate->away_cm = kGSClockMode::normal_cm;
		}
		if (!gamestate->statemachine->is_ready_for_extra_point())
		{
			if (quarter < 4) {
				quarter++;

				if (quarter == 3)
				{
					gamestate->set_to_second_half_choice();
					gamestate->balltracker.set_yardline(35);
					gamestate->balltracker.reset_dandd();
					gamestate->home_to = 3;
					gamestate->away_to = 3;
					if (gamestate->statemachine->is_ready_for_scrimmage()) {
						gamestate->statemachine->seond_half_kickoff_event();
					}
				}
			}
		}
	}

	game_seconds_remaining -= seconds;
	if (game_seconds_remaining < 0) game_seconds_remaining = 0;
	clock_running_start = clock_running_end;
	clock_running_end = true;
}

string GameClock::get_mini_quarter_desc()
{
	string desc;
	switch (quarter)
	{
	case 1:
		desc += "1st Q";
		break;
	case 2:
		desc += "2nd Q";
		break;
	case 3:
		desc += "3rd Q";
		break;
	case 4:
		desc += "4th Q";
		break;
	default:
		desc += "Ov Tm";
		break;
	}

	return desc;
}

vector<string> GameClock::GetMiniDesc()
{
	vector<string> desc;
	desc.push_back("|" + get_mini_quarter_desc() + "|");
	stringstream sstr;
	sstr << setw(5) << right << get_clock_desc();
	desc.push_back("|" + sstr.str() + (clock_running_start ? "+" : "|"));
	return desc;
}

void GameClock::stop_clock()
{
	clock_running_end = false;
}

void GameClock::dump_clock_stopped(string label)
{
	cout << label << ": START " << clock_running_start << " END " << clock_running_end << endl;
}
