#pragma once
#include <string>
#include <map>
using namespace std;

struct Stat {
	float own_points;
	float opp_points;
	int wins;
	int losses;
	int ties;
};

struct StatWithName {
	string name;
	Stat s;
};

class StatAccumulator
{
	map<string, Stat> stats;
public:
	StatAccumulator();
	~StatAccumulator() {};
	void AddGame(string team1, string team2, float t1pts, float t2pts, int t1wins, int t2wins, int ties);
	void Dump();
	void Write(string filename);
	void Merge(StatAccumulator &other);
};

