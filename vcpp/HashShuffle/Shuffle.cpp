#include <bitset>
#include "Shuffle.h"
#include "../Compress/BitTransformer.h"

namespace HashShuffle
{
	void GetIntVectorFromHash(vector<unsigned char> &hash, vector<unsigned int> &int_vector)
	{
		vector<bool> bits;
		BitTransformer::copy_char_array_to_bitstream(hash, 0, hash.size() * 8, 8, bits, true);
		BitTransformer::copy_char_array_to_bitstream(hash, 0, (hash.size() + 4) * 8, 8, bits, false);
		unsigned int word;

		for (auto x = 0u; x < hash.size() * 8; x++)
		{
			word = 0x0;
			for (auto ndx = x; ndx < x + 32; ndx++)
			{
				word = (word << 1);
				word |= bits[ndx];
			}
			int_vector.push_back(word);
		}
	}

	void GetShuffleVectorFromHash(vector<unsigned char>& hash, vector<unsigned int>& shuffle_vector, unsigned int size)
	{
		vector<unsigned int> int_vector;
		GetIntVectorFromHash(hash, int_vector);
		
		for (auto x = 0u; x < size; x++) shuffle_vector.push_back(x);

		unsigned int shuffle_ndx = 0;
		unsigned int int_vector_ndx = 0;
		for (auto ndx = 0u; ndx < shuffle_vector.size(); ndx ++)
		{
			shuffle_ndx += int_vector[int_vector_ndx++];
			shuffle_ndx = shuffle_ndx % shuffle_vector.size();
			int_vector_ndx = int_vector_ndx % int_vector.size();
			auto temp = shuffle_vector[ndx];
			shuffle_vector[ndx] = shuffle_vector[shuffle_ndx];
			shuffle_vector[shuffle_ndx] = temp;
		}
	}
}