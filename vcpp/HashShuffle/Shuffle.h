#pragma once
#include <vector>
#include <iostream>
#include <iomanip>
using namespace std;

namespace HashShuffle
{
	void GetIntVectorFromHash(vector<unsigned char>& hash, vector<unsigned int>& int_vector);
	void GetShuffleVectorFromHash(vector<unsigned char>& hash, vector<unsigned int>& shuffle_index, unsigned int size);
}