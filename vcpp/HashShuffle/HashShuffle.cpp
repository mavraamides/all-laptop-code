#include <iostream>
#include <bitset>
#include "Shuffle.h"

int main()
{
	vector<unsigned char> hash({ 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f });
	vector<unsigned int> int_vector;
	HashShuffle::GetIntVectorFromHash(hash, int_vector);
	bitset<32> b32;
	for (auto word : int_vector)
	{
		b32 = word;
		cout << b32 << " " << word << endl;
	}
	vector<unsigned int> shuffle_vector;
	HashShuffle::GetShuffleVectorFromHash(hash, shuffle_vector, 100);
	for (auto x = 0u; x < shuffle_vector.size(); x++) cout << setw(4) << x << " " << setw(4) << shuffle_vector[x] << endl;

}
