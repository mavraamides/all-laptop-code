from astropy.coordinates import EarthLocation,SkyCoord
from astropy.time import Time
from astropy import units as u
from astropy.coordinates import AltAz
from datetime import datetime
from pytz import timezone
from dateutil.relativedelta import relativedelta
from math import sin, cos, radians, sqrt
import pickle
from os import path
from astroplan import Observer
from astral import LocationInfo
from astral.sun import sun
from datetime import date


BestDayByObject = {}
BestDayByObjectHyper = {}
BestDayByObjectMain = {}
BestDayNext2Weeks = {}
BestDayNext2WeeksHyper = {}
BestDayNext2WeeksMain = {}
EverythingThisWeek = []
EverythingThisWeekHyper = []
EverythingThisWeekMain = []

Obs = [
    48, # 0
    50, # 10
    50, # 20
    50, # 30
    50, # 40
    30, # 50
    30, # 60
    25, # 70
    25, # 80
    25, # 90
    25, # 100
    25, # 110
    45, # 120
    45, # 130
    45, # 140
    45, # 150
    55, # 160
    55, # 170
    55, # 180
    55, # 190
    55, # 200
    55, # 210
    45, # 220
    40, # 230
    40, # 240
    40, # 250
    40, # 260
    40, # 270
    40, # 280
    30, # 290
    20, # 300
    20, # 310
    20, # 320
    20, # 330
    20, # 340
    48  # 350
]

object_cache = {}
day_data_cache = {}

overrides = {}
overrides['ABELL21'] = ('07h29.0m', '+13d15m')
overrides['C201'] = ('22h13.6m', '+70d18m')
overrides['C9'] = ('22h56.8m', '+62d37m')
Objects = [
    'NGC7789'
    ,'IC342'        # January
    ,'M45'
    ,'NGC1499'
    ,'IC2118'
    ,'IC405'
    ,'IC410'
    ,'M38'
    ,'M1'
    ,'M42'
    ,'IC434'
    ,'M78'         # February
    ,'M37'
    ,'NGC2170'
    ,'M35'
    ,'IC443'
    ,'NGC2237'
    ,'NGC2264'
    ,'NGC2359'
    ,'ABELL21'
    ,'NGC2392'
    ,'NGC2437'     # March
    ,'NGC2403'
    ,'M67'
    ,'NGC2903'
    ,'NGC3031'     # April
    ,'NGC3184'
    ,'NGC3190'
    ,'M95'
    ,'M97'
    ,'M65'
    ,'NGC3628'
    ,'NGC3718'
    ,'M109'        # May
    ,'NGC4244'
    ,'M106'
    ,'M100'
    ,'M86'
    ,'NGC4565'
    ,'NGC4594'
    ,'NGC4631'
    ,'NGC4725'
    ,'NGC4736'
    ,'NGC4826'
    ,'M63'
    ,'M51'
    ,'NGC5457'     # June
    ,'NGC5907'
    ,'M5'
    ,'M13'         # July
    ,'M12'
    ,'NGC6543'     # August
    ,'NGC6514'
    ,'M8'
    ,'M16'
    ,'M17'
    ,'M22'
    ,'M11'
    ,'M57'    
    ,'NGC6822'     # September
    ,'M27'
    ,'NGC6888'
    ,'NGC6946'
    ,'NGC6960'
    ,'NGC6992'
    ,'IC5070'
    ,'NGC7000'
    ,'NGC7008'
    ,'NGC7023'
    ,'M15'
    ,'IC1396'
    ,'IC5146'      # October
    ,'C201'
    ,'NGC7293'
    ,'NGC7317'
    ,'NGC7331'
    ,'NGC7380'
    ,'C9'
    ,'M52'
    ,'NGC7662'
    ,'M31'         # November
    ,'NGC255'
    ,'NGC253'
    ,'NGC281'
    ,'NGC457'
    ,'M33'
    ,'M74'
    ,'M76'         # December
    ,'NGC772'
    ,'NGC869'
    ,'NGC891'
    ,'NGC925'
    ,'IC1805'
    ,'M77'
    ,'IC1848'
]

chapter = {}
chapter['IC342'] = 'January'
chapter['M45'] = 'January'
chapter['NGC1499'] = 'January'
chapter['IC2118'] = 'January'
chapter['IC405'] = 'January'
chapter['IC410'] = 'January'
chapter['M38'] = 'January'
chapter['M1'] = 'January'
chapter['M42'] = 'January'
chapter['IC434'] = 'January'
chapter['M78'] = 'February'
chapter['M37'] = 'February'
chapter['NGC2170'] = 'February'
chapter['M35'] = 'February'
chapter['IC443'] = 'February'
chapter['NGC2237'] = 'February'
chapter['NGC2264'] = 'February'
chapter['NGC2359'] = 'February'
chapter['ABELL21'] = 'February'
chapter['NGC2392'] = 'February'
chapter['NGC2437'] = 'March'
chapter['NGC2403'] = 'March'
chapter['M67'] = 'March'
chapter['NGC2903'] = 'March'
chapter['NGC3031'] = 'April'
chapter['NGC3184'] = 'April'
chapter['NGC3190'] = 'April'
chapter['M95'] = 'April'
chapter['M97'] = 'April'
chapter['M65'] = 'April'
chapter['NGC3628'] = 'April'
chapter['NGC3718'] = 'April'
chapter['M109'] = 'May'
chapter['NGC4244'] = 'May'
chapter['M106'] = 'May'
chapter['M100'] = 'May'
chapter['M86'] = 'May'
chapter['NGC4565'] = 'May'
chapter['NGC4594'] = 'May'
chapter['NGC4631'] = 'May'
chapter['NGC4725'] = 'May'
chapter['NGC4736'] = 'May'
chapter['NGC4826'] = 'May'
chapter['M63'] = 'May'
chapter['M51'] = 'May'
chapter['NGC5457'] = 'June'
chapter['NGC5907'] = 'June'
chapter['M5'] = 'June'
chapter['M13'] = 'July'
chapter['M12'] = 'July'
chapter['NGC6543'] = 'August'
chapter['NGC6514'] = 'August'
chapter['M8'] = 'August'
chapter['M16'] = 'August'
chapter['M17'] = 'August'
chapter['M22'] = 'August'
chapter['M11'] = 'August'
chapter['M57'] = 'August'
chapter['NGC6822'] = 'September'
chapter['M27'] = 'September'
chapter['NGC6888'] = 'September'
chapter['NGC6946'] = 'September'
chapter['NGC6960'] = 'September'
chapter['NGC6992'] = 'September'
chapter['IC5070'] = 'September'
chapter['NGC7000'] = 'September'
chapter['NGC7008'] = 'September'
chapter['NGC7023'] = 'September'
chapter['M15'] = 'September'
chapter['IC1396'] = 'September'
chapter['IC5146'] = 'October'
chapter['C201'] = 'October'
chapter['NGC7293'] = 'October'
chapter['NGC7317'] = 'October'
chapter['NGC7331'] = 'October'
chapter['NGC7380'] = 'October'
chapter['C9'] = 'October'
chapter['M52'] = 'October'
chapter['NGC7662'] = 'October'
chapter['M31'] = 'November'
chapter['NGC255'] = 'November'
chapter['NGC253'] = 'November'
chapter['NGC281'] = 'November'
chapter['NGC457'] = 'November'
chapter['M33'] = 'November'
chapter['M74'] = 'November'
chapter['M76'] = 'December'
chapter['NGC772'] = 'December'
chapter['NGC869'] = 'December'
chapter['NGC891'] = 'December'
chapter['NGC925'] = 'December'
chapter['IC1805'] = 'December'
chapter['M77'] = 'December'
chapter['IC1848'] = 'December'

# c:/Users/mavra/AppData/Local/Programs/Python/Python38/python.exe c:\Users\mavra\.vscode\extensions\ms-python.python-2020.8.103604\pythonFiles\pyvsc-run-isolated.py pip install -U pylint --user   
def GetEarthLocation():
    return EarthLocation(lat='45.073955', lon='-93.239735', height=300*u.m)

def SetToSunset(dt):
    city = LocationInfo("Minneapols", "US", "US/Central", 45.073955, -93.239735)
    s = sun(city.observer, date=date(dt.year, dt.month, dt.day), tzinfo=city.timezone)
    sunset_string = f'{s["sunset"]}'
    hour = sunset_string.split(' ')[1].split(':')[0]
    minute = sunset_string.split(' ')[1].split(':')[1]
    dt = dt + relativedelta(hour=int(hour))
    dt = dt + relativedelta(minute=int(minute))
    dt = dt + relativedelta(second=0)
    dt = dt + relativedelta(microsecond=0)
    return dt

def AltAzCalc(coord, t):
    observing_location = GetEarthLocation()  
    observing_time = Time(t)  
    aa = AltAz(location=observing_location, obstime=observing_time)

    return coord.transform_to(aa)


def sep():
    sep = ''
    sep += '-' * 112
    return sep 

def sep3():
    #      2020-08-31 23:50:00 |+039.139 |+083.227 |+049.742 |+093.620 | YES   | YES   | YES   | YES   |+001.615 |+128.816|
    sep = '--------------------+---------+---------+---------+---------+-------+-------+-------+-------+---------+--------+'
    return sep 

def sep2():
    sep = ''
    sep += '=' * 112
    return sep 

def IsObstructed(alt, az):
    obstruction_index = round(az  / 10.0) % 36
    return alt <= Obs[obstruction_index]

def FieldRotation(lat, alt, az):
    return 15.04 * cos(lat) * cos(az) / cos(alt)

def PixelRotation(lat, alt, az, exp):
    camera_width = 5544
    camera_height = 3600
    camera_center_to_corner = sqrt((camera_width * camera_width) + (camera_height * camera_height)) / 2.0
    fr = FieldRotation(lat, alt, az)
    return (radians(fr) * exp / 3600.0) * camera_center_to_corner

def BuildDisplayLine(dt, result, result2, hyper_obs, hyper_obs2, main_obs, main_obs2, rotation_rate, exp):
    display_line = ''
    display_line += str(dt)
    display_line += " |{:+08.3f}".format(result[0])
    display_line += " |{:+08.3f}".format(result[1])
    display_line += " |{:+08.3f}".format(result2[0])
    display_line += " |{:+08.3f}".format(result2[1])

    if hyper_obs:
        display_line += ' |      '
    else:
        display_line += ' | YES  '

    if hyper_obs2:
        display_line += ' |      '
    else:
        display_line += ' | YES  '

    if main_obs:
        display_line += ' |      '
    else:
        display_line += ' | YES  '

    if main_obs2:
        display_line += ' |      '
    else:
        display_line += ' | YES  '

    display_line += " |{:+08.3f}".format(rotation_rate)
    display_line += " |{:+08.3f}".format(exp)

    display_line += '|'
    return display_line

def GetCoords(ob):
    coord = object_cache.get(ob, None)
    if coord: return coord

    ov_coords = overrides.get(ob, None)
    if ov_coords:
        coord = SkyCoord(ov_coords[0], ov_coords[1])
    else:
        coord = SkyCoord.from_name(ob)

    object_cache[ob] = coord

    return coord

def BuildColumnHeader():
    header = ''
    header += sep3() + '\n'
    header += 'Date                | Alt S   | Az S    | Alt E   | Az E    | HS S  | HS E  | MC S  | MC E  | RotRate | MaxExp |\n'
    header += sep3()
    return header

def GetChapter(ob):
    chap = chapter.get(ob, 'NONE')
    return '(' + chap + ')'

def BuildHeader(ob, coord):
    header = ''
    header += sep() + '\n'
    header += 'Object: ' + str(ob) + '  ' + coord.to_string('hmsdms') + ' ' + GetChapter(ob) + '\n'
    header += BuildColumnHeader()
    return header

def UpdateBestDay(object_dict, ob, exp, display_line):
    best_for_this_object = object_dict.get(ob, None)
    if not best_for_this_object:
        best_for_this_object = (0, '')

    (best_score, best_line) = best_for_this_object
    if exp > best_score:
        best_score = exp
        best_line = display_line
        object_dict[ob] = (best_score, best_line)


# (result, result2, hyper_obs, hyper_obs2, main_obs, main_obs2, rotation_rate, exp) = GetDataLine(dt, obs)
def GetDataLine(dt, coord):
    localtz = timezone('US/Central')
    utctz = timezone('UTC')

    localdt = localtz.localize(dt)
    t = str(localdt.astimezone(utctz)).split('+')[0]
    result = AltAzCalc(coord, t)
    hyper_obs = IsObstructed(result.alt.deg, result.az.deg)
    main_obs = hyper_obs or result.alt.deg >= 60.0

    localdt2 = localdt + relativedelta(hours=1)
    t2 = str(localdt2.astimezone(utctz)).split('+')[0]
    result2 = AltAzCalc(coord, t2)
    hyper_obs2 = IsObstructed(result2.alt.deg, result2.az.deg)
    main_obs2 = hyper_obs2 or result2.alt.deg >= 60.0

    #  Field Rotation Rate (arcmin/min)  =  15.04 COS(Latitude)*COS(Azimuth Angle) / COS(Altitude Angle)
    rotation_rate = abs(FieldRotation(radians(45.073955), result.alt.rad, result.az.rad))
    rotation_rate2 = abs(FieldRotation(radians(45.073955), result2.alt.rad, result2.az.rad))
    if rotation_rate2 > rotation_rate: rotation_rate = rotation_rate2
    pr30 = abs(PixelRotation(radians(45.073955), result.alt.rad, result.az.rad, 30.0))
    pr30_2 = abs(PixelRotation(radians(45.073955), result2.alt.rad, result2.az.rad, 30.0))
    if pr30_2 > pr30: pr30 = pr30_2
    exp = 100.0 / pr30

    return ((result.alt.deg, result.az.deg), (result2.alt.deg, result2.az.deg), hyper_obs, hyper_obs2, main_obs, main_obs2, rotation_rate, exp)
    
def MakeKey(dt, ob):
    key = dt.strftime("%Y%m%d") + "-" + ob
    return key

def GetDayData(dt, ob, coord):
    key = MakeKey(dt, ob)
    # print (key)
    day_data = day_data_cache.get(key, None)
    if day_data:
        # print ('chached')
        return day_data

    # print ('calculating')
    dt = SetToSunset(dt)
    dt += relativedelta(hours=1)
    day_data = []
    while True:
        (result, result2, hyper_obs, hyper_obs2, main_obs, main_obs2, rotation_rate, exp) = GetDataLine(dt, coord)
        day_data.append((dt, result, result2, hyper_obs, hyper_obs2, main_obs, main_obs2, rotation_rate, exp))

        current_hour = dt.hour
        dt = dt + relativedelta(hours=1)
        if dt.hour < current_hour:
            break
    day_data_cache[key] = day_data

    return day_data

def PrintObjectReport(ob, use_exp):
    #show_all = True
    show_all = False

    dt = datetime.now()
    coord = GetCoords(ob)

    data = []
    days = 0
    while days < 365:
        dt += relativedelta(microsecond=0)

        day_data = GetDayData(dt, ob, coord)

        for d in day_data:
            (dt, result, result2, hyper_obs, hyper_obs2, main_obs, main_obs2, rotation_rate, exp) = d
            data.append((days, dt, result, result2, hyper_obs, hyper_obs2, main_obs, main_obs2, rotation_rate, exp))

        dt = dt + relativedelta(days=1)
        days += 1


    header = BuildHeader(ob, coord)
    print (header)

    print_count = 0
    
    for data_element in data:
        (days, dt, result, result2, hyper_obs, hyper_obs2, main_obs, main_obs2, rotation_rate, exp) = data_element

        hyper_clear = not hyper_obs and not hyper_obs2
        main_clear = not main_obs and not main_obs2

        display_line = BuildDisplayLine(dt, result, result2, hyper_obs, hyper_obs2, main_obs, main_obs2, rotation_rate, exp)
        if use_exp:
            rank_key = exp
        else:
            rank_key = result[0] if result[0] > result2[0] else result2[0]
                
        if result[0] >= 20.0 and result2[0] >= 20.0:
            UpdateBestDay(BestDayByObject, ob, rank_key, display_line)
            if days <= 14:
                UpdateBestDay(BestDayNext2Weeks, ob, rank_key, display_line)
            if days < 7:
                while len(EverythingThisWeek) <= days:
                    EverythingThisWeek.append({})
                UpdateBestDay(EverythingThisWeek[days], ob, rank_key, display_line)

        if hyper_clear:
            UpdateBestDay(BestDayByObjectHyper, ob, rank_key, display_line)
            if days <= 14:
                UpdateBestDay(BestDayNext2WeeksHyper, ob, rank_key, display_line)
            if days < 7:
                while len(EverythingThisWeekHyper) <= days:
                    EverythingThisWeekHyper.append({})
                UpdateBestDay(EverythingThisWeekHyper[days], ob, rank_key, display_line)

        if main_clear:
            UpdateBestDay(BestDayByObjectMain, ob, rank_key, display_line)
            if days <= 14:
                UpdateBestDay(BestDayNext2WeeksMain, ob, rank_key, display_line)
            if days < 7:
                while len(EverythingThisWeekMain) <= days:
                    EverythingThisWeekMain.append({})
                UpdateBestDay(EverythingThisWeekMain[days], ob, rank_key, display_line)
        
        if (hyper_clear or main_clear or show_all) and print_count < 20:
            print(display_line)
            print_count += 1

    print (sep3())

def PrintBestDayReport(title, object_dict, sort_by_score=False, show_header=True):
    if show_header:
        print ('')
        print (sep2())
        print (title)
        print (sep2())
        
    best_days = []
    for ob in object_dict.keys():
        if sort_by_score:
            best_days.append("{:+08.3f}".format(object_dict[ob][0]) + "*" + object_dict[ob][1] + ' ' + ob + ' ' + GetChapter(ob))
        else:
            best_days.append(object_dict[ob][1] + ' ' + ob + ' ' + GetChapter(ob))

    best_days.sort()
    if sort_by_score:
        best_days.reverse()

    print (BuildColumnHeader())

    for best_day in best_days:
        if sort_by_score:
            best_day = best_day.split('*')[1]
        print (best_day)

    print (sep3())

cache_fname = "astro_object_cache.p"
if path.exists(cache_fname):
    object_cache = pickle.load( open( cache_fname, "rb" ) )

day_data_cache_fname = "day_data_cache.p"
if path.exists(day_data_cache_fname):
    day_data_cache = pickle.load( open( day_data_cache_fname, "rb" ) )


print (sep2())
print ('OBJECT BREAKDOWN - NEXT 20 BEST DAYS')
print (sep2())


for ob in Objects:
    PrintObjectReport(ob, False)

PrintBestDayReport('BEST DAY FOR OBJECT REGARDLESS OF OBSTRUCTION', BestDayByObject)
PrintBestDayReport('BEST DAY FOR OBJECT WITH HYPERSTAR OBSTRUCTED', BestDayByObjectHyper)
PrintBestDayReport('BEST DAY FOR OBJECT WITH MAIN CAMERA OBSTRUCTED', BestDayByObjectMain)
PrintBestDayReport('BEST DAY FOR OBJECT REGARDLESS OF OBSTRUCTION NEXT 2 WEEKS', BestDayNext2Weeks)
PrintBestDayReport('BEST DAY FOR OBJECT WITH HYPERSTAR OBSTRUCTED NEXT 2 WEEKS', BestDayNext2WeeksHyper)
PrintBestDayReport('BEST DAY FOR OBJECT WITH MAIN CAMERA OBSTRUCTED NEXT 2 WEEKS', BestDayNext2WeeksMain)

show_header = True
for report in EverythingThisWeek:
    PrintBestDayReport('BEST THIS WEEK REGARDLESS OF OBSTRUCTION', report, True, show_header)
    show_header = False

show_header = True
for report in EverythingThisWeekHyper:
    PrintBestDayReport('BEST THIS WEEK WITH HYPERSTAR OBSTRUCTED', report, True, show_header)
    show_header = False

show_header = True
for report in EverythingThisWeekMain:
    PrintBestDayReport('BEST THIS WEEK WITH MAIN CAMERA', report, True, show_header)
    show_header = False


steps = []
steps.append('SEP|PRE SETUP')
steps.append('Get it outside early.')
steps.append('!!! CHECK BALANCE !!!')
steps.append('Supplies: Light, RGlasses, PC, GamePad, T-Shirt, Dew Shield')
steps.append('SEP|ALIGN')
steps.append('Level Scope.')
steps.append('Init Scope position.')
steps.append('Scope On.')
steps.append('!!! SKIP AUTO GARAGE CLOSE !!! Move Car.')
steps.append('Put PC in Astro mode.')
steps.append('Attach PC - CPWI.')
steps.append('SS Cap Off - HS: Scope Cap Off - Plug In GamePad - Align Scope.')
steps.append('SEP|SETUP CAMERA')
steps.append('SS Cap On - Attach cooler - Attach Camera - Scope Cap Off.')
steps.append('SharpCap - Select Camera.')
steps.append('GoTo bright star.')
steps.append('Focus.')
steps.append('GoTo dark.')
steps.append('Detatch GamePad.')
steps.append('Exposure.')
steps.append('GoTo target.')
steps.append('Plate Solve. !!!MAKE SURE TRACKING SET TO SIDEREAL!!!')
steps.append('SEP|CAPTURE')
steps.append('Image.')
steps.append('Flats.')
steps.append('SEP|CLEANUP')
steps.append('Scope Cap On.')
steps.append('Stop frame capture.')
steps.append('Shutdown all software.')
steps.append('Detach USB from PC.')
steps.append('Put Away cooler.')
steps.append('Power down Scope.')
steps.append('!!! MAKE SURE USB DISCONNECTED AND OUT OF CAR !!! - Move Car.')
steps.append('Pack Up.')


print (sep2())
line = 1
for step in steps:
    if step.split('|')[0] == 'SEP':
        print(sep())
        print(step.split('|')[1])
        print(sep())
    else:
        print("{:3d}".format(line) + ') ' + step)
        line += 1
print (sep2())

pickle.dump( object_cache, open( cache_fname, "wb" ) )
pickle.dump( day_data_cache, open( day_data_cache_fname, "wb" ) )
